export default [
  // user
  {
    path: '/user',
    component: '../layouts/UserLayout',
    routes: [
      { path: '/user', redirect: '/user/login' },
      { path: '/user/login', component: './User/Login' },
      { path: '/user/register', component: './User/Register' },
      { path: '/user/register-result', component: './User/RegisterResult' },
      {
        path: '/user/registerStep',
        component: './User/StepForm',
        routes: [
          {
            path: '/user/registerStep/info', component: './User/StepForm/Step1'
          },
          {
            path: '/user/registerStep/result', component: './User/StepForm/Step2'
          },
          {
            path: '/user/registerStep/pay', component: './User/StepForm/Step3'
          }
        ],
      },
      { path: '/user/confirm', component: './User/Confirm/Confirm' },
    ],
  },
  // app
  {
    path: '/',
    component: '../layouts/BasicLayout',
    Routes: ['src/pages/Authorized'],
    routes: [
      { path: '/', redirect: '/Home/Analysis' },
      {
        path: '/Home/Analysis',
        name: 'home',
        icon: 'dashboard',
        component: './Home/Analysis',
      },
      // {
      //   name: 'schedule',
      //   icon: 'table',
      //   path: '/schedule',
      //   component: './Schedule/ScheduleList',
      //   authority: '查询课程表',
      // },
      {
        name: 'schedules',
        icon: 'table',
        path: '/schedules',
        component: './Schedule/schedulesDemo',
        authority: '查询课程表',
      },
      // {
      //   name: 'schedulesDemo',
      //   icon: 'table',
      //   path: '/schedulesDemo',
      //   component: './Schedule/schedulesDemo',
      //   authority: '查询课程表',
      // },
      {
        name: 'curriculum',
        icon: 'book',
        path: '/teaching',
        authority: ['一对一:查询', '小组授课:查询', '科目类型:查询', '科目管理:查询'],
        routes: [
          {
            name: 'subjectType',
            path: '/teaching/SubjectType',
            component: './Teaching/SubjectType/SubjectTypeList',
            authority: '科目类型:查询',
          },
          {
            name: 'classification',
            path: '/teaching/classification',
            component: './Teaching/Classification/classFicationList',
            authority: '科目管理:查询',
          },
          {
            path: '/teaching/classification/mgr',
            component: './Teaching/Classification/mgr',
            authority: '科目管理:查询',
          },
          {
            name: 'one2one',
            path: '/teaching/one2one',
            component: './Teaching/One2One/One2OneList',
            authority: '一对一:查询',
          },
          {
            path: '/teaching/one2one/mgr',
            component: './Teaching/One2One/One2One',
          },
          {
            name: 'one22one',
            path: '/teaching/one22one',
            component: './Teaching/One22One/One22OneList',
            authority: '小组授课:查询',
          },
          {
            path: '/teaching/one22one/mgr',
            component: './Teaching/One22One/One22One',
          },
        ],
      },
      {
        name: 'student',
        icon: 'user-add',
        path: '/student',
        authority: [
          '学生管理-正式学生:查询',
          '请假管理-请假记录:查询',
          '学生管理-咨询学生:查询',
          '学生交费功能:查询',
          '学生管理-试听记录:查询',
          '学生管理-潜在学生信息'
        ],
        routes: [
          {
            name: 'data',
            path: '/student/data',
            authority: '学生管理-正式学生:查询',
            component: './student/Data/StudentDataList',
          },
          {
            path: '/student/data/Mgr',
            component: './student/Data/StudentDataMgr',
          },
          {
            name: 'Audition',
            path: '/student/Audition',
            authority: '学生管理-咨询学生:查询',
            component: './student/Audition/AuditionList',
          },
          {
            name: 'Intention',
            path: '/student/Intention',
            authority: '学生管理-潜在学生信息',
            component: './student/Intention/IntentionList',
          },
          {
            name: 'AudRecord',
            path: '/student/AudRecord',
            authority: '学生管理-试听记录:查询',
            component: './student/AudRecord/AudRecordList',
          },
          // {
          //   name: 'pay',
          //   path: 'pay',
          //   authority: '学生交费功能:查询',
          //   component: './school/pay/payList',
          // },
          {
            name: 'leave',
            path: '/student/stu-leave',
            authority: '请假管理-请假记录:查询',
            component: './student/StuToLeave/studentLeave',
          },
          {
            path: '/student/Audition/Mgr',
            component: './student/Audition/AuditionMgr',
          },
        ]
      },
      {
        name: 'teacher',
        icon: 'team',
        path: '/teacher',
        authority: ['教师管理-老师:查询', '教师管理-上课反馈记录:查询', '教师管理-上课打卡记录:查询'],
        routes: [
          {
            name: 'teachData',
            path: '/teacher/teach-data',
            authority: ['教师管理-老师:查询'],
            component: './Teacher/teachers/teacherList',
          },
          // {
          //   name: 'salaryDetails',
          //   path: 'SalaryList',
          //   authority: ['教师管理-老师:查询'],
          //   component: './Teacher/salaryDetails',
          // },
          // {
          //   name: 'salary',
          //   path: 'salary',
          //   authority: ['教师管理-老师:查询'],
          //   component: './Teacher/salary',
          // },
          {
            name: 'feedback',
            path: 'feedback',
            authority: '教师管理-上课反馈记录:查询',
            component: './Teacher/feedback/feedbackList',
          },
          {
            name: 'signIn',
            path: 'signIn',
            authority: '教师管理-老师补充打卡',
            component: './Teacher/signIn',
          },
          {
            path: '/teacher/teach-data/Mgr',
            component: './Teacher/teachers/teacherMgr',
          },
        ]
      },
      {
        name: 'classroom',
        icon: 'folder',
        path: '/classroom',
        authority: ['教室管理-教室:查询', '教室管理-租赁教室订单:查询'],
        routes: [
          {
            name: 'Maintain',
            path: '/classroom/list',
            authority: '教室管理-教室:查询',
            component: './Classroom/roomList/list',
          },
          {
            name: 'classroomleaseOpen',
            path: '/classroom/classroomleaseOpen',
            authority: '教室管理-租赁教室订单:查询',
            component: './Classroom/classroomleaseOpen/classroomleaseOpenList',
          },
        ]
      },
      {
        name: 'payManagement',
        icon: 'dollar',
        path: '/payManagement',
        routes: [
          {
            name: 'pay',
            path: 'pay',
            authority: '学生交费功能:查询',
            component: './school/pay/payList',
          },
          {
            name: 'salaryDetails',
            path: 'SalaryList',
            authority: ['教师管理-老师:查询'],
            component: './Teacher/salaryDetails',
          },
          {
            name: 'salary',
            path: 'salary',
            authority: ['教师管理-老师:查询'],
            component: './Teacher/salary',
          },
        ]
      },
      {
        name: 'school',
        icon: 'switcher',
        path: '/school',
        authority: ['消息管理-系统消息:查询'],
        routes: [
          {
            name: 'news',
            path: 'news',
            authority: '消息管理-系统消息:查询',
            component: './school/MsgList',
          },
        ]
      },
      {
        name: 'personnel',
        icon: 'usergroup-add',
        path: '/personnel',
        authority: ['人员管理-校区负责人:查询', '请假管理-请假记录:查询',],
        routes: [
          {
            name: 'schoolControl',
            path: '/personnel/schoolControl',
            authority: '人员管理-校区负责人:查询',
            component: './Peoples/SchoolControl/controlList',
          },
          // {
          //   name: 'director',
          //   path: '/personnel/director',
          //   // hide: '',
          //   component: './Peoples/Director/directorList',
          // },
          {
            name: 'peopleLeave',
            path: '/personnel/leave',
            authority: '请假管理-请假记录:查询',
            component: './Peoples/Leave/LeaveList',
          },
          {
            path: '/personnel/schoolControl/mgr',
            authority: '人员管理-校区负责人:查询',
            component: './Peoples/SchoolControl/controlListMgr',
          },
          {
            path: '/personnel/director/mgr',
            authority: '人员管理-教务主管:查询',
            component: './Peoples/Director/directorMgr',
          },
        ]
      },
      // {
      //   name: 'Inventory',
      //   icon: 'shop',
      //   path: '/Inventory',
      //   authority: ['库存管理-类别管理:查询', '库存管理-产品销售记录:查看'],
      //   routes: [
      //     {
      //       name: 'productType',
      //       path: '/Inventory/productType',
      //       // authority: '人员管理-校区负责人:查询',
      //       component: './Inventory/productType',
      //     },
      //     {
      //       name: 'productManage',
      //       path: '/Inventory/productManage',
      //       // authority: '人员管理-校区负责人:查询',
      //       component: './Inventory/productManage',
      //     },
      //     {
      //       name: 'productChange',
      //       path: '/Inventory/productChange',
      //       component: './Inventory/productChange',
      //     },
      //     {
      //       name: 'orderList',
      //       path: '/Inventory/orderList',
      //       authority: '库存管理-产品销售记录:查看',
      //       component: './Inventory/orderList',
      //     },
      //     {
      //       path: '/Inventory/productManage/mgr',
      //       component: './Inventory/productManage/mgr',
      //     },
      //   ]
      // },
      // {
      //   name: 'Coupons',
      //   icon: 'credit-card',
      //   path: '/Coupons',
      //   authority: ['优惠卷:增加', '优惠卷:查询', '活动管理-显示活动信息'],
      //   routes: [
      //     {
      //       name: 'Generates',
      //       path: 'Generates',
      //       authority: '优惠卷:增加',
      //       component: './Coupons/Generates',
      //     },
      //     {
      //       name: 'SharingLottery',
      //       path: 'SharingLottery',
      //       authority: '活动管理-显示活动信息',
      //       component: './Coupons/luckDraw',
      //     },
      //     {
      //       path: '/Coupons/SharingLottery/mgr',
      //       component: './Coupons/luckDraw/DrawMgr',
      //     },
      //   ],
      // },
      {
        name: 'mechanism',
        icon: 'appstore-o',
        path: '/mechanism',
        authority: ['机构管理-我的机构:查询', '机构管理-机构法人:查询', '机构管理-机构审核:查询'],
        routes: [
          {
            name: 'mine',
            path: '/mechanism/mine',
            authority: '机构管理-我的机构:查询',
            component: './Mechanism/Mine/MineList',
          },
          {
            path: '/mechanism/mine/mgr',
            authority: '机构管理-我的机构:查询',
            component: './Mechanism/examine/examineMgr',
          },
          {
            path: '/mechanism/mine/stepForm',
            component: './Mechanism/Mine/StepForm/index',
            hideChildrenInMenu: true,
            routes: [
              {
                path: '/mechanism/mine/stepForm',
                redirect: '/mechanism/mine/StepForm/info',
              },
              {
                path: '/mechanism/mine/StepForm/info',
                component: './Mechanism/Mine/StepForm/Step1',
              },
              {
                path: '/mechanism/mine/StepForm/mine',
                component: './Mechanism/Mine/StepForm/Step2',
              },
              {
                path: '/mechanism/mine/StepForm/result',
                component: './Mechanism/Mine/StepForm/Step3',
              },
            ],
          },
          {
            name: 'examine',
            path: '/mechanism/examine',
            authority: '机构管理-机构审核:查询',
            component: './Mechanism/examine/examineList',
          },
          {
            path: '/mechanism/examine/mgr',
            authority: '机构管理-机构审核:查询',
            component: './Mechanism/examine/examineMgr',
          },
          {
            name: 'manager',
            path: '/mechanism/manager',
            authority: '机构管理-机构法人:查询',
            component: './Mechanism/Manager/ManagerList',
          },
          {
            path: '/mechanism/mine/mgr',
            authority: '机构管理-我的机构:查询',
            component: './Mechanism/Mine/MineMgr',
          },
        ]
      },
      {
        name: 'admin',
        icon: 'desktop',
        path: '/admin',
        authority: ['角色:查询', '用户:查询'],
        routes: [
          {
            name: 'user',
            path: '/admin/user',
            authority: '用户:查询',
            component: './Admin/User/UserList',
          },
          {
            name: 'role',
            path: 'role',
            authority: '角色:查询',
            component: './Admin/Role/RoleList',
          },
          {
            path: '/admin/user/mgr',
            authority: '机构管理-我的机构:查询',
            component: './Admin/User/UserMgr',
          },
          {
            path: '/admin/Role/mgr',
            authority: '角色:查询',
            component: './Admin/Role/RoleMgr',
          },
        ]
      },
      {
        name: 'system',
        icon: 'laptop',
        path: '/system',
        authority: ['系统管理-价格:查询', '账号管理-订单:查询', '账号管理-租期:查询'],
        routes: [
          {
            name: 'Price',
            path: '/system/Price',
            authority: '系统管理-价格:查询',
            component: './System/Price/PriceList',
          },
          {
            name: 'Orders',
            path: '/system/Orders',
            authority: '账号管理-订单:查询',
            component: './System/Orders/OrdersList',
          },
          {
            name: 'Term',
            path: '/system/Term',
            authority: '账号管理-租期:查询',
            component: './System/Term/TermList',
          },
        ]
      },
      {
        name: 'account',
        icon: 'user',
        path: '/account',
        authority: ['个人资料-修改', '系统设置-账号:充值'],
        routes: [
          {
            name: 'base',
            path: '/account/base',
            component: './Account/index',
            authority: '个人资料-修改',
            routes: [
              {
                path: '/account/base',
                redirect: '/account/base/info',
              },
              {
                path: '/account/base/setting',
                component: './Account/SecurityView',
              },
              {
                path: '/account/base/info',
                component: './Account/base',
              },
            ]
          },
          {
            name: 'settings',
            path: '/account/settings',
            component: './Account/Settings/Info',
            authority: '系统设置-账号:充值',
            routes: [
              {
                path: '/account/settings',
                redirect: '/account/settings/binding',
              },
              {
                path: '/account/settings/base',
                component: './Account/Settings/BaseView',
              },
              {
                path: '/account/settings/security',
                component: './Account/Settings/SecurityView',
              },
              {
                path: '/account/settings/binding',
                component: './Account/Settings/BindingView',
              },
              {
                path: '/account/settings/notification',
                component: './Account/Settings/NotificationView',
              },
            ],
          },
          {
            name: 'BusinessSetting',
            path: '/account/BusinessSetting',
            component: './Account/BusinessSetting/Info',
            authority: '机构系统设置:查询',
            routes: [
              {
                path: '/account/BusinessSetting',
                redirect: '/account/BusinessSetting/BaseView',
              },
              {
                path: '/account/BusinessSetting/HolidaySet',
                component: './Account/BusinessSetting/HolidaySet',
              },
              {
                path: '/account/BusinessSetting/Information',
                component: './Account/BusinessSetting/Information',
              },
              {
                path: '/account/BusinessSetting/WeChatPublicSet',
                component: './Account/BusinessSetting/WeChatPublicSet',
              },
              {
                path: '/account/BusinessSetting/OtherSettings',
                component: './Account/BusinessSetting/OtherSettings',
              },
              {
                path: '/account/BusinessSetting/BaseView',
                component: './Account/BusinessSetting/BaseSet',
              },
            ],
          },
        ]
      },

      // {
      //   path: '/dashboard',
      //   name: 'dashboard',
      //   icon: 'dashboard',
      //   routes: [
      //     {
      //       path: '/dashboard/analysis',
      //       name: 'analysis',
      //       component: './Dashboard/Analysis',
      //     },
      //     {
      //       path: '/dashboard/monitor',
      //       name: 'monitor',
      //       component: './Dashboard/Monitor',
      //     },
      //     {
      //       path: '/dashboard/workplace',
      //       name: 'workplace',
      //       component: './Dashboard/Workplace',
      //     },
      //   ],
      // },
      {
        component: '404',
      },
    ],
  },
];
