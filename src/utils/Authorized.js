import RenderAuthorized from '@/components/Authorized';
import { getAuthority } from './authority';
import { connect } from 'dva';

// let Authorized = RenderAuthorized(JSON.parse(getAuthority()[0])); // eslint-disable-line
let Authorized = RenderAuthorized(getAuthority()); // eslint-disable-line

// Reload the rights component
const reloadAuthorized = () => {
  Authorized = RenderAuthorized(getAuthority());
};


export { reloadAuthorized };
export default Authorized;
