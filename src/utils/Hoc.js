import React, { PureComponent } from 'react';
import { Icon } from 'antd';
import isEqual from 'lodash/isEqual';
import { scriptUrl } from './utils';

const IconFont = Icon.createFromIconfontCN({
  scriptUrl,
});

const Hoc = ({ fetchUrl }) => WrapperComponent => (
  class HocD extends PureComponent {
    constructor() {
      super();
      this.state = {
        pagination: {
          pageSize: 10,
          pageNum: 1,
        },
        currentOrg: {
          id: '',
        },
        selectedRows: [],
      };
    }

    // componentDidMount() {
    //   const { dispatch, currentOrg } = this.props;
    //   if (!currentOrg.id) return;
    //   const { pagination } = this.state;
    //   const { id } = currentOrg;
    //   console.log('mont');
    //   this.setState({
    //     currentOrg: {
    //       id,
    //     }
    //   });
    //   dispatch({
    //     type: fetchUrl,
    //     payload: {
    //       orgId: id,
    //       pagination,
    //     },
    //   });
    // }

    static getDerivedStateFromProps(nextProps, preState) {
      const { dispatch } = nextProps;
      // console.log(nextProps.currentOrg.id, 'props');
      // console.log(preState.currentOrg.id, 'state');
      if (!nextProps.currentOrg.id) return null;
      if (nextProps.currentOrg.id === preState.currentOrg.id) {
        return null;
      }
      const { pagination } = preState;
      dispatch({
        type: fetchUrl,
        payload: {
          orgId: nextProps.currentOrg.id,
          pagination,
        },
      });
      return {
        currentOrg: nextProps.currentOrg,
      };
    }

    handleStandardTableChange = (pagination, query={}) => {
      const { dispatch } = this.props;
      const { pageSize, current: pageNum } = pagination;
      const { currentOrg: { id: orgId } } = this.state;
      this.setState({
        pagination: {
          pageNum,
          pageSize,
        },
      });
      dispatch({
        type: fetchUrl,
        payload: {
          pagination: {
            pageNum,
            pageSize,
          },
          query,
          orgId,
        },
      });
    };

    forItem = (type, text) => (
      <div>
        <IconFont type={type} />
        <span>  {text}</span>
      </div>
      );

    handleSelectRows = (rows) => {
      this.setState({
        selectedRows: rows,
      });
    };

    render() {
      const newProps = {
        ...this.state,
        handleStandardTableChange: this.handleStandardTableChange,
        forItem: this.forItem,
        handleSelectRows: this.handleSelectRows,
      };
      return (
        <WrapperComponent
          {... newProps}
          {... this.props}
        />
      );
    }
  }
);
export default Hoc;
