import React, { Component } from 'react';
import { Chart, Axis, Tooltip, Geom, Legend } from 'bizcharts';
import Debounce from 'lodash-decorators/debounce';
import Bind from 'lodash-decorators/bind';
import autoHeight from '../autoHeight';
import styles from '../index.less';

@autoHeight()
class TeaRate extends Component {
  state = {
    autoHideXLabels: false,
  };

  componentDidMount() {
    window.addEventListener('resize', this.resize, { passive: true });
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resize);
  }

  handleRoot = n => {
    this.root = n;
  };

  handleRef = n => {
    this.node = n;
  };

  @Bind()
  @Debounce(400)
  resize() {
    if (!this.node) {
      return;
    }
    const canvasWidth = this.node.parentNode.clientWidth;
    const { data = [], autoLabel = true } = this.props;
    if (!autoLabel) {
      return;
    }
    const minWidth = data.length * 30;
    const { autoHideXLabels } = this.state;

    if (canvasWidth <= minWidth) {
      if (!autoHideXLabels) {
        this.setState({
          autoHideXLabels: true,
        });
      }
    } else if (autoHideXLabels) {
      this.setState({
        autoHideXLabels: false,
      });
    }
  }

  chartIns = null;

  getG2Instance = (chart) => {
    this.chartIns = chart;
  };

  render() {
    const {
      height,
      title,
      forceFit = true,
      data,
      color = 'rgba(24, 144, 255, 0.85)',
      padding,
    } = this.props;
    const { autoHideXLabels } = this.state;
    const scale = {
      x: {
        type: 'cat',
      },
      y: {
        min: 0,
      },
      z: {
        min: 0,
      },
    };

    return (
      <div className={styles.chart} style={{ height }} ref={this.handleRoot}>
        <div ref={this.handleRef}>
          {title && <h4 style={{ marginBottom: 20 }}>{title}</h4>}
          <Chart
            scale={scale}
            height={title ? height - 41 : height}
            forceFit={forceFit}
            data={data}
            padding={padding || 'auto'}
            onGetG2Instance={this.getG2Instance}
          >
            <Axis
              name="x"
              title={false}
              label={autoHideXLabels ? false : {}}
              tickLine={autoHideXLabels ? false : {}}
            />
            <Axis name="y" min={0} />
            <Axis name="z" min={0} />
            <Tooltip showTitle={false} crosshairs={false} />
            <Geom
              type="interval"
              position="x*y"
              color={color}
              tooltip={['x*y', (x, y) => {
                return {
                  name: '实际上课',
                  title: y,
                  value: y,
                };
              }]}
            />
            <Geom
              type="line"
              position="x*z"
              color="#fad248"
              tooltip={['x*z', (y, z) => {
                return {
                  name: '应上课',
                  title: z,
                  value: z,
                };
              }]}
            />
            <Legend
              custom
              allowAllCanceled
              items={[
                { value: '实际上课', name: 'y', marker: { symbol: 'square', fill: '#41a2fc', radius: 5 }  },
                { value: '应上课', name: 'z', marker: { symbol: 'square', fill: '#fad248', radius: 5 } },
              ]}
              onClick={(ev) => {
                const item = ev.item;
                const value = item.name;
                const checked = ev.checked;
                const geoms = this.chartIns.getAllGeoms();
                for (let i = 0; i < geoms.length; i+=1) {
                  const geom = geoms[i];
                  if (geom.getYScale().field === value && value === 'z') {
                    if (checked) {
                      geom.show();
                    } else {
                      geom.hide();
                    }
                  } else if (geom.getYScale().field === 'y' && value !== 'z') {
                    if (checked) {
                      geom.show();
                    } else {
                      geom.hide();
                    }
                  }
                }
              }}
            />
          </Chart>
        </div>
      </div>
    );
  }
}

export default TeaRate;
