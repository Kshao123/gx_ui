// import $ from "jquery";
import React from "react";
import {
  Chart,
  Geom,
  Axis,
  Tooltip,
} from "bizcharts";
import DataSet from "@antv/data-set";

class UserBirsday extends React.Component {

  getMonthWeek(date) {
    const year = date.getFullYear();
    const month = date.getMonth();
    const monthFirst = new Date(year, month, 0);
    const intervalDays = Math.round(
      (date.getTime() - monthFirst.getTime()) / 86400000
    );
    const index = Math.floor((intervalDays + monthFirst.getDay()) / 7);
    return index;
  }

  render() {
    const { data } = this.props;
    const { DataView } = DataSet;
    const cols = {
      month: {
        type: "cat",
        values: [
          "January",
          "February",
          "March",
          "April",
          "May",
          "June",
          "July",
          "August",
          "September",
          "October",
          "November",
          "December"
        ]
      },
      day: {
        type: "cat"
      },
      week: {
        type: "cat",
        values: ["5", "4", "3", "2", "1", "0"]
      },
      amount: {
        type: "linear",
        min: -10,
        max: 10,
        sync: true
      },
      time: {
        type: "time"
      },
      date: {
        type: "time"
      }
    };

    // 加工数据
    // 增加涨幅、跌幅
    // 添加所属月、周几、每个月的第几周
    data.forEach((obj) => {
      const date = new Date(obj["date"]);
      const month = date.getMonth();
      obj.month = month;
      obj.day = date.getDay();
      obj.week = this.getMonthWeek(date).toString();
    }); // 对数据进行排序

    const dv = new DataView();
    dv.source(data).transform({
      type: "sort-by",
      fields: ["day"],
      order: "DESC"
    });
    return (
      <div>
        <Chart
          height={400}
          data={dv}
          scale={cols}
          forceFit
        >
          <Tooltip title="date" />
          <Axis name="day" visible={false} />
          <Axis name="week" visible={false} />
          <Axis name="date" visible={false} />
          <Geom
            tooltip={['date*name*amount', (time, list) => {
              if (!list.length) {
                return null
              }
              return {
                name: '人员',
                title: time,
                value: list.join('，'),
              };
            }]}
            type="polygon"
            position="day*week*date"
            style={{
                  lineWidth: 1,
                  stroke: "#fff"
                }}
            color={[
                  "amount",
                  "#F51D27-#FA541C-#FFBE15-#FFF2D1-#E3F6FF-#85C6FF-#0086FA-#0A61D7"
                ]}
          />
        </Chart>
      </div>
    );
  }
}

export default UserBirsday;
