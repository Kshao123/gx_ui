import React from 'react';
import { Chart, Axis, Tooltip, Geom, Legend } from 'bizcharts';
import DataSet from '@antv/data-set';
import styles from '../index.less';

class CouresTable extends React.Component {

  chartIns = null;

  render() {
    const {
      data = [],
      title,
      // showLine,
    } = this.props;
    const ds = new DataSet();
    const dv = ds.createView().source(data);
    dv.transform({
      type: 'fold',
      fields: [ '新增', '减少'],
      key: 'type',
      value: 'value',
    });

    const scale = {
      line: {
        type: 'cat',
        min: 0,
      },
    };

    const getG2Instance = (chart) => {
      this.chartIns = chart;
    };
    return (
      <div className={styles.chart}>
        <div>
          {<h4 style={{ marginBottom: 20 }}>{title}</h4>}
          <Chart height={251} forceFit data={dv} scale={scale} padding="auto" onGetG2Instance={getG2Instance}>
            <Legend
              custom
              allowAllCanceled
              items={[
                { value: '新增', marker: { symbol: 'square', fill: '#41a2fc', radius: 5 }  },
                { value: '减少', marker: { symbol: 'square', fill: '#54ca76', radius: 5 } },
              ]}
              onClick={(ev) => {
                const item = ev.item;
                const value = item.value;
                const geoms = this.chartIns.getAllGeoms();
                for (let i = 0; i < geoms.length; i++) {
                  const geom = geoms[i];
                  if (geom.getYScale().field === 'value') {
                    geom.getShapes().map((shape) => {
                      if (shape._cfg.origin._origin.type == value) {
                        shape._cfg.visible = !shape._cfg.visible;
                      }
                      shape.get('canvas').draw();
                      return shape;
                    });
                  }
                }
              }}
            />
            <Axis name="date" />
            <Axis name="value" position={'left'} />
            <Tooltip />
            <Geom
              type="interval"
              position="date*value"
              color={['type', (value) => {
                if (value === 'x') {
                  return '#41a2fc';
                }
                if (value === 'y') {
                  return '#54ca76';
                }
              }]}
              adjust={[{
                type: 'dodge',
                marginRatio: 1 / 32,
              }]}
            />
            {/*<Geom type="line" position="date*value" color="#fad248" />*/}
          </Chart>
        </div>
      </div>
    );
  }
}

export default CouresTable
