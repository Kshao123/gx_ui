import { Popconfirm } from 'antd';
import React, { PureComponent } from 'react';

export default class Popcon extends PureComponent {
  render() {
    const { title, onConfirm, onCancel } = this.props;
    return (
      <Popconfirm
        title={title}
        onConfirm={onConfirm}
        onCancel={onCancel}
        okText="确认"
        cancelText="取消"
      >
        <a>删除</a>
      </Popconfirm>
    );
  }
}
