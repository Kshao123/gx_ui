/* eslint-disable linebreak-style,no-unused-vars */
import React, { PureComponent } from 'react';
import {
  Layout,
  Menu,
  Icon,
  Input,
  Spin,
  Tag,
  Dropdown,
  Avatar,
  Divider,
  Select,
  Modal,
  Form,
  message,
} from 'antd';
import moment from 'moment';
import groupBy from 'lodash/groupBy'; // 数组处理
import Debounce from 'lodash-decorators/debounce';
import { Link } from 'dva/router';
import NoticeIcon from '../NoticeIcon';
import styles from './index.less';

const { Header } = Layout;
const { Option } = Select;

@Form.create()
export default class GlobalHeader extends PureComponent {
  constructor() {
    super();
    this.state = {
      visible: false,
      modal: {
        text: '',
        title: '',
      },
    };
  }
  componentWillUnmount() {
    this.triggerResizeEvent.cancel();
  }

  getNoticeData() { // 获得通知
    const { notices = [] } = this.props;
    if (notices.length === 0) {
      return {};
    }
    const newNotices = notices.map((notice) => {
      const newNotice = { ...notice };
      if (newNotice.datetime) {
        newNotice.datetime = moment(notice.datetime).fromNow(); // 查看相对时间
      }
      // transform id to item key
      if (newNotice.id) {
        newNotice.key = newNotice.id;
      }
      if (newNotice.extra && newNotice.status) {
        const color = ({ // tag 标签颜色
          todo: '',
          processing: 'blue',
          urgent: 'red',
          doing: 'gold',
        })[newNotice.status];
        newNotice.extra = <Tag color={color} style={{ marginRight: 0 }}>{newNotice.extra}</Tag>;
      }
      return newNotice;
    });
    return groupBy(newNotices, 'type');
  }

  handleOk = () => {
    this.setState({
      visible: false,
      modal: {
        text: '',
        title: '',
      },
    });
  };
  handleCancel = () => {
    this.setState({
      visible: false,
      modal: {
        text: '',
        titlr: '',
      },
    });
  };

  handlePassOk = () => {
    const { form, dispatch, handlePassOk } = this.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      if (values.newPasswd !== values.confirmPassWord) {
        message.error('两次输入的密码不一致 请检查后重试', 3);
        return;
      } else if (values.oldPasswd === values.confirmPassWord) {
        message.error('新密码 不能与旧密码相同哦', 3);
        return;
      }
      delete values.confirmPassWord;
      handlePassOk(values);

      // 在此处调用的是 重置表单内容， 不重置的话会导致 对话框再次打开不能实时更新内容的bug
      form.resetFields();
    });
  };


  toggle = () => {
    const { collapsed, onCollapse } = this.props;
    onCollapse(!collapsed);
    this.triggerResizeEvent();
  };
  @Debounce(600)
  triggerResizeEvent() { // eslint-disable-line
    const event = document.createEvent('HTMLEvents');
    event.initEvent('resize', true, false);
    window.dispatchEvent(event);
  }
  render() {
    const {
      currentUser, collapsed, isMobile, logo,
      onNoticeVisibleChange, onMenuClick, onNoticeClear, orgs, handleOrgChange, currentOrg,
      count, changeStatu, loading, PassState, handlePassCancle, form,
    } = this.props;
    // 此处props在 layouts/BasicLayout 中调用
    const { getFieldDecorator } = form;
    const menu = (
      <Menu className={styles.menu} selectedKeys={[]} onClick={onMenuClick}>
        <Menu.Item key="setPassword"><Icon type="edit" />修改密码</Menu.Item>
        {/*<Menu.Item key="reload"><Icon type="reload" />刷新</Menu.Item>*/}
        {/*<Menu.Divider />*/}
        <Menu.Item key="logout"><Icon type="logout" />退出登录</Menu.Item>
      </Menu>
    );
    const noticeData = this.getNoticeData(); // 提醒消息数据
    let orgOptions;
    if (orgs !== undefined) {
      orgOptions = orgs.map((org) => {
        return <Option key={org.id} value={org.id}>{org.orgName}</Option>;
      });
    }
    return (
      <Header className={styles.header}>
        {isMobile && (
          [
            (
              <Link to="/" className={styles.logo} key="logo">
                <img src={logo} alt="logo" width="32" />
              </Link>
            ),
            <Divider type="vertical" key="line" />,
          ]
        )}
        <Icon
          className={styles.trigger}
          type={collapsed ? 'menu-unfold' : 'menu-fold'}
          onClick={this.toggle}
        />
        <div className={styles.right}>
          <span style={{ marginRight: '5px' }} >机构选择</span>
          <Select style={{ width: '150px', marginRight: '10px' }} showSearch placeholder="请选择组织" value={currentOrg ? currentOrg.orgName : ''} onChange={handleOrgChange}>
            {orgOptions}
          </Select>
          <NoticeIcon
            className={styles.action}
            count={count.notifyCount} // 图标上的消息总数
            onItemClick={(item, tabProps) => { // 点击列表项的回调
                const { id, title, description } = item;
                changeStatu({ id, isRead: true });
                this.setState({
                  visible: true,
                  modal: {
                    text: description,
                    title,
                  },
                });
            }}
            onClear={onNoticeClear} // 点击清空按钮的回调
            onPopupVisibleChange={onNoticeVisibleChange} // 弹出卡片显隐的回调
            loading={loading}
            popupAlign={{ offset: [20, -16] }}
          >
            <NoticeIcon.Tab
              list={noticeData.message} // 列表数据，格式参照下表
              title="通知" // 消息分类的页签标题
              emptyText="你已查看所有通知" // 针对每个 Tab 定制空数据文案
              emptyImage="https://gw.alipayobjects.com/zos/rmsportal/wAhyIChODzsoKIOBHcBk.svg"
            />
          </NoticeIcon>
          {currentUser.name ? (
            <Dropdown overlay={menu}>
              <span className={`${styles.action} ${styles.account}`}>
                <Avatar className={styles.avatar} icon="user" />
                <span className={styles.name}>{currentUser.name}</span>
              </span>
            </Dropdown>
          ) : <Spin size="small" style={{ marginLeft: 8 }} />}
        </div>
        <Modal
          title={this.state.modal.title}
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <p>{this.state.modal.text}</p>
        </Modal>
        <Modal
          title="修改密码"
          visible={PassState}
          onOk={this.handlePassOk}
          onCancel={handlePassCancle}
        >
          <Form.Item label="请输旧密码">
            {getFieldDecorator('oldPasswd', {
              rules: [{ required: true, message: '请输入' }],
            })(
              <Input type="password" placeholder="请输入" />
            )}
          </Form.Item>
          <Form.Item label="请输入新密码">
            {getFieldDecorator('newPasswd', {
              rules: [{ required: true, message: '请输入' }],
            })(
              <Input type="password" placeholder="请输入" />
            )}
          </Form.Item>
          <Form.Item label="请确认密码">
            {getFieldDecorator('confirmPassWord', {
              rules: [{ required: true, message: '请输入' }],
            })(
              <Input type="password" placeholder="请输入" />
            )}
          </Form.Item>
        </Modal>
      </Header>
    );
  }
}
