import React, { Component } from 'react';
import { connect } from 'dva';
import {
  Tooltip,
  Popover,
  Spin,
} from 'antd';

@connect(({loading, api})=>({
  api,
  loading: loading.effects['api/getWxCode'],
}))
class WxCode extends Component{

  handleChange = (visible) => {
    const { dispatch, data, userType } = this.props;
    const { id: userId } = data;
    if (visible) {
      dispatch({
        type: 'api/getWxCode',
        payload: {
          userId,
          userType,
        }
      })
    } else {
      dispatch({
        type: 'api/saveWxCode',
        payload: '',
      })
    }
  };

  render() {
    const { data={}, name= 'name', loading, api: { WxCode: code } } = this.props;
    return(
      <Spin spinning={!(Object.keys(data).length)} wrapperClassName="div">
        <Tooltip title={`点击查看${data[name] || 'loading...'}的绑定信息`}>
          <Popover
            onVisibleChange={this.handleChange}
            content={(
              <Spin spinning={!!loading} wrapperClassName="div">
                <img src={code || null} alt="loading..." />
              </Spin>
          )}
            title={`${data[name] || 'loading...'}`}
            trigger="click"
          >
            <a>{data[name] || 'loading...'}</a>
          </Popover>
        </Tooltip>
      </Spin>
    )
  }
}

export default WxCode;
