import React, { PureComponent } from 'react';
import { Modal, Form, Table, Alert, notification } from 'antd';
import { connect } from 'dva';
import debounce from 'lodash/debounce';
import styles from './index.less';

function initTotalList(columns) {
  const totalList = [];
  columns.forEach((column) => {
    if (column.needTotal) {
      totalList.push({ ...column, total: 0 });
    }
  });
  return totalList;
}
@Form.create()
@connect(({ usermgr }) => ({
  usermgr,
}))

class StandardTable extends PureComponent {
  constructor(props) {
    super(props);
    // const { columns } = props;
    // const needTotalList = initTotalList(columns);
    this.state = {
      selectedRowKeys: [],
      // needTotalList,
      show: false,
      visible: false,
      SearchList: [],
    };
  }

  componentWillMount() // do 执行一次，在初始化render之前执行
  {
    const { selectedRows = [] } = this.props;
    this.setState({
      selectedRowKeys: selectedRows,
    })
  }

  componentWillReceiveProps(nextProps) {
    const { selectedRows = [], SearchList = [] } = nextProps; // do 从父级获取选中行数的数据 方便删除操作
    this.setState({
      selectedRowKeys: selectedRows,
      SearchList,
    })
  }


  handleRowSelectChange = (selectedRowKeys, selectedRows) => { // do 选择框 发生改变时 在change中调用自带两个参数
    // do selectedRowKeys table 里的 rowkeys； selectedRows 当前行数渲染的数据
    // let needTotalList = [...this.state.needTotalList];
    const { onSelectRow } = this.props;
    let { show } = this.state;
    // needTotalList = needTotalList.map((item) => ({
    //     ...item,
    //     total: selectedRows.reduce((sum, val) => sum + parseFloat(val[item.dataIndex], 10), 0),
    //   }));
    if (onSelectRow) {
      onSelectRow(selectedRowKeys);
    }
    selectedRowKeys.length ? show = true : show = false; // do 选中时 改变删除的状态
    this.setState({ selectedRowKeys, show });
  };

  handleAlldel = () => { // do 点击删除时的回调
    const { selectedRowKeys } = this.state;
    const { onDel } = this.props;
    onDel(selectedRowKeys);
  };

  handleTableChange = (pagination, filters, sorter) => { // do 页码改变的回调，参数是改变后的页码及每页条数
    this.props.onChange(pagination, filters, sorter);
  };

  cleanSelectedKeys = () => { // do 点击清空时的回调
      const { selectedRowKeys } = this.state;
      selectedRowKeys.length ? this.handleRowSelectChange([], []) : ''
  };

  // handleChange = (value) => { // do 当选中搜索内容时的调用 参数value 为每次点击的搜索结果的数组
  //   let SearchList = [];
  //   const { data: { list }, SelKey } = this.props;
  //   const { key } = value;
  //   SearchList = list.filter(item => item[SelKey[1]] === key);
  //   this.setState({
  //     value,
  //     SearchList,
  //   });
  // };

  handleCancel = ()=>
  {
      this.setState({ visible: false })
  };

  handleOk = ()=>
  {
      this.setState({
        visible: false,
      })
  };
  render() {
    const { selectedRowKeys, needTotalList, visible, SearchList } = this.state;
    const {
      bordered,
      Rowselect,
      data: { list = [], pagination },
      loading,
      columns,
      scroll,
      expandedRowRender,
      showAllSel,
      footer,
    } = this.props;
    const paginationProps = {
      showSizeChanger: true,
      showQuickJumper: true,
      ...pagination,
    };
    const rowSelection = {
      selectedRowKeys,
      onChange: this.handleRowSelectChange,
      getCheckboxProps: record => ({
        disabled: record.disabled,
      }),
    };
    return (
      <div className={styles.standardTable}>
        <div style={{ display: showAllSel ? 'block' : 'none' }} className={styles.tableAlert}>
          <Alert
            message={(
              <div>
                已选择 <a style={{ fontWeight: 600 }}>{selectedRowKeys.length}</a> 项&nbsp;&nbsp;
                {/*{*/}
                {/*  needTotalList.map(item => (*/}
                {/*    <span style={{ marginLeft: 8 }} key={item.dataIndex}>{item.title}总计&nbsp;*/}
                {/*      <span style={{ fontWeight: 600 }}>*/}
                {/*        {item.render ? item.render(item.total) : item.total}*/}
                {/*      </span>*/}
                {/*    </span>*/}
                {/*    )*/}
                {/*  )*/}
                {/*}*/}
                <a onClick={this.cleanSelectedKeys} style={{ marginLeft: 24 }}>清空</a>&nbsp;&nbsp;
                <a onClick={this.handleAlldel} style={{ marginLeft: 24, display: this.state.show ? 'inline-block' : 'none' }}>删除</a>
              </div>
            )}
            type="info"
            showIcon
          />
        </div>
        <Table
          bordered={bordered}
          loading={loading}
          rowKey={record => record.id}
          rowSelection={!Rowselect ? null : rowSelection}
          dataSource={list}
          columns={columns}
          pagination={paginationProps}
          onChange={this.handleTableChange}
          scroll={scroll}
          expandedRowRender={expandedRowRender || null}
          footer={footer || null}
        />
        <Modal
          visible={visible}
          onCancel={this.handleCancel}
          onOk={this.handleOk}
        >
          <Table
            loading={loading}
            rowKey={record => record.id}
            dataSource={SearchList}
            columns={columns}
          />
        </Modal>
      </div>
    );
  }
}

export default StandardTable;

// 此版本算 简单精简了，去掉了，搜索 小组件，无需 再showSearch
