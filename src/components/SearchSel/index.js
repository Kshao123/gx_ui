import { Select, Spin, notification, message } from 'antd';
import debounce from 'lodash/debounce';
import React, { PureComponent } from 'react';
import { deteleObject } from '../../utils/utils';

const { Option } = Select;
const defaultOptions = {
  credentials: 'include',
};
const codeMessage = {
  200: '服务器成功返回请求的数据',
  201: '新建或修改数据成功。',
  202: '一个请求已经进入后台排队（异步任务）',
  204: '删除数据成功。',
  400: '发出的请求有错误，服务器没有进行新建或修改数据,的操作。',
  401: '用户没有权限（令牌、用户名、密码错误）。',
  403: '用户得到授权，但是访问是被禁止的。',
  404: '发出的请求针对的是不存在的记录，服务器没有进行操作',
  406: '请求的格式不可得。',
  410: '请求的资源被永久删除，且不会再得到的。',
  422: '当创建一个对象时，发生一个验证错误。',
  500: '服务器发生错误，请检查服务器',
  502: '网关错误',
  503: '服务不可用，服务器暂时过载或维护',
  504: '网关超时',
};
function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const errortext = codeMessage[response.status] || response.statusText;
  if (response.status !== 401) {
    notification.error({
      message: `请求错误 ${response.status}: ${response.url}`,
      description: errortext,
    });
  }
  const error = new Error(errortext);
  error.name = response.status;
  error.response = response;
  throw error;
}
export default class UserRemoteSelect extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      value: props.value || [],
      fetching: false,
      defaultValue: props.value || '',
      list: [],
    };
    this.lastFetchId = 0;
    this.fetchUser = debounce(this.fetchUser, 800);
  }

  componentWillReceiveProps(nextProps) {
    if ('value' in nextProps) {
      this.setState({
        value: nextProps.value || [],
        defaultValue: nextProps.value,
      });
    }
  }


  fetchUser = (value) => {
    if (value.length === 0) return;
    console.log('fetching user', value);
    this.lastFetchId += 1;
    const fetchId = this.lastFetchId;
    this.setState({ data: [], fetching: true });

    const {
      queryUrl,
      keyValue,
      searchData,
      placeholder,
      attr: { id, name },
      orgId,
      removal,
      leaveIdean,
      hidePhone,
    } = this.props;
    const body = {
      pageNum: 1,
      [keyValue]: value,
      pageSize: 1000,
    };
    if (leaveIdean) body.leaveIdean = leaveIdean;
    if (orgId) body.orgId = orgId;
    const newOptions = { ...defaultOptions, ...{ method: 'POST', body } };
    if (newOptions.method === 'POST' || newOptions.method === 'PUT') {
      newOptions.headers = {
        Accept: 'application/json',
        'Content-Type': 'application/json; charset=utf-8',
        ...newOptions.headers,
      };
      newOptions.body = JSON.stringify(newOptions.body);
    }

    fetch(queryUrl, newOptions)
      .then(res => {
        if (!res || res.redirected || res.status !== 200) {
          return ''
        }
        return res.json();
      })
      .then((res) => {
        if (fetchId !== this.lastFetchId) { // for fetch callback order
          return;
        }
        if (!res || !res.status) {
          notification.error({
            message: '查询失败',
            description: '服务连接失败',
          });
          this.setState({
            fetching: false,
          });
          return;
        }
        const { status, result: { list }, message: msg } = res;
        let data;
        if (!list.length) {
          message.success('查询成功, 暂无记录', 0.5);
          data = [];
        } else {
          message.success(`${placeholder}为${value}`, 1);
          data = list.map((item) => {
            let phone = '';
            if (!hidePhone) {
              if (item.phone || item.motherPhone || item.fatherPhone) phone = `-${item.phone || item.motherPhone || item.fatherPhone}`;
            }
            return {
              text: `${item[name]}${phone}`,
              value: item.id,
            };
          });
        }
        if (searchData) searchData(res);
        if (removal) data = deteleObject(data, 'text');
        this.setState({ data, fetching: false, list: res.result });
      });
  };

  handleChange = (value) => {
    const { onChange, attr: { id } } = this.props;
    if (onChange) onChange(value, id, this.state.list);
    this.setState({
      value,
      data: [],
      fetching: false,
    });
    this.sel.blur();
  };

  handleFocus = () => {
    this.lastFetchId += 1;
    const fetchId = this.lastFetchId;
    this.setState({ data: [], fetching: true });

    const {
      queryUrl,
      searchData,
      attr: { name },
      orgId,
      removal,
      hidePhone,
    } = this.props;
    const body = {
      pageNum: 1,
      pageSize: 1000,
    };
    if (orgId) body.orgId = orgId;
    const newOptions = { ...defaultOptions, ...{ method: 'POST', body } };
    if (newOptions.method === 'POST' || newOptions.method === 'PUT') {
      newOptions.headers = {
        Accept: 'application/json',
        'Content-Type': 'application/json; charset=utf-8',
        ...newOptions.headers,
      };
      newOptions.body = JSON.stringify(newOptions.body);
    }

    fetch(queryUrl, newOptions)
      .then(res => {
        if (!res || res.redirected || res.status !== 200) {
          return ''
        }
        return  res.json()
      })
      .then((res) => {
        if (fetchId !== this.lastFetchId) { // for fetch callback order
          return;
        }
        if (!res && !res.status) {
          notification.error({
            message: '查询失败',
            description: '服务连接失败',
          });
          return;
        }
        const { result: { list }, message: msg } = res;
        let data;
        if (!list.length) {
          data = [];
        } else {
          data = list.map((item) => {
            let phone = '';
            if (!hidePhone) {
              if (item.phone || item.motherPhone || item.fatherPhone) phone = `-${item.phone || item.motherPhone || item.fatherPhone}`;
            }
            return {
              text: `${item[name]}${phone}`,
              value: item.id,
            };
          });
        }
        if (searchData) searchData(res);
        if (removal) data = deteleObject(data, 'text');
        this.setState({ data, fetching: false, list: res.result });
      });
  };


  render() {
    const { fetching, data, value } = this.state;
    const {
      placeholder,
      showSearch,
      width,
      disabled,
      allowClear,
    } = this.props;
    return (
      <Select
        showSearch={showSearch}
        mode={showSearch ? false : 'multiple'}
        labelInValue
        value={value}
        placeholder={placeholder}
        notFoundContent={fetching ? <Spin size="small" /> : null}
        filterOption={false}
        onSearch={this.fetchUser}
        onChange={this.handleChange}
        style={{ width }}
        disabled={disabled}
        onFocus={this.handleFocus}
        ref={sel => this.sel = sel}
        allowClear={allowClear}
      >
        {data.map(d => <Option key={d.value}>{d.text}</Option>)}
      </Select>
    );
  }
}

// 需要参数 keyValue: post 需要查询的key名
//         placeholder： 默认显示的文字
//         showSearch：单个或多个查询
//         query：查询的url
//         onChange: 点击下拉框的回调
//         attr:Obj { id，name } // id onchange 中setstate 所需保存的键名 name 是查询中在下拉框显示的键名
//         orgId: 机构ID
//         searchData 搜索完成的回调 // 搜索时的回调 接受参数为 根据关键词返回的所有列表
//         removal 此值存在时将会 对 data（搜索时 生成的下拉框数据） 中的某一个 key值进行 去重
//         hidePhone 是否取消手机号码的显示 true 取消
//         allowClear 是否显示清空按钮 true 为显示
