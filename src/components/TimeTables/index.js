import React, { Component } from 'react';
import classnames from 'classnames'
import {
  Modal,
  Form,
  Radio,
  Spin,
  Popover,
  DatePicker,
  Popconfirm,
} from 'antd';
import moment from 'moment'
import styles from './timeTables.less';
import { parseTime, url } from '../../utils/utils';
import SearchSel from '@/components/SearchSel';

const RadioGroup = Radio.Group;
const timeType = {
  8: 0,
  9: 1,
  10: 2,
  11: 3,
  12: 4,
  13: 5,
  14: 6,
  15: 7,
  16: 8,
  17: 9,
  18: 10,
  19: 11,
  20: 12,
  21: 13,
  22:14,
};
const defaultPalette = ['#48a8e4', '#f05261', '#ffd061', '#52db9a', '#70d3e6', '#52db9a', '#3f51b5', '#f3d147', '#4adbc3', '#673ab7', '#f3db49', '#76bfcd', '#b495e1', '#ff9800', '#8bc34a'];

@Form.create()
class TimeTables extends Component{
  constructor() {
    super();
    this.state = {
      props: {
        onChange: this.handleValueChange,
        queryUrl: `${url}/classroom/queryAll`,
        attr: {
          id: 'classroomId',
          name: 'classroomName',
        },
        placeholder: '搜索教室',
        keyValue: 'name',
      },
      elWidth: undefined,
      contentWidth: undefined,
      listWidth: undefined,
      visible: false,
      currentEl: undefined,
      timePos: undefined,
      nearTime: undefined,
      showMenu: false,
      menuItems: [],
      studentcourseids: [],
    };
  }

  componentDidMount() {
    // 储存元素的值 渲染表格时所需
    this.setState({
      elWidth: this.coursesContent.offsetWidth,
      contentWidth: parseInt((this.coursesContent.offsetWidth - this.listWidth.offsetWidth) / 7),
      listWidth: this.listWidth.offsetWidth,
    })
  }

  renderBorderList = (max = 60) => {
    const list = [];
    const { elWidth, listWidth } = this.state;
    for (let i = 0; i < max; i += 1) {
      if ((i+1) % 4 === 0 && i !== 0) {
        // 这里的 15 为当前需要边框的元素的高度，28 为星期的高度
        list.push(
          <div
            style={{ position: 'absolute', width: '100%', height: 15, top: 15 * i + 28, left: 0, borderBottom: '1px solid rgb(238, 238, 238)' }}
            key={`${i}`}
          />
        )
      }else {
        list.push(
          <div
            style={{ position: 'absolute', width: elWidth - listWidth, height: 15, top: 15 * i + 28, left: listWidth, borderBottom: '1px dashed rgb(238, 238, 238)' }}
            key={`${i}`}
          />
        )
      }
    }
    return list;
  };

  ContextMenu = (e) => {
    e.preventDefault();
    const el = e.currentTarget;
    const studentcourseids = e.currentTarget.getAttribute('studentcourseids');
    const showMenu = !!studentcourseids && studentcourseids !== 0;
    const left =e.clientX - this.courseWrapper.getBoundingClientRect().left;
    const top = e.clientY - this.courseWrapper.getBoundingClientRect().top;
    // console.log(left, top);
    this.menu.style.left = `${left - 1}px`;
    this.menu.style.top = `${top - 4}px`;
    this.menu.onmouseout = this.MouseOut;
    this.menu.onmouseover = this.MouseOver;
    this.setState({
      showMenu,
      menuItems: (
        <div>
          <Popconfirm title="确认跳过此节课？" onConfirm={e => this.handleStopCourse(studentcourseids)} okText="确认" cancelText="取消">
            <p>
              跳课
            </p>
          </Popconfirm>
        </div>

      )
    });
  };

  MouseOver = e => {
    this.setState({
      showMenu: true,
    })
  };

  MouseOut = (e) => {
    this.setState({
      showMenu: false,
    })
  };

  handleStopCourse = (studentcourseids) => {
    const { handleSkipCourse } = this.props;
    handleSkipCourse(studentcourseids);
  };

  mouseDown = (e) => {
    this.disX = 0;
    this.disY = 0;
    // 记录鼠标抬起时 移动的距离
    this.top = 0;
    this.left = 0;
    // 为 0 则为鼠标左键
    if (e.button === 0) {
      let el = e.target;
      let cEl;
      this.scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
      this.scrollLeft = document.documentElement.scrollLeft || document.body.scrollLeft;
      // 如果点击的元素为子元素 span 的话则需要给父级 div 加上浅色背景
      if (el.parentNode.className.split(' ').includes(classnames(styles.courseRect))) {
        el = el.parentNode;
      }
      this.disX = e.clientX + this.scrollLeft - el.offsetLeft;
      this.disY = e.clientY + this.scrollTop - el.offsetTop;
      cEl = el.cloneNode(true);
      el.style.opacity = 0.5;
      if (cEl) {
        cEl.id = 'cEl';
        cEl.style.zIndex = 4;
        this.courseWrapper.appendChild(cEl);
      }
      // 记录当前元素的初始位置
      this.oldPos = { top: parseFloat(el.style.top), left: parseFloat(el.style.left) };
      document.onmousemove = e => this.mouseMove(e, el);
      document.onmouseup = e => this.mouseUp(e, el);
    }
    e.preventDefault();
  };

  mouseMove = (e) => {
    const cEl = document.getElementById('cEl');
    const left = Math.min(Math.max(e.clientX + this.scrollLeft - this.disX, 66), 800);
    const Top = Math.min(Math.max(e.clientY + this.scrollTop - this.disY, 28), 923);
    this.top = Top;
    this.left = left;
    if (cEl) {
      cEl.style.left = `${left  }px`;
      cEl.style.top = `${Top  }px`;
    }
  };

  mouseUp = (e, el) => {
    const cEl = document.getElementById('cEl');
    if (cEl) {
      const top = this.top || parseFloat(cEl.style.top);
      const left = this.left || parseFloat(cEl.style.left);
      const timePos = this.checkPos({ top, left }, this.timePos, this.weekPos);
      this.courseWrapper.removeChild(cEl);
      el.style.top = `${top}px`;
      el.style.left = `${left}px`;
      const { top: oldTop, left: oldLeft } = this.oldPos;
      // 现在位置与移动之前的位置对比
      if (oldLeft) {
        if (!(oldLeft === left) || !(oldTop === top)) {
          this.handleMoveConfirm(el, timePos, { top, left });
        }
      }
    }
    el.style.opacity = 1;
    document.onmouseup = document.onmousemove = null;
  };

  checkPos = (nowPos, topPos, leftPos) => {
    // 查找移动后的元素的大概位置
    const { left, top } = nowPos;
    const length1 = Object.keys(topPos).length;
    const length = Object.keys(leftPos).length;
    let T = 0;
    let L = 0;
    for (let i = 0; i < length1; i += 1 ) {
      if (i >= length1 - 1) {
        T = length1 -1;
        break;
      }
      if (topPos[i].top === top) {
        T = i;
        break;
      }
      if (topPos[i].top < top && topPos[i + 1].top > top) {
        T = i;
        break;
      }
    }
    for (let j = 0; j < length; j += 1 ) {
      if (j >= length - 1) {
        L = length -1;
        break;
      }
      if (leftPos[j].left === left) {
        L = j;
        break;
      }
      if (leftPos[j].left < left + 60 && leftPos[j + 1].left > left + 60) {
        L = j;
        break;
      }
    }
    return { top: T, left: L }

  };

  handleMoveConfirm = (el, timePos, newPos) => {
    const { top } = timePos;
    const { top: newTop } = newPos;
    const oldTime = this.timePos[top].top;
    // 记录当前的时间值
    const currentTime = this.timePos[top].time;
    const nearTime = Math.min(Math.round((newTop - oldTime)/5)*5, 55);
    this.setState({
      visible: true,
      currentEl: el,
      currentTime,
      nearTime,
      timePos,
    });
  };

  handleOk = () => {
    const { form, onOk } = this.props;
    const { resetFields } = form;
    const { currentEl, timePos } = this.state;
    // const top = this.timePos
    // console.log(currentEl);
    const { validateFieldsAndScroll } = form;
    validateFieldsAndScroll((error, values) => {
      if (error) return;
      // 此操作待改善
      const data = values;
      const { timeType: timeTypes, startTime, timeDate } = data;
      // 获取时间
      // console.log(timeDate.toDate().getDay(), );
      const Day = timeDate.toDate();
      const Hours = this.timePos[timePos.top].time.split(':')[0];
      Day.setHours(Hours);
      Day.setMinutes(parseInt(startTime, 10));
      Day.setSeconds(0);
      // 获取正确的开始时间
      const getTime = Day.getTime();
      const top = `${this.timePos[timePos.top].top + startTime}px`;
      // 获取当天时间
      const left = `${this.weekPos[timePos.left].left}px`;
      const studentCourseIds = currentEl.getAttribute('studentcourseids') || null;
      currentEl.style.top = top;
      currentEl.style.left = left;
      onOk(
        {
        studentCourseIds,
        startTime: getTime,
        timeType: timeTypes,
        // classroomId: parseInt(classroomId.key, 10)
        },
        () => {
          resetFields();
          this.setState({
            visible: false
          });
        }
      )
    });

    // resetFields();
    // this.setState({
    //   visible: false
    // });
  };

  handleCancel = () => {
    const { currentEl } = this.state;
    const { top, left } = this.oldPos;
    currentEl.style.top = `${top}px`;
    currentEl.style.left = `${left}px`;
    this.props.form.resetFields();
    this.setState({
      visible: false
    });
  };

  renderWeek = () => {
    const { currentDay: xDay } = this.props;
    const data = xDay ? new Date(xDay) : new Date();
    let week = data.getDay();
    if (week === 0) {
      week = 6
    } else {
      week -= 1
    }
    const currentDay = week !== 0 ? data.getTime() - (24 * week) * 60 * 60 * 1000 : data.getTime();
    // console.log(currentDay);
    const content = [];
    for (let i = 0; i < 7; i += 1) {
      content.push({
        date: currentDay + (24 * i) * 60 * 60 * 1000
      })
    }
    return content;
  };

  renderRadioList = () => {
    const RadioList = [];
    const { currentTime } = this.state;
    if (!currentTime) return;
    const HOURS = currentTime.split(':')[0];
    for (let i = 0; i < 60; i += 5) {
      let MIN = i;
      if (MIN < 10) {
        MIN = `0${  MIN}`;
      }
      RadioList.push(
        <Radio key={i} value={i}>{`${HOURS}:${MIN}`}</Radio>
      )
    }
    return RadioList;
  };

  renderRectEl = (timetables) => {
    const arr = [];
    if (!Object.keys(this.weekPos).length) return;
    for (const item in timetables) {
      const list = timetables[item];
      list.forEach((items) => {
        const data = new Date(items.startTime);
        const TOP = data.getHours();
        if (timeType[TOP] !== 0 && !timeType[TOP]) return;
        const top = `${this.timePos[timeType[TOP]].top + data.getMinutes()}px`;
        const left = `${this.weekPos[parseInt(item, 10) - 1].left}px`;
        const height = `${(items.endTime - items.startTime) / 1000 / 60}px`;
        const content = (
          <div>
            <ul className={styles.rectList}>
              <li>
                教师姓名：{items.teaName}
              </li>
              <li>
                学生姓名：{items.stuNameList.join('，')}
              </li>
              <li>
                课程：{items.courseName}
              </li>
              <li>
                等级：{items.gradeName}
              </li>
              <li>
                教室：{items.classroomName}
              </li>
              <li>
                开始时间：{parseTime(items.startTime, '{h}:{i}')}
              </li>
              <li>
                结束时间：{parseTime(items.endTime, '{h}:{i}')}
              </li>
              <li>
                授课类型：{['', '一对一授课', '小组授课'][items.courseType]}
              </li>
            </ul>
          </div>
        );
        arr.push(
          <Popover key={items.startTime} placement="bottom" content={content} title={items.teaName}>
            <div
              className={styles.courseRect}
              style={{
                top,
                left,
                width: 122,
                height,
                backgroundColor: '#def3fc',
              }}
              onMouseDown={this.mouseDown}
              onContextMenu={this.ContextMenu}
              studentcourseids={items.courseIdList.join(',')}
            >
              <p>
                课程：{items.courseName}
              </p>
              <p>
                学生：{items.stuNameList[0]}
              </p>
            </div>
          </Popover>
        )
      })
    }
    return arr;
  };

  render() {
    const { handleOk, handleCancel, renderWeek } = this;
    const { form, timetableType, currentOrg, loading, timetables } = this.props;
    const { getFieldDecorator } = form;
    const {
      contentWidth,
      listWidth,
      visible,
      nearTime,
      timePos,
      showMenu,
      menuItems,
    } = this.state;
    this.timePos = {};
    this.weekPos = {};
    const formItemLayout = {
      labelCol: { span: 4 },
      wrapperCol: { span: 8 },
    };
    return(
      <section style={{ height: 928, width: 920 }} className={styles.coursesTable} onContextMenu={e => e.preventDefault()}>

        <div ref={e => { this.courseWrapper = e }} className={styles.courseWrapper} style={{ height: 928, width: 920 }}>

          {/* 头部数据 渲染每周 */}
          <div className={styles.coursesHead} style={{ height: 28 }}>
            {contentWidth ? (
              renderWeek().map((item, index) => (
                <div key={item.date} style={{ width: contentWidth, left: contentWidth * index + listWidth}}>
                  {
                    <div className={(new Date().getDate() === new Date(item.date).getDate()) && (new Date().getMonth() === new Date(item.date).getMonth()) ? styles.heightLightWeek : ''} style={{ textAlign: 'center', width: '100%'}}>
                      <span>{`周${parseTime(item.date, '{a}')}`}</span>
                      <span>{`${parseTime(item.date, '{m}-{d}')}`}</span>
                    </div>
                  }
                </div>
                ))
            ) : 'loading'}
          </div>

          {/* 边框渲染 */}
          <div className={styles.rgird}>
            <div style={{ height: 28, borderBottom: '1px dashed #cbc5c7' }}>
              {/* 占位 此处的高度为顶部 星期的高度 */}
            </div>
            {contentWidth ? this.renderBorderList() : 'loading'}
          </div>

          {/* 表格具体内容数据渲染 */}
          <div ref={(e) => { this.coursesContent = e }} className={styles.coursesContent} style={{ width: 920 }}>
            {contentWidth ? (
              renderWeek().map((item, index) => {
                const left = contentWidth * index + listWidth;
                this.weekPos[index] = { left, top: 0, time: item.date };
                return (
                  <div
                    key={item.date}
                    style={{ width: contentWidth, left, height: '100%', borderRight: '1px solid rgb(238, 238, 238)' }}
                    index={index}
                  />
                )
              })
            ) : 'loading'}
          </div>

          {/* 表格侧边时间渲染 */}
          <div ref={(e) => { this.listWidth = e }} style={{ width: 66 }} className={styles.coursesLeftHand}>
            {/* 此处 height 为动态修改 时间轴的占位元素 */}
            {
              timetableType ? timetableType.map((item, num) => {
                const { time, index } = item;
                const top = 60 * num + 28;
                this.timePos[num] = { left: 0, top, time, index };
                return (
                  <div
                    key={index}
                    className={styles.timeList}
                    style={{ width: 66, height: 60, lineHeight: '60px', top, }}
                  >
                    <span>
                      {time}
                    </span>
                  </div>
                )
              }) : 'loading'
            }
          </div>

          <div>
            {/* 具体课程元素 */}
            {this.renderRectEl(timetables)}
          </div>

          {/* 右键菜单 */}
          <div
            className={styles.menu}
            ref={e => this.menu = e}
            style={{ display: showMenu ? 'block' : 'none'  }}
          >
            {menuItems}
          </div>

        </div>

        <Modal
          title="选择课程周数"
          visible={visible}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <Spin spinning={loading}>
            <Form layout="horizontal">
              <Form.Item label="时长" {...formItemLayout}>
                {getFieldDecorator('timeType', {
                  initialValue: 1,
                  rules: [{ required: true, message: '请选择调课状态' }],
                })(
                  <RadioGroup>
                    <Radio value={0}>短期</Radio>
                    <Radio value={1}>长期</Radio>
                  </RadioGroup>
                )}
              </Form.Item>
              <Form.Item label="日期" {...formItemLayout}>
                {getFieldDecorator('timeDate', {
                  initialValue: timePos ? moment(parseTime(this.weekPos[timePos.left].time)) : moment(new Date()),
                  rules: [{ required: true, message: '请选择具体的时间' }],
                })(
                  <DatePicker />
                )}
              </Form.Item>

              <Form.Item label="开始时间" {...formItemLayout}>
                {getFieldDecorator('startTime', {
                  initialValue: nearTime || nearTime === 0 ? nearTime : 5,
                  rules: [{ required: true, message: '请选择开始时阿' }],
                })(
                  <RadioGroup>
                    {this.renderRadioList()}
                  </RadioGroup>
                )}
              </Form.Item>
              {/* <Form.Item label="教室" {...formItemLayout}> */}
              {/*  {getFieldDecorator('classroomId', { */}
              {/*    rules: [{ required: true, message: '请选择教室' }], */}
              {/*  })( */}
              {/*    <SearchSel showSearch orgId={currentOrg.id || ''} {...this.state.props} /> */}
              {/*  )} */}
              {/* </Form.Item> */}
            </Form>
          </Spin>
        </Modal>
      </section>
    )
  }
}

export default TimeTables
