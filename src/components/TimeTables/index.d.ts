import * as React from 'react';

export interface ITime {
  timetables?: any[]; // 必填 二维数组 数组第二维中每项长度需要和 timetableType 中每一项的长度的累计总和一致, 长度不足时会自动以空字符串追加补全。同一天内临近的日程相同时会自动合并为一格展示为false时不自动合并)。
  timetableType?: any[]; // 二维数组 表格时间内容数据
  week: any[]; // 星期的数组 周一至周五
  highlightWeek?: number; // 接受时间戳 为判断周几 给对应的加上高亮class
  styles?: any; // 表格样式: **Gheight** 为表格内每一个单元格高度(number)单位为'px' <br/>  **leftHandWidth** 为表格左侧日程分类样式宽带度(number)单位为'px'<br/>**palette** 为合并相同课程单元格后颜色调色盘,默认有15种颜色,可以传入颜色数组自定义(传入的颜色会与默认颜色合并,并优先使用自定义颜色)
  setOption?: any;
  gridOnClick?: (e: any) => void,
  currentDay?: Date
}

export default class TimeTables extends React.Component<ITime, any> {}
