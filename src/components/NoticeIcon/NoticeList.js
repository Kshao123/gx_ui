import React from 'react';
import { Avatar, List } from 'antd';
import classNames from 'classnames';
import styles from './NoticeList.less';

const Persontype = ['', '校区负责人', '机构法人', '高级法人'];
export default function NoticeList({
                                     data = [], onClick, onClear, title, locale, emptyText, emptyImage,
                                   }) {
  if (data.length === 0) {
    return (
      <div className={styles.notFound}>
        {emptyImage ? (
          <img src={emptyImage} alt="not found" />
        ) : null}
        <div>{emptyText || locale.emptyText}</div>
      </div>
    );
  }
  const list = data.filter(item => item.msgStatus === 2);
  return (
    <div>
      <List className={styles.list}>
        {list.map((item, i) => {
          const itemCls = classNames(styles.item, {
            [styles.read]: item.read,
          });
          return (
            <List.Item className={itemCls} key={item.key || i} onClick={() => onClick(item)}>
              <List.Item.Meta
                className={styles.meta}
                avatar={item.avatar ? <Avatar className={styles.avatar} src={item.avatar} /> : null}
                title={
                  <div className={styles.title}>
                    {item.title}
                    <div className={styles.extra}>{item.extra}</div>
                  </div>
                }
                description={
                  <div>
                    <div className={styles.description} title={item.description}>
                      {item.description}
                    </div>
                    <div className={styles.datetime}>{item.datetime}
                      <span style={{ marginLeft: 5 }}>{item.isRead ? '已读' : '未读'}</span>
                      <span style={{ marginLeft: 5, float: 'right' }}>{item.msgReleaseName}</span>
                      <span style={{ float: 'right' }}>{Persontype[item.msgReleaseType]}</span>
                    </div>
                  </div>
                }
              />
            </List.Item>
          );
        })}
      </List>
      <div className={styles.clear} onClick={onClear}>
        清空已读消息
      </div>
    </div>
  );
}
