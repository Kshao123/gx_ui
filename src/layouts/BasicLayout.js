import React, { Suspense } from 'react';
import { Layout, message } from 'antd';
import DocumentTitle from 'react-document-title';
import isEqual from 'lodash/isEqual';
import memoizeOne from 'memoize-one';
import { connect } from 'dva';
import { ContainerQuery } from 'react-container-query';
import classNames from 'classnames';
import pathToRegexp from 'path-to-regexp';
import Media from 'react-media';
import { formatMessage } from 'umi/locale';
import Authorized from '@/utils/Authorized';
import Footer from './Footer';
import Context from './MenuContext';
import Exception403 from '../pages/Exception/403';
import PageLoading from '@/components/PageLoading';
import SiderMenu from '@/components/SiderMenu';
import GlobalHeader from '@/components/NewHeader';
import logo from '../assets/logo.png';
import Newlogo from '../assets/Newlogo.png';

import styles from './BasicLayout.less';

const SettingDrawer = React.lazy(() => import('@/components/SettingDrawer'));

const { Content } = Layout;

const query = {
  'screen-xs': {
    maxWidth: 575,
  },
  'screen-sm': {
    minWidth: 576,
    maxWidth: 767,
  },
  'screen-md': {
    minWidth: 768,
    maxWidth: 991,
  },
  'screen-lg': {
    minWidth: 992,
    maxWidth: 1199,
  },
  'screen-xl': {
    minWidth: 1200,
    maxWidth: 1599,
  },
  'screen-xxl': {
    minWidth: 1600,
  },
};

class BasicLayout extends React.PureComponent {
  constructor(props) {
    super(props);
    this.getPageTitle = memoizeOne(this.getPageTitle);
    this.matchParamsPath = memoizeOne(this.matchParamsPath, isEqual);
    const { dispatch } = props;
    dispatch({
      type: 'layoutloading/fetchAuth',
    })
  }

  componentDidMount() {
    const {
      dispatch,
      route: { routes, authority },
    } = this.props;
    dispatch({
      type: 'setting/getSetting',
    });
    dispatch({
      type: 'menu/getMenuData',
      payload: { routes, authority },
    });
    dispatch({
      type: 'global/fetchMyOrg',
    });
    dispatch({
      type: 'global/fetchPersonTips',
    });

  }

  componentDidUpdate(preProps) {
    // After changing to phone mode,
    // if collapsed is true, you need to click twice to display
    const { collapsed, isMobile } = this.props;
    if (isMobile && !preProps.isMobile && !collapsed) {
      this.handleMenuCollapse(false);
    }
  }

  getContext() {
    const { location, breadcrumbNameMap } = this.props;
    return {
      location,
      breadcrumbNameMap,
    };
  }

  matchParamsPath = (pathname, breadcrumbNameMap) => {
    const pathKey = Object.keys(breadcrumbNameMap).find(key => pathToRegexp(key).test(pathname));
    return breadcrumbNameMap[pathKey];
  };

  getRouterAuthority = (pathname, routeData) => {
    let routeAuthority = ['noAuthority'];
    const getAuthority = (key, routes) => {
      routes.map(route => {
        if (route.path && pathToRegexp(route.path).test(key)) {
          routeAuthority = route.authority;
        } else if (route.routes) {
          routeAuthority = getAuthority(key, route.routes);
        }
        return route;
      });
      return routeAuthority;
    };
    return getAuthority(pathname, routeData);
  };

  getPageTitle = (pathname, breadcrumbNameMap) => {
    const currRouterData = this.matchParamsPath(pathname, breadcrumbNameMap);

    if (!currRouterData) {
      return '冠蓄科技';
    }
    const pageName = formatMessage({
      id: currRouterData.locale || currRouterData.name,
      defaultMessage: currRouterData.name,
    });

    return `${pageName} - 冠蓄科技`;
  };

  getLayoutStyle = () => {
    const { fixSiderbar, isMobile, collapsed, layout } = this.props;
    if (fixSiderbar && layout !== 'topmenu' && !isMobile) {
      return {
        paddingLeft: collapsed ? '80px' : '256px',
      };
    }
    return null;
  };

  handleMenuCollapse = collapsed => {
    const { dispatch } = this.props;
    dispatch({
      type: 'global/changeLayoutCollapsed',
      payload: collapsed,
    });
  };

  renderSettingDrawer = () => {
    // Do not render SettingDrawer in production
    // unless it is deployed in preview.pro.ant.design as demo
    if (process.env.NODE_ENV === 'production' && APP_TYPE !== 'site') {
      return null;
    }
    return <SettingDrawer />;
  };

  // ------------- 2018-12-21 ---------
  changeStatu = (payload) => {
    this.props.dispatch({
      type: 'global/_setNotices',
      payload,
    });
  };

  handleOrgChange = (value, option) => {
    message.success(`切换组织成功，当前组织：${option.props.children}`, 3);
    this.props.dispatch({
      type: 'global/changeCurrentOrg',
      payload: {
        id: option.props.value,
        orgName: option.props.children,
      },
    });
  };

  handleNoticeClear = (id) => {
    this.props.dispatch({
      type: 'global/_delOneNotices',
      payload: { userId: this.props.userId },
    });
  };

  handleMenuClick = ({ key }) => {
    const { dispatch } = this.props;
    switch (key) {
      case 'logout':
        dispatch({
          type: 'login/_logout',
        });
        break;
      case 'reload':
        dispatch({
          type: 'layoutloading/reload',
        });
        break;
      case 'setPassword':
        dispatch({
          type: 'user/changePassState',
        });
        break;
      default:
        break;
    }
  };

  handleNoticeVisibleChange = (visible) => { // 弹出卡片显隐的回调
    if (visible) {
      setTimeout(() => {
        const { dispatch } = this.props;
        dispatch({
          type: 'global/fetchPersonTips',
        });
      }, 200);
    }
  };

  handlePassCancle = () => {
    this.props.dispatch({
      type: 'user/changePassState',
    });
  };

  handlePassOk = (values) => {
    const { userId } = this.props;
    values.id = userId;
    this.props.dispatch({
      type: 'user/setPassword',
      payload: values,
    });
  };

  render() {
    const {
      navTheme,
      layout: PropsLayout,
      children,
      location: { pathname },
      isMobile,
      menuData,
      breadcrumbNameMap,
      route: { routes },
      fixedHeader,
    //  2018-12-21
      count,
      loading,
      layoutloading,
      fetchingNotices,
      orgs,
      currentOrg,
      notices,
      collapsed,
      PassState,
    } = this.props;
    const { currentUser } = layoutloading;
    const isTop = PropsLayout === 'topmenu';
    const routerConfig = this.getRouterAuthority(pathname, routes);
    const contentStyle = !fixedHeader ? { paddingTop: 0 } : {};
    // console.log(menuData);
    const layout = (
      <Layout>
        {isTop && !isMobile ? null : (
          <SiderMenu
            logo={Newlogo}
            theme={navTheme}
            onCollapse={this.handleMenuCollapse}
            menuData={menuData}
            isMobile={isMobile}
            {...this.props}
          />
        )}
        <Layout
          style={{
            ...this.getLayoutStyle(),
            minHeight: '100vh',
          }}
        >
          <GlobalHeader
            changeStatu={this.changeStatu}
            logo={logo}
            count={count}
            loading={loading}
            currentUser={currentUser}
            fetchingNotices={fetchingNotices}
            orgs={orgs}
            currentOrg={currentOrg}
            notices={notices}
            handleOrgChange={this.handleOrgChange}
            collapsed={collapsed}
            isMobile={isMobile}
            onNoticeClear={this.handleNoticeClear}
            onCollapse={this.handleMenuCollapse}
            onMenuClick={this.handleMenuClick}
            onNoticeVisibleChange={this.handleNoticeVisibleChange}
            PassState={PassState}
            handlePassCancle={this.handlePassCancle}
            handlePassOk={this.handlePassOk}
          />
          <Content className={styles.content} style={contentStyle}>
            <Authorized authority={routerConfig} noMatch={<Exception403 />}>
              {children}
            </Authorized>
          </Content>
          <Footer />
        </Layout>
      </Layout>
    );
    return (
      <React.Fragment>
        <DocumentTitle title={this.getPageTitle(pathname, breadcrumbNameMap)}>
          <ContainerQuery query={query}>
            {params => (
              <Context.Provider value={this.getContext()}>
                <div className={classNames(params)}>{layout}</div>
              </Context.Provider>
            )}
          </ContainerQuery>
        </DocumentTitle>
        <Suspense fallback={<PageLoading />}>{this.renderSettingDrawer()}</Suspense>
      </React.Fragment>
    );
  }
}

export default connect(({ global, setting, menu, user, loading, layoutloading }) => ({
  collapsed: global.collapsed,
  layout: setting.layout,
  menuData: menu.menuData,
  breadcrumbNameMap: menu.breadcrumbNameMap,
  ...setting,
  // 2018-12-21
  count: user.currentUser,
  loading: loading.global,
  fetchingNotices: loading.effects['global/fetchNotices'],
  orgs: global.orgs,
  currentOrg: global.currentOrg,
  notices: global.personNotices,
  PassState: user.PassState,
  layoutloading,
  userId: layoutloading.currentUser.userid,
}))(props => (
  <Media query="(max-width: 599px)">
    {isMobile => <BasicLayout {...props} isMobile={isMobile} />}
  </Media>
));
