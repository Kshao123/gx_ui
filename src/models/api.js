import { notification } from 'antd';
import {
  getWxCode,
  getStuCoupon,
} from '@/services/api';

export default {
  namespace: 'api',

  state: {
    WxCode: ''
  },

  effects: {
    *getWxCode({ payload }, { call, put }) {
      const response = yield call(getWxCode, payload);
      if (!response) {
        notification.error({
          message: '查询信息失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveWxCode',
            payload: result
          });
          break;
        case false:
          notification.error({
            message: '查询信息失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    *getStuCoupon({ payload }, { call, put }) {
      const response = yield call(getStuCoupon, payload);
      if (!response) {
        notification.error({
          message: '查询信息失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          // yield put({
          //   type: 'saveWxCode',
          //   payload: result
          // });
          return result.list;
        case false:
          notification.error({
            message: '查询信息失败',
            description: message,
          });
          return false;
        default:
          break;
      }
    },
  },

  reducers: {
    saveWxCode(state, { payload }) {
      return {
        ...state,
        WxCode: payload,
      };
    },
  },
}
