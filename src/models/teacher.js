import { notification } from 'antd';
import { routerRedux } from 'dva/router';
import {
  getAllTeachers,
  deleteTeaById,
  updateTeaById,
  getFeedbackList,
  delFeedbackList,
  getSalary,
  TeaSign,
  getTeaExcel,
  getAllSalary,
  getTeaSign,
} from '@/services/teacher';

export default {
  namespace: 'teacher',

  state: {
    oldPage: [],
    data: {
      list: [],
      pagination: {},
    },
    payList: {
      list: [],
      pagination: [],
    },
    feedback: {
      list: [],
      pagination: [],
    },
    OldPay: [],
    salaryList: {
      list: [],
      pagination: {},
    },
    AllSalaryList: {
      list: [],
      pagination: {},
    },
    SignList: {
      list: [],
      pagination: {},
    },
  },

  effects: {
    // 查询教师信息
    *_getAllTeachers({ payload: { pagination, orgId } }, { call, put }) {
      const response = yield call(getAllTeachers, { ...pagination, orgId });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'save',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 添加/修改教师
    *_updateTeaById({ payload }, { call, put }) {
      const response = yield call(updateTeaById, payload);
      if (!response) {
        notification.error({
          message: '添加/修改失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '添加/修改成功',
          });
          yield put(routerRedux.push({
            pathname: '/teacher/teach-data',
          }));
          break;
        case false:
          notification.error({
            message: '添加失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 删除教师
    *_deleteTeaById({ payload: { ids, pagination, orgId } }, { call, put }) {
      const response = yield call(deleteTeaById, ids);
      if (!response) {
        notification.error({
          message: '删除失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '删除成功',
          });
          yield put({
            type: '_getAllTeachers',
            payload: {
              pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    // 查询反馈记录
    *_getFeedbackList({ payload: { pagination, orgId } }, { call, put }) {
      const response = yield call(getFeedbackList, { ...pagination, orgId });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveFeedback',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 删除反馈记录
    *_delFeedbackList({ payload: { id, pagination, orgId } }, { call, put }) {
      const response = yield call(delFeedbackList, { id });
      if (!response) {
        notification.error({
          message: '删除失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '删除成功',
          });
          yield put({
            type: '_getFeedbackList',
            payload: {
              ...pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 查询工资提成
    *_getSalary({ payload: { pagination, orgId, query = {} } }, { call, put }) {
      const response = yield call(getSalary, { ...pagination, orgId, ...query });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveSalaryList',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 查询老师总工资
    *_getAllSalary({ payload }, { call, put }) {
      const response = yield call(getAllSalary, payload );
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveAllSalaryList',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 查询打卡记录
    *_getTeaSign({ payload: { pagination, orgId, query={} } }, { call, put }) {
      const response = yield call(getTeaSign, { ...pagination, orgId, ...query });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveSignList',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 老师补打卡
    *_TeaSign({ payload: { data, studentCourseIdList } }, { call, put }) {
      const response = yield call(TeaSign, { studentCourseIdList });
      if (!response) {
        notification.error({
          message: '打卡失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '打卡成功',
          });
          yield put({
            type: '_getTeaSign',
            payload: {
              ... data
            },
          });
          break;
        case false:
          notification.error({
            message: '打卡失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    // 获取教师的Excel表格
    *_getTeaExcel({ payload }, { call }) {
      yield call(getTeaExcel, payload);
    },

    // 搜索的回调
    *_searchData({ payload: { type, value } }, { put }) {
      yield put({
        type,
        payload: value,
      });
    },
  },

  reducers: {
    save(state, action) {
      const { list, total, pageSize, current } = action.payload;
      return {
        ...state,
        data: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
        oldPage: list,
      };
    },
    // 保存反馈记录
    saveFeedback(state, action) {
      const { list, total, pageSize, current } = action.payload;
      return {
        ...state,
        feedback: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
        oldPage: list,
      };
    },
    // 保存工资提成
    saveSalaryList(state, action) {
      const { list, total, pageSize, current } = action.payload;
      return {
        ...state,
        salaryList: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
      };
    },
    // 保存总工资
    saveAllSalaryList(state, action) {
      const { list, total, pageSize, current } = action.payload;
      return {
        ...state,
        AllSalaryList: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
      };
    },
    // 保存打卡记录
    saveSignList(state, action) {
      const { list, total, pageSize, current } = action.payload;
      return {
        ...state,
        SignList: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
      };
    },

    updata(state, action) {
      const { pagination, data: { list }, oldPage } = state;
      let arr = list.filter(item => item.userName === action.payload);
      if (action.payload === 'rest') arr = oldPage;
      return {
        ...state,
        data: {
          list: arr,
          pagination,
        },
      };
    },
    // 储存缴费信息列表
    savePay(state, { payload: { list, total, pageSize, current } }) {
      return {
        ...state,
        payList: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
        OldPay: list,
      };
    },

    updataPay(state, { payload: { attr, key } }) {
      const { pagination, payList: { list }, OldPay } = state;
      let arr = list.filter(item => item[attr] === key);
      if (attr === 'rest') arr = OldPay;
      return {
        ...state,
        payList: {
          list: arr,
          pagination,
        },
      };
    },
  },
};
