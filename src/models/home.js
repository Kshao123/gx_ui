import { notification } from 'antd';
import {
  getOnline,
  getPayData,
  getCourseNumChange,
  getStudentNum,
  getStudentChange,
  getStuBirthday,
  getTeaBirthday,
  getClassMoney,
  getTeaClassRate,
} from '@/services/user';


export default {
  namespace: 'home',

  state: {
    data: {},
    payData: {
      recharge: 0, // 充值
      refund: 0, // 退费
      dayAllMoney: 0, // 日销售额
    },
    courseInfo: [],
    StudentInfo: {
      allStudentNum: '',
      InactiveStudentNum: ''
    },
    StudentChangeInfo: [],
    UserBirthdayInfo: [],
    ClassMoneyInfo: [],
    TeaClassRateInfo: [],
  },

  effects: {
    *_getOnline({ payload }, { call, put }) {
      const response = yield call(getOnline, payload);
      if (!response) {
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          result.everyDayOnlinePeopleNumber.reverse();
          yield put({
            type: 'save',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 查询机构营业额
    *getPayData({ payload }, { call, put }) {
      const response = yield call(getPayData, payload);
      if (!response) {
        return;
      }
      const { status, result } = response;
      switch (status) {
        case true:
          yield put({
            type: 'savePayData',
            payload: result,
          });
          break;
        case false:
          break;
        default:
          break;
      }
    },
    // 课程数据查询
    *getCourseNumChange({ payload }, { call, put }) {
      const response = yield call(getCourseNumChange, payload);
      if (!response) {
        return;
      }
      const { status, result } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveCourseInfo',
            payload: result,
          });
          break;
        case false:
          break;
        default:
          break;
      }
    },
    // 获取机构学员人数
    *getStudentNum({ payload }, { call, put }) {
      const response = yield call(getStudentNum, payload);
      if (!response) {
        return;
      }
      const { status, result } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveStudentInfo',
            payload: result,
          });
          break;
        case false:
          break;
        default:
          break;
      }
    },

    *getStudentChange({ payload }, { call, put }) {
      const response = yield call(getStudentChange, payload);
      if (!response) {
        return;
      }
      const { status, result } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveStudentChangeInfo',
            payload: result,
          });
          break;
        case false:
          break;
        default:
          break;
      }
    },

    *getUserBirthday({ payload: { type = 1, ...payload } }, { call, put }) {
      const response = yield call(type === 1 ? getStuBirthday : getTeaBirthday, payload);
      if (!response) {
        return;
      }
      const { status, result } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveUserBirthdayInfo',
            payload: result,
          });
          break;
        case false:
          break;
        default:
          break;
      }
    },

    *getClassMoney({ payload }, { call, put }) {
      const response = yield call(getClassMoney, payload);
      if (!response) {
        return;
      }
      const { status, result } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveClassMoneyInfo',
            payload: result,
          });
          break;
        case false:
          break;
        default:
          break;
      }
    },

    *getTeaClassRate({ payload }, { call, put }) {
      const response = yield call(getTeaClassRate, payload);
      if (!response) {
        return;
      }
      const { status, result } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveTeaClassRateInfo',
            payload: result,
            //   [
            //   {
            //     time: 1555726452000,
            //     dayRealityCourseNum: 50,
            //     dayShouldCourseNum: 50
            //   },
            //   {
            //     time: 1555812852000,
            //     dayRealityCourseNum: 15,
            //     dayShouldCourseNum: 15
            //   },
            //   {
            //     time: 1555912852000,
            //     dayRealityCourseNum: 1,
            //     dayShouldCourseNum: 20
            //   },
            //   {
            //     time: 1556012852000,
            //     dayRealityCourseNum: 12,
            //     dayShouldCourseNum: 20
            //   },
            //   {
            //     time: 1556112852000,
            //     dayRealityCourseNum: 2,
            //     dayShouldCourseNum: 20
            //   },
            // ],
          });
          break;
        case false:
          break;
        default:
          break;
      }
    },
  },

  reducers: {
    save(state, { payload: data }) {
      return {
        ...state,
        data,
      };
    },
    savePayData(state, { payload: payData }) {
      return {
        ...state,
        payData,
      };
    },
    saveCourseInfo(state, { payload: courseInfo }) {
      return {
        ...state,
        courseInfo,
      };
    },
    saveStudentInfo(state, { payload: StudentInfo }) {
      return {
        ...state,
        StudentInfo,
      };
    },
    saveStudentChangeInfo(state, { payload: StudentChangeInfo }) {
      return {
        ...state,
        StudentChangeInfo,
      };
    },
    saveUserBirthdayInfo(state, { payload: UserBirthdayInfo }) {
      return {
        ...state,
        UserBirthdayInfo,
      };
    },
    saveClassMoneyInfo(state, { payload: ClassMoneyInfo }) {
      return {
        ...state,
        ClassMoneyInfo,
      };
    },
    saveTeaClassRateInfo(state, { payload: TeaClassRateInfo }) {
      return {
        ...state,
        TeaClassRateInfo,
      };
    },
  },
};
