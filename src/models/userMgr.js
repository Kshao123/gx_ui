import { routerRedux } from 'dva/router';
import { notification } from 'antd';
import { isArray } from '@/utils/utils';
import { Query as queryUsers, submit, queryRolesByUser, saveRoles, deleteUser } from '../services/user';

export default {
  namespace: 'userMgr',
  state: {
    data: {
      list: [],
      pagination: {},
    },
    modal: {
      visible: false,
      data: {},
    },
  },

  effects: {
    // 用户查询
    *fetch({ payload }, { call, put }) {
      const response = yield call(queryUsers, payload);
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'save',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    *delete({ payload: { data, pagination, orgId } }, { call, put }) { // do 点击删除时 触发的事件
      let response;
      if (isArray(data)) { // do 传过来的是数组
        response = yield call(deleteUser, data);
      } else {
        response = yield call(deleteUser, data.id);
      }
      if (!response) {
        notification.error({
          message: '删除失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '删除成功',
            description: message,
          });
          yield put({
            type: 'fetch',
            payload: {
              ...pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    *submit({ payload }, { call, put }) {
      const response = yield call(submit, payload);
      if (response && response.status === true) {
        yield put(routerRedux.replace({
          pathname: '/admin/user',
        }));
      } else if (response && response.status === false) {
        notification.error({
          message: '保存失败',
          description: '用户名已存在',
        });
      }
    },
    *saveRoles({ payload }, { call }) {
      const response = yield call(saveRoles, payload);
      return response;
    },
    *queryRolesByUser({ payload }, { call }) {
      const response = yield call(queryRolesByUser, payload.id);
      return response;
    },
  },

  reducers: {
    save(state, { payload }) {
      const { list, total, pageSize, current } = payload;
      return {
        ...state,
        data: {
          list, // 数据
          pagination: {
            total, // 数据总条数
            pageSize, // 10 一页展示多少条数
            current,
          },
        },
      };
    },

    changeVisible(state, action) {
      return {
        ...state,
        modal: {
          visible: !state.modal.visible,
          data: action ? action.data : '',
        },
      };
    },

    handleTransfer(state, action) {
      return {
        ...state,
        modal: {
          visible: state.modal.visible,
          data: {
            ...state.modal.data,
            roles: action.payload,
          },
        },
      };
    },
  },
};
