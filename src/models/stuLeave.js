import { notification } from 'antd';
import {
  queryLeave,
  leaveApproval,
} from '../services/Leave';

export default {
  namespace: 'stuLeave',
  state: {
    data: {
      list: [],// 表格数据
      pagination: {},// 页码数据
    },
    Options: [],
  },

  effects: {
    // 默认查询
    *_queryLeave({ payload: { pagination, orgId } }, { call, put }) {
      const response = yield call(queryLeave, { ...pagination, orgId });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'save',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 发送请假审批
    *_leaveApproval({ payload: { data, pagination, orgId } }, { call, put }) {
      const response = yield call(leaveApproval, data);
      if (!response) {
        notification.error({
          message: '请求失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '发送成功',
            description: message,
          });
          yield put({
            type: '_queryLeave',
            payload: {
              pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '请求失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    *_searchData({ payload }, { put }) {
      yield put({
        type: 'save',
        payload,
      });
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        data: {
          list: action.payload.list,
          pagination: {
            total: action.payload.total,
            pageSize: action.payload.pageSize,
            current: action.payload.current,
          },
        },
      };
    },
  },
};
