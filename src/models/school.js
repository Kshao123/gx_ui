import { notification } from 'antd';
import {
  getAttendance,
  getStuPayList,
  changeStuPayList,
  delStuPayList,
} from '../services/school';

export default {
  namespace: 'school',

  state: {
    oldPage: [],
    data: {
      list: [], // 表格数据
      pagination: {}, // 页码数据
    },
    payList: {
      list: [],
      pagination: [],
    },
    OldPay: [],
  },

  effects: {
    // 查询考勤列表
    *_getAttendance({ payload: { pagination, orgId } }, { call, put }) {
      const response = yield call(getAttendance, { ...pagination, orgId });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'save',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 获取缴费信息
    *_getStuPayList({ payload: { pagination, orgId, query = {} } }, { call, put }) {
      const response = yield call(getStuPayList, { ...pagination, orgId, ...query });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'savePay',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 添加或修改 缴费信息
    *_changeStuPayList({ payload: { pagination, orgId, data } }, { call, put }) {
      const response = yield call(changeStuPayList, data);
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '修改金额成功',
          });
          yield put({
            type: 'studentDatas/queryStu',
            payload: {
              pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '修改失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 删除缴费信息
    *_delStuPayList({ payload: { id, pagination, orgId, query= {} } }, { call, put }) {
      const response = yield call(delStuPayList, { id });
      if (!response) {
        notification.error({
          message: '删除失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '删除成功',
          });
          yield put({
            type: '_getStuPayList',
            payload: {
              pagination,
              orgId,
              query,
            },
          });
          break;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    *_Updata({ payload }, { call, put }) {
      yield put({
        type: 'updata',
        payload,
      });
    },

    *_UpdataPay({ payload }, { call, put }) {
      yield put({
        type: 'updataPay',
        payload,
      });
    },
  },

  reducers: {
    save(state, action) {
      const { list, total, pageSize, current } = action.payload;
      return {
        ...state,
        data: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
        oldPage: list,
      };
    },

    updata(state, action) {
      const { pagination, data: { list }, oldPage } = state;
      let arr = list.filter(item => item.userName === action.payload);
      if (action.payload === 'rest') arr = oldPage;
      return {
        ...state,
        data: {
          list: arr,
          pagination,
        },
      };
    },
    // 储存缴费信息列表
    savePay(state, { payload: { list, total, pageSize, current } }) {
      return {
        ...state,
        payList: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
        OldPay: list,
      };
    },

    updataPay(state, { payload: { attr, key } }) {
      const { pagination, payList: { list }, OldPay } = state;
      let arr = list.filter(item => item[attr] === key);
      if (attr === 'rest') arr = OldPay;
      return {
        ...state,
        payList: {
          list: arr,
          pagination,
        },
      };
    },
  },
};
