import { notification } from 'antd';
import { routerRedux } from 'dva/router';

import {
  changeSchoolManger,
  getSchoolManger,
  delSchoolManger,
  getDirector,
  delDirector,
  changeDirector,
  getLeave,
} from '@/services/schoolControl';
import { leaveApproval } from '../services/Leave';

export default {
  namespace: 'schoolControl',

  state: {
    oldPage: [],
    data: {
      list: [], // 表格数据
      pagination: {}, // 页码数据
    },
    // 教务主管
    directorList: {
      list: [],
      pagination: {},
    },
    // 请假
    leaveList: {
      list: [],
      pagination: {},
    },
  },

  effects: {
    // 获取当前机构的校区负责人
    *_getSchoolManger({ payload: { pagination, orgId } }, { call, put }) {
      const response = yield call(getSchoolManger, { ...pagination, orgId });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'save',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 修改校区负责人
    *_changeSchoolManger({ payload }, { call, put }) {
      const response = yield call(changeSchoolManger, payload);
      if (!response) {
        notification.error({
          message: '添加/修改 失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '添加/修改 成功',
          });
          yield put(routerRedux.replace({
            pathname: '/personnel/schoolControl',
          }));
          break;
        case false:
          notification.error({
            message: '添加/修改 失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 删除校区负责人
    *_delSchoolManger({ payload: { pagination, ids, orgId } }, { call, put }) {
      const response = yield call(delSchoolManger, ids);
      if (!response) {
        notification.error({
          message: '删除失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '删除成功',
          });
          yield put({
            type: '_getSchoolManger',
            payload: {
              ...pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    // 获取当前机构的教务主管
    *_getDirector({ payload: { pagination, orgId } }, { call, put }) {
      const response = yield call(getDirector, { ...pagination, orgId });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveDirector',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 修改教务主管
    *_changeDirector({ payload }, { call, put }) {
      const response = yield call(changeDirector, payload);
      if (!response) {
        notification.error({
          message: '添加/修改 失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '添加/修改 成功',
          });
          console.log(123);
          yield put(routerRedux.replace({
            pathname: '/personnel/director',
          }));
          break;
        case false:
          notification.error({
            message: '添加/修改 失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 删除教务主管
    *_delDirector({ payload: { ids, pagination, orgId } }, { call, put }) {
      const response = yield call(delDirector, ids);
      if (!response) {
        notification.error({
          message: '删除失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '删除成功',
          });
          yield put({
            type: '_getDirector',
            payload: {
              ...pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    // 搜索成功时保存数据
    *_searchData({ payload: { payload, value } }, { put }) {
      yield put({
        type: value,
        payload,
      });
    },

    *_getLeave({ payload: { pagination, orgId } }, { call, put }) {
      const response = yield call(getLeave, { ...pagination, orgId });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveLeave',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 请假动作
    *_leaveApproval({ payload: { pagination, orgId, data } }, { call, put }) {
      const response = yield call(leaveApproval, data);
      if (!response) {
        notification.error({
          message: '请求失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '发送成功',
            description: message,
          });
          yield put({
            type: '_getLeave',
            payload: {
              pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '请求失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
  },

  reducers: {
    save(state, action) {
      const { list, total, pageSize, current } = action.payload;
      return {
        ...state,
        data: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
        oldPage: list,
      };
    },
    // 教务主管
    saveDirector(state, action) {
      const { list, total, pageSize, current } = action.payload;
      return {
        ...state,
        directorList: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
      };
    },
    // 请假
    saveLeave(state, action) {
      const { list, total, pageSize, current } = action.payload;
      return {
        ...state,
        leaveList: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
      };
    },

    updata(state, action) {
      const { pagination, data: { list }, oldPage } = state;
      let arr = list.filter(item => item.userName === action.payload);
      if (action.payload === 'rest') arr = oldPage;
      return {
        ...state,
        data: {
          list: arr,
          pagination,
        },
      };
    },
    // 储存缴费信息列表
    savePay(state, { payload: { list, total, pageSize, current } }) {
      return {
        ...state,
        payList: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
        OldPay: list,
      };
    },

    updataPay(state, { payload: { attr, key } }) {
      const { pagination, payList: { list }, OldPay } = state;
      let arr = list.filter(item => item[attr] === key);
      if (attr === 'rest') arr = OldPay;
      return {
        ...state,
        payList: {
          list: arr,
          pagination,
        },
      };
    },
  },
};
