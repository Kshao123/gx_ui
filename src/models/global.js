import { notification } from 'antd';
import {
  queryMyOrg,
  getPersonNotices,
  delOneNotices,
  getNotices,
  delNotices,
  changeNotices,
  setNotices,
} from '@/services/api';

export default {
  namespace: 'global',

  state: {
    collapsed: false,
    notices: [],
    personNotices: [],
    oldMsg: [], // 此处为默认的通知信息
    currentOrg: {},
    pagination: {},
    visible: false,
  },

  effects: {
    // 查询通知
    *fetchNotices({ payload: { pagination: page, orgId } }, { call, put }) { // 弹出卡片显隐的出发的事件
      const response = yield call(getNotices, { ...page, orgId });
      if (!response) {
        // notification.error({
        //   message: '查询失败',
        //   description: '服务连接失败',
        // });
        return;
      }
      const { status, result: { list, ...pagination }, message } = response;
      switch (status) {
        case true:
          const arr = list.map((item) => {
            const {
              msgReleaseName,
              id,
              msgTitle,
              msgDetails,
              msgReleaseTime,
              isRead,
              msgReleaseType,
              msgStatus,
            } = item;
            return {
              id,
              avatar: 'https://t1.picb.cc/uploads/2018/11/07/JpPQeR.jpg',
              title: msgTitle,
              description: msgDetails,
              datetime: msgReleaseTime,
              type: 'message',
              clickClose: true,
              isRead,
              msgReleaseType,
              msgReleaseName,
              msgStatus,
            };
          });
          let length = 0;
          list.forEach((item) => {
            if (item.msgStatus === 2 && !item.isRead) {
              length += 1;
            }
          });
          yield put({
            type: 'saveNotices',
            payload: {
              arr,
              oldMsg: list,
              pagination,
            },
          });
          break;
        case false:
          // notification.error({
          //   message: '查询失败',
          //   description: message,
          // });
          break;
        default:
          break;
      }
    },
    // 删除通知
    *_delNotices({ payload: { pagination, orgId, data } }, { put, call }) {
      const response = yield call(delNotices, { ...data });
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '删除成功',
          });
          yield put({
            type: 'fetchNotices',
            payload: {
              pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    // 添加或修改通知
    *_changeNotices({ payload: { data, pagination, orgId } }, { put, call }) {
      const response = yield call(changeNotices, data);
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '添加/修改成功',
          });
          yield put({
            type: 'fetchNotices',
            payload: {
              pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // --- new 2018-12-20 ---
    *fetchMyOrg(_, { call, put }) {
      const response = yield call(queryMyOrg);
      if (response && response.status === true) {
        yield put({
          type: 'changeMyOrg',
          payload: response.result,
        });
      }
    },
// 查询个人通知
    *fetchPersonTips(_, { call, put }) { // 弹出卡片显隐的出发的事件
      const response = yield call(getPersonNotices);
      if (!response) {
        // notification.error({
        //   message: '查询失败',
        //   description: '服务连接失败',
        // });
        return;
      }
      const { status, result: list, message } = response;
      let arr = [];
      switch (status) {
        case true:
          if (!list) {
            return;
          }
          arr = list.map((item) => {
            const {
              msgReleaseName,
              id,
              msgTitle,
              msgDetails,
              msgReleaseTime,
              isRead,
              msgReleaseType,
              msgStatus,
            } = item;
            return {
              id,
              avatar: 'https://t1.picb.cc/uploads/2018/11/07/JpPQeR.jpg',
              title: msgTitle,
              description: msgDetails,
              datetime: msgReleaseTime,
              type: 'message',
              clickClose: true,
              isRead,
              msgReleaseType,
              msgReleaseName,
              msgStatus,
            };
          });
          let length = 0;
          list.forEach((item) => {
            if (item.msgStatus === 2 && !item.isRead) {
              length += 1;
            }
          });
          yield put({
            type: 'savePersonNotices',
            payload: {
              arr,
              oldMsg: list,
            },
          });
          yield put({
            type: 'user/changeNotifyCount',
            payload: length,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
  // ---- 2018 - 12 - 12 ----
// 删除个人通知
    *_delOneNotices({ payload }, { put, call }) {
      const response = yield call(delOneNotices, payload);
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '删除成功',
          });
          yield put({
            type: 'fetchPersonTips',
          });
          break;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    // 修改通知
    *_setNotices({ payload }, { put, call }) {
      const response = yield call(setNotices, payload);
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'fetchPersonTips',
          });
          break;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
  },

  reducers: {
  //  ------
    savePersonNotices(state, { payload: { arr, oldMsg } }) {
      return {
        ...state,
        personNotices: arr,
      };
    },

    // 2018 -12 -12
    changeCurrentOrg(state, { payload }) {
      return {
        ...state,
        currentOrg: payload,
      };
    },

    changeLayoutCollapsed(state, { payload }) {
      return {
        ...state,
        collapsed: payload,
      };
    },

    saveNotices(state, { payload: { arr, oldMsg, pagination } }) {
      return {
        ...state,
        notices: arr,
        oldMsg,
        pagination,
      };
    },

    changeMyOrg(state, { payload }) {
      let currentOrg = {};
      if (payload && payload.length > 0) {
        currentOrg = {
          id: payload[0].id,
          orgName: payload[0].orgName,
        };
      }
      return {
        ...state,
        orgs: payload,
        currentOrg,
      };
    },
  },

  subscriptions: {
    setup({ history }) {
      // Subscribe history(url) change, trigger `load` action if pathname is `/`
      return history.listen(({ pathname, search }) => {
        if (typeof window.ga !== 'undefined') {
          window.ga('send', 'pageview', pathname + search);
        }
      });
    },
  },
};
