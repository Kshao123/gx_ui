import { notification } from 'antd';
import { routerRedux } from 'dva/router';
import pathToRegexp from 'path-to-regexp';
import {
  queryOne22One,
  submitOne22One,
  deleteOne22One,
  getStuCourse,
  changeStuCourse,
} from '@/services/one22one';

export default {
  namespace: 'one22one',

  state: {
    data: {
      list: [],
      pagination: {},
    },
    myCoursePurchase: [],
    changeCourse: {
      list: [],
      pagination: {},
    },
  },

  effects: {
    *_getAllone2Many({ payload: { pagination, orgId, query={} } }, { call, put }) {
      const response = yield call(queryOne22One, { ...pagination, orgId, ...query});
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'save',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 删除一对多
    *delete({ payload: { pagination, orgId, ids, query } }, { call, put }) {
      const response = yield call(deleteOne22One, ids);
      if (!response) {
        notification.error({
          message: '删除失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '删除成功',
            description: message,
          });
          yield put({
            type: '_getAllone2Many',
            payload: {
              pagination,
              orgId,
              query,
            },
          });
          break;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 添加或修改一对多
    *submitOne22One({ payload: { values, flag, data } }, { call, put }) {
      const response = yield call(submitOne22One, values);
      if (!response) {
        notification.error({
          message: '添加/修改失败',
          description: '服务连接失败',
        });
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '添加/修改成功',
          });
          if (flag) {
            yield put({
              type: '_getAllone2Many',
              payload: {
                ...data
              }
            })
          } else {
            yield put(routerRedux.push({
              pathname: '/teaching/one22one',
            }));
          }

          return true;
          // break;
        case false:
          notification.error({
            message: '添加失败',
            description: message,
          });
          return false;
          // break;
        default:
          break;
      }
    },
    // 搜索成功时保存数据
    *_searchData({ payload }, { put }) {
      yield put({
        type: 'save',
        payload,
      });
    },

    //  查询学生剩余课程
    *_getStuCourse({ payload: { pagination, stuCourseArrangeIds } }, { call, put }) {
      const response = yield call(getStuCourse, { stuCourseArrangeIds, ...pagination});
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveStuCourse',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    //  给学生排课
    *_changeStuCourse({ payload: { data, pagination, stuCourseArrangeIds } }, { call, put }) {
      const response = yield call(changeStuCourse, data);
      if (!response) {
        notification.error({
          message: '修改失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '排课成功',
          });
          yield put({
            type: '_getStuCourse',
            payload: {
              pagination,
              stuCourseArrangeIds,
            }
          });
          break;
        case false:
          notification.error({
            message: '排课失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        data: {
          list: action.payload.list,
          pagination: {
            total: action.payload.total,
            pageSize: action.payload.pageSize,
            current: action.payload.current,
          },
        },
      };
    },

    saveStuCourse(state, action) {
      const { list, total, pageSize, current } = action.payload;
      return {
        ...state,
        changeCourse: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
      };
    },


    saveCoursePurchase(state, action) {
      return {
        ...state,
        myCoursePurchase: action.payload,
      };
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        const match = pathToRegexp('/teach/one22one/query').exec(location.pathname);
        if (match) {
          dispatch({ type: 'fetch' });
        }
      });
    },
  },
};
