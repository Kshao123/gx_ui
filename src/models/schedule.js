import {
  getSchedule,
  changeSchedule,
} from '@/services/schedule';
import { notification } from 'antd';

export default {
  namespace: 'schedule',

  state: {
    data: {},
  },

  effects: {
    // 查询课程表
    *_getSchedules({ payload }, { call, put }) {
      const response = yield call(getSchedule, payload);
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'save',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    *_changeSchedule({ payload: { obj, data } }, { call, put }) {
      const response = yield call(changeSchedule, data);
      if (!response) {
        notification.error({
          message: '修改失败',
          description: '服务连接失败',
        });
        return false;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          yield put({
            type: '_getSchedules',
            payload: {
              ...obj
            },
          });
          return true;
        case false:
          notification.error({
            message: '修改失败',
            description: message,
          });
          return false;
        default:
          break;
      }
    },

    // 查询课程表 新
    *_getSchedule({ payload  }, { call, put }) {
      const response = yield call(getSchedule, payload);
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          return result;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    *_searchData({ payload }, { put }) {
      yield put({
        type: 'save',
        payload,
      });
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        data: action.payload,
      };
    },
  },
};
