import { routerRedux } from 'dva/router';
import { notification } from 'antd';
import {
  AddStu,
  DelStu,
  queryCourse,
  queryStu,
  queryCourseType,
  saveCourse,
  delCourse,
  saveCourseType,
  delCourseType,
  queryAuditionStu,
  AddAuditionStu,
  stuPayment,
  changeStuStatus,
  getAudRecordList,
  setAudRecordList,
  delAudRecordList,
  getStuExcel,
  getStuIntention,
  changeAuditionStu,
  changeTrueStu,
  delVisStu,
} from '@/services/studentDatas';
import { filterArr } from '../utils/utils';


export default {
  namespace: 'studentDatas',

  state: {
    Options: [], // 修改后的科目查询
    OptionsTypes: [], // 科目分类
    OldOptions: [], // 为更改前的科目分类
    data: {
      list: [],
      pagination: {},
    },
    AuditionList: {
      list: [],
      pagination: {},
    },
    modal: {
      data: {},
      visible: false,
      functionVisible: false,
    },
    selectData: [],
    queryCourseType: [],
    AudRecordList: {
      list: [],
      pagination: {},
    },
    StuIntentionInfo: {
      list: [],
      pagination: {},
    }
  },

  effects: {
    // 查询学生
    *queryStu({ payload: { orgId, pagination, teaId = null } }, { call, put }) {
      const response = yield call(queryStu, { orgId, ...pagination, teaId });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'save',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 添加/修改学生
    *AddStu({ payload }, { call, put }) {
      const response = yield call(AddStu, payload);
      if (!response) {
        notification.error({
          message: '添加失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '添加/修改成功',
          });
          yield put(routerRedux.push({
            pathname: '/student/data',
          }));
          break;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 删除 学生/试听学生
    *DelStu({ payload: { orgId, ids, pagination, value } }, { call, put }) {
      const response = yield call(DelStu, ids);
      if (!response) {
        notification.error({
          message: '删除失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '删除成功',
          });
          yield put({
            type: value,
            payload: {
              pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 查询试听学生
    *_queryAuditionStu({ payload: { pagination, orgId } }, { call, put }) {
      const response = yield call(queryAuditionStu, { ...pagination, orgId });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveAudition',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 添加/修改试听学生
    *_AddAuditionStu({ payload }, { call, put }) {
      const response = yield call(AddAuditionStu, payload);
      if (!response) {
        notification.error({
          message: '添加失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '添加/修改成功',
          });
          yield put(routerRedux.push({
            pathname: '/student/Audition',
          }));
          break;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 查询科目
    *_queryCourse({ payload: { pagination, orgId } }, { call, put }) {
      const response = pagination ?
        yield call(queryCourse, { ...pagination, orgId }) :
        yield call(queryCourse, { orgId });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      const { list } = result;
      const Options = filterArr(list);
      switch (status) {
        case true:
          yield put({
            type: 'saveOptions',
            payload: { Options, result },
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 删除科目
    *DelCourse({ payload: { orgId, ids, pagination } }, { call, put }) {
      const response = yield call(delCourse, { ids });
      if (!response) {
        notification.error({
          message: '删除失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          yield put({
            type: '_queryCourse',
            payload: {
              ...pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 保存科目
    *saveCourse({ payload: { data } }, { call, put }) {
      const response = yield call(saveCourse, { ...data });
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '添加/修改成功',
          });
          yield put(routerRedux.push({
            pathname: '/teaching/classification',
          }));
          break;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 查询科目类型
    *_queryCourseType({ payload: { pagination, orgId } }, { call, put }) {
      const response = pagination ?
        yield call(queryCourseType, { ...pagination, orgId }) :
        yield call(queryCourseType, { orgId });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      const { list } = result;
      const OptionsTypes = list.map(item => ({
        value: item.id,
        label: item.subjectTypeName,
      }));
      switch (status) {
        case true:
          yield put({
            type: 'saveOptionsType',
            payload: { result, OptionsTypes },
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 删除科目类型
    *DelCourseType({ payload: { orgId, ids, pagination } }, { call, put }) {
      const response = yield call(delCourseType, ids);
      if (!response) {
        notification.error({
          message: '删除失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '删除成功',
          });
          yield put({
            type: '_queryCourseType',
            payload: {
              pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 保存科目类型
    *saveCourseType({ payload: { orgId, data, pagination } }, { call, put }) {
      const response = yield call(saveCourseType, { ...data });
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '添加/修改成功',
          });
          yield put({
            type: '_queryCourseType',
            payload: {
              ...pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    // 学生充值
    *_stuPayment({ payload }, { call }) {
      const response = yield call(stuPayment, payload);
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          return result;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          return status;
        default:
          break;
      }
    },

    // 变更试听学生的状态
    *changeStuStatus({ payload: { orgId, id: stuId, pagination } }, { call, put }) {
      const response = yield call(changeStuStatus, { stuId });
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '变更成功',
          });
          yield put({
            type: '_queryAuditionStu',
            payload: {
              pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '变更失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    // 查询试听学生的记录
    *_getAudRecordList({ payload: { pagination, orgId } }, { call, put }) {
      const response = yield call(getAudRecordList, { ...pagination, orgId });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveAudRecord',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 添加或修改 试听记录
    *_AddAudRecordList({ payload: { pagination, orgId, data } }, { call, put }) {
      const response = yield call(setAudRecordList, data);
      if (!response) {
        notification.error({
          message: '添加失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '添加/修改成功',
          });
          yield put({
            type: '_getAudRecordList',
            payload: {
              pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    *delAudRecordList({ payload: { orgId, ids, pagination } }, { call, put, select }) {
      const response = yield call(delAudRecordList, ids);
      if (!response) {
        notification.error({
          message: '删除失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      // const pagination = page;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '删除成功',
          });
          yield select(state => {
            const { total, pageSize, current } = state.studentDatas.AudRecordList.pagination;
            if (total % pageSize === 1 && current !== 1) {
              pagination.pageNum = current - 1;
            }
          });
          yield put({
            type: '_getAudRecordList',
            payload: {
              pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    *getStuIntention({ payload: { pagination, orgId, query = {} } }, { call, put }) {
      const response = yield call(getStuIntention, { ...pagination, orgId, ...query });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveStuIntention',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    // 变更为试听 / 正式 学生
    *changeAuditionStu({ payload: { orgId, ids, pagination, type, query } }, { call, put }) {
      let response = null;
      if (type === 0) {
        response = yield call(changeAuditionStu, { orgId, potentialStudentIdList: ids });
      } else if (type === 1) {
        response = yield call(changeTrueStu, { orgId, potentialStudentIdList: ids});
      } else if (type === 2) {
        response = yield call(delVisStu, { orgId, potentialStudentIdList: ids});
      }

      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '操作成功',
          });
          yield put({
            type: 'getStuIntention',
            payload: {
              pagination,
              orgId,
              query,
            },
          });
          break;
        case false:
          notification.error({
            message: '操作失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    // 获取学生的Excel表格
    *_getStuExcel({ payload }, { call }) {
      yield call(getStuExcel, payload);
    },

    *_searchData({ payload: { data, value } }, { put }) {
      yield put({
        type: value,
        payload: data,
      });
    },
  },

  reducers: {
    save(state, { payload }) {
      const { list, total, pageSize, current } = payload;
      return {
        ...state,
        data: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
      };
    },

    saveAudRecord(state, { payload }) {
      const { list, total, pageSize, current } = payload;
      return {
        ...state,
        AudRecordList: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
      };
    },

    saveStuIntention(state, { payload }) {
      const { list, total, pageSize, current } = payload;
      return {
        ...state,
        StuIntentionInfo: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
      };
    },

    saveAudition(state, { payload }) {
      const { list, total, pageSize, current } = payload;
      return {
        ...state,
        AuditionList: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
      };
    },

    saveOptions(state, { payload: { Options, result } }) {
      const { list, total, pageSize, current } = result;
      return {
        ...state,
        Options,
        OldOptions: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
      };
    },

    saveOptionsType(state, { payload: { result, OptionsTypes } }) {
      const { list, total, pageSize, current } = result;
      return {
        ...state,
        OptionsTypes,
        queryCourseType: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
      };
    },
  },
};
