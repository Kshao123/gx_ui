import { notification } from 'antd';
import { routerRedux } from 'dva/router';
import {
  addInventory,
  getInventory,
  delInventory,
  addInventoryManage,
  delInventoryManage,
  getInventoryManage,
  rollInProduct,
  rollOutProduct,
  getOrgList,
  ProductChange,
  getProductOrder,
  confirmProductOrder,
  refundProduct,
  buyProduct,
  getProductReport
} from '../services/Inventory';

export default {
  namespace: 'Inventory',

  state: {
    InventoryTypes: {
      list: [],
      pagination: {},
    },

    InventoryManage: {
      list: [],
      pagination: {},
    },

    OrgList: {
      list: [],
      pagination: {},
    },

    ProductChange: {
      list: [],
      pagination: {},
    },

    orderList: {
      list: [],
      pagination: {},
    },

    productReport: {
      list: [],
      pagination: {},
    }
  },

  effects: {

    *getInventoryType({ payload: { pagination, orgId, productTypeName } }, { call, put }) {
      const response = yield call(getInventory, { ...pagination, orgId, productTypeName });
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message, result } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveInventoryTypes',
            payload: result
          });
          break;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    *getOrgList({ payload: { pagination, orgId, orgName } }, { call, put }) {
      const response = yield call(getOrgList, { ...pagination, orgName, orgId });
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message, result } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveOrgList',
            payload: result
          });
          break;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    *delInventoryType({ payload: { orgId, idList, pagination, productTypeName } }, { call, put }) {
      const response = yield call(delInventory, { idList, orgId });
      if (!response) {
        notification.error({
          message: '删除失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '删除成功',
          });
          yield put({
            type: 'getInventoryType',
            payload: {
              pagination,
              orgId,
              productTypeName,
            },
          });
          break;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    *addInventoryType({ payload: { orgId, data, pagination } }, { call, put }) {
      const response = yield call(addInventory, { ...data });
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '添加/修改成功',
          });
          yield put({
            type: 'getInventoryType',
            payload: {
              ...pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    *getInventoryManage({ payload: { pagination, orgId, query = {} } }, { call, put }) {
      const response = yield call(getInventoryManage, { ...pagination, orgId, ... query });
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message, result } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveInventoryManage',
            payload: result
          });
          break;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    *addInventoryManage({ payload }, { call, put }) {
      const response = yield call(addInventoryManage, payload);
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '添加/修改成功',
          });
          yield put(routerRedux.push({
            pathname: '/Inventory/productManage',
          }));
          break;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    *delInventoryManage({ payload: { productIdList, orgId, pagination, query = {} } }, { call, put, select }) {
      const response = yield call(delInventoryManage, { productIdList, orgId });
      if (!response) {
        notification.error({
          message: '删除失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '删除成功',
          });
          yield select(state => {
            const { total, pageSize, current } = state.Inventory.InventoryManage.pagination;
            if (total % pageSize === 1 && current !== 1) {
              pagination.pageNum = current - 1;
            }
            if (total === 1) query = {};
          });
          yield put({
            type: 'getInventoryManage',
            payload: {
              pagination,
              orgId,
              query
            },
          });
          break;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    *rollInProduct({ payload: { data, pagination, orgId, query } }, { call, put }) {
      const response = yield call(rollInProduct, data);
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '入库成功',
          });
          yield put({
            type: 'getInventoryManage',
            payload: {
              pagination,
              orgId,
              query
            },
          });
          return true;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          return false;
        default:
          break;
      }
    },

    *rollOutProduct({ payload: { data, pagination, orgId, query } }, { call, put }) {
      const response = yield call(rollOutProduct, data);
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '出库成功',
          });
          yield put({
            type: 'getInventoryManage',
            payload: {
              pagination,
              orgId,
              query
            },
          });
          return true;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          return false;
        default:
          break;
      }
    },

    *ProductChange({ payload: { pagination, orgId, query = {} } }, { call, put }) {
      const response = yield call(ProductChange, { ...pagination, orgId, ...query });
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message, result } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveProductChange',
            payload: result
          });
          break;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    *getProductOrder({ payload: { pagination, orgId, query = {} } }, { call, put }) {
      const response = yield call(getProductOrder, { ...pagination, orgId, ...query });
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message, result } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveOrderList',
            payload: result
          });
          break;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    *confirmProductOrder({ payload: { data, query, } }, { call, put }) {
      const response = yield call(confirmProductOrder, data);
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '交付成功',
          });
          yield put({
            type: 'getProductOrder',
            payload: {
              ... query
            },
          });
          return true;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          return false;
        default:
          break;
      }
    },

    *refundProduct({ payload: { data, query, } }, { call, put }) {
      const response = yield call(refundProduct, data);
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '退款成功',
          });
          yield put({
            type: 'getProductOrder',
            payload: {
              ... query
            },
          });
          return true;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          return false;
        default:
          break;
      }
    },

    *buyProduct({ payload: { data, query, } }, { call, put }) {
      const response = yield call(buyProduct, data);
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '添加成功',
          });
          yield put({
            type: 'getProductOrder',
            payload: {
              ... query
            },
          });
          return true;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          return false;
        default:
          break;
      }
    },

    *getProductReport({ payload: { pagination, orgId, query = {} } }, { call, put }) {
      const response = yield call(getProductReport, { ...pagination, orgId, ...query } );
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveProductReport',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    *_searchData({ payload: { type, value } }, { put }) {
      yield put({
        type,
        payload: value,
      });
    },
  },

  reducers: {

    saveInventoryTypes(state, action) {
      const { list, total, pageSize, current } = action.payload;
      return {
        ...state,
        InventoryTypes: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
      };
    },

    saveOrgList(state, action) {
      const { list, total, pageSize, current } = action.payload;
      return {
        ...state,
        OrgList: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
      };
    },

    saveInventoryManage(state, action) {
      const { list, total, pageSize, current } = action.payload;
      return {
        ...state,
        InventoryManage: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
      };
    },

    saveProductChange(state, action) {
      const { list, total, pageSize, current } = action.payload;
      return {
        ...state,
        ProductChange: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
      };
    },

    saveOrderList(state, action) {
      const { list, total, pageSize, current } = action.payload;
      return {
        ...state,
        orderList: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
      };
    },

    saveProductReport(state, action) {
      const { list, total, pageSize, current } = action.payload;
      return {
        ...state,
        productReport: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
      };
    },

  },
};
