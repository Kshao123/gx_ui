import { notification } from 'antd';
import { query, submit, queryFunction, saveFunction, deleteRole } from '@/services/role';


export default {
  namespace: 'role',

  state: {
    data: {
      list: [], // 表格数据
      pagination: {}, // 页码数据
    },
    modal: {// 对话框 状态控制
      data: {},
      visible: false, // 控制 新建对话框显示
      functionVisible: false, // 控制 设置校色权限对话框显示
    },
  },

  effects: {
    // 查询角色
    *fetch({ payload: { pagination, orgId } }, { call, put }) {
      const response = yield call(query, { ...pagination, orgId });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'save',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    *submit({ payload }, { call }) {
      const response = yield call(submit, payload);
      return response;
    },

    *saveFunction({ payload }, { call }) {
      const response = yield call(saveFunction, payload);
      return response;
    },

    *queryFunction({ payload }, { call }) {
      const response = yield call(queryFunction, payload.id);
      return response;
    },
    // 删除角色
    *delete({ payload: { pagination, orgId, ids } }, { call, put }) {
      const response = yield call(deleteRole, ids);
      if (!response) {
        notification.error({
          message: '删除失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '删除成功',
            description: message,
          });
          yield put({
            type: 'fetch',
            payload: {
              pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        data: {
          list: action.payload.list,
          pagination: {
            total: action.payload.total,
            pageSize: action.payload.pageSize,
            current: action.payload.current,
          },
        },
      };
    },
    handleModalVisible(state) {
      return {
        ...state,
        modal: {
          visible: !state.modal.visible,
        },
      };
    },
    handleFunctionRight(state, action) {
      return {
        ...state,
        modal: {
          functionVisible: !state.modal.functionVisible,
          data: action.data,
        },
      };
    },
    handleTransfer(state, action) {
      return {
        ...state,
        modal: {
          functionVisible: state.modal.functionVisible,
          data: {
            ...state.modal.data,
            functions: action.payload,
          },
        },
      };
    },
  },
};
