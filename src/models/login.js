import { routerRedux } from 'dva/router';
import router from 'umi/router';
import { Modal } from 'antd';
import { fakeAccountLogin, getFakeCaptcha, logout } from '@/services/api';
import { setAuthority } from '@/utils/authority';
import { getPageQuery } from '@/utils/utils';
import { reloadAuthorized } from '@/utils/Authorized';

export default {
  namespace: 'login',

  state: {
    status: undefined,
    endTime: undefined,
  },

  effects: {

    *getCaptcha({ payload }, { call }) {
      yield call(getFakeCaptcha, payload);
    },
    // -------------------

    *_login({ payload }, { call, put, select }) {
      const response = yield call(fakeAccountLogin, payload);
      if (response && response.status === true) {
        const { result: { endTime } } = response;
        yield put({
          type: 'changeLoginStatus',
          payload: response,
        });
        reloadAuthorized();

        // const endTime = 1546699410000;
        if (endTime) {
          const Now = new Date().getTime();
          const diff = Math.floor((endTime - Now) / 1000 / 60 /60);
          yield put({
            type: 'saveEndTime',
            payload: diff,
          });
          if (diff < 72) {
            setTimeout(() => {
              const modal = Modal.confirm();
              let num = 5;
              const time = setInterval(() => {
                num -= 1;
                modal.update({
                  okText: `将在${num}秒后跳转至续费页面`,
                });
                if (num <= 0) {
                  clearInterval(time);
                  modal.destroy();
                  router.push('/account/settings/binding');
                }
              }, 1000);
              modal.update({
                title: '注意！您的可用时间已不足 72 小时！！',
                content: '请立即充值续费！逾期系统将自动限制您的登陆！',
                maskClosable: false,
                okText: `将在${num}秒后跳转至续费页面`,
                cancelText: '知道了',
                onOk() {
                  if (time) clearInterval(time);
                  router.push('/account/settings/binding');
                },
                onCancel() {
                  if (time) clearInterval(time);
                }
              });
            },1500);
          }
        }

        router.replace('/');
      } else {
        yield put({
          type: 'changeLoginStatus',
          payload: response,
        });
      }
    },
    *_logout(_, { call, put, select }) {
      try {
        // 清除当前储存用户信息 否则可能会导致信息 为上一个用户
        yield select(state => {
          state.layoutloading.currentUser = {};
        });
        yield call(logout);
      } finally {
        setAuthority();
        router.push('/user/login')
      }
    },
    *noAuth(_, { put }) {
      router.push('/user/login')
    },
  },

  reducers: {
    changeLoginStatus(state, { payload }) {
      if (!payload) {
        return {
          ...state,
        }
      }
      if (!payload.status) {
        return {
          ...state,
          status: payload.status,
          type: payload.type, // undefined
        }
      }
      setAuthority(payload.result.powers);
      return {
        ...state,
        status: payload.status,
        type: payload.type, // undefined
      };
    },
    saveEndTime(state, { payload }) {
      return {
        ...state,
        endTime: payload,
      }
    }
  },
};
