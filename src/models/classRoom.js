import { notification } from 'antd';
import {
  getAllClassroom,
  addClassroom,
  delClassroom,
  getClassRoomOrder,
} from '@/services/classroom';

export default {
  namespace: 'classRoom',

  state: {
    data: {
      list: [], // 表格数据
      pagination: {}, // 页码数据
    },
    Leasing: {
      list: [], // 表格数据
      pagination: {}, // 页码数据
    },
  },

  effects: {
    // 查询所有教室信息
    *_getAllClassroom({ payload: { pagination, orgId } }, { call, put }) {
      const response = yield call(getAllClassroom, { orgId, ...pagination });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'save',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 添加/修改教室
    *_addClassroom({ payload: { pagination, orgId, data } }, { call, put }) {
      const response = yield call(addClassroom, data);
      if (!response) {
        notification.error({
          message: '添加/修改失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '添加/修改成功',
          });
          yield put({
            type: '_getAllClassroom',
            payload: {
              pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '添加失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 删除教室
    *_delClassroom({ payload: { ids, pagination, orgId } }, { call, put }) {
      const response = yield call(delClassroom, ids);
      if (!response) {
        notification.error({
          message: '删除失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '删除成功',
          });
          yield put({
            type: '_getAllClassroom',
            payload: {
              pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
  //   ---------

    // 查询所有教室订单信息
    *_getClassRoomOrder({ payload: { orgId, pagination, query={} } }, { call, put }) {
      const response = yield call(getClassRoomOrder, {  orgId, ...pagination, ...query });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveLeasing',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

  },

  reducers: {
    save(state, action) {
      const { list, total, pageSize, current } = action.payload;
      return {
        ...state,
        data: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
      };
    },
    saveLeasing(state, action) {
      const { list, total, pageSize, current } = action.payload;
      return {
        ...state,
        Leasing: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
        oldPage: list,
      };
    },
  },
};
