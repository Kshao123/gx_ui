import { message, notification } from 'antd';
import {
  queryAuth,
  reload,
  userMsg,
  getLeave,
  getLeaveList,
  getOneselfInfo,
  setUserMsg,
} from '../services/user';
import { setAuthority } from '@/utils/authority';

export default {
  namespace: 'layoutloading',

  state: {
    loading: true,
    currentUser: {},
    userMsg: {},
    leaveList: {
      list: [],
      pagination: {},
    },
    // 个人信息
    OneselfInfo: {},

  },

  effects: {
    *fetchAuth(_, { call, put }) { // 获取用户信息
      const response = yield call(queryAuth);
      if (response && response.status === true) {
        yield put({
          type: 'save',
          payload: response.result,
        });
        return response.result;
      }

    },
    *reload(_, { call, put, select }) {
      const response = yield call(reload);
      if (response && response.status === true) {
        yield put({
          type: 'reloadRole',
          payload: response.result,
        });
        message.success('角色权限刷新成功！');
      }
    },
    // mock 请求用户信息
    *getUserMsg({ payload }, { call, put }) {
      const response = yield call(userMsg, payload);
      if (!response) {
        // notification.error({
        //   message: '查询失败',
        //   description: '服务连接失败',
        // });
        return;
      }
      const { status, result, message: msg } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveMsg',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: msg,
          });
          break;
        default:
          break;
      }
    },
    // 登陆用户 发起请假
    *_getLeave({ payload: { data, pagination, orgId, userId } }, { call, put }) {
      const response = yield call(getLeave, data);
      if (!response) {
        notification.error({
          message: '请假失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message: msg } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '请假成功 getUserMsg',
          });
          yield put({
            type: '_getLeaveList',
            payload: {
              pagination,
              orgId,
              userId,
            },
          });
          break;
        case false:
          notification.error({
            message: '请假失败',
            description: msg,
          });
          break;
        default:
          break;
      }
    },
    // 登陆用户 查询请假
    *_getLeaveList({ payload: { pagination, orgId, userId } }, { call, put }) {
      const response = yield call(getLeaveList, { ...pagination, orgId, userId });
      if (!response) {
        notification.error({
          message: '请假失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message: msg } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveLeave',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '请假失败',
            description: msg,
          });
          break;
        default:
          break;
      }
    },
    // 登陆用户 查询个人信息
    *_getOneselfInfo({ payload: { orgId } }, { call, put }) {
      const response = yield call(getOneselfInfo, { orgId });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message: msg } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveOneselfInfo',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: msg,
          });
          break;
        default:
          break;
      }
    },
    // 登陆用户 保存个人信息
    *_setUserMsg({ payload }, { call, put }) {
      const response = yield call(setUserMsg, payload );
      if (!response) {
        notification.error({
          message: '保存失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message: msg } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '用户信息保存成功',
          });
          yield put({
            type: 'getUserMsg',
            payload: payload.id,
          });
          break;
        case false:
          notification.error({
            message: '保存失败',
            description: msg,
          });
          break;
        default:
          break;
      }
    },
  },

  reducers: {
    save(state, { payload }) {
      const { powerNames } = payload;
      payload.roles = powerNames;
      setAuthority(powerNames);
      return {
        ...state,
        currentUser: payload,
        loading: false,
      };
    },
    saveMsg(state, { payload }) {
      return {
        ...state,
        userMsg: payload,
      };
    },
    // 更新请假列表
    saveLeave(state, { payload }) {
      const { list, total, pageSize, current } = payload;
      return {
        ...state,
        leaveList: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
      };
    },
    // 更新个人信息列表
    saveOneselfInfo(state, { payload }) {
      return {
        ...state,
        OneselfInfo: payload,
      };
    },
    reloadRole(state, action) {
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          roles: action.payload,
        },
        loading: false,
      };
    },
  },
};
