import { notification } from 'antd';
import {
  skipCourse,
  stopCourse,
} from '../services/stuApi';

export default {
  namespace: 'stuApi',

  state: {
  },

  effects: {

    *skipCourse({ payload: { orgId, teaId, nowWeek, studentCourseIds } }, { call, put }) {
      const response = yield call(skipCourse, { studentCourseIds });
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'schedule/_getSchedules',
            payload: {
              teaId,
              nowWeek,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    *stopCourse({ payload: { orgId, teaId, isStopCourse, id, pagination } }, { call, put }) {
      const response = yield call(stopCourse, { id, isStopCourse });
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '修改成功',
          });
          yield put({
            type: 'studentDatas/queryStu',
            payload: {
              teaId,
              pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
  },

  reducers: {
  },
};
