import { routerRedux } from 'dva/router';
import { notification } from 'antd';
import {
  queryOne2One,
  submitOne2One,
  deleteOne2One,
  getStuCourse,
  changeStuCourse
} from '../services/one2one';

export default {
  namespace: 'one2one',

  state: {
    data: {
      list: [],
      pagination: {},
    },
    myCoursePurchase: [],
    changeCourse: {
      list: [],
      pagination: {},
    },
  },

  effects: {
    // 查询一对一课程
    *_getAllOno2one({ payload: { pagination, orgId, name } }, { call, put }) {
     const response = yield call(queryOne2One, { orgId, ...pagination, stuName: name || '' });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'save',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 搜索的回调
    *_searchData({ payload }, { put }) {
      yield put({
        type: 'save',
        payload,
      });
    },
    *delete({ payload: { pagination, orgId, ids } }, { call, put }) {
      const response = yield call(deleteOne2One, ids);
      if (!response) {
        notification.error({
          message: '删除失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '删除成功',
            description: message,
          });
          yield put({
            type: '_getAllOno2one',
            payload: {
              pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 添加/修改 一对一
    *submitOne2One({ payload: { values, flag, data } }, { call, put }) {
      const response = yield call(submitOne2One, values);
      if (!response) {
        notification.error({
          message: '添加/修改失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '添加/修改成功',
          });
          if (flag) {
            yield put({
              type: '_getAllOno2one',
              payload: {
                ...data
              }
            });
            return true
          }

          yield put(routerRedux.push({
            pathname: '/teaching/one2one',
          }));

          break;
        case false:
          notification.error({
            message: '添加失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
  //  查询学生剩余课程
    *_getStuCourse({ payload: { pagination, stuCourseArrangeId } }, { call, put }) {
      const response = yield call(getStuCourse, { stuCourseArrangeId, ...pagination});
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveStuCourse',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

  //  给学生排课
    *_changeStuCourse({ payload: { data, pagination, stuCourseArrangeId } }, { call, put }) {
      const response = yield call(changeStuCourse, data);
      if (!response) {
        notification.error({
          message: '修改失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '排课成功',
          });
          yield put({
            type: '_getStuCourse',
            payload: {
              pagination,
              stuCourseArrangeId,
            }
          });
          break;
        case false:
          notification.error({
            message: '排课失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

  },

  reducers: {
    save(state, action) {
      const { list, total, pageSize, current } = action.payload;
      return {
        ...state,
        data: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
      };
    },
    saveStuCourse(state, action) {
      const { list, total, pageSize, current } = action.payload;
      return {
        ...state,
        changeCourse: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
      };
    },

    saveCoursePurchase(state, action) {
      return {
        ...state,
        myCoursePurchase: action.payload,
      };
    },
  },
};
