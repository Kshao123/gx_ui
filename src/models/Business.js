import { notification } from 'antd';
import {
  getWeChatPublic,
  upDateWeChatPublic,
  getHolidayInfo,
  setHolidayInfo,
  delHolidayInfo,
  getBaseInfo,
  getInformation,
  getCanSignTime,
  getCanLeaveNum,
} from '@/services/Business';

const TYPE = {
  'getCanSignTime': {
    get: getCanSignTime,
    save: 'CanSignDay'
  },
  'getCanLeaveNum': {
    get: getCanLeaveNum,
    save: 'CanLeaveNums'
  },
};

export default {
  namespace: 'Business',

  state: {
    WeChatPublic : {
    },
    HolidayInfo: {
      list: [],
      pagination: {},
    },
    BaseInfo: {
      imgUrlList: [],
      logoUrl: '',
      orgDetails: '',
    },
    Information: {
      contactTeacherInfo2: '',
      contactTeacherInfo1: '',
      contactTeacherInfo3: '',
      contactWeChatNo: '',
      contactWeChatQrCode: '',
    },
    CanLeaveNums: 0,
    CanSignDay: 0,
  },

  effects: {
    *getWeChatPublic({ payload }, { call, put }) {
      const response = yield call(getWeChatPublic);
      if (!response) {
        notification.error({
          message: '查询用户信息失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveWeChatPublic',
            payload: result
          });
          return result;
        case false:
          notification.error({
            message: '查询用户信息失败',
            description: message,
          });
          return null;
        default:
          break;
      }
    },

    *upDateWeChatPublic({ payload }, { call, put }) {
      const response = yield call(upDateWeChatPublic, payload);
      if (!response) {
        notification.error({
          message: '更新用户信息失败',
          description: '服务连接失败',
        });
        return false;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'getWeChatPublic',
          });
          return true;
        case false:
          notification.error({
            message: '更新用户信息失败',
            description: message,
          });
          return false;
        default:
          break;
      }
    },

    *getHolidayInfo({ payload: { pagination, orgId } }, { call, put }) {
      const response =  yield call(getHolidayInfo, { ...pagination, orgId });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveHolidayInfo',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    *delHolidayInfo({ payload: { data, ids } }, { call, put }) {
      const response = yield call(delHolidayInfo, { ids });
      if (!response) {
        notification.error({
          message: '删除失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '删除成功',
          });
          yield put({
            type: 'getHolidayInfo',
            payload: {
              ...data
            }
          });
          break;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    *setHolidayInfo({ payload: { data, value } }, { call, put }) {
      const response = yield call(setHolidayInfo, value);
      if (!response) {
        notification.error({
          message: '添加/修改失败',
          description: '服务连接失败',
        });
        return false;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '添加/修改成功',
          });
          yield put({
            type: 'getHolidayInfo',
            payload: {
              ...data
            }
          });
          return true;
        case false:
          notification.error({
            message: '添加失败',
            description: message,
          });
          return false;
        default:
          break;
      }
    },
  //  查询商户的基本信息
    *getBaseInfo({ payload }, { call, put } ) {
      const response =  yield call(getBaseInfo, payload );
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveBaseInfo',
            payload: result,
          });
          return result;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
  //  查询商户的联系信息
    *getInformation({ payload }, { call, put } ) {
      const response =  yield call(getInformation, payload );
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveInformation',
            payload: result,
          });
          return result;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    *setInformation({ payload: { orgId, data } }, { call, put }) {
      const response =  yield call(getInformation, { orgId, ...data} );
      if (!response) {
        notification.error({
          message: '修改失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '更新成功',
          });
          yield put({
            type: 'getInformation',
            payload: {
              orgId
            },
          });
          break;
        case false:
          notification.error({
            message: '修改失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
  //  修改商户的基本信息
    *setBaseInfo({ payload: { orgId, ...data } }, { call, put } ) {
      const response =  yield call(getBaseInfo, { orgId, isRead: 2, ...data} );
      if (!response) {
        notification.error({
          message: '修改失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'getBaseInfo',
            payload: {
              orgId
            },
          });
          break;
        case false:
          notification.error({
            message: '修改失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    *getOtherInfo({ payload: { orgId, type= 'getCanSignTime' } }, { call, put } ) {
      const response =  yield call(TYPE[type].get, { orgId } );
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveOthers',
            payload: {
              payload: result,
              type: TYPE[type].type, // 汝何秀
            },
          });
          return result;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    *setOtherInfo({ payload: { orgId, type= 'getCanSignTime', data } }, { call, put } ) {
      const response =  yield call(TYPE[type].get, { orgId, isRead: 2, ...data } );
      if (!response) {
        notification.error({
          message: '修改失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '更新成功',
            duration: 1
          });
          yield put({
            type: 'getOtherInfo',
            payload: {
              orgId,
              type, // 汝何秀
            },
          });
          break;
        case false:
          notification.error({
            message: '修改失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
  },

  reducers: {
    saveWeChatPublic(state, { payload }) {
      return {
        ...state,
        WeChatPublic: payload,
      }
    },

    saveHolidayInfo(state, { payload }) {
      const { list, total, pageSize, current } = payload;
      return {
        ...state,
        HolidayInfo: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        },
      };
    },

    saveBaseInfo(state, { payload }) {
      const { imgUrlList= [], logoUrl = '', orgDetails = '' } = payload;
      return {
        ...state,
        BaseInfo: {
          imgUrlList,
          logoUrl,
          orgDetails,
        },
      };
    },

    saveInformation(state, { payload }) {
      const {
        contactTeacherInfo2= '',
        contactTeacherInfo1= '',
        contactTeacherInfo3= '',
        contactWeChatNo= '',
        contactWeChatQrCode= '',
      } = payload;
      return {
        ...state,
        Information: {
          contactTeacherInfo2,
          contactTeacherInfo1,
          contactTeacherInfo3,
          contactWeChatNo,
          contactWeChatQrCode
        },
      };
    },

    saveOthers(state, { payload: { payload, type } }) {
      return {
        ... state,
        [type]: payload,
      }
    }
  },
}
