import { notification } from 'antd';
import {
  getSystemPrice,
  setSystemPrice,
  delSystemPrice,
  getSystemOrders,
  delSystemOrders,
  getSystemTerm,
  setSystemTerm,
  delSystemTerm,
} from '@/services/system';

export default {
  namespace: 'system',

  state: {
    data: [],
    Orders: {
      list: [],
      pagination: {},
    },
    Term: {
      list: [],
      pagination: {},
    }
  },

  effects: {
    // 查询系统价格
    *_getSystemPrice(_, { call, put }) {
      const response = yield call(getSystemPrice);
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'save',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 添加或修改 系统价格
    *_setSystemPrice({ payload }, { call, put }) {
      const response = yield call(setSystemPrice, payload);
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '添加/修改成功',
          });
          yield put({
            type: '_getSystemPrice',
          });
          break;
        case false:
          notification.error({
            message: '添加/修改失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 删除系统价格
    *_delSystemPrice({ payload }, { call, put }) {
      const response = yield call(delSystemPrice, payload);
      if (!response) {
        notification.error({
          message: '删除失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '删除成功',
          });
          yield put({
            type: '_getSystemPrice',
          });
          break;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    // 查询系统订单
    *_getSystemOrders({ payload }, { call, put }) {
      const response = yield call(getSystemOrders, payload);
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveOrders',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    // 删除系统价格
    *_delSystemOrders({ payload: { payload, ids } }, { call, put }) {
      const response = yield call(delSystemOrders, ids);
      if (!response) {
        notification.error({
          message: '删除失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '删除成功',
          });
          yield put({
            type: '_getSystemOrders',
            payload,
          });
          break;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    //------------------
    // 查询账号租期
    *_getSystemTerm({ payload }, { call, put }) {
      const response = yield call(getSystemTerm, payload);
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveTerm',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 添加或修改 账号租期
    *_setSystemTerm({ payload:{ data } }, { call, put }) {
      const response = yield call(setSystemTerm, data);
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '添加/修改成功',
          });
          return status;
        case false:
          notification.error({
            message: '添加/修改失败',
            description: message,
          });
          return status;
        default:
          break;
      }
    },
    // 删除账号租期
    *_delSystemTerm({ payload: { ids, pagination } }, { call, put }) {
      const response = yield call(delSystemTerm, ids);
      if (!response) {
        notification.error({
          message: '删除失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '删除成功',
          });
          yield put({
            type: '_getSystemTerm',
            payload: pagination,
          });
          break;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    *_Updata({ payload: { data, value } }, { put }) {
      yield put({
        type: value,
        payload: data,
      });
    },
  },

  reducers: {
    save(state, { payload: data }) {
      return {
        ...state,
        data,
      };
    },

    // 更新订单
    saveOrders(state, action) {
      const { list, total, pageSize, current } = action.payload;
      return {
        ...state,
        Orders: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        }
      };
    },
    // 更新账号租期
    saveTerm(state, action) {
      const { list, total, pageSize, current } = action.payload;
      return {
        ...state,
        Term: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        }
      };
    },

  },
};
