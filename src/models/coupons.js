import { notification } from 'antd';
import { routerRedux } from 'dva/router';
import {
  addGenerates,
  getGenerates,
  delGenerates,
  getLuckDraw,
  openLuckDraw,
  closeLuckDraw
} from '@/services/coupons';
import { submitOne2One } from '../services/one2one';

export default {
  namespace: 'coupons',

  state: {
    generates: {
      list: [],
      pagination: {},
    },

    luckDrawInfo: {
      list: [],
      pagination: {},
    }
  },

  effects: {
    *getGenerates({ payload: { pagination, orgId, query = {} } }, { call, put }) {
      const response =  yield call(getGenerates, { ...pagination, orgId, ...query });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveGenerates',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    *addGenerates({ payload: { values, query } }, { call, put }) {
      const response =  yield call(addGenerates, values);
      if (!response) {
        notification.error({
          message: '添加失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '添加成功',
          });
          yield put({
            type: 'getGenerates',
            payload: query,
          });
          return true;
        case false:
          notification.error({
            message: '添加失败',
            description: message,
          });
          return false;
        default:
          break;
      }
    },
    *delGenerates({ payload: { courseCouponIds, query } }, { call, put }) {
      const response =  yield call(delGenerates, {courseCouponIds});
      if (!response) {
        notification.error({
          message: '添加失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '删除成功',
          });
          yield put({
            type: 'getGenerates',
            payload: query,
          });
          return true;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          return false;
        default:
          break;
      }
    },

    *getLuckDraw({ payload: { pagination, orgId } }, { call, put }) {
      const response =  yield call(getLuckDraw, { ...pagination, orgId });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveLuckDrawInfo',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    *openLuckDraw({ payload: { values, flag, orgId, pagination }  }, { call, put }) {
      const response = yield call(openLuckDraw, values);
      if (!response) {
        notification.error({
          message: '添加/修改失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '开启成功',
          });

          if (flag) {
            yield put({
              type: 'getLuckDraw',
              payload: {
                orgId,
                pagination,
              },
            });
            return;
          }
          yield put(routerRedux.push({
            pathname: '/Coupons/SharingLottery',
          }));

          break;
        case false:
          notification.error({
            message: '添加失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    *closeLuckDraw({ payload: { orgId, pagination } }, { call, put }) {
      const response =  yield call(closeLuckDraw, {orgId});
      if (!response) {
        notification.error({
          message: '添加失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '删除成功',
          });
          yield put({
            type: 'getLuckDraw',
            payload: {
              pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
  },

  reducers: {
    saveGenerates(state, { payload: { list, total, pageSize, current } }) {
      return {
        ... state,
        generates: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        }
      }
    },

    saveLuckDrawInfo(state, { payload: { list, total, pageSize, current } }) {
      return {
        ... state,
        luckDrawInfo: {
          list,
          pagination: {
            total,
            pageSize,
            current,
          },
        }
      }
    }
  }
}
