import { routerRedux } from 'dva/router';
import { notification } from 'antd';
import {
  query,
  submit,
  deleteMine,
  getManager,
  getExamine,
  ToAudit,
  setManager,
  delManager,
} from '@/services/mine';
import { submitOne2One } from '../services/one2one';

export default {
  namespace: 'mine',

  state: {
    data: {
      list: [],
      pagination: {},
    },
    modal: {
      visible: false,
      data: {},
    },
    mangerList: {
      list: [],
      pagination: {},
    },
  },

  effects: {
    // 查询我的机构
    *fetch({ payload }, { call, put }) {
      const response = yield call(query, payload);
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'save',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 添加或修改 机构
    *submit({ payload }, { call, put }) {
      const response = yield call(submit, payload);
      if (!response) {
        notification.error({
          message: '添加/修改失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      if (status) return status;
      notification.error({
        message: '添加/修改失败',
        description: message,
      });
    },

    *delete({ payload: { ids, pagination, userId } }, { call, put }) {
      const response = yield call(deleteMine, ids);
      if (!response) {
        notification.error({
          message: '删除失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '删除成功',
          });
          yield put({
            type: 'fetch',
            payload: {
              ...pagination,
              userId,
            },
          });
          break;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 查询机构法人
    *_getManager({ payload: { pagination, orgId } }, { call, put }) {
      const response = yield call(getManager, { ...pagination, orgId });
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveManager',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 删除机构法人
    *_delManager({ payload: { ids, pagination, orgId } }, { call, put }) {
      const response = yield call(delManager, ids);
      if (!response) {
        notification.error({
          message: '删除失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '删除成功',
          });
          yield put({
            type: '_getManager',
            payload: {
              pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '删除失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 添加/修改 法人
    *_setManager({ payload:{ values, pagination, orgId } }, { call, put }) {
      const response = yield call(setManager, values);
      if (!response) {
        notification.error({
          message: '添加/修改失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '添加/修改成功',
          });
          yield put({
            type: '_getManager',
            payload: {
              pagination,
              orgId,
            },
          });
          break;
        case false:
          notification.error({
            message: '添加失败',
            description: "用户名重复，请检测后再试",
          });
          break;
        default:
          break;
      }
    },

    // 查询机构审批
    *_getExamine({ payload }, { call, put }) {
      const response = yield call(getExamine, payload);
      if (!response) {
        notification.error({
          message: '查询失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'saveManager',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '查询失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    //  机构审批
    *_ToAudit({ payload: { pagination, userId, data } }, { call, put }) {
      const response = yield call(ToAudit, data);
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '审批成功',
          });
          yield put({
            type: '_getExamine',
            payload: {
              ...pagination,
              userId,
            },
          });
          break;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
  },
  reducers: {
    // 我的机构信息
    save(state, action) {
      return {
        ...state,
        data: {
          list: action.payload.list,
          pagination: {
            total: action.payload.total,
            pageSize: action.payload.pageSize,
            current: action.payload.current, // 页码跳转为后台传过来的数据pageNum
          },
        },
      };
    },
    // 机构法人信息
    saveManager(state, { payload: { list, total, pageSize, current } }) {
      return {
        ...state,
        mangerList: {
          list,
          pagination: {
            total,
            pageSize,
            current, // 页码跳转为后台传过来的数据pageNum
          },
        },
      };
    },
  },
};
