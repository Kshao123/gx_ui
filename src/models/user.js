import { notification } from 'antd';
import { routerRedux } from 'dva/router';
import {
  setPassword,
  Register,
  getRegister,
  getPrice,
  getEwm,
  getPayRes,
  getEwmXf,
} from '@/services/user';

export default {
  namespace: 'user',

  state: {
    list: [],
    currentUser: {},
  //  2018 -12-21
    passState: false,
    Price: [],
  },

  effects: {
  //  2018 -12 -21
// 改变修改密码弹窗的 显隐状态
    *changePassState(_, { put }) {
      yield put({
        type: 'savePassState',
      });
    },

    *setPassword({ payload }, { call, put }) {
      const response = yield call(setPassword, payload);
      if (!response) {
        notification.error({
          message: '修改失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, message } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '密码修改成功',
          });
          yield put({
            type: 'changePassState',
          });
          yield put(routerRedux.replace({
            pathname: '/user/login',
          }));
          break;
        case false:
          notification.error({
            message: '修改失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 申请账户
    *Register({ payload }, { call }) {
      const response = yield call(Register, payload);
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status } = response;
      switch (status) {
        case true:
          notification.success({
            message: '成功',
            description: '申请成功',
          });
          return status;
        case false:
          notification.error({
            message: '失败',
            description: '用户名或者手机号码已存在！请检查后再提交',
          });
          break;
        default:
          break;
      }
    },
    // 查询账号审核状态
    *getRegister({ payload }, { call }) {
      const response = yield call(getRegister, payload);
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          return result;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
    // 查询月份所对应的价格
    *getPrice({ payload }, { call, put }) {
      const response = yield call(getPrice, payload);
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          yield put({
            type: 'savePrice',
            payload: result,
          });
          break;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    // 查询二维码
    *getEwm({ payload }, { call }) {
      const response = yield call(getEwm, payload);
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return false;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          return result;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    // 查询续费二维码
    *getEwmXf({ payload }, { call }) {
      const response = yield call(getEwmXf, payload);
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return false;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          return result;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },

    // 查询支付结果
    *getPayRes({ payload }, { call }) {
      const response = yield call(getPayRes, payload);
      if (!response) {
        notification.error({
          message: '失败',
          description: '服务连接失败',
        });
        return;
      }
      const { status, result, message } = response;
      switch (status) {
        case true:
          return result.userPayResult;
        case false:
          notification.error({
            message: '失败',
            description: message,
          });
          break;
        default:
          break;
      }
    },
  },

  reducers: {
  //  2018 - 12-21
    savePassState(state) {
      return {
        ...state,
        PassState: !state.PassState,
      };
    },

    changeNotifyCount(state, action) {
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          notifyCount: action.payload,
        },
      };
    },
    // 储存价格
    savePrice(state, { payload: Price }) {
      return {
        ...state,
        Price,
      };
    },
  },
};
