import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Card,
  Form,
  Icon,
  Button,
  Divider,
  Table,
} from 'antd';
import { routerRedux } from 'dva/router';
import isEqual from 'lodash/isEqual';
import StandardTable from '@/components/DelTable';
import styles from './classFicationList.less';
import Popcon from '@/components/Popconfirm';
import { scriptUrl } from '@/utils/utils';
import Authorized from '@/utils/Authorized';

const { check } = Authorized;
const IconFont = Icon.createFromIconfontCN({
  scriptUrl,
});

@connect(({ global, studentDatas, loading }) => ({
  currentOrg: global.currentOrg,
  loading: loading.effects['studentDatas/_queryCourse'],
  studentDatas,
}))
@Form.create()
class classFicationList extends PureComponent {
  constructor() {
    super();
    this.state = {
      selectedRows: [],
      pagination: {
        pageSize: 10,
        pageNum: 1,
      },
      currentOrg: {
        id: '',
      },
    };
  }

  componentDidMount() {
    const { dispatch, currentOrg } = this.props;
    if (!currentOrg.id) return;
    const { pagination } = this.state;
    dispatch({
      type: 'studentDatas/_queryCourse',
      payload: {
        pagination,
        orgId: currentOrg.id,
      },
    });
    dispatch({
      type: 'studentDatas/_queryCourseType',
      payload: {
        orgId: currentOrg.id,
      },
    });
  }

  static getDerivedStateFromProps(nextProps, preState) {
    const { dispatch } = nextProps;
    if (!nextProps.currentOrg.id) return null;
    if (isEqual(nextProps.currentOrg.id, preState.currentOrg.id)) {
      return null;
    }
    const { pagination } = preState;
    dispatch({
      type: 'studentDatas/_queryCourse',
      payload: {
        pagination,
        orgId: nextProps.currentOrg.id,
      },
    });
    dispatch({
      type: 'studentDatas/_queryCourseType',
      payload: {
        orgId: nextProps.currentOrg.id,
      },
    });
    return {
      currentOrg: nextProps.currentOrg,
    };
  }

  handleStandardTableChange = (pagination) => {
    const { dispatch } = this.props;
    const { pageSize, current: pageNum } = pagination;
    this.setState({
      pagination: {
        pageNum,
        pageSize,
      },
    });
    dispatch({
      type: 'studentDatas/_queryCourse',
      payload: {
        pagination: {
          pageNum,
          pageSize,
        },
        orgId: this.state.currentOrg.id,
      },
    });
  };

  handleEdit = (record) => {
    const { dispatch } = this.props;
    dispatch(routerRedux.push({
      pathname: '/teaching/classification/mgr',
      state: record,
    }));
  };

  handleDelete = (record) => {
    const { subjectId, gradeVos } = record;
    const { pagination, currentOrg } = this.state;
    const gradeIds = gradeVos.map(item => item.gradeId);
    const { dispatch } = this.props;
    dispatch({
      type: 'studentDatas/DelCourse',
      payload: {
        ids: [subjectId],
        gradeIds,
        pagination,
        orgId: currentOrg.id,
      },
    });
  };

  forItem = (type, text) => (
    <div>
      <IconFont type={type} />
      <span>  {text}</span>
    </div>
    );

  handleTabChange = (item) => {
    const subList = item.filter(items => !items.editable);
    this.setState({
      subList,
    });
  };

  expandedRowRender = (data) => {
    const { forItem } = this;
    const colums = [
      {
        title: '级别名称',
        dataIndex: 'gradeName',
        align: 'center',
        key: 'gradeName',
        render(text) {
          return forItem('icon-mingcheng', `${text}`);
        },
      },
      {
        title: '价格 / 元',
        dataIndex: 'price',
        align: 'center',
        key: 'price',
        render(text) {
          return forItem('icon-jiage', `${text}`);
        },
      },
    ];
    return (
      <Table
        rowKey={(record) => record.orgId}
        bordered={false}
        columns={colums}
        dataSource={data.gradeVos}
        pagination={false}
      />
    );
  };

  render() {
    const { forItem } = this;
    const columns = [
      {
        title: '科目类型',
        dataIndex: 'subjectTypeName',
        childrenColumnName: 'father',
        render(text) {
          return forItem('icon-jibie', text);
        },
      },
      {
        title: '科目名称',
        dataIndex: 'subjectName',
        render(text) {
          return forItem('icon-kemu', text);
        },
      },
      {
        title: '默认时长',
        dataIndex: 'defaultCourseTime',
        render(text) {
          return forItem('icon-shijian', (text || 0) + '分钟');
        },
      },
      {
        title: '操作',
        align: 'center',
        render: (text, record) => (
          <Fragment>
            {check('科目管理:修改', <a onClick={() => this.handleEdit(record)}>修改</a>)}
            <Divider type="vertical" />
            {check('科目管理:删除', (
              <Popcon
                title="是否确认删除"
                onConfirm={() => this.handleDelete(record)}
              />
            ))}
          </Fragment>
        ),
      },
    ];

    const { studentDatas: { OldOptions }, loading } = this.props;
    const { selectedRows } = this.state;
    return (
      <Card>
        <div className={styles.tableList}>
          <div className={styles.tableListOperator}>
            {check('科目管理:增加', (
              <Button icon="plus" type="primary" onClick={() => this.handleEdit(undefined)}>
                新建
              </Button>
            ))}
          </div>
          <StandardTable
            bordered
            loading={loading}
            selectedRows={selectedRows}
            data={OldOptions}
            columns={columns}
            onChange={this.handleStandardTableChange}
            expandedRowRender={record => (this.expandedRowRender(record))}
          />
        </div>
      </Card>
    );
  }
}
export default classFicationList;
