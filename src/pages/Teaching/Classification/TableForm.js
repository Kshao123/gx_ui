import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Table,
  Button,
  Input,
  message,
  Popconfirm,
  Divider,
  InputNumber,
} from 'antd';
import isEqual from 'lodash/isEqual';
import styles from './classFicationList.less';

class TableForm extends PureComponent {
  constructor(props) {
    super(props);
    const { dataList } = props.value;
    this.state = {
      data: dataList || [],
      loading: false,
      value: props.value,
      ShowBtn: true,
    };
  }

  static getDerivedStateFromProps(nextProps, preState) {
    if (isEqual(nextProps.value, preState.value)) {
      return null;
    }
    return {
      data: nextProps.value.dataList || nextProps.value || [], // 这样写是因为变化时 父组件中的判断有问题 会导致传来一个空数组
      value: nextProps.value,
    };
  }


  getRowByKey(key, newData) {
    const { data } = this.state;
    return (newData || data).filter(item => item.key === key)[0];
  }

  index = 0;

  cacheOriginData = {};

  toggleEditable = (e, key) => {
    e.preventDefault();
    const { data } = this.state;
    const newData = data.map(item => ({ ...item }));
    const target = this.getRowByKey(key, newData);
    if (target) {
      // 进入编辑状态时保存原始数据
      if (!target.editable) {
        this.cacheOriginData[key] = { ...target };
      }
      target.editable = !target.editable;
      this.setState({ data: newData });
    }
  };

  newMember = () => {
    const { data } = this.state;
    const newData = data.map(item => ({ ...item }));
    newData.push({
      key: `NEW_TEMP_ID_${this.index}`,
      gradeName: { label: '' },
      price: '',
      editable: true,
      isNew: true,
      new: true,
    });
    this.index += 1;
    this.setState({ data: newData });
  };

  remove(key) {
    const { data } = this.state;
    const { onChange } = this.props;
    const newData = data.filter(item => item.key !== key);
    this.setState({ data: newData });
    onChange(newData);
  }

  handleKeyPress(e, key) {
    if (e.key === 'Enter') {
      this.saveRow(e, key);
    }
  }

  handleFieldChange(e, fieldName, key) {
    const { data } = this.state;
    const newData = data.map(item => ({ ...item }));
    const target = this.getRowByKey(key, newData);
    if (target) {
      target[fieldName] = e.target.value;
      this.setState({ data: newData });
    }
  }

  saveRow(e, key) {
    e.persist();
    this.setState({
      loading: true,
    });
    setTimeout(() => {
      if (this.clickedCancel) {
        this.clickedCancel = false;
        return;
      }
      const target = this.getRowByKey(key) || {};
      if (!target.gradeName.label.length || !target.price || !Number(target.price)) {
        message.error('请填写完整信息。');
        e.target.focus();
        this.setState({
          loading: false,
        });
        return;
      }
      delete target.isNew;
      this.toggleEditable(e, key);
      const { data } = this.state;
      const { onChange } = this.props;
      onChange(data);
      this.setState({
        loading: false,
        data,
      });
    }, 500);
  }

  cancel(e, key) {
    this.clickedCancel = true;
    e.preventDefault();
    const { data } = this.state;
    const newData = data.map(item => ({ ...item }));
    const target = this.getRowByKey(key, newData);
    if (this.cacheOriginData[key]) {
      Object.assign(target, this.cacheOriginData[key]);
      delete this.cacheOriginData[key];
    }
    target.editable = false;
    this.setState({ data: newData });
    this.clickedCancel = false;
  }

  handlePrice = (value, attr, record) => {
    const { key } = record;
    const { data } = this.state;
    if (attr === 'gradeName') {
      const target = data.map((item) => {
        if (item.key === key) {
          item[attr].label = value;
        }
        return item;
      });
      this.setState({
        data: target,
      });
      return;
    }
    const target = data.map((item) => {
      if (item.key === key) {
        item[attr] = value;
      }
      return item;
    });
    this.setState({
      data: target,
    });
  };

  render() {
    const { handlePrice } = this;
    const columns = [
      {
        title: '级别',
        dataIndex: 'gradeName',
        key: 'gradeName',
        render: (text, record) => {
          if (record.editable) {
            return (
              <Input value={text.label} onChange={value => handlePrice(value.target.value, 'gradeName', record)} placeholder="请输入级别名称" />
            );
          }
          return text.label;
        },
      },
      {
        title: '价格 / 元',
        dataIndex: 'price',
        key: 'price',
        render: (text, record) => {
          if (record.editable) {
            return (
              <InputNumber onChange={value => handlePrice(value, 'price', record)} value={text} />
            );
          }
          return text;
        },
      },
      {
        title: '操作',
        key: 'action',
        render: (text, record) => {
          const { loading } = this.state;
          if (!!record.editable && loading) {
            return null;
          }
          if (record.editable) {
            if (record.isNew) {
              return (
                <span>
                  <a onClick={e => this.saveRow(e, record.key)}>添加</a>
                  <Divider type="vertical" />
                  <Popconfirm title="是否要删除此行？" onConfirm={() => this.remove(record.key)}>
                    <a>删除</a>
                  </Popconfirm>
                </span>
              );
            }
            return (
              <span>
                <a onClick={e => this.saveRow(e, record.key)}>保存</a>
                <Divider type="vertical" />
                <a onClick={e => this.cancel(e, record.key)}>取消</a>
              </span>
            );
          }
          return (
            <span>
              <a onClick={e => this.toggleEditable(e, record.key)}>编辑</a>
              <Divider type="vertical" />
              <Popconfirm title="是否要删除此行？" onConfirm={() => this.remove(record.key)}>
                <a>删除</a>
              </Popconfirm>
            </span>
          );
        },
      },
    ];

    const { loading, data } = this.state;
    return (
      <Fragment>
        <Table
          loading={loading}
          columns={columns}
          dataSource={data}
          pagination={false}
          rowKey={(record) => record.key}
          rowClassName={record => (record.editable ? styles.editable : '')}
        />
        <Button
          style={{ width: '100%', marginTop: 16, marginBottom: 8 }}
          type="dashed"
          onClick={this.newMember}
          icon="plus"
        >
          新增科目
        </Button>
      </Fragment>
    );
  }
}

export default connect(({ studentDatas, loading }) => ({
  studentDatas,
  loading: loading.global,
}))(TableForm);
