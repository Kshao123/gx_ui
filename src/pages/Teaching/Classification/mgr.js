import React, { PureComponent, Fragment } from 'react';
import {
  Card,
  Button,
  Form,
  Icon,
  Col,
  Row,
  Input,
  Popover,
  InputNumber,
  message,
  Upload,
  Select,
} from 'antd';
import { connect } from 'dva';
import FooterToolbar from '@/components/FooterToolbar';
import styles from './index.less';
import TableForm from './TableForm';
import { url } from '@/utils/utils';

const { Option } = Select;
const { TextArea } = Input;
const fieldLabels = {
  subjectTypeName: '科目类型',
  subjectName: '科目名称',
  defaultCourseTime: '默认时长',
  courseCharacteristics: '课程特色',
  learningObjectives: '学习目标',
  subjectPictureUrl: '科目图片',
};

class One2One extends PureComponent {

  constructor(props) {
    super(props);
    const { location: { state } } = this.props;
    let subList = [];
    let fileList = [];
    if (state) {
      subList = state.gradeVos.map((items, index) => ({
            key: `NEW_TEMP_ID_${index}112`,
            gradeName: {
              value: items.gradeId,
              label: items.gradeName,
            },
            price: items.price,
            editable: false,
            isNew: false,
            new: false,
      }));
      const url = state.subjectPictureUrl
      if (url) {
        fileList = [
          {
            url,
            name: fieldLabels.subjectPictureUrl,
            uid: url,
          }
        ]
      }
    }
    this.state = {
      subList,
      fileList,
    };
  }

  componentDidMount() {
    setTimeout(() => {
      const { dispatch, currentOrg: { id } } = this.props;
      if (!id) return;
      dispatch({
        type: 'studentDatas/_queryCourseType',
        payload: {
          orgId: id,
        },
      });
    },200)
  }

  handleTabChange = (item) => {
    const subList = item.filter(items => !items.editable);
    this.setState({
      subList,
    });
  };

  handleUpload = (file) => {
    const { name, size } = file;
    const Rxp = /.+\.(jpg|jpeg|png|bmp)$/;
    if (!Rxp.test(name)) {
      message.error('请上传正确的图片文件 仅支持 jpg | png | jpeg | bmp ', 3);
      return false;
    } else if ((size / 1024) > 6000) {
      message.error('图片文件大于 5M', 3);
      return false;
    }
    return true;
  };

  handleRemove = (file) => {
    const { form: { resetFields } } = this.props;
    resetFields(['subjectPictureUrl']);
    const { uid } = file;
    const fileList = this.state.fileList.filter(item => item.uid !== uid);
    this.setState({
      fileList,
    });
    return true;
  };

  handleChange = ({ file, fileList} ) => {
    let state = false;
    const { name, size, status } = file;
    const Rxp = /.+\.(jpg|jpeg|png|bmp)$/;
    if (!Rxp.test(name) || (size / 1024) > 3000) {
      return;
    }
    let fileList1 = fileList;
    fileList1 = fileList1.map((item) => {
      if (item.response) {
        item.url = item.response.result;
      }
      return item;
    });
    fileList1 = fileList1.filter((item) => {
      const { response } = item;
      if (response) {
        const { status: states, message: msg } = response;
        !states ? message.error(msg) : '';
        state = states;
        return states === true;
      }
      return true;
    });
    (status === 'done' && state) ? message.success('上传成功') : '';
    this.setState({ fileList: fileList1 });
  };


  fixCommit = (data) => {
    const { stuDatas: { OptionsTypes } } = this.props;
    const { fileList } = this.state;
    const { gradeVos, subjectTypeName, subjectId } = data;
    let subjectTypeId = '';
    const list = gradeVos.map((item) => {
      if (item.new) {
        return {
          gradeName: item.gradeName.label,
          price: item.price,
        };
      }
      return {
        gradeId: item.gradeName.value,
        gradeName: item.gradeName.label,
        price: item.price,
      };
    });
    if (OptionsTypes.length) {
      subjectTypeId = OptionsTypes.filter(item => item.label === subjectTypeName)[0].value;
    }
    let subjectPictureUrl = '';
    if (fileList.length) {
      try {
        subjectPictureUrl = fileList[0].url;
      }catch (e) {
        console.log(e)
      }
    }

    return {
      ...data,
      subjectTypeId,
      gradeVos: list,
      subjectId,
      subjectPictureUrl,
    };
  };

  validate = () => {
    const { form: { validateFieldsAndScroll }, location: { state }, dispatch } = this.props;
    validateFieldsAndScroll((error, values) => {
      if (!error) {
        const { subList } = this.state;
        values.gradeVos = subList;
        values = this.fixCommit(values);
        if (!state) {
          delete values.subjectId;
        }
        dispatch({
          type: 'studentDatas/saveCourse',
          payload: {
            data: values,
          },
        });
      }
    });
  };

  render() {
    const {
      form,
      submitting,
      currentOrg,
      location,
      stuDatas: { OptionsTypes },
      dispatch,
    } = this.props;
    const { getFieldDecorator, getFieldsError } = form;
    const { state } = location;
    const { subList, fileList } = this.state;
    const {
      subjectName,
      subjectTypeName,
      defaultCourseTime,
      courseCharacteristics,
      learningObjectives,
      subjectPictureUrl,
    } = fieldLabels;
    const errors = getFieldsError();
    const getErrorInfo = () => {
      const errorCount = Object.keys(errors).filter(key => errors[key]).length;
      if (!errors || errorCount === 0) {
        return null;
      }
      const scrollToField = (fieldKey) => {
        const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
        if (labelNode) {
          labelNode.scrollIntoView(true);
        }
      };
      const errorList = Object.keys(errors).map((key) => {
        if (!errors[key]) {
          return null;
        }
        return (
          <li key={key} className={styles.errorListItem} onClick={() => scrollToField(key)}>
            <Icon type="cross-circle-o" className={styles.errorIcon} />
            <div className={styles.errorMessage}>{errors[key][0]}</div>
            <div className={styles.errorField}>{fieldLabels[key]}</div>
          </li>
        );
      });
      return (
        <span className={styles.errorIcon}>
          <Popover
            title="表单校验信息"
            content={errorList}
            overlayClassName={styles.errorPopover}
            trigger="click"
            getPopupContainer={trigger => trigger.parentNode}
          >
            <Icon type="exclamation-circle" />
          </Popover>
          {errorCount}
        </span>
      );
    };

    return (
      <Fragment>
        <Card title="科目基础信息" className={styles.card} bordered={false}>
          <Form layout="vertical">
            {getFieldDecorator('subjectId', {
              initialValue: state ? state.subjectId : '' })(
                <Input type="hidden" />
            )}
            {getFieldDecorator('subjectTypeId', {
              initialValue: state ? state.subjectTypeId : '' })(
                <Input type="hidden" />
            )}
            {getFieldDecorator('orgId', {
              initialValue: state && state.orgId ? state.orgId : (currentOrg.id || ''),
            })(
              <Input type="hidden" />
            )}
            <Row gutter={16}>
              <Col lg={8} md={8} sm={24}>
                <Form.Item label={subjectTypeName}>
                  {getFieldDecorator('subjectTypeName', {
                    initialValue: state ? state.subjectTypeName : '',
                    rules: [{ required: true, message: '请选择' }],
                  })(
                    <Select>
                      {OptionsTypes
                        ? OptionsTypes.map(item => <Option key={item.label}>{item.label}</Option>)
                        : null}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col lg={8} md={8} sm={24}>
                <Form.Item label={subjectName}>
                  {getFieldDecorator('subjectName', {
                    initialValue: state ? state.subjectName : '',
                    rules: [{ required: true, message: '请输入科目名称' }],
                  })(
                    <Input placeholder="科目名称" />
                  )}
                </Form.Item>
              </Col>
              <Col lg={8} md={8} sm={24}>
                <Form.Item label={defaultCourseTime + ' / 分'}>
                  {getFieldDecorator('defaultCourseTime', {
                    initialValue: state ? state.defaultCourseTime : 45,
                    rules: [{ required: true, message: '请输入时长' }],
                  })(
                    <InputNumber placeholder="时长 / 分" style={{ width: '100%' }} min={1} max={99999} />
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col lg={12} md={12} sm={24}>
                <Form.Item label={subjectPictureUrl}>
                  {getFieldDecorator('subjectPictureUrl', {
                    initialValue: state ? fileList : [],
                    rules: [
                      { required: true, message: '请重新上传' }],
                  })(
                    <Upload
                      accept="image/gif,image/jpg,image/png"
                      action= {`${url}/mechanism/saveImage`}
                      listType= {'picture-card'}
                      name= "file1"
                      className='upload-list-inline'
                      beforeUpload= {this.handleUpload}
                      onRemove= {this.handleRemove}
                      onChange= {this.handleChange}
                      fileList={fileList}
                    >
                      { fileList.length >= 1 ? null : (
                        <div>
                          <Icon className="ant-icon" type="plus" />
                          <div className="ant-upload-text">{subjectPictureUrl}</div>
                        </div>
                      ) }
                    </Upload>
                  )}
                </Form.Item>
              </Col>
            </Row>

          </Form>
        </Card>

        <Card title="特色简介" className={styles.card} bordered={false}>
          <Row gutter={16}>
            <Col lg={20} md={20} sm={20}>
              <Form.Item label={courseCharacteristics}>
                {getFieldDecorator('courseCharacteristics', {
                  initialValue: state ? state.courseCharacteristics : '',
                  rules: [{ required: true, message: '请输入内容' }],
                })(
                  <TextArea placeholder={courseCharacteristics} autosize={{ minRows: 2, maxRows: 6 }} />
                )}
              </Form.Item>
            </Col>
            <Col lg={20} md={20} sm={20}>
              <Form.Item label={learningObjectives}>
                {getFieldDecorator('learningObjectives', {
                  initialValue: state ? state.learningObjectives : '',
                  rules: [{ required: true, message: '请输入内容' }],
                })(
                  <TextArea placeholder={learningObjectives} autosize={{ minRows: 2, maxRows: 6 }} />
                )}
              </Form.Item>
            </Col>
          </Row>
        </Card>

        <Card title="科目级别" className={styles.card} bordered={false}>
          <Form.Item label="科目级别">
            {getFieldDecorator('gradeVos', {
              initialValue: state || subList.length ? { dataList: subList } : [],
              rules: [{ required: true, message: '请输入级别' }],
            })(
              <TableForm dispatch={dispatch} onChange={this.handleTabChange} />
            )}
          </Form.Item>
        </Card>
        <FooterToolbar style={{ width: '100%' }}>
          {getErrorInfo()}
          <Button type="primary" onClick={this.validate} loading={submitting}>
            提交
          </Button>
        </FooterToolbar>
      </Fragment>
    );
  }
}

export default connect(({ global, studentDatas }) => ({
  stuDatas: studentDatas,
  currentOrg: global.currentOrg,
}))(Form.create()(One2One));
