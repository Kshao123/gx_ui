import React, { PureComponent, Fragment } from 'react';
import moment from 'moment';
import { Table, Button, message, Popconfirm, Divider, TimePicker, Select } from 'antd';
import styles from '@/utils/global.less';

import { parseTime } from "@/utils/utils";

const { Option } = Select;

const format = 'HH:mm';

export default class TimeArrange extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: props.value || [],
      loading: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if ('value' in nextProps) {
      this.setState({
        data: nextProps.value,
      });
    }
  }

  getRowByKey(key, newData) {
    return (newData || this.state.data).filter(item => item.key === key)[0];
  }

  index = 0;

  cacheOriginData = {};

  toggleEditable=(e, key) => {
    e.preventDefault();
    const newData = this.state.data.map(item => ({ ...item }));
    const target = this.getRowByKey(key, newData);
    if (target) {
      // 进入编辑状态时保存原始数据
      if (!target.editable) {
        this.cacheOriginData[key] = { ...target };
      }
      target.editable = !target.editable;
      this.setState({ data: newData });
    }
  };

  remove(key) {
    const newData = this.state.data.filter(item => item.key !== key);
    this.setState({ data: newData });
    this.props.onChange(newData);
  }

  newMember = () => {
    const newData = this.state.data.map(item => ({ ...item }));
    newData.push({
      key: `NEW_TEMP_ID_${this.index}`,
      whichWeek: '',
      startTime: new Date().getTime(),
      endTime: new Date().getTime(),
      editable: true,
      isNew: true,
    });
    this.index += 1;
    this.setState({ data: newData });
  };

  handleKeyPress(e, key) {
    if (e.key === 'Enter') {
      this.saveRow(e, key);
    }
  }

  handleFieldChange(e, fieldName, key) {
    const newData = this.state.data.map(item => ({ ...item }));
    const target = this.getRowByKey(key, newData);
    if (target) {
      target[fieldName] = e.target.value;
      this.setState({ data: newData });
    }
  }

  handleSelectChange(e, fieldName, key) {
    const newData = this.state.data.map(item => ({ ...item }));
    const target = this.getRowByKey(key, newData);
    if (target) {
      target[fieldName] = e;
      this.setState({ data: newData });
    }
  }

  handleTimeChange(time, timestring, fieldName, key) {
    const { defaultTime } = this.props;
    const newData = this.state.data.map(item => ({ ...item }));
    const target = this.getRowByKey(key, newData);
    if (target && time) {
      target[fieldName] = new Date(time.toDate()).getTime();
      if (fieldName === 'startTime') {
        target.endTime = new Date(time.toDate()).getTime() + defaultTime * 60 * 1000;
      }
      this.setState({ data: newData });
    }
  }

  saveRow(e, key) {
    e.persist();
    this.setState({
      loading: true,
    });
    setTimeout(() => {
      if (this.clickedCancel) {
        this.clickedCancel = false;
        return;
      }
      const target = this.getRowByKey(key) || {};
      if (!target.whichWeek || !target.startTime || !target.endTime) {
        message.error('请填写完整成员信息。');
        e.target.focus();
        this.setState({
          loading: false,
        });
        return;
      } if (target.startTime >= target.endTime || ((target.endTime - target.startTime) <= 19 * 60 * 1000)) {
        message.error('请选择正确的时间！每节课不少于20分钟！');
        e.target.focus();
        this.setState({
          loading: false,
        });
        return;
      }
      delete target.isNew;
      this.toggleEditable(e, key);
      this.props.onChange(this.state.data);
      this.setState({
        loading: false,
      });
    }, 500);
  }

  cancel(e, key) {
    this.clickedCancel = true;
    e.preventDefault();
    const newData = this.state.data.map(item => ({ ...item }));
    const target = this.getRowByKey(key, newData);
    if (this.cacheOriginData[key]) {
      Object.assign(target, this.cacheOriginData[key]);
      target.editable = false;
      delete this.cacheOriginData[key];
    }
    this.setState({ data: newData });
    this.clickedCancel = false;
  }

  render() {
    const columns = [{
      title: '周几',
      dataIndex: 'whichWeek',
      key: 'whichWeek',
      width: '30%',
      render: (text, record) => {
        if (record.editable) {
          return (
            <Select placeholder="请选择周几" value={text} style={{ width: '80%' }} onChange={e => this.handleSelectChange(e, 'whichWeek', record.key)}>
              <Option key="1" value="周一">周一</Option>
              <Option key="2" value="周二">周二</Option>
              <Option key="3" value="周三">周三</Option>
              <Option key="4" value="周四">周四</Option>
              <Option key="5" value="周五">周五</Option>
              <Option key="6" value="周六">周六</Option>
              <Option key="7" value="周日">周日</Option>
            </Select>
          );
        }
        return text;
      },
    }, {
      title: '开始时间',
      dataIndex: 'startTime',
      key: 'startTime',
      width: '30%',
      render: (text, record) => {
        if (record.editable) {
          return (
            <TimePicker format={format} value={moment(parseTime(text, '{h}:{i}'), 'HH:mm')} onChange={(time, timestring) => this.handleTimeChange(time, timestring, 'startTime', record.key)} />
          );
        }
        return parseTime(text).split(' ')[1].substr(0, 5);
      },
    }, {
      title: '结束时间',
      dataIndex: 'endTime',
      key: 'endTime',
      width: '30%',
      render: (text, record) => {
        if (record.editable) {
          return (
            <TimePicker format={format} value={moment(parseTime(text, '{h}:{i}'), 'HH:mm')} onChange={(time, timestring) => this.handleTimeChange(time, timestring, 'endTime', record.key)} />
          );
        }
        return parseTime(text).split(' ')[1].substr(0, 5);
      },
    }, {
      title: '操作',
      key: 'action',
      render: (text, record) => {
        if (!!record.editable && this.state.loading) {
          return null;
        }
        if (record.editable) {
          if (record.isNew) {
            return (
              <span>
                <a onClick={e => this.saveRow(e, record.key)}>添加</a>
                <Divider type="vertical" />
                <Popconfirm title="是否要删除此行？" onConfirm={() => this.remove(record.key)}>
                  <a>删除</a>
                </Popconfirm>
              </span>
            );
          }
          return (
            <span>
              <a onClick={e => this.saveRow(e, record.key)}>保存</a>
              <Divider type="vertical" />
              <a onClick={e => this.cancel(e, record.key)}>取消</a>
            </span>
          );
        }
        return (
          <span>
            <a onClick={e => this.toggleEditable(e, record.key)}>编辑</a>
            <Divider type="vertical" />
            <Popconfirm title="是否要删除此行？" onConfirm={() => this.remove(record.key)}>
              <a>删除</a>
            </Popconfirm>
          </span>
        );
      },
    }];

    return (
      <Fragment>
        <Table
          loading={this.state.loading}
          columns={columns}
          dataSource={this.state.data}
          pagination={false}
          rowClassName={(record) => record.editable ? styles.editable : ''}
        />
        <Button
          style={{ width: '100%', marginTop: 16, marginBottom: 8 }}
          type="dashed"
          onClick={this.newMember}
          icon="plus"
        >
          新增
        </Button>
      </Fragment>
    );
  }
}

