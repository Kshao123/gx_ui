import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import {
  Row,
  Col,
  Card,
  Form,
  Icon,
  Button,
  Divider,
  Tooltip,
  Modal,
  InputNumber,
  Menu,
  Dropdown,
  Table,
  Spin,
  Select,
} from 'antd';
import StandardTable from '@/components/DelTable';
import SearchSel from '@/components/SearchSel';
import styles from '@/utils/global.less';
import { scriptUrl, parseTime, url } from '@/utils/utils';
import Ellipsis from '@/components/Ellipsis';
import Popcon from '@/components/Popconfirm';
import Hoc from '@/utils/Hoc';
import Authorized from '@/utils/Authorized';

const { check } = Authorized;
const { Option } = Select;
const FormItem = Form.Item;
const IconFont = Icon.createFromIconfontCN({
  scriptUrl,
});

@connect(({ one2one, loading, global }) => ({
  one2one,
  currentOrg: global.currentOrg,
  loading: loading.effects['one2one/_getAllOno2one'],
  crouseLoading: loading.effects['one2one/_getStuCourse'],
  addLoading: loading.effects['api/getStuCoupon']
}))
@Form.create()
@Hoc({ fetchUrl: 'one2one/_getAllOno2one' })
class One2OneList extends PureComponent {
  constructor() {
    super();
    this.state = {
      visible: false,
      FormObj: {},
      couponsList: [],
      props: {
        removal: true,
        onChange: this.handleValueChange,
        queryUrl: `${url}/one2one/selectOne2One`,
        keyValue: 'name',
        showSearch: true,
        attr: {
          id: 'stuId',
          name: 'stuName',
        },
        placeholder: '搜索学生',
      },
    };
  }

  handleEdit = (record) => {
    const { dispatch } = this.props;
    dispatch(routerRedux.push({
      pathname: '/teaching/one2one/mgr',
      state: record,
    }));
  };

  handleDelete = ({ id }) => {
    const { dispatch, currentOrg, pagination } = this.props;
    dispatch({
      type: 'one2one/delete',
      payload: {
        ids: [id],
        orgId: currentOrg.id,
        pagination,
      },
    });
  };

  handleFormReset = () => {
    const { dispatch, currentOrg, pagination } = this.props;
    if (!currentOrg.id) return;
    dispatch({
      type: 'one2one/_getAllOno2one',
      payload: {
        pagination,
        orgId: currentOrg.id,
      },
    });
  };

  handleValueChange = (value, id, data) => {
    const { list } = data;
    const { label } = value;
    if (!list.length) return;
    if (list.length >= 2) {
      data.list = list.filter(item => item.stuName === label);
    }
    const { dispatch } = this.props;
    dispatch({
      type: 'one2one/_searchData',
      payload: data,
    });
  };

  handleSerChange = ({ key }, id) => {
    this.setState({
      [id]: Number(key),
    });
  };

  renderSearchForm() {
    const { currentOrg  } = this.props;
    return (
      <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="学生">
              <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...this.state.props} />
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <Button onClick={this.handleFormReset}>重置</Button>
          </Col>
        </Row>
      </Form>
    );
  }


  handleOk = () => {
    const { dispatch, pagination, currentOrg, form } = this.props;
    const { FormObj } = this.state;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      dispatch({
        type: 'one2one/submitOne2One',
        payload: {
          values: Object.assign(FormObj || {}, values),
          data: {
            pagination,
            orgId: currentOrg.id,
          },
          flag: true,
        },
      })
        .then(res => {
          if (!res) return;
          form.resetFields();
          this.setState({
            visible: false,
          });
        })
    });
  };

  handleCancel = () => {
    this.props.form.resetFields();
    this.setState({
      visible: false,
      couponsList: [],
    });
  };


  handleAddCourse = (record) => {
    const { stuId } = record;
    this.props.dispatch({
      type: 'api/getStuCoupon',
      payload: {
        stuId,
      }
    }).then(res => {
      if (res && res.length) {
        this.setState({
          couponsList: res,
        })
      }
    });
    this.setState({
      visible: true,
      FormObj: record,
    });
  };


  render() {
    const { forItem } = this.props;
    const columns = [
      {
        title: '学生姓名',
        key: 'stuName',
        dataIndex: 'stuName',
        render(text) {
          return forItem('icon-mingcheng', text);
        },
      },
      {
        title: '教师姓名',
        key: 'teaName',
        dataIndex: 'teaName',
        render(text) {
          return forItem('icon-jiaoshi1', `${text}`);
        },
      },
      {
        title: '课程名称',
        key: 'courseName',
        dataIndex: 'courseName',
        render(text, record) {
          return (
            <Tooltip title={`所属类型：${record.subjectTypeName}`}>
              { forItem('icon-kechengbiao', `${text} | ${record.grade}`)}
            </Tooltip>
          );
        },
      },
      {
        title: '课数',
        key: 'courseNum',
        align: 'center',
        dataIndex: 'courseNum',
        render(text) {
          return forItem('icon-leixing', `${text} 节`);
        },
      },
      {
        title: '单价',
        key: 'price',
        align: 'center',
        dataIndex: 'price',
        render(text) {
          return forItem('icon-jiage', `${text} 元`);
        },
      },
      {
        title: '总价',
        align: 'center',
        key: 'courseTotalPrice',
        dataIndex: 'courseTotalPrice',
        render(text) {
          return forItem('icon-jiage', `${text} 元`);
        },
      },
      {
        title: '教室名称',
        align: 'center',
        key: 'classroomName',
        dataIndex: 'classroomName',
        render(text) {
          return (
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <IconFont style={{ marginRight: 4 }} type="icon-jiaoshi" />
              <Ellipsis length={6} tooltip>
                {text}
              </Ellipsis>
            </div>
          );
        },
      },
      // {
      //   title: '时间',
      //   children: [
      //     {
      //       title: '所在周',
      //       key: 'weeks',
      //       align: 'center',
      //       dataIndex: 'weeks',
      //       render(text) {
      //         return forItem('icon-yigexingqi', text);
      //       },
      //     },
      //     {
      //       title: '开始时间',
      //       align: 'center',
      //       key: 'startCourseTime',
      //       dataIndex: 'startCourseTime',
      //       render(text) {
      //         return forItem('icon-shijian1', (parseTime(text).split(' ')[1].substr(0, 5)));
      //       },
      //     },
      //     {
      //       title: '结束时间',
      //       align: 'center',
      //       key: 'endCourseTime',
      //       dataIndex: 'endCourseTime',
      //       render(text) {
      //         return forItem('icon-shijian1', parseTime(text).split(' ')[1].substr(0, 5));
      //       },
      //     },
      //   ],
      // },
      {
        title: '生效时间',
        key: 'firstCourseTime',
        align: 'center',
        dataIndex: 'firstCourseTime',
        render(text) {
          return forItem('icon-shijian1', text ? parseTime(text, '{y}-{m}-{d}') : '')
          // return forItem('icon-shijian1', `${parseTime(text).split(' ')[0]} ${parseTime(record.startCourseTime).split(' ')[1].substr(0, 5)}`);
        },
      },
      {
        title: '已上课时',
        align: 'center',
        key: 'alreadyCourse',
        dataIndex: 'alreadyCourse',
        render(text) {
          return forItem('icon-keshi', !text ? '暂无' : `${text} 节`);
        },
      },
      {
        title: '操作',
        fixed: 'right',
        width: 125,
        render: (text, record) => (
          <Fragment>
            {check('一对一:修改', (
              <Dropdown overlay={(
                <Menu>
                  <Menu.Item>
                    <a onClick={() => this.handleEdit(record)}>{(`${record.stuName } | `) || ''}课程修改</a>
                  </Menu.Item>
                  <Menu.Item>
                    <a onClick={() => this.handleAddCourse(record)}>添加课时</a>
                  </Menu.Item>
                </Menu>
              )}
              >
                <a className="ant-dropdown-link">
                  修改 <Icon type="down" />
                </a>
              </Dropdown>
            ))}
            <Divider type="vertical" />
            {check('一对一:删除', (
              <Popcon
                title="是否确认删除"
                onConfirm={() => this.handleDelete(record)}
              />
            ))}
          </Fragment>
        ),
      },
    ];
    const expandedRowRender = (data) => {
      const column = [
        {
          title: '所在星期',
          dataIndex: 'weeks',
          key: 'weeks',
          render(text) {
            return forItem('icon-yigexingqi', text);
          }
        },
        {
          title: '开始时间',
          dataIndex: 'startCourseTime',
          key: 'startCourseTime',
          render(text) {
            return forItem('icon-shijian1', text ? parseTime(text, '{h}:{i}') : '');
          }
        },
        {
          title: '结束时间',
          dataIndex: 'endCourseTime',
          key: 'endCourseTime',
          render(text){
            return forItem('icon-shijian1', text ? parseTime(text, '{h}:{i}') : '');
          }
        },
      ];

      return (
        <Table
          rowKey={(record) => JSON.stringify(record)}
          bordered={false}
          columns={column}
          dataSource={data.courseInfo}
          pagination={false}
        />
      );
    };
    const {
      one2one: { data },
      loading,
      handleStandardTableChange,
      form: { getFieldDecorator },
      addLoading,
    } = this.props;
    const { visible, couponsList } = this.state;

    return (
      <section>
        <Card className={styles.tableListForm}>
          {this.renderSearchForm()}
        </Card>
        <Card>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              {check('一对一:增加', (
                <Button icon="plus" type="primary" onClick={() => this.handleEdit(undefined)}>
                  新建
                </Button>
              ))}
            </div>
            <StandardTable
              bordered
              loading={loading}
              data={data}
              scroll={{ x: 1360 }}
              columns={columns}
              expandedRowRender={record => (expandedRowRender(record))}
              onChange={handleStandardTableChange}
            />
          </div>
        </Card>
        <Modal
          title="添加课时"
          visible={visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Spin spinning={!!addLoading}>
            <Form layout="vertical">
              <Form.Item label="课时">
                {getFieldDecorator('courseNum', {
                  rules: [{ required: true, message: '请输入课时数' }],
                })(
                  <InputNumber placeholder="输入课时数 / 节" style={{ width: '100%' }} min={1} max={99999} />
                )}
              </Form.Item>
              <Form.Item label="优惠券">
                {getFieldDecorator('courseCouponIds', {
                  rules: [{ required: false, message: '请选择优惠券' }],
                })(
                  <Select
                    mode="multiple"
                    style={{ width: '100%' }}
                    placeholder="选择优惠券"
                  >
                    {
                      couponsList.map(item => {
                        const { id, courseCouponType, lessNum, fixedAmountCouponFulfilMoney } = item;
                        return (
                          <Option key={id}>
                            {courseCouponType === 1 ? `满${fixedAmountCouponFulfilMoney}元减${lessNum}元` : `比例优惠 ${lessNum}%`}
                          </Option>
                        )
                      })
                    }
                  </Select>
                )}
              </Form.Item>
            </Form>
          </Spin>
        </Modal>
      </section>
    );
  }
}
export default One2OneList;
