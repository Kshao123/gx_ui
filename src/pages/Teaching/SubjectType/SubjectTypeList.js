import React, { Fragment, PureComponent } from 'react';
import { connect } from 'dva';
import {
  Card,
  Form,
  Input,
  Button,
  List,
  Modal,
  Spin,
} from 'antd';
import styles from './list.less';
import Popcon from '@/components/Popconfirm';
import Hoc from '../../../utils/Hoc';
import Authorized from '@/utils/Authorized';

const { check } = Authorized;

@connect(({ global, studentDatas, loading }) => ({
  currentOrg: global.currentOrg,
  loading: loading.effects['studentDatas/_queryCourseType'],
  studentDatas,
}))
@Form.create()
@Hoc({ fetchUrl: 'studentDatas/_queryCourseType' })
class SubjectTypeList extends PureComponent {
  constructor() {
    super();
    this.state = {
      visible: false,
      isEdit: false,
      FormObj: {},
    };
  }

  handleEdit = (item, value) => {
    let str = false;
    let obj = { id: '' };
    if (value) {
      str = true;
      obj = item;
    }
    this.setState({
      visible: true,
      isEdit: str,
    });
    setTimeout(() => {
      this.setState({
        FormObj: obj,
      });
    }, 150);
  };

  handleDelete = ({ id }) => {
    const { dispatch, pagination, currentOrg } = this.props;
    dispatch({
      type: 'studentDatas/DelCourseType',
      payload: {
        ids: [id],
        pagination,
        orgId: currentOrg.id,
      },
    });
  };

  handleOk = () => {
    const { form, dispatch, pagination, currentOrg } = this.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      if (!this.state.isEdit) {
        delete values.id;
      }
      dispatch({
        type: 'studentDatas/saveCourseType',
        payload: {
          data: values,
          pagination,
          orgId: currentOrg.id,
        },
      });

      // 在此处调用的是 重置表单内容， 不重置的话会导致 对话框再次打开不能实时更新内容的bug
      form.resetFields();
      this.setState({
        visible: false,
        isEdit: false,
        FormObj: {},
      });
    });
  };

  handleCancel = () => {
    this.props.form.resetFields();
    this.setState({
      visible: false,
      isEdit: false,
      FormObj: {},
    });
  };

  Info = (text) => (
    <div className={styles.textImg}>
      <p>{text.toString()[0]}</p>
      <p>{text}</p>
    </div>
  );

  render() {
    const {
      studentDatas: { queryCourseType: { list, pagination } },
      loading,
      form,
      currentOrg,
      handleStandardTableChange,
    } = this.props;
    const { getFieldDecorator } = form;
    const { isEdit, FormObj } = this.state;
    return (
      <Card>
        <div className={styles.standardList}>

          <Card
            className={styles.listCard}
            bordered={false}
            title="科目类型"
            style={{ marginTop: 15 }}
            bodyStyle={{ padding: '0 32px 40px 32px' }}
          >
            {check('科目类型:增加', (
              <Button
                type="dashed"
                style={{ width: '100%', marginBottom: 8 }}
                icon="plus"
                onClick={this.handleEdit}
              >
                添加
              </Button>
            ))}
            <List
              size="large"
              rowKey="id"
              loading={loading}
              pagination={
                { ...pagination, onChange: handleStandardTableChange }
              }
              dataSource={list}
              renderItem={item => (
                <List.Item
                  actions={[
                    <div>
                      {check('科目类型:修改', (
                        <a
                          onClick={(e) => {
                            e.preventDefault();
                            this.handleEdit(item, true);
                          }}
                        >
                          编辑
                        </a>
                      ))}
                    </div>,
                    <div>
                      {check('科目类型:删除', (
                        <Popcon
                          title="是否确认删除"
                          onConfirm={() => this.handleDelete(item)}
                        />
                      ))}
                    </div>,
                  ]}
                >
                  {this.Info(item.subjectTypeName)}
                </List.Item>
              )}
            />
          </Card>
        </div>
        <Modal
          title="编辑/修改信息"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Spin spinning={!Object.keys(FormObj).length}>
            <Form layout="vertical">
              {getFieldDecorator('id', {
                initialValue: isEdit ? FormObj.id : '' })(
                  <Input type="hidden" />
              )}
              {getFieldDecorator('orgId', {
                initialValue: currentOrg ? currentOrg.id : '' })(
                  <Input type="hidden" />
              )}
              <Form.Item label="科目类型">
                {getFieldDecorator('subjectTypeName', {
                  initialValue: isEdit ? FormObj.subjectTypeName : '',
                  rules: [{ required: true, message: '请输入类型' }],
                })(
                  <Input placeholder="科目类型" />
                )}
              </Form.Item>
            </Form>
          </Spin>
        </Modal>
      </Card>
    );
  }
}

export default SubjectTypeList;
