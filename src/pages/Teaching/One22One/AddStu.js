import React, { Component } from 'react';
import { Modal, Form, InputNumber, Spin } from 'antd';
import SearchSel from '@/components/SearchSel';


@Form.create()
class AddStu extends Component{
  handleOk = () => {
    const { form, handleStuOk, record, dispatch, pagination, name, currentOrg } = this.props;
    const { validateFieldsAndScroll, resetFields } = form;
    validateFieldsAndScroll((error, values) => {
      if (!error) {
        const { stuId, courseNum } = values;
        const stuList = [{
          courseNum,
          stuId: parseInt(stuId.key, 10)
        }];
        dispatch({
          type: 'one22one/submitOne22One',
          payload: {
            values: {
              ...record,
              stuList,
            },
            data: {
              pagination,
              query: name,
              orgId: currentOrg.id,
            },
            flag: true,
          },
        }).then((res) => {
          if (res)  {
            handleStuOk();
            resetFields();
          }
        });
      }
    });
  };

  handleCancel = () => {
    const { form, handleStuCancel } = this.props;
    form.resetFields();
    handleStuCancel();
  };

  render() {
    const { form: { getFieldDecorator }, stuVisible, url, currentOrg, loading } = this.props;
    return(
      <Modal
        title="学生调课"
        visible={stuVisible}
        onOk={this.handleOk}
        onCancel={this.handleCancel}
      >
        <Spin spinning={loading}>
          <Form layout="vertical">
            <Form.Item label="学生">
              {getFieldDecorator('stuId', {
                rules: [{ required: true, message: '请选择学生' }],
              })(
                <SearchSel
                  orgId={currentOrg ? currentOrg.id : ''}
                  removal
                  queryUrl={`${url}/stuConsult/queryStu`}
                  keyValue="name"
                  showSearch
                  attr={
                    {
                      id: 'stuList',
                      name: 'stuName',
                    }
                  }
                  placeholder="搜索学生"
                />
              )}
            </Form.Item>

            <Form.Item label="课时数">
              {getFieldDecorator('courseNum', {
                rules: [{ required: true, message: '请输入课时数' }],
              })(
                <InputNumber placeholder="课时" style={{ width: '100%' }} min={1} max={99999} />
              )}
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    )
  }
}

export default AddStu
