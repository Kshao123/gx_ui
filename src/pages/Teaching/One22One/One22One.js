import React, { PureComponent, Fragment } from 'react';
import {
  Card,
  Button,
  Form,
  Icon,
  Col,
  Row,
  DatePicker,
  Input,
  Popover,
  InputNumber,
  message,
  Cascader,
  Select,
} from 'antd';
import moment from 'moment';
import { connect } from 'dva';
import FooterToolbar from '@/components/FooterToolbar';

import styles from './style.less';
import SearchSel from '@/components/SearchSel';
import TimeArrange from './TimeArrange';
import { parseTime, url } from '@/utils/utils';

const fieldLabels = {
  stuId: '学生',
  teacherId: '教师',
  courseId: '课程',
  classroomId: '教室',
  courseNum: '课时/节',
  price: '单价/元',
  firstCourseTime: '生效时间',
};
const { Option } = Select;
const grid = {
  xl: 8,
  lg: 8,
  md: 8,
  sm: 24,
};

class One2One extends PureComponent {
  static queryId({ grade, courseName, subjectTypeName }, arr) {
    const obj = arr.filter(item => item.label === subjectTypeName);
    if (!obj.length) return [];
    const subjectTypeId = obj[0].value;
    const course = obj[0].children.filter(item => item.label === courseName);
    if (!course.length) return [];
    const courseId = course[0].value;
    const Id = course[0].children.filter(item => item.text === grade);
    if (!Id.length) return [];
    const gradeId = Id[0].value;
    return [subjectTypeId, courseId, gradeId];
  }

  constructor(props) {
    super(props);
    const { location: { state } } = this.props;
    let orgData = {
      classroomId: '',
      teaId: '',
      stuId: [],
    };
    let editObj = { timeArrangeList: [] };
    let disabled = false;
    if (state) {
      state.timeArrangeList = state.courseInfo.map((item) => (
        {
          editable: false,
          endTime: item.endCourseTime,
          key: `${item.id}`,
          startTime: item.startCourseTime,
          whichWeek: item.weeks,
        }
      ));
      const { classroomId, teaId, stuList } = state;
      const stuId = stuList.map(item => ({ stuId: item.stuId, id: item.id }));
      orgData = {
        classroomId,
        teaId,
        stuId,
      };
      editObj = state;
      disabled = true;
    }
    this.state = {
      width: '100%',
      editObj,
      Required: true,
      props: {
        onChange: this.handleStuChange,
        queryUrl: `${url}/stuConsult/queryStu`,
        keyValue: 'name',
        placeholder: '搜索学生',
        attr: {
          id: 'stuList',
          name: 'stuName',
        },
      },
      props1: {
        onChange: this.handleValueChange,
        queryUrl: `${url}/teacher/selectTeaByCondition`,
        attr: {
          id: 'teaId',
          name: 'name',
        },
        keyValue: 'name',
        placeholder: '搜索教师',
      },
      props2: {
        onChange: this.handleValueChange,
        queryUrl: `${url}/classroom/queryAll`,
        attr: {
          id: 'classroomId',
          name: 'classroomName',
        },
        placeholder: '搜索教室',
        keyValue: 'name',
      },
      classroomId: orgData.classroomId,
      teaId: orgData.teaId,
      stuId: orgData.stuId,
      stuList: [],
      disabled,
      defaultTime: 45,
    };
  }

  componentDidMount() {
    setTimeout(() => {
      const { dispatch, currentOrg: { id } } = this.props;
      dispatch({
        type: 'studentDatas/_queryCourse',
        payload: {
          orgId: id || '',
        },
      });
    },150)
  }

  onDataChange = (data) => {
    const timeArrangeList = data.filter(item => !item.editable);
    this.setState({
      editObj: {
        ...this.state.editObj,
        timeArrangeList,
      },
    });
  };

  // 当搜索框发生变化时
  handleValueChange = ({ key }, id) => {
    this.setState({
      [id]: Number(key),
    });
  };

  handleStuChange = (data, id) => {
    const { stuId } = this.state;
    if (data.length && stuId.some(item => item.stuId === Number(data[0].key))) {
      message.error('不能选择同一位/或删除已添加学生哦', 3);
      return;
    }
    const list = data.map(item => ({ stuId: Number(item.key) }));
    this.setState({
      [id]: list,
    });
  };

  content = (currentOrg) => (
    <SearchSel width={200} orgId={currentOrg ? currentOrg.id : ''} {...this.state.props} />
  );

  fixCommitList = (item) => {
    const firstCourseTime = new Date(parseTime(new Date(item.firstCourseTime.toDate()).getTime()).split(' ')[0].split('-').join('/')).getTime();

    const courseInfo = item.timeArrangeList.map(items => (
      {
        endCourseTime: items.endTime,
        startCourseTime: items.startTime,
        weeks: items.whichWeek
      }
    ));

    let courseName;
    const [subjectTypeId, name, gradeId] = item.courseId;
    const { stuDatas } = this.props;
    if (stuDatas.Options) {
      const arr = stuDatas.Options.filter(items => items.value === subjectTypeId)[0];
      courseName = arr.children.filter(items => items.value == name)[0].label;
    }
    item.stuList.forEach(items => {
      items.courseNum = item.courseNum
    });
    return {
      ...item,
      firstCourseTime,
      courseInfo,
      courseName,
      gradeId,
      subjectTypeId,
    };
  };

  handleSelChange = (value) => {
    const { stuDatas: { OldOptions: { list } } } = this.props;
    const [ _, subId ] = value;
    let defaultTime = 45;
    try {
      defaultTime = list.filter(item => item.subjectId === subId)[0].defaultCourseTime;
    } catch (e) {
      console.log(e)
    }
    this.setState({
      defaultTime,
    })
  };

  render() {
    const {
      form,
      submitting,
      currentOrg,
      location,
      stuDatas,
      dispatch,
    } = this.props;
    const { getFieldDecorator, validateFieldsAndScroll, getFieldsError } = form;
    const { state } = location;
    const { editObj, Required, disabled, defaultTime } = this.state;
    const validate = () => {
      validateFieldsAndScroll((error, values) => {
        if (!error) {
          const { teaId, stuId, classroomId, stuList } = this.state;
          const list = [...stuList, ...stuId];
          values.classroomId = classroomId;
          values.teaId = teaId;
          values.stuList = list;
          if (!editObj || !editObj.timeArrangeList.length) {
            message.error('请选择上课时间', 3);
            return;
          }
          values.timeArrangeList = editObj.timeArrangeList;
          values = this.fixCommitList(values);
          delete values.timeArrangeList;
          delete values.courseId;
          if (!state) {
            delete values.id;
          }
          delete values.stuId;
          delete values.courseNum;
          dispatch({
            type: 'one22one/submitOne22One',
            payload: {
              values
            },
          });
        }
      });
    };
    const errors = getFieldsError();
    const getErrorInfo = () => {
      const errorCount = Object.keys(errors).filter(key => errors[key]).length;
      if (!errors || errorCount === 0) {
        return null;
      }
      const scrollToField = (fieldKey) => {
        const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
        if (labelNode) {
          labelNode.scrollIntoView(true);
        }
      };
      const errorList = Object.keys(errors).map((key) => {
        if (!errors[key]) {
          return null;
        }
        return (
          <li key={key} className={styles.errorListItem} onClick={() => scrollToField(key)}>
            <Icon type="cross-circle-o" className={styles.errorIcon} />
            <div className={styles.errorMessage}>{errors[key][0]}</div>
            <div className={styles.errorField}>{fieldLabels[key]}</div>
          </li>
        );
      });
      return (
        <span className={styles.errorIcon}>
          <Popover
            title="表单校验信息"
            content={errorList}
            overlayClassName={styles.errorPopover}
            trigger="click"
            getPopupContainer={trigger => trigger.parentNode}
          >
            <Icon type="exclamation-circle" />
          </Popover>
          {errorCount}
        </span>
      );
    };
    return (
      <Fragment>
        <Card title="小组授课" className={styles.card} bordered={false}>
          <Form layout="vertical">
            {getFieldDecorator('orgId', {
              initialValue: state && state.orgId ? state.orgId : (currentOrg.id || ''),
              rules: [{ required: Required, message: '请选择组织编号' }],
            })(
              <Input disabled type="hidden" />
            )}
            <Row gutter={16}>
              <Col {...grid}>
                <Form.Item label={fieldLabels.stuId}>
                  {getFieldDecorator('stuId', {
                    initialValue: state ?
                      state.stuList.map(item => ({ key: item.stuId, label: item.stuName }))
                      : [],
                    rules: [{ required: Required, message: '请选择' }],
                  })(
                    <SearchSel disabled={!!state} orgId={currentOrg.id || ''} {...this.state.props} />
                  )}
                </Form.Item>
              </Col>
              <Col {...grid}>
                <Form.Item label={fieldLabels.teacherId}>
                  {getFieldDecorator('teaId', {
                    initialValue: state ? { key: state.teaName, value: state.teaId } : '',
                    rules: [{ required: Required, message: '请输入' }],
                  })(
                    <SearchSel disabled={state ? !!state.flag : false} orgId={currentOrg.id || ''} showSearch {...this.state.props1} />
                  )}
                </Form.Item>
              </Col>
              <Col {...grid}>
                <Form.Item label={fieldLabels.classroomId}>
                  {getFieldDecorator('classroomId', {
                    initialValue: state ? { key: state.classroomName, value: state.classroomId } : '',
                    rules: [{ required: Required, message: '请选择' }],
                  })(
                    <SearchSel disabled={state ? !!state.flag : false} orgId={currentOrg.id || ''} showSearch {...this.state.props2} />
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col xl={12} lg={12} md={12} sm={24}>
                <Form.Item label={fieldLabels.courseId}>
                  {getFieldDecorator('courseId', {
                    initialValue: state ?
                      stuDatas.Options.length ?
                        One2One.queryId(
                          {
                            grade: state.grade,
                            courseName: state.courseName,
                            subjectTypeName: state.subjectTypeName,
                          },
                          stuDatas.Options
                        ) : []
                      : [],
                    rules: [{ required: Required, message: '请选择' }],
                  })(
                    <Cascader onChange={this.handleSelChange} disabled={state ? !!state.flag : false} options={stuDatas.Options} placeholder="请选择科目" />
                  )}
                </Form.Item>
              </Col>
              <Col xl={12} lg={12} md={12} sm={24}>
                <Form.Item label={fieldLabels.courseNum}>
                  {getFieldDecorator('courseNum', {
                    initialValue: state ? state.courseNum : '',
                    rules: [{ required: !state, message: '请输入课时数' }],
                  })(
                    <InputNumber disabled={!!state} placeholder="课时" style={{ width: '100%' }} max={99999} />
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col {...grid}>
                <Form.Item label={fieldLabels.firstCourseTime}>
                  {getFieldDecorator('firstCourseTime', {
                    initialValue: state ? moment(parseTime(state.firstCourseTime)) : moment(new Date()),
                    rules: [{ required: Required, message: '请选择生效时间' }],
                  })(
                    <DatePicker disabled={!!state} />
                  )}
                </Form.Item>
              </Col>
              <Col {...grid}>
                <Form.Item label="课程类型">
                  {getFieldDecorator('courseType', {
                    initialValue: state ? state.courseType : 2 ,
                    rules: [{ required: true, message: '请选择身份' }],
                  })(
                    <Select disabled style={{ width: '100%' }}>
                      <Option value={1}>一对一</Option>
                      <Option value={2}>小组授课</Option>
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Card>
        <Card title="上课时间安排" className={styles.card} bordered={false}>
          {getFieldDecorator('timeArrangeList', {
            initialValue:
              state && state.timeArrangeList ? state.timeArrangeList : [],
          })(<TimeArrange onChange={this.onDataChange} defaultTime={defaultTime} />)}
        </Card>
        <FooterToolbar style={{ width: this.state.width }}>
          {getErrorInfo()}
          <Button style={{ display: state ? state.flag ? 'none' : 'initial' : 'initial' }} type="primary" onClick={validate} loading={submitting}>
            提交
          </Button>
        </FooterToolbar>
      </Fragment>
    );
  }
}

export default connect(({ global, studentDatas }) => ({
  stuDatas: studentDatas,
  currentOrg: global.currentOrg,
}))(Form.create()(One2One));
