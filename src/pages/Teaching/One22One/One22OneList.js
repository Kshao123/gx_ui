import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import {
  Row,
  Col,
  Card,
  Icon,
  Form,
  Button,
  Divider,
  Table,
  Tooltip,
  Modal,
  InputNumber,
  Dropdown,
  Menu,
  Select,
} from 'antd';
import StandardTable from '@/components/DelTable';
import SearchSel from '@/components/SearchSel';
import styles from '@/utils/global.less';
import { parseTime, scriptUrl, url } from '@/utils/utils';
import Ellipsis from '@/components/Ellipsis';
import Popcon from '@/components/Popconfirm';
import Hoc from '@/utils/Hoc';
import Authorized from '@/utils/Authorized';
import AddStu from './AddStu';

const { check } = Authorized;
const FormItem = Form.Item;
const { Option } = Select;
const IconFont = Icon.createFromIconfontCN({
  scriptUrl,
});

@connect(({ one22one, global, loading }) => ({
  one22one,
  currentOrg: global.currentOrg,
  stuLoading: loading.global,
  loading: loading.effects['one22one/_getAllone2Many'],
  crouseLoading: loading.effects['one22one/_getStuCourse'],
}))
@Form.create()
@Hoc({ fetchUrl: 'one22one/_getAllone2Many' })
class One22OneList extends PureComponent {
  constructor() {
    super();
    this.state = {
      visible: false,
      stuVisible: false,
      record: {},
      FormObj: {},
      formValues: {},
      courseNum: 1,
      couponsList: [],
      selValue: [],
    };
  }

  handleEdit = (record, flag = false) => {
    const { dispatch } = this.props;
    record && (record.flag = flag);
    dispatch(routerRedux.push({
      pathname: '/teaching/one22one/mgr',
      state: record,
    }));
  };

  handleDelete = (record, value) => {
    const { dispatch, currentOrg, pagination } = this.props;
    // value 为 true 时为全部删除
    const { formValues } = this.state;
    if (value) {
      const ids = record.stuList.map(item => item.id);
      dispatch({
        type: 'one22one/delete',
        payload: {
          ids,
          orgId: currentOrg.id,
          pagination,
          query: formValues
        },
      });
    } else {
      dispatch({
        type: 'one22one/delete',
        payload: {
          ids: [record.id],
          orgId: currentOrg.id,
          pagination,
          query: formValues
        },
      });
    }
  };


  handleFormReset = () => {
    const { dispatch, pagination, currentOrg, form  } = this.props;
    if (currentOrg.id) {
      this.setState({
        name: null,
      });
      form.resetFields();
      dispatch({
        type: 'one22one/_getAllone2Many',
        payload: {
          pagination,
          orgId: currentOrg.id,
        },
      });
    }
  };

  expandedRowRender = (data) => {
    const { forItem } = this.props;
    const colums = [
      {
        title: '学生姓名',
        dataIndex: 'stuName',
        align: 'center',
        key: 'stuName',
        render(text) {
          return forItem('icon-mingcheng', `${text}`);
        },
      },
      {
        title: '联系手机',
        dataIndex: 'phone',
        align: 'center',
        key: 'phone',
        render(text) {
          return forItem('icon-shouji', `${text}`);
        },
      },
      {
        title: '性别',
        dataIndex: 'sex',
        align: 'center',
        key: 'last',
        render(text) {
          return forItem('icon-keshi', `${text ? '男' : '女'}`);
        },
      },
      {
        title: '课数',
        key: 'courseNum',
        align: 'center',
        dataIndex: 'courseNum',
        render(text) {
          return forItem('icon-leixing', `${text} 节`);
        },
      },
      {
        title: '总价',
        align: 'center',
        key: 'courseTotalPrice',
        dataIndex: 'courseTotalPrice',
        render(text) {
          return forItem('icon-jiage', `${text} 元`);
        },
      },
      {
        title: '操作',
        width: 125,
        render: (text, record) => (
          <Fragment>
            {check('一对一:修改', (
              <a onClick={() => this.handleAddCourse(text, data)}>添加课时</a>
            ))}
            <br />
            {check('小组授课:删除', (
              <Popcon
                title="是否确认删除"
                onConfirm={() => this.handleDelete(record, false)}
              />
            ))}
          </Fragment>
          ),
      },
    ];
    return (
      <Table
        rowKey={(record) => record.stuId}
        bordered={false}
        columns={colums}
        dataSource={data.stuList}
        pagination={false}
      />
    );
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, currentOrg, form } = this.props;
    const pagination = {
      pageNum: 1,
      pageSize: 10,
    };
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const { teaName, stuName, classroomName } = fieldsValue;
      const query = {};
      if (teaName) {
        query.teaName = teaName.label
      }
      if (stuName) {
        query.stuName = stuName.label.split('-')[0]
      }
      if (classroomName) {
        query.classroomName = classroomName.label.split('-')[0]
      }
      if (!Object.keys(query).length) return;
      this.setState({
        formValues: query,
      });

      dispatch({
        type: 'one22one/_getAllone2Many',
        payload: {
          pagination,
          orgId: currentOrg.id,
          query,
        },
      })
    });
  };

  renderSearchForm() {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    const teaProps = {
      removal: true,
      queryUrl: `${url}/one2many/selectOne2Many`,
      keyValue: 'teaName',
      showSearch: true,
      attr: {
        id: 'teaId',
        name: 'teaName',
      },
      placeholder: '搜索老师',
    };
    const stuProps = {
      removal: true,
      queryUrl: `${url}/stuConsult/queryStu`,
      keyValue: 'name',
      showSearch: true,
      attr: {
        id: 'teaId',
        name: 'stuName',
      },
      placeholder: '搜索学生',
    };
    const classProps = {
      removal: true,
      queryUrl: `${url}/one2many/selectOne2Many`,
      keyValue: 'classroomName',
      showSearch: true,
      attr: {
        id: 'teaId',
        name: 'classroomName',
      },
      placeholder: '搜索教室',
    };
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="教师">
              {getFieldDecorator('teaName')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...teaProps} />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="学生">
              {getFieldDecorator('stuName')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {... stuProps} />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="教室">
              {getFieldDecorator('classroomName')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {... classProps} />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">查询</Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  handleSerChange = ({ key }, id) => {
    this.setState({
      [id]: Number(key),
    });
  };

  handleOk = () => {
    const { dispatch, pagination, currentOrg } = this.props;
    const { FormObj: { record, list }, formValues, courseNum, selValue } = this.state;
    if (!courseNum || courseNum < 0) return;
    list.courseNum = courseNum;
    list.courseCouponIds = selValue;
    record.stuList = [list];
    dispatch({
      type: 'one22one/submitOne22One',
      payload: {
        values: record,
        data: {
          pagination,
          orgId: currentOrg.id,
          query: formValues,
        },
        flag: true,
      },
    }).then(res => {
      if (res) {
        this.setState({
          visible: false,
          FormObj: {},
          courseNum: 1,
          couponsList: [],
          selValue: [],
        });
      }
    });
  };

  handleCancel = () => {
    console.log(1);
    this.setState({
      visible: false,
      FormObj: {},
      courseNum: 1,
      selValue: [],
      couponsList: [],
    });
  };

  handleAddCourse = (text, record) => {
    const { stuId } = text;
    this.props.dispatch({
      type: 'api/getStuCoupon',
      payload: {
        stuId,
      }
    }).then(res => {
      if (res && res.length) {
        this.setState({
          couponsList: res,
        })
      }
    });
    this.setState({
      visible: true,
      FormObj: {
        list: text,
        record,
      },
    });
  };

  handleAddStu = (record) => {
    this.setState({
      stuVisible: true,
      record,
    })
  };

  handleStuOk = () => {
    this.setState({
      stuVisible: false,
    })
  };

  handleStuCancel = () => {
    this.setState({
      stuVisible: false,
      record: {},
    })
  };

  handleCoursenumChange = (value) => {
    if (!value) value = 0;
    this.setState({
      courseNum: parseInt(value, 10)
    })
  };

  handleSelChange = (selValue) => {
    this.setState({
      selValue,
    })
  };


  render() {
    const {
      one22one: { data },
      forItem,
      loading,
      handleStandardTableChange,
      currentOrg,
      dispatch,
      pagination,
      stuLoading,
    } = this.props;
    const { handleEdit } = this;
    const columns = [
      {
        title: '教师姓名',
        key: 'teaName',
        align: 'center',
        dataIndex: 'teaName',
        render(text, record) {
          return forItem('icon-fuzeren', (
            <Tooltip title={`查看${text}课程的详细信息`}>
              <a onClick={() => handleEdit(record, true)}>{text}</a>
            </Tooltip>
          ));
        },
      },
      {
        title: '课程名称',
        key: 'courseName',
        align: 'center',
        dataIndex: 'courseName',
        render(text, record) {
          return (
            <Tooltip title={`所属类型：${record.subjectTypeName}`}>
              { forItem('icon-kechengbiao', `${text} | ${record.grade}`)}
            </Tooltip>
          )
        },
      },
      {
        title: '单价',
        key: 'price',
        align: 'center',
        dataIndex: 'price',
        render(text) {
          return forItem('icon-jiage', `${text} 元`);
        },
      },
      {
        title: '教室名称',
        align: 'center',
        key: 'classroomName',
        dataIndex: 'classroomName',
        render(text) {
          return (
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <IconFont style={{ marginRight: 4 }} type="icon-jiaoshi" />
              <Ellipsis length={6} tooltip>
                {text}
              </Ellipsis>
            </div>
          );
        },
      },
      {
        title: '生效时间',
        key: 'firstCourseTime',
        align: 'center',
        dataIndex: 'firstCourseTime',
        render(text) {
          return forItem('icon-shijian1', text ? parseTime(text) : '');
        },
      },
      {
        title: '以上课时',
        align: 'center',
        key: 'alreadyCourse',
        dataIndex: 'alreadyCourse',
        render(text) {
          return forItem('icon-keshi', !text ? '暂无' : `${text} 节`);
        },
      },
      {
        title: '操作',
        width: 125,
        render: (text, record) => (
          <Fragment>
            {check('小组授课:修改', (
              <Dropdown overlay={(
                <Menu>
                  <Menu.Item>
                    <a onClick={() => this.handleEdit(record)}>修改课程</a>
                  </Menu.Item>
                  <Menu.Item>
                    <a onClick={() => this.handleAddStu(record)}>添加学生</a>
                  </Menu.Item>
                </Menu>
              )}
              >
                <a className="ant-dropdown-link">
                  修改 <Icon type="down" />
                </a>
              </Dropdown>
            ))}
            <Divider type="vertical" />
            {check('小组授课:删除', (
              <Popcon
                title="是否确认删除"
                onConfirm={() => this.handleDelete(record, true)}
              />
            ))}
          </Fragment>
        ),
      },
    ];

    const { visible, stuVisible, record, formValues, courseNum, couponsList, selValue } = this.state;

    return (
      <section>
        <Card className={styles.tableListForm}>
          {this.renderSearchForm()}
        </Card>
        <Card>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              {check('小组授课:增加', (
                <Button icon="plus" type="primary" onClick={() => this.handleEdit(undefined)}>
                  新建
                </Button>
              ))}
            </div>
            <StandardTable
              bordered
              loading={loading}
              data={data}
              scroll={{ x: 1400 }}
              columns={columns}
              onChange={e => handleStandardTableChange(e, formValues)}
              expandedRowRender={records => (this.expandedRowRender(records))}
            />
          </div>
        </Card>
        <Modal
          title="添加课时"
          visible={visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Form.Item label="课时">
            <InputNumber
              autoFocus
              placeholder="输入课时数 / 节"
              style={{ width: '100%' }}
              min={1}
              max={99999}
              onChange={this.handleCoursenumChange}
              value={courseNum}
            />
          </Form.Item>
          <Form.Item label="优惠券">
            <Select
              mode="multiple"
              value={selValue}
              style={{ width: '100%' }}
              placeholder="选择优惠券"
              onChange={this.handleSelChange}
            >
              {
                couponsList.map(item => {
                  const { id, courseCouponType, lessNum, fixedAmountCouponFulfilMoney } = item;
                  return (
                    <Option key={id}>
                      {courseCouponType === 1 ? `满${fixedAmountCouponFulfilMoney}元减${lessNum}元` : `比例优惠 ${lessNum}%`}
                    </Option>
                  )
                })
              }
            </Select>
          </Form.Item>
        </Modal>
        <AddStu
          stuVisible={stuVisible}
          url={url}
          handleStuCancel={this.handleStuCancel}
          record={record}
          currentOrg={currentOrg}
          handleStuOk={this.handleStuOk}
          dispatch={dispatch}
          pagination={pagination}
          name={formValues}
          loading={stuLoading}
        />
      </section>
    );
  }
}
export default One22OneList;
