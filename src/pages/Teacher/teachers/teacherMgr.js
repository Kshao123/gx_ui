import React, { PureComponent, Fragment } from 'react';
import {
  Card,
  Button,
  Form,
  Icon,
  Col,
  Row,
  DatePicker,
  Input,
  Select,
  Popover,
  InputNumber,
  message,
  Upload,
  Divider, Tooltip,
} from 'antd';
import moment from 'moment';
import { connect } from 'dva';
import FooterToolbar from '@/components/FooterToolbar';
import styles from './style.less';
import { parseTime, url } from '@/utils/utils';

const { Option } = Select;
const { TextArea } = Input;
const MsgForm = {
  id: 1,
  name: '姓名',
  sex: '性别',
  age: '年龄',
  teaStatus: '状态',
  courseType: '授课类型',
  birthday: '出生日期',
  entryTime: '入职时间',
  workYears: '工龄',
  phone: '联系手机',
  teaEmail: '邮箱',
  baseSalary: '底薪 / 元',
  extraUp: '额外提成',
  unitPrice: '固定提成 / 元',
  ratioValue: '比例提成 / %',
  teaPicture: '图片',
  teaAbstract: '简介',
  address: '居住地址',
  teaSalaryPlanId: 1,
  lessonUnitPriceOne22one: '每节课提成 / 元'
};

class One2One extends PureComponent {
  constructor(props) {
    super(props);
    const { state } = props.location;
    let planOne2one = {};
    let planOne22one = {};
    let fileList = [];
    let disabled = false;
    if (state) {
      const { teaPicture, disabled: bol, planVos } = state;
      fileList = teaPicture ? [
        {
          name: '相片',
          uid: 1,
          url: teaPicture,
        },
      ] : [];
      disabled = !!bol;
      planVos.forEach((item) => {
        const { courseType, ratioValue, id, unitPrice, lessonUnitPrice } = item;
        switch (courseType) {
          case 1:
            planOne2one = {
              ratioValue,
              id,
              unitPrice,
              courseType,
              lessonUnitPrice
            };
            break;
          case 2:
            planOne22one = {
              ratioValue,
              id,
              unitPrice,
              courseType,
              lessonUnitPrice,
            };
            break;
          default:
            break;
        }
      })
    }
    this.state = {
      width: '100%',
      Required: true,
      fileList,
      // checkList,
      disabled,
      planOne2one,
      planOne22one,
      one22onePrice: 1,
    };
  }

  beforeUpload = (file) => {
    // /1024 即可得到实际大小 file.type 为格式
    const Rxp = /.+\.(jpg|jpeg|png|bmp)$/;
    const { name, size } = file;
    if (!Rxp.test(name)) {
      message.error('请上传正确的图片文件 仅支持 jpg | png | jpeg | bmp ', 3);
      return false;
    } if ((size / 1024) > 6000) {
      message.error('图片文件大于 5M', 3);
      return false;
    }
    return true;
  };

  handleRemove = (file) => {
    const { uid } = file;
    const fileList = this.state.fileList.filter(item => item.uid !== uid);
    this.setState({
      fileList,
    });
    return true;
  };

  handleChange = ({ fileList, file }) => {
    let fileList1 = fileList;
    fileList1 = fileList1.map((item) => {
      if (item.response) {
        item.url = item.response.result;
      }
      return item;
    });
    fileList1 = fileList1.filter((item) => {
      if (item.response) {
        return item.response.status === true;
      }
      return true;
    });
    file.status === 'done' ? (message.success('上传成功', 3)) : '';
    this.setState({ fileList: fileList1 });
  };

  handleSelChange = (e, state) => {
    this.setState({
      [state]: e,
    })
  };

  render() {
    const { form, submitting, currentOrg, location, dispatch } = this.props;
    const { getFieldDecorator, validateFieldsAndScroll, getFieldsError } = form;
    const { state } = location;
    const { Required, disabled, planOne2one, planOne22one, one22onePrice } = this.state;
    const validate = () => {
      validateFieldsAndScroll((error, values) => {
        if (error) {
          return;
        }
        const data = values;
        const {
          birthday,
          entryTime,
          teaPicture,
          unitPriceOfOne2one,
          ratioValueOfOne2one,
          unitPriceOfOne22one,
          ratioValueOfOne22one,
          lessonUnitPriceOne22one,
          lessonUnitPriceOne2one
        } = data;
        let a = 0;
        if (ratioValueOfOne22one > 0) a+=1;
        if (unitPriceOfOne22one > 0) a+=1;
        if (lessonUnitPriceOne22one > 0) a+=1;
        if ((unitPriceOfOne2one > 0 && ratioValueOfOne2one > 0) || (a > 1 && a!== 0)) {
          message.error('提成只能选择一项哦！', 1.4);
          return;
        }
        data.birthday = new Date(birthday.toDate()).getTime();
        data.entryTime = new Date(entryTime.toDate()).getTime();
        if (teaPicture && Object.keys(teaPicture).length) {
          const { fileList } = this.state;
          data.teaPicture = fileList.length ? fileList[0].url: '';
        }else {
          data.teaPicture = '';
        }
        const planVos = [
          {
            id: state ? planOne2one.id : null,
            courseType: 1,
            ratioValue: ratioValueOfOne2one,
            unitPrice: unitPriceOfOne2one,
            lessonUnitPrice: lessonUnitPriceOne2one,
          },
          {
            id: state ? planOne22one.id : null,
            courseType: 2,
            ratioValue: ratioValueOfOne22one,
            unitPrice: unitPriceOfOne22one,
            lessonUnitPrice: lessonUnitPriceOne22one,
          },
        ];
        if (!state) {
          delete data.id;
          planVos.forEach(item => delete item.id)
        }

        delete data.unitPriceOfOne2one;
        delete data.ratioValueOfOne2one;
        delete data.unitPriceOfOne22one;
        delete data.ratioValueOfOne22one;
        delete data.lessonUnitPriceOne22one;
        delete data.lessonUnitPriceOne2one;

        data.planVos = planVos;

        dispatch({
          type: 'teacher/_updateTeaById',
          payload: data,
        });
      });
    };

    const errors = getFieldsError();
    const getErrorInfo = () => {
      const errorCount = Object.keys(errors).filter(key => errors[key]).length;
      if (!errors || errorCount === 0) {
        return null;
      }
      const scrollToField = (fieldKey) => {
        const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
        if (labelNode) {
          labelNode.scrollIntoView(true);
        }
      };
      const errorList = Object.keys(errors).map((key) => {
        if (!errors[key]) {
          return null;
        }
        return (
          <li key={key} className={styles.errorListItem} onClick={() => scrollToField(key)}>
            <Icon type="cross-circle-o" className={styles.errorIcon} />
            <div className={styles.errorMessage}>{errors[key][0]}</div>
            <div className={styles.errorField}>{MsgForm[key]}</div>
          </li>
        );
      });
      return (
        <span className={styles.errorIcon}>
          <Popover
            title="表单校验信息"
            content={errorList}
            overlayClassName={styles.errorPopover}
            trigger="click"
            getPopupContainer={trigger => trigger.parentNode}
          >
            <Icon type="exclamation-circle" />
          </Popover>
          {errorCount}
        </span>
      );
    };
    return (
      <Fragment>
        <Card title={state ? disabled ? '教师信息查看' : '教师信息添加/修改' : '教师信息添加/修改'} className={styles.card} bordered={false}>
          <Form layout="vertical">
            {getFieldDecorator('id', {
                initialValue: state ? state.id : '' })(
                  <Input type="hidden" />
              )}
            {getFieldDecorator('orgId', {
                initialValue: state ? state.orgId : currentOrg.id && currentOrg.id })(
                  <Input type="hidden" />
              )}
            {getFieldDecorator('orgName', {
                initialValue: state ? state.orgName : currentOrg.id && currentOrg.orgName })(
                  <Input type="hidden" />
              )}
            <Row gutter={24}>
              <Col xl={12} lg={12} md={12} sm={24} xs={24}>
                <Form.Item label={MsgForm.name}>
                  {getFieldDecorator('name', {
                        initialValue: state ? state.name : '',
                        rules: [{ required: Required, message: `请输入${MsgForm.name}` }],
                      })(
                        <Input disabled={disabled} placeholder={MsgForm.name} />
                      )}
                </Form.Item>
              </Col>
              <Col xl={12} lg={12} md={12} sm={24} xs={24}>
                <Form.Item label={MsgForm.sex}>
                  {getFieldDecorator('sex', {
                        initialValue: state ? state.sex : '',
                        rules: [{ required: Required, message: `请输入${MsgForm.sex}` }],
                      })(
                        <Select disabled={disabled}>
                          <Option value={true}>男</Option>
                          <Option value={false}>女</Option>
                        </Select>
                      )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 12 }} sm={24}>
                <Form.Item label={MsgForm.phone}>
                  {getFieldDecorator('phone', {
                      initialValue: state ? state.phone : '',
                      rules: [{ pattern: /^((13[0-9])|(16[0-9])|(17[0-9])|(19[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(18[0,5-9]))\d{8}$/, required: Required, message: '格式错误，请重新输入' }],
                    })(
                      <Input disabled={disabled} placeholder="手机" />
                    )}
                </Form.Item>
              </Col>
              <Col xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 12 }} sm={24}>
                <Form.Item label={MsgForm.teaEmail}>
                  {getFieldDecorator('teaEmail', {
                      initialValue: state ? state.teaEmail : '',
                      rules: [{ pattern: /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/, required: false, message: '邮箱格式有误，请重新输入' }],
                    })(
                      <Input disabled={disabled} placeholder="邮箱" />
                    )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 12 }} sm={24}>
                <Form.Item label={MsgForm.birthday}>
                  {getFieldDecorator('birthday', {
                      initialValue: state ? moment(parseTime(state.birthday)) : moment('2018'),
                      rules: [{ required: Required, message: `请输入${MsgForm.birthday}` }],
                    })(
                      <DatePicker disabled={disabled} syle={{ width: '100%' }} />
                    )}
                </Form.Item>
              </Col>
              <Col xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 12 }} sm={24}>
                <Form.Item label={MsgForm.teaPicture}>
                  {getFieldDecorator('teaPicture', {
                      initialValue: state ? this.state.fileList : '',
                      rules: [{ required: false, message: `请上传${MsgForm.teaPicture}` }],
                    })(
                      <Upload
                        action={`${url}/mechanism/saveImage`}
                        listType="picture"
                        name="file1"
                        onChange={this.handleChange}
                        onRemove={this.handleRemove}
                        fileList={this.state.fileList}
                        beforeUpload={this.beforeUpload}
                      >
                        {this.state.fileList.length >= 1 ? null : (
                          <Button disabled={disabled}>
                            <Icon type="upload" />上传相片
                          </Button>
                        )}
                      </Upload>
                    )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 12 }} sm={24}>
                <Form.Item label={MsgForm.teaAbstract}>
                  {getFieldDecorator('teaAbstract', {
                    initialValue: state ? state.teaAbstract : '',
                    rules: [{ required: true, message: `请输入${MsgForm.address}` }],
                  })(
                    <TextArea disabled={disabled} placeholder={MsgForm.teaAbstract} autosize={{ minRows: 2, maxRows: 6 }} />
                  )}
                </Form.Item>
              </Col>
              <Col xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 12 }} sm={24}>
                <Form.Item label={MsgForm.address}>
                  {getFieldDecorator('address', {
                    initialValue: state ? state.address : '',
                    rules: [{ required: false, message: '请输入地址' }],
                  })(
                    <TextArea disabled={disabled} placeholder="地址" autosize={{ minRows: 2, maxRows: 6 }} />
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Card>
        <Card title={`校区管理：${currentOrg && currentOrg.orgName}`} className={styles.card} bordered={false}>
          <Form layout="vertical">
            <Row>
              <Row gutter={24}>
                <Col xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 12 }} sm={24}>
                  <Form.Item label={MsgForm.baseSalary}>
                    {getFieldDecorator('baseSalary', {
                      initialValue: state ? state.baseSalary : '',
                      rules: [{ required: false, message: `请输入${MsgForm.baseSalary}` }],
                    })(
                      <InputNumber disabled={disabled} style={{ width: '100%' }} min={0} max={999999} />
                    )}
                  </Form.Item>
                </Col>
                <Col xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 12 }} sm={24}>
                  <Form.Item label={MsgForm.teaStatus}>
                    {getFieldDecorator('teaStatus', {
                      initialValue: state ? state.teaStatus : '',
                      rules: [{ required: Required, message: `请输入${MsgForm.teaStatus}` }],
                    })(
                      <Select disabled={disabled}>
                        <Option value={true}>在职</Option>
                        <Option value={false}>离职</Option>
                      </Select>
                    )}
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={24}>
                <Col xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 12 }} sm={24}>
                  <Form.Item label={MsgForm.entryTime}>
                    {getFieldDecorator('entryTime', {
                      initialValue: state ? moment(parseTime(state.entryTime)) : moment(new Date()),
                      rules: [{ required: Required, message: `请输入${MsgForm.entryTime}` }],
                    })(
                      <DatePicker disabled={disabled} syle={{ width: '100%' }} />
                    )}
                  </Form.Item>
                </Col>
                <Col xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 12 }} sm={24}>
                  <Form.Item label={MsgForm.workYears}>
                    {getFieldDecorator('workYears', {
                      initialValue: state ? state.workYears : '',
                      rules: [{ required: Required, message: `请输入${MsgForm.workYears}` }],
                    })(
                      <InputNumber disabled={disabled} style={{ width: '100%' }} min={1} max={150} />
                    )}
                  </Form.Item>
                </Col>
              </Row>
            </Row>
          </Form>
        </Card>

        <Card title="提成管理" className={styles.card} bordered={false}>
          <Form layout="vertical">
            <Row gutter={24}>
              <Col xl={{ span: 8 }} lg={{ span: 8 }} md={{ span: 8 }} sm={24}>
                <Form.Item style={{ fontSize: 16, fontWeight: 'bold' }}>
                  <Tooltip title="提成项 二选一">
                    <span>一对一提成设置<Icon type="question-circle" />：</span>
                  </Tooltip>
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={24}>
              <Col xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 12 }} sm={24}>
                <Form.Item label={MsgForm.unitPrice}>
                  {getFieldDecorator('unitPriceOfOne2one', {
                    initialValue: state ? planOne2one.unitPrice : 0,
                    rules: [{ required: false, message: `请输入${MsgForm.unitPrice}` }],
                  })(
                    <InputNumber precision={2} disabled={disabled} style={{ width: '100%' }} min={0} max={999999} />
                  )}
                </Form.Item>
              </Col>
              <Col xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 12 }} sm={24}>
                <Form.Item label={MsgForm.ratioValue}>
                  {getFieldDecorator('ratioValueOfOne2one', {
                initialValue: state ? planOne2one.ratioValue : 0,
                rules: [{ required: false, message: `请输入${MsgForm.ratioValue}` }],
              })(
                <InputNumber disabled={disabled} style={{ width: '100%' }} min={0} max={999999} />
              )}
                </Form.Item>
              </Col>
              {getFieldDecorator('lessonUnitPriceOne2one', {
                initialValue: 0,
              })(
                <Input type="hidden" />
              )}
            </Row>
            <Divider />
            <Row gutter={2}>
              <Col xl={{ span: 5 }} lg={{ span: 5 }} md={{ span: 5 }} sm={24}>
                <Form.Item style={{ fontSize: 16, fontWeight: 'bold' }}>
                  <Tooltip title="提成项 三选一">
                    <span>一对多提成设置 <Icon type="question-circle" />：</span>
                  </Tooltip>
                </Form.Item>
              </Col>
              {/*<Col xl={{ span: 8 }} lg={{ span: 8 }} md={{ span: 8 }} sm={24}>*/}
              {/*  <Select value={one22onePrice} onChange={e => this.handleSelChange(e, 'one22onePrice')}>*/}
              {/*    <Option value={1}>固定提成</Option>*/}
              {/*    <Option value={2}>比例提成</Option>*/}
              {/*    <Option value={3}>每节课提成</Option>*/}
              {/*  </Select>*/}
              {/*</Col>*/}
            </Row>

            <Row gutter={24}>
              <Col style={{ display: one22onePrice === 1 ? 'initial' : 'initial' }} xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 12 }} sm={24}>
                <Form.Item label={MsgForm.unitPrice}>
                  {getFieldDecorator('unitPriceOfOne22one', {
                    initialValue: state ? planOne22one.unitPrice : 0,
                    rules: [{ required: false, message: `请输入${MsgForm.unitPrice}` }],
                  })(
                    <InputNumber precision={2} disabled={disabled} style={{ width: '100%' }} min={0} max={999999} />
                  )}
                </Form.Item>
              </Col>
              <Col style={{ display: one22onePrice === 2 ? 'initial' : 'initial' }} xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 12 }} sm={24}>
                <Form.Item label={MsgForm.ratioValue}>
                  {getFieldDecorator('ratioValueOfOne22one', {
                initialValue: state ? planOne22one.ratioValue : 0,
                rules: [{ required: false, message: `请输入${MsgForm.ratioValue}` }],
              })(
                <InputNumber disabled={disabled} style={{ width: '100%' }} min={0} max={999999} />
              )}
                </Form.Item>
              </Col>
              <Col style={{ display: one22onePrice === 3 ? 'initial' : 'initial' }} xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 12 }} sm={24}>
                <Form.Item label={MsgForm.lessonUnitPriceOne22one}>
                  {getFieldDecorator('lessonUnitPriceOne22one', {
                initialValue: state ? planOne22one.lessonUnitPrice : 0,
                rules: [{ required: false, message: `请输入${MsgForm.lessonUnitPriceOne22one}` }],
              })(
                <InputNumber disabled={disabled} style={{ width: '100%' }} min={0} max={999999} />
              )}
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Card>
        <FooterToolbar style={{ width: this.state.width, display: state ? disabled ? 'none' : 'block' : 'block' }}>
          {getErrorInfo()}
          <Button type="primary" onClick={validate} loading={submitting}>
            提交
          </Button>
        </FooterToolbar>
      </Fragment>
    );
  }
}

export default connect(({ global, loading }) => ({
  courses: global.courses,
  currentOrg: global.currentOrg,
  loading: loading.global,
}))(Form.create()(One2One));
