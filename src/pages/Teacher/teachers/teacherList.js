import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import {
  Card,
  Form,
  Icon,
  Button,
  Divider,
  Badge,
  Tooltip,
  Upload,
  message,
} from 'antd';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import Ellipsis from '@/components/Ellipsis';
import Popcon from '@/components/Popconfirm';
import { scriptUrl, parseTime, url } from '@/utils/utils';
import Hoc from '@/utils/Hoc';
import Authorized from '@/utils/Authorized';
import WxCode from '@/components/WxCode';

const { check } = Authorized;
const status = [
  {
    text: '在职',
    value: 'success',
  }, {
    text: '离职',
    value: 'default',
  },
];
const IconFont = Icon.createFromIconfontCN({
  scriptUrl,
});

@connect(({ teacher, global, loading }) => ({
  teacher,
  currentOrg: global.currentOrg,
  loading: loading.effects['teacher/_getAllTeachers'],
}))
@Form.create()
@Hoc({ fetchUrl: 'teacher/_getAllTeachers' })
class TeacherList extends PureComponent {

  handleEdit = (item, value) => {
    if (value) item.disabled = true;
    this.props.dispatch(routerRedux.push({
      pathname: '/teacher/teach-data/mgr',
      state: item,
    }));
  };

  handleDelete = ({ id }) => {
    const { dispatch, pagination, currentOrg: { id: orgId } } = this.props;
    dispatch({
      type: 'teacher/_deleteTeaById',
      payload: {
        pagination,
        ids: [id],
        orgId,
      },
    });
  };

  getExcel = () => {
    const { currentOrg: { id : orgId }, dispatch } = this.props;
    dispatch({
      type: 'teacher/_getTeaExcel',
      payload: {
        orgId
      },
    })
  };

  handleFormReset = () => {
    const { dispatch, currentOrg, pagination } = this.props;
    if (currentOrg.id) {
      dispatch({
        type: 'teacher/_getAllTeachers',
        payload: {
          pagination,
          orgId: currentOrg.id,
        },
      });
    }
  };

  handleChange = ({fileList, file}) => {
    if (file.status === 'done') {
      message.success('导入成功', 3);
      this.handleFormReset();
    }
  };

  render() {
    const { forItem, currentOrg } = this.props;
    const { handleEdit } = this;
    const columns = [
      {
        title: '姓名',
        dataIndex: 'name',
        align: 'center',
        render: (text, record) => (
          <WxCode data={record} userType={3} />
        ),
      },
      {
        title: '性别',
        dataIndex: 'sex',
        align: 'center',
        render(text) {
          return forItem('icon-xingbie', text ? '男' : '女');
        },
      },
      {
        title: '手机号',
        dataIndex: 'phone',
        render(text) {
          return forItem('icon-shouji', text);
        },
      },
      {
        title: '入职时间',
        dataIndex: 'entryTime',
        align: 'center',
        render(text) {
          return forItem('icon-fuzeren', parseTime(text));
        },
      },
      {
        title: '状态',
        dataIndex: 'teaStatus',
        align: 'center',
        render(text) {
          // teaStatus
          const num = text ? 0 : 1;
          return <Badge status={status[num].value} text={status[num].text} />;
        },
      },
      {
        title: '工龄',
        dataIndex: 'workYears',
        align: 'center',
        render(text) {
          return forItem('icon-shijian1', `${text} 年`);
        },
      },
      {
        title: '地址',
        dataIndex: 'address',
        render(text) {
          return (
            <div className={styles.TableEllipsis}>
              <IconFont style={{ marginRight: 5 }} type="icon-dizhi" />
              <Ellipsis length={8} tooltip>
                {text}
              </Ellipsis>
            </div>

          );
        },
      },
      {
        title: '操作',
        fixed: 'right',
        key: 'change',
        width: 120,
        render: (text, record) => (
          <Fragment>
            {check('教师管理-老师:修改', <a onClick={() => this.handleEdit(record)}>修改</a>)}
            <Divider type="vertical" />
            {check('教师管理-老师:删除', (
              <Popcon
                title="是否确认删除"
                onConfirm={() => this.handleDelete(record)} // 点击确认时
              />
            ))}
          </Fragment>
        ),
      },
    ];
    const { teacher: { data }, loading, handleStandardTableChange } = this.props;
    const uploadprops = {
      // 这里我们只接受excel2007以后版本的文件，accept就是指定文件选择框的文件类型
      accept: 'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      name: 'file',
      // /teacher/downloadTeacherInfo?orgId=42
      action: `${url}/teacher/uploadTeacherInfo`,
      data: { orgId: currentOrg ? currentOrg.id : null },
      // headers: {
      //   authorization: 'authorization-text',
      // },
      showUploadList: false,
      // 把excel的处理放在beforeUpload事件，否则要把文件上传到通过action指定的地址去后台处理
      // 这里我们没有指定action地址，因为没有传到后台
      onChange: this.handleChange
    };
    return (
      <Card>
        <div className={styles.tableList}>
          <div className={styles.tableListOperator}>
            {check('教师管理-老师:增加', (
              <Button icon="plus" type="primary" onClick={() => this.handleEdit(undefined)}>
                新建
              </Button>
            ))}
            {check('教师管理-老师:增加', (
              <Upload {...uploadprops}>
                <Tooltip title='导入excel文件'>
                  <Button type="primary">
                    <Icon type="upload" /> 导入
                  </Button>
                </Tooltip>
              </Upload>
            ))}
            {check('教师管理-老师:增加', (
              <Tooltip title='导出excel文件'>
                <Button onClick={this.getExcel} type="primary">
                  <Icon type="download" /> 导出
                </Button>
              </Tooltip>
            ))}
          </div>
          <StandardTable
            bordered
            loading={loading}
            data={data}
            columns={columns}
            scroll={{ x: 960 }}
            onChange={handleStandardTableChange}
          />
        </div>
      </Card>
    );
  }
}
export default TeacherList;
