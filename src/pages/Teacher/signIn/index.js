import React, { Fragment, PureComponent } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Button,
  Badge,
  Icon,
  Select,
  DatePicker,
} from 'antd';
import moment from 'moment';
import Ellipsis from '@/components/Ellipsis';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import { parseTime, scriptUrl, url } from '@/utils/utils';
import SearchSel from '@/components/SearchSel';
import Hoc from '@/utils/Hoc';
import Authorized from '@/utils/Authorized'

const { check } = Authorized;
const statusMap = {
  1: {
    text: '未打卡',
    value: 'warning',
  },
  2: {
    text: '已打卡',
    value: 'success',
  },
};
const IconFont = Icon.createFromIconfontCN({
  scriptUrl,
});
const FormItem = Form.Item;
const Option = Select.Option;

@connect(({ teacher, global, loading }) => ({
  teacher,
  currentOrg: global.currentOrg,
  loading: loading.effects['teacher/_getTeaSign'],
}))
@Form.create()
@Hoc({ fetchUrl: 'teacher/_getTeaSign' })
class Index extends PureComponent {
  constructor() {
    super();
    this.state = {
      formValues: {},
    };
  }

  handleFormReset = () => {
    const { dispatch, pagination, currentOrg, form  } = this.props;
    if (currentOrg.id) {
      form.resetFields();
      this.setState({
        formValues: {},
      });
      dispatch({
        type: 'teacher/_getTeaSign',
        payload: {
          pagination,
          orgId: currentOrg.id,
        },
      });
    }
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, pagination, currentOrg, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const { name, time, classSignState, timeRangeFlag } = fieldsValue;
      const query = {};
      if (name) {
        query.name = name.label
      }
      if (time) {
        query.time = time.toDate().getTime();
      }
      query.classSignState = classSignState;
      query.timeRangeFlag = timeRangeFlag;
      this.setState({
        formValues: query,
      });

      dispatch({
        type: 'teacher/_getTeaSign',
        payload: {
          pagination,
          orgId: currentOrg.id,
          query,
        },
      })
    });
  };

  handleTeaSign = (record) => {
    const { dispatch, pagination, currentOrg } = this.props;
    const { formValues: query } = this.state;
    const { studentCourseId } = record;

    dispatch({
      type: 'teacher/_TeaSign',
      payload: {
        data: {
          pagination,
          orgId: currentOrg.id,
          query,
        },
        studentCourseIdList: [ studentCourseId ]
      },
    })
  };

  renderSearchForm() {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    const teaProps = {
      removal: true,
      queryUrl: `${url}/teacher/displayTeacherClassSignInfo`,
      keyValue: 'name',
      showSearch: true,
      attr: {
        id: 'teaId',
        name: 'teaName',
      },
      placeholder: '搜索老师',
    };
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={6} sm={24}>
            <FormItem label="教师">
              {getFieldDecorator('name')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...teaProps} />
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="时间">
              {getFieldDecorator('time',{
                initialValue: moment(new Date()),
              })(
                <DatePicker />
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="时间范围">
              {getFieldDecorator('timeRangeFlag',{
                initialValue: 0,
              })(
                <Select>
                  <Option value={0}>当天</Option>
                  <Option value={1}>本周</Option>
                  <Option value={2}>本月</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="打卡状态">
              {getFieldDecorator('classSignState',{
                initialValue: 0,
              })(
                <Select>
                  <Option value={0}>全选</Option>
                  <Option value={1}>未打卡</Option>
                  <Option value={2}>已打卡</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">查询</Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }


  render() {
    const { forItem } = this.props;
    const columns = [
      {
        title: '老师姓名',
        dataIndex: 'teaName',
        key: 'teaName',
        align: 'center',
        render(text) {
          return forItem('icon-mingcheng', text);
        },
      },
      {
        title: '学生姓名',
        dataIndex: 'stuName',
        key: 'stuName',
        align: 'center',
        render(text) {
          return forItem('icon-mingcheng', text);
        },
      },
      {
        title: '课程名称',
        dataIndex: 'courseName',
        key: 'courseName',
        align: 'center',
        render(text) {
          return (
            <div className={styles.TableEllipsis}>
              <IconFont style={{ marginRight: 5 }} type="icon-kemu" />
              <Ellipsis length={7} tooltip>
                {text}
              </Ellipsis>
            </div>
          );
        },
      },
      {
        title: '班级名称',
        dataIndex: 'classroomName',
        key: 'classroomName',
        align: 'center',
        render(text) {
          return (
            <div className={styles.TableEllipsis}>
              <IconFont style={{ marginRight: 5 }} type="icon-jiaoshi" />
              <Ellipsis length={7} tooltip>
                {text}
              </Ellipsis>
            </div>
          );
        },
      },
      {
        title: '打卡',
        align: 'center',
        children: [
          {
            title: '状态',
            dataIndex: 'classSignState',
            key: 'classSignState',
            align: 'center',
            render(text) {
              return (
                <Badge
                  status={statusMap[text].value}
                  text={statusMap[text].text}
                />
              )
            },
          },
          {
            title: '上课时间',
            dataIndex: 'startTime',
            key: 'startTime',
            align: 'center',
            render(text) {
              return forItem('icon-daqia', parseTime(text, '{m}-{d} {h}:{i}'));
            },
          },
          {
            title: '下课时间',
            dataIndex: 'endTime',
            key: 'endTime',
            align: 'center',
            render(text) {
              return forItem('icon-daqia', parseTime(text, '{h}:{i}'));
            },
          },
        ],
      },
      {
        title: '操作',
        width: 80,
        fixed: 'right',
        render: (text, record) => (
          <Fragment>
            {check('教师管理-老师补充打卡', (
              <a onClick={() => this.handleTeaSign(record)}>打卡</a>
            ))}
          </Fragment>
        ),
      },
    ];
    const { teacher: { SignList: data }, loading, handleStandardTableChange } = this.props;
    const { formValues } = this.state;
    return (
      <section>
        <Card className={styles.tableListForm}>
          {this.renderSearchForm()}
        </Card>
        <Card>
          <div className={styles.tableList}>
            <StandardTable
              bordered
              loading={loading}
              data={data}
              columns={columns}
              scroll={{ x: 700 }}
              onChange={e => handleStandardTableChange(e, formValues)}
            />
          </div>
        </Card>
      </section>
    );
  }
}
export default Index
