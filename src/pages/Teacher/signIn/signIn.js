import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import isEqual from 'lodash/isEqual';
import {
  Row,
  Col,
  Card,
  Form,
  Button,
  Badge,
  Icon,
} from 'antd';
import Ellipsis from '../../../components/Ellipsis';
import StandardTable from '../../../components/DelTable';
import styles from '../../../utils/global.less';
import { parseTime, scriptUrl } from '../../../utils/utils';
import Search from '../../../components/SearchSel';

const statusMap = {
  true: 'success',
  false: 'error',
};
const IconFont = Icon.createFromIconfontCN({
  scriptUrl,
});
@connect(({ teacher, global, loading }) => ({
  teacher,
  currentOrg: global.currentOrg,
  loading: loading.effects['teacher/_getTeaLessonClock'],
}))
@Form.create()
export default class SignIn extends PureComponent {
  constructor() {
    super();
    this.state = {
      selectedRows: [],
      props: {
        showSearch: true,
        searchData: this.handleValueChange,
        queryUrl: '/teacher/selectTeaLessonClock',
        keyValue: 'name',
        placeholder: '搜索老师',
        width: '185px',
        attr: {
          id: 'id',
          name: 'teaName',
        },
      },
      pagination: {
        pageSize: 10,
        pageNum: 1,
      },
      currentOrg: {
        id: '',
      },
    };
  }

  componentDidMount() {
    const { dispatch, currentOrg } = this.props;
    if (!currentOrg) return;
    const { pagination } = this.state;
    dispatch({
      type: 'teacher/_getTeaLessonClock',
      payload: {
        pagination,
        orgId: currentOrg.id,
      },
    });
  }

  static getDerivedStateFromProps(nextProps, preState) {
    const { dispatch } = nextProps;
    if (!nextProps.currentOrg) return null;
    if (isEqual(nextProps.currentOrg.id, preState.currentOrg.id)) {
      return null;
    }
    const { pagination } = preState;
    dispatch({
      type: 'teacher/_getTeaLessonClock',
      payload: {
        pagination,
        orgId: nextProps.currentOrg.id,
      },
    });
    return {
      currentOrg: nextProps.currentOrg,
    };
  }

  handleStandardTableChange = (pagination) => {
    const { dispatch } = this.props;
    const { pageSize, current: pageNum } = pagination;
    this.setState({
      pagination: {
        pageNum,
        pageSize,
      },
    });
    dispatch({
      type: 'teacher/_getTeaLessonClock',
      payload: {
        pagination: {
          pageNum,
          pageSize,
        },
        orgId: this.state.currentOrg.id,
      },
    });
  };

  handleSelectRows = (rows) => {
    this.setState({
      selectedRows: rows,
    });
  };


  handleFormReset = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'teacher/_getTeaLessonClock',
      payload: {
        pageNum: 1,
        pageSize: 10,
      },
    });
  };

  forItem = (type, text) => {
    return (
      <div>
        <IconFont type={type} />
        <span> {text}</span>
      </div>
    );
  };

  handleValueChange = (res) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'teacher/_upDataSignList',
      payload: res,
    });
  };

  renderSearchForm() { // do 查询表单
    return (
      <Row gutter={{ md: 4, lg: 4, xl: 4 }}>
        <Col md={8} sm={8} style={{ marginBottom: 10 }}>
          <span>人员查询：</span>
          <Search {...this.state.props} />
        </Col>
        <Col md={4} sm={4}>
          <Button
            style={{ marginLeft: 8, verticalAlign: top }}
            onClick={this.handleFormReset}
          >
            重置
          </Button>
        </Col>
      </Row>
    );
  }

  render() {
    const { forItem } = this;
    const columns = [
      {
        title: '姓名',
        dataIndex: 'teaName',
        key: 'teaName',
        align: 'center',
        render(text) {
          return forItem('icon-mingcheng', text);
        },
      },
      {
        title: '课程名称',
        dataIndex: 'courseName',
        key: 'courseName',
        align: 'center',
        render(text) {
          return (
            <div>
              <IconFont style={{ marginRight: 5 }} type="icon-kemu" />
              <Ellipsis length={7} tooltip >
                {text}
              </Ellipsis>
            </div>
          );
        },
      },
      {
        title: '班级名称',
        dataIndex: 'classroomName',
        key: 'classroomName',
        align: 'center',
        render(text) {
          return (
            <div>
              <IconFont style={{ marginRight: 5 }} type="icon-jiaoshi" />
              <Ellipsis length={7} tooltip >
                {text}
              </Ellipsis>
            </div>
          );
        },
      },
      {
        title: '打卡',
        align: 'center',
        children: [
          {
            title: '上课时间',
            dataIndex: 'lessonStarttimeClock',
            key: 'lessonStarttimeClock',
            align: 'center',
            render(text) {
              return forItem('icon-daqia', parseTime(text));
            },
          },
          {
            title: '上课打卡',
            dataIndex: 'lessonTruestartClock',
            key: 'lessonTruestartClock',
            align: 'center',
            render(text, record) {
              return (
                <div>
                  <Badge
                    status={statusMap[text <= record.lessonStarttimeClock]}
                    text={parseTime(text)}
                  />
                </div>
              );
            },
          },
          {
            title: '下课时间',
            dataIndex: 'lessonEndtimeClock',
            key: 'lessonEndtimeClock',
            align: 'center',
            render(text) {
              return forItem('icon-daqia', parseTime(text));
            },
          },
          {
            title: '下课打卡',
            dataIndex: 'lessonTrueendClock',
            key: 'lessonTrueendClock',
            align: 'center',
            render(text, record) {
              return (
                <div>
                  <Badge
                    status={statusMap[text >= record.lessonTrueendClock]}
                    text={parseTime(text)}
                  />
                </div>
              );
            },
          },
        ],
      },
    ];
    const { teacher: { SignList: data }, loading } = this.props;
    const { selectedRows } = this.state;
    return (
      <Card>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>
            {this.renderSearchForm()}
          </div>
          <div className={styles.tableListOperator} />
          <StandardTable
            showAllSel
            HideAnother
            bordered
            selectedRows={selectedRows}
            loading={loading}
            data={data}
            columns={columns}
            scroll={{ x: 1100 }}
            onSelectRow={this.handleSelectRows}
            onChange={this.handleStandardTableChange}
          />
        </div>
      </Card>
    );
  }
}
