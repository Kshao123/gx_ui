import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Card,
  Row,
  Col,
  Form,
  Button,
  DatePicker,
  Select,
} from 'antd';
import moment from 'moment';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import Hoc from '@/utils/Hoc';
import { url } from '@/utils/utils';
import SearchSel from '@/components/SearchSel';
import { parseTime } from '../../../utils/utils';

const Option = Select.Option;
const FormItem = Form.Item;
const { MonthPicker } = DatePicker;
const TYPE = {
  1: '一对一',
  2: '一对多',
};

@connect(({ teacher, global, loading }) => ({
  teacher,
  currentOrg: global.currentOrg,
  loading: loading.effects['teacher/_getSalary'],
}))
@Form.create()
@Hoc({ fetchUrl: 'teacher/_getSalary' })

class SalaryList extends PureComponent {
  constructor() {
    super();
    this.state = {
      formValues: {},
    };
  }

  handleFormReset = () => {
    const { dispatch, pagination, currentOrg, form  } = this.props;
    if (currentOrg.id) {
      form.resetFields();
      this.setState({
        formValues: {},
      });
      dispatch({
        type: 'teacher/_getSalary',
        payload: {
          pagination,
          orgId: currentOrg.id,
        },
      });
    }
  };

  handleSearch = (e) => {
    e.preventDefault();
    const { dispatch, pagination, currentOrg, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const { name, time, royaltyType, courseType } = fieldsValue;
      const query = {};
      if (name) {
        query.name = name.label.split('-')[0]
      }
      if (royaltyType >= 0) {
        query.royaltyType = royaltyType;
      }
      if (courseType >= 0) {
        query.courseType = courseType;
      }
      if (time) {
        query.time = time.toDate().getTime();
      }
      this.setState({
        formValues: query,
      });
      dispatch({
        type: 'teacher/_getSalary',
        payload: {
          pagination,
          orgId: currentOrg.id,
          query,
        },
      })
    });
  };

  disabledDate = (current) => {
    // Can not select days before today and today
    return current && current > moment().endOf('day');
  };

  footer = (data) => {
    if (!data.length) return ''
    return `总提成：${data[0].selectAllSalary} 元`
  };

  renderSearchForm() {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    const teaProps = {
      removal: true,
      queryUrl: `${url}/teacher/selectTeaByCondition`,
      keyValue: 'name',
      showSearch: true,
      attr: {
        id: 'teaId',
        name: 'name',
      },
      placeholder: '老师姓名',
    };

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="老师">
              {getFieldDecorator('name')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...teaProps} />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="课程类型">
              {getFieldDecorator('courseType', {
                initialValue: 0,
              })(
                <Select>
                  <Option value={0}>全选</Option>
                  <Option value={1}>一对一</Option>
                  <Option value={2}>一对多</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="提成类型">
              {getFieldDecorator('royaltyType', {
                initialValue: 0,
              })(
                <Select>
                  <Option value={0}>全选</Option>
                  <Option value={1}>固定提成</Option>
                  <Option value={2}>比例提成</Option>
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col md={16} sm={24}>
            <FormItem label="时间">
              {getFieldDecorator('time',{
                initialValue: moment(),
              })(
                <MonthPicker placeholder="选择月份" disabledDate={this.disabledDate} />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">查询</Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const { forItem } = this.props;
    const { formValues } = this.state;
    const columns = [
      {
        title: '老师姓名',
        dataIndex: 'teaName',
        align: 'center',
        render(text) {
          return forItem('icon-xingming', text);
        },
      },
      {
        title: '学生姓名',
        dataIndex: 'stuName',
        align: 'center',
        render(text) {
          return forItem('icon-xingming', text);
        },
      },
      {
        title: '课程名称',
        dataIndex: 'courseName',
        align: 'center',
        render(text, record) {
          return forItem('icon-mingcheng', `${record.courseName}|${record.gradeName}` );
        },
      },
      {
        title: '课程类型',
        dataIndex: 'courseType',
        align: 'center',
        render(text, record) {
          return forItem('icon-leixing', `${TYPE[record.courseType]}` );
        },
      },
      {
        title: '教室名称',
        dataIndex: 'classroomName',
        align: 'center',
        render(text) {
          return forItem('icon-mingcheng', text );
        },
      },
      {
        title: '时间',
        dataIndex: 'endTime',
        align: 'center',
        render(text, record) {
          return forItem('icon-shijian', text ? parseTime(text, '{d}日 {h}:{i}') : '' );
        },
      },
      {
        title: '本节课工资 / 元',
        dataIndex: 'salary',
        align: 'center',
        render(text) {
          return forItem('icon-tichengshezhi', text === null ? '暂无' : `${text} 元`);
        }
      },
    ];

    const { teacher: { salaryList: data }, loading, handleStandardTableChange } = this.props;
    return (
      <section>
        <Card className={styles.tableListForm}>
          {this.renderSearchForm()}
        </Card>
        <Card>
          <div className={styles.tableList}>
            <StandardTable
              bordered
              loading={loading}
              data={data}
              columns={columns}
              onChange={e => handleStandardTableChange(e, formValues)}
              footer={this.footer}
              scroll={{ x: 900 }}
            />
          </div>
        </Card>
      </section>
    );
  }
}
export default SalaryList;
