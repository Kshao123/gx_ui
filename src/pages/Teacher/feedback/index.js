/* eslint-disable linebreak-style */
import React, { PureComponent } from 'react';
import { Route, Redirect, Switch } from 'dva/router';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import { Card } from 'antd';
import NotFound from '../../Exception/404';
import { getRoutes } from '../../../utils/utils';

export default class ScheduleIndex extends PureComponent {
  render() {
    const { match, routerData } = this.props;
    return (
      <PageHeaderLayout>
        <Switch>
          {
                        getRoutes(match.path, routerData).map(item => (
                          <Route
                            key={item.key}
                            path={item.path}
                            component={item.component}
                            exact={item.exact}
                          />
                        ))
                    }
          <Redirect exact from="/teacher/feedback" to="/teacher/feedback/List" />
          <Route render={NotFound} />
        </Switch>
      </PageHeaderLayout>
    );
  }
}
