import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Card,
} from 'antd';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import Ellipsis from '@/components/Ellipsis';
import Popcon from '@/components/Popconfirm';
import Hoc from '@/utils/Hoc';
import Authorized from '@/utils/Authorized';

const { check } = Authorized;
const feedbackType = [
  '',
  {
    text: '不满意',
    value: 'icon-bumanyi',
  },
  {
    text: '不错',
    value: 'icon-manyi',
  },
  {
    text: '很满意',
    value: 'icon-feichangmanyi',
  },
];

@connect(({ teacher, global, loading }) => ({
  teacher,
  currentOrg: global.currentOrg,
  loading: loading.global,
}))
@Hoc({ fetchUrl: 'teacher/_getFeedbackList' })
class feedbackList extends PureComponent {
  constructor() {
    super();
    this.state = {
    };
  }

  handleDelete = ({ id }) => {
    const { dispatch, pagination, currentOrg: { id: orgId } } = this.props;
    dispatch({
      type: 'teacher/_delFeedbackList',
      payload: {
        id,
        pagination,
        orgId,
      },
    });
  };

    render() {
      const { forItem } = this.props;
      const columns = [
        {
          title: '学生姓名',
          dataIndex: 'stuName',
          align: 'center',
          render(text) {
            return forItem('icon-xingming', text);
          },
        },
        {
          title: '老师姓名',
          dataIndex: 'teaName',
          align: 'center',
          render(text) {
            return forItem('icon-xingming', text);
          },
        },
        {
          title: '课程名称',
          dataIndex: 'courseName',
          align: 'center',
          render(text) {
            return forItem('icon-mingcheng', text);
          },
        },
        {
          title: '评价',
          dataIndex: 'feedbackType',
          align: 'center',
          render(text) {
            return forItem(feedbackType[text].value, feedbackType[text].text);
          },
        },
        {
          title: '授课类型',
          dataIndex: 'teachingType',
          align: 'center',
          render(text) {
            return forItem('icon-wode', text === 1 ? '一对一授课' : '一对多授课');
          },
        },
        {
          title: '反馈信息',
          dataIndex: 'feedbackInfo',
          align: 'center',
          render(text) {
            return (
              <div>
                <Ellipsis length={8} tooltip>
                  {text}
                </Ellipsis>
              </div>

            );
          },
        },
        {
          title: '操作',
          render: (text, record) => (
            <Fragment>
              {check('教师管理-上课反馈记录:删除', (
                <Popcon
                  title="是否确认删除"
                  onConfirm={() => this.handleDelete(record)}
                />
              ))}
            </Fragment>
          ),
        },
      ];

      const { teacher: { feedback: data }, loading, handleStandardTableChange } = this.props;
      return (
        <Card>
          <div className={styles.tableList}>
            <StandardTable
              bordered
              loading={loading}
              data={data}
              columns={columns}
              onChange={handleStandardTableChange}
            />
          </div>
        </Card>
      );
    }
}
export default feedbackList;
