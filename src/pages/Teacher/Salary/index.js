import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Card,
  Row,
  Col,
  Form,
  Button,
  Icon,
} from 'antd';
import isEqual from 'lodash/isEqual';
import monent from 'moment';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import styles1 from './Analysis.less';

import { url, getTimeDistance } from '@/utils/utils';
import SearchSel from '@/components/SearchSel';
import SalesCard from './SalesCard'
import { scriptUrl } from '../../../utils/utils';

// const SalesCard = React.lazy(() => import('./SalesCard'));
const FormItem = Form.Item;
const IconFont = Icon.createFromIconfontCN({
  scriptUrl,
});

@connect(({ teacher, global, loading }) => ({
  teacher,
  currentOrg: global.currentOrg,
  loading: loading.effects['teacher/_getAllSalary'],
}))
@Form.create()
class index extends PureComponent {
  constructor() {
    super();
    this.state = {
      name: '',
      props: {
        removal: true,
        onChange: this.handleValueChange,
        queryUrl: `${url}/teacher/selectTeacherInfoInMonth`,
        keyValue: 'name',
        showSearch: true,
        attr: {
          id: 'name',
          name: 'name',
        },
        placeholder: '查询老师',
      },
      rangePickerValue: monent(new Date()),
      currentOrg: {
        id: '',
      },
      pagination: {
        pageSize: 10,
        pageNum: 1,
      },
    };
  }

  componentDidMount() {
    const { dispatch, currentOrg } = this.props;
    if (!currentOrg.id) return;
    const { rangePickerValue, pagination } = this.state;
    dispatch({
      type: 'teacher/_getAllSalary',
      payload: {
        orgId: currentOrg.id,
        ... pagination,
        time: rangePickerValue.toDate().getTime(),
      },
    })
  }

  static getDerivedStateFromProps(nextProps, preState) {
    const { dispatch } = nextProps;
    if (!nextProps.currentOrg.id) return null;
    if (isEqual(nextProps.currentOrg.id, preState.currentOrg.id)) {
      return null;
    }
    const { pagination, rangePickerValue } = preState;
    const data = {
      orgId: nextProps.currentOrg.id,
      ...pagination,
      time: rangePickerValue.toDate().getTime()
    };
    dispatch({
      type: 'teacher/_getAllSalary',
      payload: data
    });
    return {
      currentOrg: nextProps.currentOrg,
    };
  }

  handleValueChange = (value, name, data) => {
    const { list } = data;
    const { label } = value;
    if (!list.length) return;
    if (list.length >= 2) {
      data.list = list.filter(item => item.name === label);
    }
    this.setState({
      [name]: label,
    });
    const { dispatch } = this.props;
    dispatch({
      type: 'teacher/_searchData',
      payload: {
        type: 'saveAllSalaryList',
        value: data
      },
    });
  };

  handleFormReset = () => {
    const { dispatch, form } = this.props;
    const { currentOrg, pagination, rangePickerValue } = this.state;
    if (!currentOrg.id) return;
    this.setState({
      name: null,
      rangePickerValue: monent(new Date())
    });
    form.resetFields();
    dispatch({
      type: 'teacher/_getAllSalary',
      payload: {
        ... pagination,
        orgId: currentOrg.id,
        time: rangePickerValue.toDate().getTime(),
      },
    });
  };

  isActive = type => {
    const { rangePickerValue } = this.state;
    const value = getTimeDistance(type);
    if (
      rangePickerValue.isSame(value[0], 'month')
    ) {
      return styles1.currentDate;
    }
    return '';
  };

  handleRangePickerChange = rangePickerValue => {
    const { dispatch } = this.props;

    const { currentOrg, pagination, name } = this.state;
    const data = {
      orgId: currentOrg.id || null,
      ... pagination,
      time: rangePickerValue.toDate().getTime(),
    };
    if (name && name.length) data.name = name;
    this.setState({
      rangePickerValue,
    });
    dispatch({
      type: 'teacher/_getAllSalary',
      payload: data,
    });
  };

  selectDate = type => {
    const { dispatch } = this.props;
    const rangePickerValue = monent(getTimeDistance(type)[0]);
    const { currentOrg, pagination, name } = this.state;
    const data = {
      orgId: currentOrg.id || null,
      ... pagination,
      time: rangePickerValue.toDate().getTime(),
    };
    if (name && name.length) data.name = name;
    this.setState({
      rangePickerValue,
    });
    dispatch({
      type: 'teacher/_getAllSalary',
      payload: data,
    });
  };

  handleStandardTableChange = (pagination) => {
    const { dispatch } = this.props;
    const { pageSize, current: pageNum } = pagination;
    const { currentOrg: { id: orgId }, rangePickerValue, name } = this.state;
    this.setState({
      pagination: {
        pageNum,
        pageSize,
      },
    });
    const data = {
      pageNum,
      pageSize,
      orgId,
      time: rangePickerValue.toDate().getTime(),
    };
    if (name && name.length) data.name = name;
    dispatch({
      type: 'teacher/_getAllSalary',
      payload: data,
    });
  };

  renderSearchForm() {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    const { reset } = this.state;
    return (
      <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="老师">
              {getFieldDecorator('teaName')(
                <SearchSel ref={e => {this.sel = e}} reset={reset} orgId={currentOrg ? currentOrg.id : ''} {...this.state.props} />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>重置</Button>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
   const forItem = (type, text) => (
     <div>
       <IconFont type={type} />
       <span>  {text}</span>
     </div>
    );
    const columns = [
      {
        title: '老师姓名',
        dataIndex: 'name',
        align: 'center',
        render(text) {
          return forItem('icon-xingming', text);
        },
      },
      {
        title: '底薪金额 / 元',
        dataIndex: 'baseSalary',
        align: 'center',
        render(text) {
          return forItem('icon-tichengshezhi', text === null ? '暂无' : `${text} 元`);
        }
      },
      {
        title: '提成',
        align: 'center',
        children: [
          {
            title: '比例提成',
            align: 'center',
            dataIndex: 'ratioCommission',
            render(text) {
              return forItem('icon-tichengshezhi', text === null ? '暂无' : `${text} 元`);
            },
          },
          {
            title: '固定提成/元',
            align: 'center',
            dataIndex: 'fixedCommission',
            render(text) {
              return forItem('icon-tichengshezhi', text === null ? '暂无' : `${text} 元`);
            }
          },
          // {
          //   title: '每节课提成/元',
          //   align: 'center',
          //   dataIndex: 'fixedCommission',
          //   render(text) {
          //     return forItem('icon-tichengshezhi', text === null ? '暂无' : `${text} 元`);
          //   }
          // },
          {
            title: '总提成/元',
            align: 'center',
            dataIndex: 'allCommission',
            render(text) {
              return forItem('icon-tichengshezhi', text === null ? '暂无' : `${text} 元`);
            }
          },
        ],
      },
      {
        title: '总工资 / 元',
        dataIndex: 'allSalary',
        align: 'center',
        render(text) {
          return forItem('icon-tichengshezhi', text === null ? '暂无' : `${text} 元`);
        }
      },
    ];

    const { teacher: { AllSalaryList: data }, loading } = this.props;
    const { rangePickerValue, salesData } = this.state;
    return (
      <section>
        <Card className={styles.tableListForm}>
          {this.renderSearchForm()}
        </Card>
        <Card>
          <SalesCard
            rangePickerValue={rangePickerValue}
            salesData={salesData}
            isActive={this.isActive}
            handleRangePickerChange={this.handleRangePickerChange}
            loading={loading}
            selectDate={this.selectDate}
          />
          <div className={styles.tableList}>
            <StandardTable
              bordered
              loading={loading}
              data={data}
              columns={columns}
              onChange={this.handleStandardTableChange}
              scroll={{ x: 650 }}
            />
          </div>
        </Card>
      </section>
    );
  }
}
export default index;
