import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Card,
  Row,
  Col,
  Form,
  Button,
} from 'antd';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import Hoc from '@/utils/Hoc';
import { url } from '@/utils/utils';
import SearchSel from '@/components/SearchSel';

const FormItem = Form.Item;

@connect(({ teacher, global, loading }) => ({
  teacher,
  currentOrg: global.currentOrg,
  loading: loading.effects['teacher/_getSalary'],
}))
@Form.create()
@Hoc({ fetchUrl: 'teacher/_getSalary' })
class salaryList extends PureComponent {
  constructor() {
    super();
    this.state = {
      props: {
        removal: true,
        onChange: this.handleValueChange,
        queryUrl: `${url}/teacher/selectTeaSalaryInfo`,
        keyValue: 'name',
        showSearch: true,
        attr: {
          id: 'teaId',
          name: 'teaName',
        },
        placeholder: '查询老师',
      },
    };
  }

  handleValueChange = (value, id, data) => {
    const { list } = data;
    const { label } = value;
    if (!list.length) return;
    if (list.length >= 2) {
      data.list = list.filter(item => item.teaName === label);
    }
    const { dispatch } = this.props;
    dispatch({
      type: 'teacher/_searchData',
      payload: {
        type: 'saveSalaryList',
        value: data
      },
    });
  };

  handleFormReset = () => {
    const { dispatch, currentOrg, pagination } = this.props;
    if (!currentOrg.id) return;
    dispatch({
      type: 'teacher/_getSalary',
      payload: {
        pagination,
        orgId: currentOrg.id,
      },
    });
  };

  renderSearchForm() {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    return (
      <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="老师">
              {getFieldDecorator('stuId')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...this.state.props} />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>重置</Button>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const { forItem } = this.props;
    const columns = [
      {
        title: '老师姓名',
        dataIndex: 'teaName',
        align: 'center',
        render(text) {
          return forItem('icon-xingming', text);
        },
      },
      {
        title: '授课类型',
        dataIndex: 'courseType',
        align: 'center',
        render(text) {
          return forItem('icon-wode', text === 1 ? '一对一授课' : '一对多授课');
        },
      },
      {
        title: '底薪金额 / 元',
        dataIndex: 'baseSalary',
        align: 'center',
        render(text) {
          return forItem('icon-tichengshezhi', text === null ? '暂无' : `${text} 元`);
        }
      },
      {
        title: '提成',
        align: 'center',
        children: [
          {
            title: '比例提成',
            align: 'center',
            dataIndex: 'ratioValue',
            render(text) {
              return forItem('icon-tichengshezhi', text === null ? '暂无' : `${text} %`);
            },
          },
          {
            title: '固定提成/元',
            align: 'center',
            dataIndex: 'unitPrice',
            render(text) {
              return forItem('icon-tichengshezhi', text === null ? '暂无' : `${text} 元`);
            }
          },
        ],
      },
    ];

    const { teacher: { salaryList: data }, loading, handleStandardTableChange } = this.props;
    return (
      <section>
        <Card className={styles.tableListForm}>
          {this.renderSearchForm()}
        </Card>
        <Card>
          <div className={styles.tableList}>
            <StandardTable
              showAllSel
              HideAnother
              bordered
              loading={loading}
              data={data}
              ShowSearch={false}
              columns={columns}
              onChange={handleStandardTableChange}
            />
          </div>
        </Card>
      </section>
    );
  }
}
export default salaryList;
