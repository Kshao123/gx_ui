import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Form,
  Button,
  Badge,
  Popconfirm,
  Row,
  Col,
  Card,
} from 'antd';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import { parseTime, url } from '@/utils/utils';
import SearchSel from '@/components/SearchSel';
import Hoc from '@/utils/Hoc';
import Authorized from '@/utils/Authorized';

const { check } = Authorized;
const FormItem = Form.Item;

const status = [
  {
    text: '未审批',
    value: 'warning',
  },
  {
    text: '通过',
    value: 'success',
  },
  {
    text: '未通过',
    value: 'error',
  },
];

const people = ['', '学生', '老师', '员工'];

@connect(({ schoolControl, global, loading }) => ({
  schoolControl,
  loading: loading.global,
  currentOrg: global.currentOrg,
}))
@Form.create()
@Hoc({ fetchUrl: 'schoolControl/_getLeave' })
class LeaveList extends PureComponent {
  constructor() {
    super();
    this.state = {
      props: {
        removal: true,
        onChange: this.handleValueChange,
        queryUrl: `${url}/leave/selectLeaveRecord`,
        keyValue: 'name',
        showSearch: true,
        attr: {
          id: 'userId',
          name: 'name',
        },
        placeholder: '搜索员工',
      },
      props1: {
        removal: true,
        onChange: this.handleValueChange,
        queryUrl: `${url}/leave/selectLeaveRecord`,
        keyValue: 'teaName',
        showSearch: true,
        attr: {
          id: 'teaId',
          name: 'name',
        },
        placeholder: '搜索老师',
      },
    };
  }

  handleConfirm = (record, leaveStatus) => { // do 点击确认的回调
    const { id } = record;
    const { pagination, currentOrg, dispatch } = this.props;
    dispatch({
      type: 'schoolControl/_leaveApproval',
      payload: {
        data: {
          leaveStatus,
          id,
        },
        pagination,
        orgId: currentOrg.id,
      },
    });
  };

  ConfirmTime = (record) => {
    const { leaveStartTime } = record;
    const str = parseTime(leaveStartTime);
    const time = parseTime(leaveStartTime).substr(0, str.length - 3);
    return `${time}-${parseTime(leaveStartTime).split(' ')[1].substr(0, 5)}`;
  };

  handleValueChange = (value, id, data) => {
    const { list } = data;
    const { label } = value;
    if (!list.length) return;
    if (list.length >= 2) {
      data.list = list.filter(item => item.name === label);
    }
    const { dispatch } = this.props;
    dispatch({
      type: 'schoolControl/_searchData',
      payload: {
        payload: data,
        value: 'saveLeave',
      },
    });
  };

  handleFormReset = () => {
    const { dispatch, currentOrg, pagination } = this.props;
    if (currentOrg.id) {
      dispatch({
        type: 'schoolControl/_getLeave',
        payload: {
          pagination,
          orgId: currentOrg.id,
        },
      });
    }
  };

  renderSearchForm() {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="员工">
              {getFieldDecorator('stuId')(
                <SearchSel leaveIdean={3} orgId={currentOrg ? currentOrg.id : ''} {...this.state.props} />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="老师">
              {getFieldDecorator('teaId')(
                <SearchSel leaveIdean={2} orgId={currentOrg ? currentOrg.id : ''} {...this.state.props1} />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>重置</Button>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const columns = [
      {
        title: '姓名',
        dataIndex: 'name',
      },
      {
        title: '请假时间',
        dataIndex: 'leaveStartTime',
        render: (text, record) => (
          <p>{this.ConfirmTime(record)}</p>
        ),
      },
      {
        title: '请假身份',
        dataIndex: 'leaveIdean',
        render(index) {
          return <div>{people[index]}</div>;
        },
      },
      {
        title: '状态',
        dataIndex: 'leaveStatus',
        render(val) {
          return <Badge status={status[val].value} text={status[val].text} />;
        },
      },
      {
        title: '操作',
        render: (text, record) => (
          record.leaveStatus !== 0 ? (
            ''
          ) : (
            <Fragment>
              {check('请假管理-请假审批', (
                <Button style={{ marginBottom: 10 }} type="primary" size="small" onClick={() => this.handleConfirm(record, 1)}>通过</Button>
              ))}
              <br />
              {check('请假管理-请假审批', (
                <Popconfirm
                  onConfirm={() => this.handleConfirm(record, 2)}
                  title="是否拒绝请假？"
                >
                  <Button type="danger" size="small">拒绝</Button>
                </Popconfirm>
              ))}
            </Fragment>
          )
        ),
      },
    ];
    const { schoolControl: { leaveList: data }, loading, handleStandardTableChange } = this.props;
    return (
      <Card>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>
            {this.renderSearchForm()}
          </div>
          <StandardTable
            loading={loading}
            data={data}
            columns={columns}
            onChange={handleStandardTableChange}
            expandedRowRender={record => <p style={{ marginLeft: -46 }}>请假原因：{record.leaveReason}</p>}
          />
        </div>
      </Card>
    );
  }
}
export default LeaveList;
