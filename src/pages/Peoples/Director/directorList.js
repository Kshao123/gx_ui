import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import {
  Row,
  Col,
  Card,
  Form,
  Button,
  Divider,
  Badge,
} from 'antd';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import Ellipsis from '@/components/Ellipsis';
import Popcon from '@/components/Popconfirm';
import { parseTime, url } from '@/utils/utils';
import SearchSel from '@/components/SearchSel';
import Hoc from '../../../utils/Hoc';
import Authorized from '@/utils/Authorized';

const { check } = Authorized;
const FormItem = Form.Item;
const MsgForm = {
  id: 1, // (用户id,Integer类型)
  name: '姓名', // (校区负责人姓名,String类型)
  birthday: '生日', // (年龄,Byte类型)
  sex: '性别', // (性别,0表示男,1表示女)
  mobile: '手机', // (手机,String类型)
  email: '邮箱', // (邮箱,String类型)
  orgName: '机构', // (机构名称,String类型)
  address: '住址', // (住址,String类型)
  status: '状态', // (false表示离职,true表示在职)
  picture: '相片', // (头像,String类型)
  salary: '薪资/元',
};
const status = [
  {
    text: '在职',
    value: 'success',
  }, {
    text: '离职',
    value: 'default',
  },
];

@connect(({ schoolControl, global, mine, loading }) => ({
  schoolControl,
  currentOrg: global.currentOrg,
  mine,
  loading: loading.global,
}))
@Form.create()
@Hoc({ fetchUrl: 'schoolControl/_getDirector' })
class directorList extends PureComponent {
  constructor() {
    super();
    this.state = {
      props: {
        onChange: this.handleValueChange,
        queryUrl: `${url}/people/selectEduAdmin`,
        keyValue: 'name',
        showSearch: true,
        attr: {
          id: 'teaId',
          name: 'name',
        },
        placeholder: '搜索',
      },
    };
  }

  handleEdit = (item, value) => {
    const state = value ? item : undefined;
    this.props.dispatch(routerRedux.push({
      pathname: '/personnel/director/mgr',
      state,
    }));
  };

  handleDelete = ({ id }) => {
    const { dispatch, currentOrg, pagination } = this.props;
    if (!currentOrg) return;
    dispatch({
      type: 'schoolControl/_delDirector',
      payload: {
        ids: [id],
        orgId: currentOrg.id,
        pagination,
      },
    });
  };

  handleValueChange = (value, id, data) => {
    const { list } = data;
    const { key } = value;
    if (!list.length) return;
    if (list.length >= 2) {
      data.list = list.filter(item => item.id === Number(key));
    }
    const { dispatch } = this.props;
    dispatch({
      type: 'schoolControl/_searchData',
      payload: {
        payload: data,
        value: 'saveDirector',
      },
    });
  };

  handleFormReset = () => {
    const { dispatch, pagination, currentOrg } = this.props;
    if (!currentOrg) return;
    dispatch({
      type: 'schoolControl/_getDirector',
      payload: {
        pagination,
        orgId: currentOrg.id,
      },
    });
  };

  renderSearchForm() {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    return (
      <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="查询主管">
              {getFieldDecorator('stuId')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...this.state.props} />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>重置</Button>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const { forItem } = this.props;
    const columns = [
      {
        title: MsgForm.name,
        dataIndex: 'name',
        align: 'center',
        render(text) {
          return forItem('icon-fuzeren', text);
        },
      },
      {
        title: MsgForm.birthday,
        dataIndex: 'birthday',
        align: 'center',
        render(text) {
          return forItem('icon-shijian1', text ? parseTime(text).split(' ')[0] : '暂无');
        },
      },
      {
        title: MsgForm.sex,
        dataIndex: 'sex',
        align: 'center',
        render(text) {
          return forItem('icon-xingbie', text ? '男' : '女');
        },
      },
      {
        title: MsgForm.mobile,
        dataIndex: 'mobile',
        align: 'center',
        render(text) {
          return forItem('icon-shouji', text);
        },
      },
      {
        title: MsgForm.status,
        dataIndex: 'status',
        align: 'center',
        render(text) {
          const num = text ? 0 : 1;
          return <Badge status={status[num].value} text={status[num].text} />;
        },
      },
      {
        title: MsgForm.salary,
        dataIndex: 'salary',
        align: 'center',
        render(text) {
          return forItem('icon-zhuankoukuan', text || '暂无');
        },
      },
      {
        title: MsgForm.address,
        dataIndex: 'address',
        render(text) {
          return (
            <div>
              <Ellipsis length={8} tooltip>
                {text || '暂无'}
              </Ellipsis>
            </div>

          );
        },
      },
      {
        title: '操作',
        fixed: 'right',
        width: 120,
        render: (text, record) => (
          <Fragment>
            {check('人员管理-校区负责人:修改', <a onClick={() => this.handleEdit(record, true)}>修改</a>)}
            <Divider type="vertical" />
            {check('人员管理-校区负责人:删除', (
              <Popcon
                title="是否确认删除"
                onConfirm={() => this.handleDelete(record)} // 点击确认时
              />
            ))}
          </Fragment>
        ),
      },
    ];

    const { schoolControl: { directorList: data }, loading, handleStandardTableChange } = this.props;
    return (
      <section>
        <Card className={styles.tableListForm}>
          {this.renderSearchForm()}
        </Card>
        <Card>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              {check('人员管理-校区负责人:删除', (
                <Button icon="plus" type="primary" onClick={this.handleEdit}>
                  新建
                </Button>
              ))}
            </div>
            <StandardTable
              showAllSel
              HideAnother
              bordered
              loading={loading}
              data={data}
              ShowSearch={false}
              columns={columns}
              scroll={{ x: 960 }}
              onChange={handleStandardTableChange}
            />
          </div>
        </Card>
      </section>
    );
  }
}

export default directorList;
