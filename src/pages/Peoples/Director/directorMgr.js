import React, { PureComponent, Fragment } from 'react';
import {
  Card,
  Button,
  Form,
  Icon,
  Col,
  Row,
  DatePicker,
  Input,
  Select,
  Popover,
  InputNumber,
  message,
  Upload,
} from 'antd';
import moment from 'moment';
import { connect } from 'dva';
import FooterToolbar from '@/components/FooterToolbar';
import styles from '../SchoolControl/style.less';
import { parseTime, url } from '@/utils/utils';

const { Option } = Select;
const { TextArea } = Input;
const MsgForm = {
  id: 1, // (用户id,Integer类型)
  name: '姓名', // (校区负责人姓名,String类型)
  age: '年龄', // (年龄,Byte类型)
  sex: '性别', // (性别,0表示男,1表示女)
  mobile: '手机', // (手机,String类型)
  email: '邮箱', // (邮箱,String类型)
  orgName: '机构', // (机构名称,String类型)
  address: '住址', // (住址,String类型)
  status: '状态', // (false表示离职,true表示在职)
  picture: '相片', // (头像,String类型)
  salary: '薪资/元',
  birthday: '生日',
};

class One2One extends PureComponent {
  constructor(props) {
    super(props);
    const { state } = props.location;
    const fileList = state && state.picture.length ? [
      {
        name: '相片',
        uid: 1,
        url: state.picture[0],
      },
    ] : [];
    this.state = {
      width: '100%',
      Required: false,
      fileList,
    };
  }

  beforeUpload = (file, filelist) => {
    // /1024 即可得到实际大小 file.type 为格式
    const Rxp = /.+\.(jpg|jpeg|png|bmp)$/;
    const { name, size } = file;
    if (!Rxp.test(name)) {
      message.error('请上传正确的图片文件 仅支持 jpg | png | jpeg | bmp ', 3);
      console.log(filelist);
      this.setState({
        fileList: [],
      });
      return false;
    } else if ((size / 1024) > 6000) {
      message.error('图片文件大于 5M', 3);
      return false;
    }
    // console.log(file);
    // console.log(filelist);
    return true;
  };

  handleRemove = (file) => {
    const { uid } = file;
    const fileList = this.state.fileList.filter(item => item.uid !== uid);
    this.setState({
      fileList,
    });
    return true;
  };

  handleChange = ({ fileList, file }) => {
    let fileList1 = fileList;
    fileList1 = fileList1.map((item) => {
      if (item.response) {
        item.url = item.response.result;
      }
      return item;
    });
    fileList1 = fileList1.filter((item) => {
      if (item.response) {
        return item.response.status === true;
      }
      return true;
    });
    file.status === 'done' ? (message.success('上传成功', 3)) : '';
    this.setState({ fileList: fileList1 });
  };

  render() {
    const { form, submitting, currentOrg, location, stuDatas } = this.props;
    const { getFieldDecorator, validateFieldsAndScroll, getFieldsError } = form;
    const { state } = location;
    const { Required } = this.state;

    const validate = () => {
      validateFieldsAndScroll((error, values) => {

        if (error) {
          return;
        }
        if (!state) delete values.id;

        if (values.picture && values.picture.length) {
          values.picture = [this.state.fileList[0].url];
        }else {
          values.picture = [];
        }
        values.birthday = new Date(values.birthday.toDate()).getTime();
        console.log(values);
        this.props.dispatch({
          type: 'schoolControl/_changeDirector',
          payload: values,
        });
      });
    };
    const errors = getFieldsError();
    const getErrorInfo = () => {
      const errorCount = Object.keys(errors).filter(key => errors[key]).length;
      if (!errors || errorCount === 0) {
        return null;
      }
      const scrollToField = (fieldKey) => {
        const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
        if (labelNode) {
          labelNode.scrollIntoView(true);
        }
      };
      const errorList = Object.keys(errors).map((key) => {
        if (!errors[key]) {
          return null;
        }
        return (
          <li key={key} className={styles.errorListItem} onClick={() => scrollToField(key)}>
            <Icon type="cross-circle-o" className={styles.errorIcon} />
            <div className={styles.errorMessage}>{errors[key][0]}</div>
            <div className={styles.errorField}>{MsgForm[key]}</div>
          </li>
        );
      });
      return (
        <span className={styles.errorIcon}>
          <Popover
            title="表单校验信息"
            content={errorList}
            overlayClassName={styles.errorPopover}
            trigger="click"
            getPopupContainer={trigger => trigger.parentNode}
          >
            <Icon type="exclamation-circle" />
          </Popover>
          {errorCount}
        </span>
      );
    };
    return (
      <Fragment>
        <Card title="校区负责人添加/修改" className={styles.card} bordered={false}>
          <Form layout="vertical">
            {getFieldDecorator('id', {// 这里留个隐藏输入框是为了获取 当前点击的id 第一个参数  'id'
              // 在 form.validateFields((err, values)=>...) 中 values 为一个对象 是所有输入里的值
              // 属性名则为 getFieldDecorator 第一个参数 ID 值为value
              initialValue: state ? state.id : '' })(
                <Input type="hidden" />
            )}
            {getFieldDecorator('orgId', {// 这里留个隐藏输入框是为了获取 当前点击的id 第一个参数  'id'
              // 在 form.validateFields((err, values)=>...) 中 values 为一个对象 是所有输入里的值
              // 属性名则为 getFieldDecorator 第一个参数 ID 值为value
              initialValue: state ? state.orgId : currentOrg ? currentOrg.id : '' })(
                <Input type="hidden" />
            )}
            <Row gutter={24}>
              <Col xl={12} lg={12} md={12} sm={12} xs={24}>
                <Form.Item label={MsgForm.name}>
                  {getFieldDecorator('name', {
                    initialValue: state ? state.name : '',
                    rules: [{ required: true, message: `请输入${MsgForm.name}` }],
                  })(
                    <Input placeholder={MsgForm.name} />
                  )}
                </Form.Item>
              </Col>
              <Col xl={12} lg={12} md={12} sm={12} xs={24}>
                <Form.Item label={MsgForm.birthday}>
                  {getFieldDecorator('birthday', {
                    initialValue: state ? moment(parseTime(state.birthday)) : moment('2018'),
                    rules: [{ required: true, message: `请输入${MsgForm.birthday}` }],
                  })(
                    <DatePicker />
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xl={12} lg={12} md={12} sm={12} xs={24}>
                <Form.Item label={MsgForm.sex}>
                  {getFieldDecorator('sex', {
                    initialValue: state ? state.sex : '',
                    rules: [{ required: true, message: `请输入${MsgForm.sex}` }],
                  })(
                    <Select style={{ width: '100%' }} >
                      <Option value>男</Option>
                      <Option value={false}>女</Option>
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col xl={12} lg={12} md={12} sm={12} xs={24}>
                <Form.Item label={MsgForm.status}>
                  {getFieldDecorator('status', {
                    initialValue: state ? state.status : '',
                    rules: [{ required: true, message: `请输入${MsgForm.status}` }],
                  })(
                    <Select style={{ width: '100%' }} >
                      <Option value>在职</Option>
                      <Option value={false}>离职</Option>
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 12 }} sm={24}>
                <Form.Item label={MsgForm.mobile}>
                  {getFieldDecorator('mobile', {
                    initialValue: state ? state.mobile : '',
                    rules: [{ pattern: /^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$/, required: true, message: '格式错误，请重新输入' }],
                  })(
                    <Input placeholder="手机" />
                  )}
                </Form.Item>
              </Col>
              <Col xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 12 }} sm={24}>
                <Form.Item label={MsgForm.email}>
                  {getFieldDecorator('email', {
                    initialValue: state ? state.email : '',
                    rules: [{ pattern: /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/, required: false, message: '邮箱格式有误，请重新输入' }],
                  })(
                    <Input placeholder="邮箱" />
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 12 }} sm={24}>
                <Form.Item label={MsgForm.salary}>
                  {getFieldDecorator('salary', {
                    initialValue: state ? state.salary : '',
                    rules: [{ required: true, message: `请输入${MsgForm.age}` }],
                  })(
                    <InputNumber style={{ width: '100%' }} min={1} max={150} />
                  )}
                </Form.Item>
              </Col>
              <Col xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 12 }} sm={24}>
                <Form.Item label={MsgForm.picture}>
                  {getFieldDecorator('picture', {
                    initialValue: state ? this.state.fileList : '',
                    rules: [{ required: false, message: `请输入${MsgForm.picture}` }],
                  })(
                    <Upload
                      action={`${url}/mechanism/saveImage`}
                      listType="picture"
                      name="file1"
                      onChange={this.handleChange}
                      onRemove={this.handleRemove}
                      fileList={this.state.fileList}
                      beforeUpload={this.beforeUpload}
                    >
                      {this.state.fileList.length >= 1 ? null : (
                        <Button>
                          <Icon type="upload" />上传相片
                        </Button>
                      )}
                    </Upload>
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col xl={{ span: 12 }} lg={{ span: 24 }} md={{ span: 24 }} sm={24}>
                <Form.Item label={MsgForm.address}>
                  {getFieldDecorator('address', {
                    initialValue: state ? state.address : '',
                    rules: [{ required: false, message: '请输入地址' }],
                  })(
                    <TextArea placeholder="地址" autosize={{ minRows: 2, maxRows: 6 }} />
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Card>
        <FooterToolbar style={{ width: this.state.width }}>
          {getErrorInfo()}
          <Button type="primary" onClick={validate} loading={submitting}>
            提交
          </Button>
        </FooterToolbar>
      </Fragment>
    );
  }
}

export default connect(({ global, loading }) => ({
  courses: global.courses,
  currentOrg: global.currentOrg,
}))(Form.create()(One2One));
