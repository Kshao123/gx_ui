import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Button,
  Icon,
  Spin,
} from 'antd';
import styles from '@/utils/global.less';
import timeStyles from './index.less';
import { url } from '@/utils/utils';
import SearchSel from '@/components/SearchSel';
import isEqual from 'lodash/isEqual';
import TimeTables from '@/components/TimeTables';

const FormItem = Form.Item;

@connect(({ schedule, global, loading }) => ({
  schedule,
  currentOrg: global.currentOrg,
  loading: loading.effects['schedule/_getSchedules'],
  timeLoading: loading.global
}))
@Form.create()
class ScheduleList extends PureComponent {
  constructor() {
    super();
    this.state = {
      props: {
        removal: true,
        onChange: this.handleValueChange,
        queryUrl: `${url}/teacher/selectTeaByCondition`,
        keyValue: 'name',
        showSearch: true,
        attr: {
          id: 'stuId',
          name: 'name',
        },
        placeholder: '请选择老师',
      },
      currentWeek: 0,
      currentDay: new Date().getTime(),
      currentOrg: {
        id: '',
      },
      teaId: '',
    };
  }

  componentDidMount() {
    const { dispatch, currentOrg } = this.props;
    if (!currentOrg.id) return;
    dispatch({
      type: 'schedule/_getSchedules',
      payload: {
        orgId: currentOrg.id,
        nowWeek: 0,
      },
    })

  }

  static getDerivedStateFromProps(nextProps, preState) {
    const { dispatch } = nextProps;
    if (!nextProps.currentOrg.id) return null;
    if (isEqual(nextProps.currentOrg.id, preState.currentOrg.id)) {
      return null;
    }
    dispatch({
      type: 'schedule/_getSchedules',
      payload: {
        orgId: nextProps.currentOrg.id,
        teaId: preState.teaId,
        nowWeek: preState.currentWeek,
      },
    });
    return {
      currentOrg: nextProps.currentOrg,
    };
  }

  handleFormReset = () => {
    const { currentWeek: nowWeek, currentOrg: { id: orgId }, teaId } = this.state;
    const { dispatch } = this.props;

    if (!orgId) return;
    dispatch({
      type: 'schedule/_getSchedules',
      payload: {
        orgId,
        teaId,
        nowWeek,
      },
    })
  };

  handleValueChange = (value, id, datas) => {
    const { key } = value;
    const { currentWeek: nowWeek, currentOrg: { id: orgId } } = this.state;
    const { dispatch } = this.props;
    this.setState({
      teaId: parseInt(key, 10)
    });
    dispatch({
      type: 'schedule/_getSchedules',
      payload: {
        orgId,
        teaId: Number(key),
        nowWeek,
      },
    })
  };

  addWeek = () => {
    let { currentWeek: nowWeek, currentOrg: { id: orgId }, teaId } = this.state;
    const { dispatch } = this.props;
    // 获取当前的时间
    const date = new Date();
    nowWeek += 1;
    const currentDay = date.getTime() + (24 * nowWeek * 7) * 60 * 60 * 1000;
    this.setState({
      currentWeek: nowWeek,
      currentDay,
    });
    dispatch({
      type: 'schedule/_getSchedules',
      payload: {
        orgId,
        teaId,
        nowWeek,
      },
    })
  };

  preWeek = () => {
    let { currentWeek: nowWeek, currentOrg: { id: orgId }, teaId, currentDay: date } = this.state;
    const { dispatch } = this.props;
    // let time = date;
    nowWeek -= 1;
    // if (nowWeek === 0) time = new Date().getTime();
    // console.log(time, 'pre')
    const currentDay = date - (24 * 7 * 60 * 60 * 1000);
    this.setState({
      currentWeek: nowWeek,
      currentDay,
    });
    dispatch({
      type: 'schedule/_getSchedules',
      payload: {
        orgId,
        teaId,
        nowWeek,
      },
    })
  };

  checkWeek = (weeks) => {
    if(weeks === 0){
      return '本周'
    }
    if(weeks > 0){
      return `下${weeks}周`
    }
    if(weeks < 0){
      return `上${Math.abs(weeks)}周`
    }
    return ''
  };

  backWeek = () => {
    const { currentOrg: { id: orgId }, teaId } = this.state;
    const { dispatch } = this.props;
    this.setState({
      currentWeek: 0,
      currentDay: new Date().getTime(),
    });
    dispatch({
      type: 'schedule/_getSchedules',
      payload: {
        orgId,
        teaId,
        nowWeek: 0,
      },
    })
  };

  renderSearchForm() {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    const { currentWeek } = this.state;
    const { addWeek, preWeek, checkWeek, backWeek } = this;
    return (
      <Row style={{ textAlign: 'center' }} gutter={{ md: 3, lg: 3, xl: 3 }}>
        <Col md={5} sm={5}>
          <Button onClick={preWeek} type="primary" ghost>
            <Icon type="left" />上一周
          </Button>
        </Col>
        <Col style={{ lineHeight: '32px' }} md={3} sm={3}>
          {checkWeek(currentWeek)}
        </Col>
        <Col md={5} sm={5}>
          <Button onClick={addWeek} type="primary" ghost>
            下一周<Icon type="right" />
          </Button>
        </Col>
        <Col md={5} sm={5}>
          <a onClick={backWeek} style={{ display: currentWeek === 0 ? 'none' : 'block', lineHeight: '32px' }}>回到本周</a>
        </Col>
      </Row>
    );
  }

  SearchForm() {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    return (
      <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="老师">
              {getFieldDecorator('stuId')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...this.state.props} />
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }

  handleConfirm = (data, callBack) => {
    const { dispatch } = this.props;
    const { teaId, currentOrg: { id: orgId }, currentWeek: nowWeek, } = this.state;
    // console.log(callBack());
    // return;
    dispatch({
      type: 'schedule/_changeSchedule',
      payload: {
        data,
        obj: {
          teaId,
          orgId,
          nowWeek,
        }
      }
    }).then((res) => {
      if (res) {
        callBack()
      }
    })
  };

  handleSkipCourse = studentCourseIds => {
    const { dispatch } = this.props;
    const { teaId, currentOrg: { id: orgId }, currentWeek: nowWeek, } = this.state;
    dispatch({
      type: 'stuApi/skipCourse',
      payload: {
        orgId,
        teaId,
        nowWeek,
        studentCourseIds,
      }
    })
  };

  render() {
    const { currentDay } = this.state;
    // console.log(parseTime(currentDay), currentDay, currentWeek);
      const courseType = [
        {
          time: '8:00',
          index: 0,
        },
        {
          time: '9:00',
          index: 1
        },
        {
          time: '10:00',
          index: 2
        },
        {
          time: '11:00',
          index: 3
        },
        {
          time: '12:00',
          index: 4
        },
        {
          time: '13:00',
          index: 5
        },
        {
          time: '14:00',
          index: 6
        },
        {
          time: '15:00',
          index: 7
        },
        {
          time: '16:00',
          index: 8
        },
        {
          time: '17:00',
          index: 9
        },
        {
          time: '18:00',
          index: 10
        },
        {
          time: '19:00',
          index: 11
        },
        {
          time: '20:00',
          index: 12
        },
        {
          time: '21:00',
          index: 13
        },
        {
          time: '22:00',
          index: 14
        },
      ];
    const { loading, currentOrg, timeLoading, schedule: { data } } = this.props;
    return (
      <section>
        <Card className={styles.tableListForm}>
          {this.renderSearchForm()}
        </Card>
        <Card>
          <div className={styles.tableListForm}>
            {this.SearchForm()}
          </div>
        </Card>

        <Spin spinning={!!loading} tip="Loading...">
          <div className={timeStyles.timeTables}>
            <TimeTables
              timetables={data}
              timetableType={courseType}
              currentDay={currentDay}
              currentOrg={currentOrg}
              loading={timeLoading}
              onOk={this.handleConfirm}
              handleSkipCourse={this.handleSkipCourse}
            />
          </div>
        </Spin>
      </section>
    );
  }
}
export default ScheduleList;
