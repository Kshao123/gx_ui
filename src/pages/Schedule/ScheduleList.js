import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Button,
  Badge,
} from 'antd';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import { parseTime, url } from '@/utils/utils';
import SearchSel from '@/components/SearchSel';
import Hoc from '@/utils/Hoc';

const FormItem = Form.Item;

@connect(({ schedule, global, loading }) => ({
  schedule,
  currentOrg: global.currentOrg,
  loading: loading.effects['schedule/_getSchedule'],
}))
@Hoc({ fetchUrl: 'schedule/_getSchedule' })
@Form.create()
class ScheduleList extends PureComponent {
  constructor() {
    super();
    this.state = {
      props: {
        removal: true,
        onChange: this.handleValueChange,
        queryUrl: `${url}/courseTable/query`,
        keyValue: 'stuName',
        showSearch: true,
        attr: {
          id: 'stuId',
          name: 'stuName',
        },
        placeholder: '搜索',
      },
    };
  }

  handleFormReset = () => {
    const { dispatch, pagination, currentOrg } = this.props;
    if (!currentOrg) return;
    dispatch({
      type: 'schedule/_getSchedule',
      payload: {
        pagination,
        orgId: currentOrg.id,
      },
    });
  };

  handleValueChange = (value, id, data) => {
    const { list } = data;
    const { label } = value;
    if (!list.length) return;
    if (list.length >= 2) {
      data.list = list.filter(item => item.stuName === label);
    }
    const { dispatch } = this.props;
    dispatch({
      type: 'schedule/_searchData',
      payload: data,
    });
  };

  renderSearchForm() {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    const { props } = this.state;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="查询学生">
              {getFieldDecorator('stuId')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...props} />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>重置</Button>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const { forItem } = this.props;
    const columns = [
      {
        title: '学生姓名',
        dataIndex: 'stuName',
        key: 'stuName',
        width: '8%',
        render(text) {
          return forItem('icon-xingming', text);
        },
      },
      {
        title: '教师姓名',
        dataIndex: 'teaName',
        key: 'teaName',
        width: '8%',
        render(text) {
          return forItem('icon-xingming', text);
        },
      },
      {
        title: '课程名称',
        dataIndex: 'courseName',
        key: 'courseName',
        width: '8%',
        render(text) {
          return forItem('icon-mingcheng', text);
        },
      },
      {
        title: '课程级别',
        dataIndex: 'courseGrade',
        key: 'courseGrade',
        width: '8%',
        render(text) {
          return forItem('icon-jibie', text);
        },
      },
      {
        title: '教室名称',
        dataIndex: 'classroomName',
        key: 'classroomName',
        width: '8%',
        render(text) {
          return forItem('icon-jiaoshi', text);
        },
      },
      {
        title: '日期',
        dataIndex: 'goCourseDate',
        width: '10%',
        children: [
          {
            title: '上课日期',
            dataIndex: 'goCourseDate',
            width: '10%',
            render(text) {
              return forItem('icon-data', parseTime(text).split(' ')[0]);
            },
          },
          {
            title: '所在星期',
            dataIndex: 'weeks',
            width: '8%',
            render(text) {
              return forItem('icon-yigexingqi', text);
            },
          },
          {
            title: '开始时间',
            dataIndex: 'startTime',
            width: '7%',
            render(time) {
              return forItem('icon-shijian1', parseTime(time).split(' ')[1].substr(0, 5));
            },
          },
          {
            title: '结束时间',
            dataIndex: 'endTime',
            width: '7%',
            render(time) {
              return forItem('icon-shijian1', parseTime(time).split(' ')[1].substr(0, 5));
            },
          },
        ],
      },
      {
        title: '是否上课',
        dataIndex: 'isAlreadyOver',
        width: '8%',
        render: (text) => (
          <div>
            <Badge status={text === 0 ? 'warning' : 'success'} text={text === 0 ? '未上' : '已上'} />
          </div>
          ),
      },
    ];
    const { schedule: { data }, loading, handleStandardTableChange } = this.props;

    return (
      <section>
        <Card className={styles.tableListForm}>
          {this.renderSearchForm()}
        </Card>
        <Card>
          <div className={styles.tableList}>
            <StandardTable
              showAllSel
              HideAnother
              bordered
              loading={loading}
              data={data}
              columns={columns}
              onChange={handleStandardTableChange}
              scroll={{ x: 1300 }}
              expandedRowRender={record => <p style={{ margin: 0 }}>请假原因：{record.askLeaveReson}</p>}
            />
          </div>
        </Card>
      </section>
    );
  }
}
export default ScheduleList;
