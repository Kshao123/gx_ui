import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Button,
  Icon,
  Spin,
  Modal,
} from 'antd';
import styles from '@/utils/global.less';
import { parseTime, url } from '@/utils/utils';
import SearchSel from '@/components/SearchSel';
import Timetables from 'timetables';
// import './index.css'
import isEqual from 'lodash/isEqual';
// require('./index.css');

let Timetable;
const FormItem = Form.Item;
const courseType = [
  [{index: '1',name: '8:00'}, 1],
  [{index: '2',name: '9:00'}, 1],
  [{index: '3',name: '10:00'}, 1],
  [{index: '4',name: '11:00'}, 1],
  [{index: '5',name: '12:00'}, 1],
  [{index: '6',name: '13:00'}, 1],
  [{index: '7',name: '14:00'}, 1],
  [{index: '8',name: '15:00'}, 1],
  [{index: '9',name: '16:00'}, 1],
  [{index: '10',name: '17:00'}, 1],
  [{index: '11',name: '18:00'}, 1],
  [{index: '12',name: '19:00'}, 1],
  [{index: '13',name: '20:00'}, 1],
  [{index: '14',name: '21:00'}, 1],
];

const timeNum = {
  8: 1,
  9: 2,
  10: 3,
  11: 4,
  12: 5,
  13: 6,
  14: 7,
  15: 8,
  16: 9,
  17: 10,
  18: 11,
  19: 12,
  20: 13,
  21: 14,
};

const week = window.innerWidth > 360 ? ['周一', '周二', '周三', '周四', '周五', '周六', '周日'] :
  ['一', '二', '三', '四', '五', '六', '日'];

const timeCheck = time => {
  const { startTime, endTime } = time;
  const startNum = timeNum[new Date(startTime).getHours()];
  const diffNum = timeNum[new Date(endTime).getHours()] - startNum;
  return { startNum, diffNum }
};

const filterTable = (Data) => {
  const arr = [];
  for (let i = 0; i< 7; i+=1) {
    arr[i] = [];
    for (let j = 0; j< 14; j+=1) {
      arr[i][j] = '';
    }
  }
  for (let item in Data) {
    const list = Data[item];
    list.map(items => {
      const { startNum, diffNum } = timeCheck({ startTime: items.startTime, endTime: items.endTime});
      let str = '';
      if (items.stuNameList.length > 1) {
        str = `${items.courseName} | ${items.gradeName}.
    .${items.teaName}.
    .${parseTime(items.startTime, '{h}:{i}')}-${parseTime(items.endTime, '{h}:{i}')}.
    .${items.stuNameList.join('、')}.
    .${items.classroomName}`;
      } else {
         str = `.${items.stuNameList.join('')}.
         .${items.courseName} | ${items.gradeName}
    .${parseTime(items.startTime, '{h}:{i}')}-${parseTime(items.endTime, '{h}:{i}')}.
    .${items.teaName}.
    .${items.classroomName}`;
      }


      arr[item - 1][startNum - 1] = str;
      if (diffNum > 1) {
        for (let k = 1; k <= diffNum; k +=1 ) {
          arr[item - 1][startNum - 1 + k - 1 ] = str;
        }
      }
      return items;
    });
  }
  return arr
};

const confirmModal = (value) => {
  const { name } = value;
  if (value && name.length) {
    const [courseName, a, teaName, b, time, f, stuName, G, classRoom] = name.split('.');
    courseName.length ? (
      Modal.info({
        title: `老师${teaName}详情`,
        content: (
          <div>
            <p>
              <span>教师姓名：</span>
              <span>{teaName}</span>
            </p>
            <p>
              <span>上课时间：</span>
              <span>{time}</span>
            </p>
            <p>
              <span>学生：</span>
              <span>{stuName}</span>
            </p>
            <p>
              <span>教室名称：</span>
              <span>{classRoom}</span>
            </p>
          </div>
        ),
      })
      ) : (
      Modal.info({
        title: `老师${teaName}详情`,
        content: (
          <div>
            <p>
              <span>教师姓名：</span>
              <span>{stuName}</span>
            </p>
            <p>
              <span>上课时间：</span>
              <span>{time}</span>
            </p>
            <p>
              <span>学生：</span>
              <span>{a}</span>
            </p>
            <p>
              <span>教室名称：</span>
              <span>{classRoom}</span>
            </p>
          </div>
        ),
      })
    )

  }
};

@connect(({ schedule, global, loading }) => ({
  schedule,
  currentOrg: global.currentOrg,
  loading: loading.effects['schedule/_getSchedules'],
}))
@Form.create()
class ScheduleList extends PureComponent {
  constructor() {
    super();
    this.state = {
      props: {
        removal: true,
        onChange: this.handleValueChange,
        queryUrl: `${url}/teacher/selectTeaByCondition`,
        keyValue: 'name',
        showSearch: true,
        attr: {
          id: 'stuId',
          name: 'name',
        },
        placeholder: '搜索',
      },
      currentWeek: 0,
      currentOrg: {
        id: '',
      },
      teaId: '',
    };
  }

  componentDidMount() {
    const day = new Date().getDay();
    const { dispatch, currentOrg } = this.props;
    if (!currentOrg.id) return;
    dispatch({
      type: 'schedule/_getSchedules',
      payload: {
        orgId: currentOrg.id,
        nowWeek: 0,
      },
    }).then(res => {
      if (!res) return;
      const Table = filterTable(res);
      setTimeout(() => {
        const el = document.querySelector('#courseWrapper');
        if (el) {
          location.reload();
        } else {
          Timetable = new Timetables({
            el: '#coursesTable',
            timetables: Table,
            week,
            timetableType: courseType,
            highlightWeek: day,
            gridOnClick: (e) => {
              confirmModal(e)
            },
            styles:{
              Gheight: 50
            }
          });
        }
      }, 200)
    });

  }

  static getDerivedStateFromProps(nextProps, preState) {
    const { dispatch } = nextProps;
    if (!nextProps.currentOrg.id) return null;
    if (isEqual(nextProps.currentOrg.id, preState.currentOrg.id)) {
      return null;
    }
    dispatch({
      type: 'schedule/_getSchedules',
      payload: {
        orgId: nextProps.currentOrg.id,
        teaId: preState.teaId,
        nowWeek: preState.currentWeek,
      },
    }).then(res => {
      if (!res) return;
      const day = new Date().getDay();
      const Table = filterTable(res);
      setTimeout(() => {
        const el = document.querySelector('#courseWrapper');
        if (Timetable && el) {
          Timetable.setOption({
            timetables: Table,
            week,
            timetableType:courseType,
          })
        } else {
          Timetable = new Timetables({
            el: '#coursesTable',
            timetables: Table,
            week,
            timetableType: courseType,
            highlightWeek: day,
            gridOnClick: (e) => {
              confirmModal(e)
            },
            styles:{
              Gheight: 50
            }
          });
        }
      }, 200)
    });
    return {
      currentOrg: nextProps.currentOrg,
    };
  }

  handleFormReset = () => {
    location.reload()
  };

  handleValueChange = (value, id, datas) => {
    const { key } = value;
    const { currentWeek: nowWeek, currentOrg: { id: orgId } } = this.state;
    const { dispatch } = this.props;
    this.setState({
      teaId: Number(key)
    });
    dispatch({
      type: 'schedule/_getSchedules',
      payload: {
        orgId,
        teaId: Number(key),
        nowWeek,
      },
    }).then((res) => {
      if (!res) return;
      const Table = filterTable(res);
      Timetable.setOption({
        timetables: Table,
        })
    })
  };

  addWeek = () => {
    let { currentWeek: nowWeek, currentOrg: { id: orgId }, teaId } = this.state;
    const { dispatch } = this.props;
    nowWeek += 1;
    this.setState({
      currentWeek: nowWeek,
    });
    dispatch({
      type: 'schedule/_getSchedules',
      payload: {
        orgId,
        teaId,
        nowWeek,
      },
    }).then((res) => {
      if (!res) return;
      const Table = filterTable(res);
      Timetable.setOption({
        timetables: Table,
        week,
        timetableType:courseType,
      })
    })
  };

  preWeek = () => {
    let { currentWeek: nowWeek, currentOrg: { id: orgId }, teaId } = this.state;
    const { dispatch } = this.props;
    nowWeek -= 1;
    this.setState({
      currentWeek: nowWeek,
    });
    dispatch({
      type: 'schedule/_getSchedules',
      payload: {
        orgId,
        teaId,
        nowWeek,
      },
    }).then((res) => {
      if (!res) return;
      const Table = filterTable(res);
      Timetable.setOption({
        timetables: Table,
        week,
        timetableType:courseType,
      })
    })
  };

  checkWeek = (weeks) => {
    if(weeks === 0){
      return '本周'
    }
    if(weeks > 0){
      return `下${weeks}周`
    }
    if(weeks < 0){
      return `上${Math.abs(weeks)}周`
    }
    return ''
  };

  backWeek = () => {
    const { currentOrg: { id: orgId }, teaId } = this.state;
    const { dispatch } = this.props;
    this.setState({
      currentWeek: 0
    });
    dispatch({
      type: 'schedule/_getSchedules',
      payload: {
        orgId,
        teaId,
        nowWeek: 0,
      },
    }).then((res) => {
      if (!res) return;
      const Table = filterTable(res);
      Timetable.setOption({
        timetables: Table,
        week,
        timetableType:courseType,
      })
    })
  };

  renderSearchForm() {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    const { currentWeek } = this.state;
    const { addWeek, preWeek, checkWeek, backWeek } = this;
    return (
      <Row style={{ textAlign: 'center' }} gutter={{ md: 3, lg: 3, xl: 3 }}>
        <Col md={5} sm={5}>
          <Button onClick={preWeek} type="primary" ghost>
            <Icon type="left" />上一周
          </Button>
        </Col>
        <Col style={{ lineHeight: '32px' }} md={3} sm={3}>
          {checkWeek(currentWeek)}
        </Col>
        <Col md={5} sm={5}>
          <Button onClick={addWeek} type="primary" ghost>
            下一周<Icon type="right" />
          </Button>
        </Col>
        <Col md={5} sm={5}>
          <a onClick={backWeek} style={{ display: currentWeek === 0 ? 'none' : 'block', lineHeight: '32px' }}>回到本周</a>
        </Col>
      </Row>
    );
  }

  SearchForm() {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    return (
      <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="老师">
              {getFieldDecorator('stuId')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...this.state.props} />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>重置</Button>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const { loading } = this.props;
    return (
      <section>
        <Card className={styles.tableListForm}>
          {this.renderSearchForm()}
        </Card>
        <Card>
          <div className={styles.tableListForm}>
            {this.SearchForm()}
          </div>
          <Spin spinning={!!loading} tip="Loading...">
            <div style={{ textAlign: 'center' }} id="coursesTable" />
          </Spin>
        </Card>
      </section>
    );
  }
}
export default ScheduleList;
