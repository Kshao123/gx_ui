import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Card,
  Form,
  Col,
  DatePicker,
  Row,
  Button,
  Icon
} from 'antd';
import moment from 'moment';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import { scriptUrl, parseTime, url } from '../../../utils/utils';
import Ellipsis from '@/components/Ellipsis';
import Hoc from '@/utils/Hoc';
import SearchSel from '@/components/SearchSel';


const FormItem = Form.Item;
const IconFont = Icon.createFromIconfontCN({
  scriptUrl,
});
const { RangePicker } = DatePicker;

@connect(({ classRoom, global, loading }) => ({
  classRoom,
  currentOrg: global.currentOrg,
  loading: loading.effects['classRoom/_getClassRoomOrder'],
}))
@Form.create()
@Hoc({fetchUrl: 'classRoom/_getClassRoomOrder'})

class ClassroomleaseOpenList extends PureComponent {
  constructor() {
    super();
    this.state = {
      formValues: {},
    };
  }

  footer = (data) => {
    let allPrice = 0;
    let allNum = 0;
    if (data && data.length) {
      allPrice = data[0].allRentFee;
      allNum = data[0].allNum;
    }
    return (
      <div style={{ display: 'flex' }}>
        <p style={{ flex: '1' }}>
          <span style={{ fontWeight: 'bold' }}>总订单数：</span>
          <span style={{ color: 'black' }}>{allNum}</span>
        </p>
        <p style={{ flex: '1' }}>
          <span style={{ fontWeight: 'bold' }}>总金额：</span>
          <span style={{ color: 'black' }}>{allPrice} 元</span>
        </p>
      </div>
    )
  };

  handleFormReset = () => {
    const { dispatch, pagination, currentOrg, form  } = this.props;
    if (currentOrg.id) {
      form.resetFields();
      this.setState({
        formValues: {},
      });
      dispatch({
        type: 'classRoom/_getClassRoomOrder',
        payload: {
          pagination,
          orgId: currentOrg.id,
        },
      });
    }
  };

  handleSearch = (e) => {
    e.preventDefault();

    const { dispatch, pagination, currentOrg, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const { name, time, classroomName } = fieldsValue;
      const query = {};
      if (name) {
        query.name = name.label
      }
      if (classroomName) {
        query.classroomName = classroomName.label
      }
      const [ startTime, endTime ] = time;
      query.startTime = startTime.toDate().getTime();
      query.endTime = endTime.toDate().getTime();
      this.setState({
        formValues: query,
      });
      dispatch({
        type: 'classRoom/_getClassRoomOrder',
        payload: {
          pagination,
          orgId: currentOrg.id,
          query,
        },
      })
    });
  };

  renderSearchForm() {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const teaProps = {
      removal: true,
      queryUrl: `${url}/classroom/findRentOrder`,
      keyValue: 'name',
      showSearch: true,
      attr: {
        id: 'teaId',
        name: 'tenantName',
      },
      placeholder: '用户姓名',
    };

    const classProps = {
      removal: true,
      queryUrl: `${url}/classroom/findRentOrder`,
      keyValue: 'classroomName',
      showSearch: true,
      attr: {
        id: 'teaId',
        name: 'classroomName',
      },
      placeholder: '搜索教室',
    };
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="用户">
              {getFieldDecorator('teaName')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...teaProps} />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="教室">
              {getFieldDecorator('classroomName')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {... classProps} />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col md={16} sm={24}>
            <FormItem label="时间">
              {getFieldDecorator('time',{
                initialValue: [
                  moment(new Date()).set({'minute': 0, 'second': 0, 'hour': 0}),
                  moment().set({'minute': 59, 'second': 59, 'hour': 23})
                ],
              })(
                <RangePicker showTime={{ format: 'HH:mm' }} format="YYYY-MM-DD HH:mm" />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">查询</Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const { classRoom: { Leasing: data }, loading, handleStandardTableChange, forItem } = this.props;
    const columns = [
      {
        title: '租户姓名',
        dataIndex: 'tenantName',
        align: 'center',
        render(text) {
          return forItem('icon-mingcheng', text);
        },
      },
      {
        title: '手机',
        dataIndex: 'tenantMobile',
        align: 'center',
        render(text) {
          return forItem('icon-shouji', text);
        },
      },
      {
        title: '教室',
        dataIndex: 'classroomName',
        align: 'center',
        render(text) {
          return (
            <div className={styles.TableEllipsis}>
              <IconFont type="icon-jiaoshi" />
              <Ellipsis length={5} tooltip>
                {text}
              </Ellipsis>
            </div>
          );
        },
      },
      {
        title: '单价/元',
        dataIndex: 'rentPrice',
        align: 'center',
        render(text) {
          return forItem('icon-jiage', text);
        },
      },
      {
        title: '费用/元',
        dataIndex: 'rentFee',
        align: 'center',
        render(text) {
          return forItem('icon-jiage', (text));
        },
      },
      {
        title: '租赁时间',
        align: 'center',
        children: [
          {
            title: '开始时间',
            dataIndex: 'rentStartTime',
            align: 'center',
            render(text) {
              return forItem('icon-shijian1', parseTime(text));
            },
          },
          {
            title: '结束时间',
            dataIndex: 'rentEndTime',
            align: 'center',
            render(text) {
              return forItem('icon-shijian1', parseTime(text));
            },
          },
        ],
      },
    ];
    const { formValues } = this.state;
    return (
      <section>
        <Card className={styles.tableListForm}>
          {this.renderSearchForm()}
        </Card>
        <Card>
          <div className={styles.tableList}>
            <StandardTable
              bordered
              loading={loading}
              data={data}
              columns={columns}
              scroll={{ x: 950 }}
              onChange={e => handleStandardTableChange(e, formValues)}
              footer={this.footer}
            />
          </div>
        </Card>
      </section>
    );
  }
}

export default ClassroomleaseOpenList;
