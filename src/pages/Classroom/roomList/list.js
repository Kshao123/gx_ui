import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Card,
  Form,
  Input,
  Select,
  Button,
  Divider,
  Modal,
  Badge,
  TimePicker,
  InputNumber,
} from 'antd';
import moment from 'moment';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import Popcon from '@/components/Popconfirm';
import { parseTime } from '@/utils/utils';
import Hoc from '@/utils/Hoc';
import Authorized from '@/utils/Authorized';

const { check } = Authorized;
const { Option } = Select;
const MsgForm = {
  orgName: '机构名称',
  classroomNo: '教室编号',
  classroomName: '教室名称',
  setStartTime: '开始时间',
  setEndTime: '结束时间',
  isUsable: '状态',
};
const status = [
  {
    text: '可用',
    value: 'success',
  }, {
    text: '使用中',
    value: 'warning',
  },
];

@connect(({ classRoom, global, loading }) => ({
  classRoom,
  currentOrg: global.currentOrg,
  loading: loading.effects['classRoom/_getAllClassroom'],
}))
@Form.create()
@Hoc({ fetchUrl: 'classRoom/_getAllClassroom' })
class List extends PureComponent {
  constructor() {
    super();
    this.state = {
      visible: false,
      isEdit: false,
      FormObj: {},
    };
  }

  handleEdit = (item, value) => {
    let str = false;
    let obj = {};
    if (value) {
      str = true;
      obj = item;
    }
    this.setState({
      visible: true,
      isEdit: str,
      FormObj: obj,
    });
  };

  handleDelete = ({ id }) => {
    const { dispatch, pagination, currentOrg: { id: orgId } } = this.props;
    dispatch({
      type: 'classRoom/_delClassroom',
      payload: {
        ids: [id],
        pagination,
        orgId,
      },
    });
  };

  handleOk = () => {
    const { form, dispatch, pagination, currentOrg: { id: orgId } } = this.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      if (!this.state.isEdit) {
        delete (values.id);
      }
      delete values.orgName;
      values.setEndTime = parseTime(new Date(values.setEndTime._d).getTime()).split(' ')[1].substr(0, 5);
      values.setStartTime = parseTime(new Date(values.setStartTime._d).getTime()).split(' ')[1].substr(0, 5);
      dispatch({
        type: 'classRoom/_addClassroom',
        payload: {
          data: values,
          pagination,
          orgId,
        },
      });

      // 在此处调用的是 重置表单内容， 不重置的话会导致 对话框再次打开不能实时更新内容的bug
      form.resetFields();
      this.setState({
        visible: false,
        isEdit: false,
      });
    });
  };

  handleCancel = () => {
    this.props.form.resetFields();
    this.setState({
      visible: false,
      isEdit: false,
    });
  };

  render() {
    const { forItem, classRoom: { data }, currentOrg, loading, form, handleStandardTableChange } = this.props;
    const columns = [
      {
        title: MsgForm.classroomName,
        dataIndex: 'classroomName',
        align: 'center',
        render(text) {
          return forItem('icon-jiaoshi', text);
        },
      },
      {
        title: MsgForm.classroomNo,
        dataIndex: 'classroomNo',
        align: 'center',
        render(text) {
          return forItem('icon-jibie', text);
        },
      },
      {
        title: MsgForm.setStartTime,
        dataIndex: 'setStartTime',
        align: 'center',
        render(text) {
          return forItem('icon-shijian1', text);
        },
      },
      {
        title: MsgForm.setEndTime,
        dataIndex: 'setEndTime',
        align: 'center',
        render(text) {
          return forItem('icon-shijian1', (text));
        },
      },
      {
        title: MsgForm.isUsable,
        dataIndex: 'isUsable',
        align: 'center',
        render(text) {
          const num = text ? 0 : 1;
          return <Badge status={status[num].value} text={status[num].text} />;
        },
      },
      {
        title: '操作',
        render: (text, record) => (
          <Fragment>
            {check('教室管理-教室:修改', <a onClick={() => this.handleEdit(record, true)}>修改</a>)}
            <Divider type="vertical" />
            {check('教室管理-教室:删除', (
              <Popcon
                title="是否确认删除"
                onConfirm={() => this.handleDelete(record)} // 点击确认时
              />
            ))}
          </Fragment>
        ),
      },
    ];

    const { getFieldDecorator } = form;
    const { isEdit, FormObj, visible } = this.state;
    return (
      <Card>
        <div className={styles.tableList}>
          <div className={styles.tableListOperator}>
            {check('教室管理-教室:增加', (
              <Button icon="plus" type="primary" onClick={this.handleEdit}>
                新建
              </Button>
            ))}
          </div>
          <StandardTable
            bordered
            loading={loading}
            data={data}
            columns={columns}
            onChange={handleStandardTableChange}
          />
        </div>
        <Modal
          title="编辑/修改信息"
          visible={visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Form layout="vertical">
            {getFieldDecorator('id', {
              initialValue: isEdit ? FormObj.id : '' })(
                <Input type="hidden" />
            )}
            {getFieldDecorator('orgId', {
              initialValue: currentOrg && currentOrg.id ? currentOrg.id : '' })(
                <Input type="hidden" />
            )}
            <Form.Item label={MsgForm.classroomName}>
              {getFieldDecorator('classroomName', {
                initialValue: isEdit ? FormObj.classroomName : '',
                rules: [{ required: true, message: `请输入${MsgForm.classroomName}` }],
              })(
                <Input placeholder={MsgForm.name} />
              )}
            </Form.Item>
            <Form.Item label={MsgForm.classroomNo}>
              {getFieldDecorator('classroomNo', {
                initialValue: isEdit ? FormObj.classroomNo : '',
                rules: [{ required: true, message: `请输入${MsgForm.classroomNo}` }],
              })(
                <Input placeholder={MsgForm.classroomNo} />
              )}
            </Form.Item>

            <Form.Item label={MsgForm.setStartTime}>
              {getFieldDecorator('setStartTime', {
                initialValue: isEdit ? moment(FormObj.setStartTime, 'HH:mm') : moment('08:00', 'HH:mm'),
                rules: [{ required: true, message: `请输入${MsgForm.setStartTime}` }],
              })(
                <TimePicker />
              )}
            </Form.Item>

            <Form.Item label={MsgForm.setEndTime}>
              {getFieldDecorator('setEndTime', {
                initialValue: isEdit ? moment(FormObj.setEndTime, 'HH:mm') : moment('20:45', 'HH:mm'),
                rules: [{ required: true, message: `请输入${MsgForm.setEndTime}` }],
              })(
                <TimePicker />
              )}
            </Form.Item>

            <Form.Item label={MsgForm.isUsable}>
              {getFieldDecorator('isUsable', {
                initialValue: isEdit ? FormObj.isUsable : '',
                rules: [{ required: true, message: `请输入${MsgForm.isUsable}` }],
              })(
                <Select style={{ width: '100%' }}>
                  <Option value>可用</Option>
                  <Option value={false}>使用中</Option>
                </Select>
              )}
            </Form.Item>

            <Form.Item label="价格 /小时">
              {getFieldDecorator('price', {
                initialValue: isEdit ? FormObj.price : '',
                rules: [{ required: true, message: `请输入${MsgForm.orgName}` }],
              })(
                <InputNumber placeholder="单价" style={{ width: '100%' }} min={1} max={99999} />
              )}
            </Form.Item>

            <Form.Item label={MsgForm.orgName}>
              {getFieldDecorator('orgName', {
                initialValue: currentOrg && currentOrg.orgName ? currentOrg.orgName : '',
                rules: [{ required: true, message: `请输入${MsgForm.orgName}` }],
              })(
                <Input disabled placeholder={MsgForm.orgName} />
              )}
            </Form.Item>


          </Form>
        </Modal>
      </Card>
    );
  }
}

export default List;
