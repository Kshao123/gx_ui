import React, { Component } from 'react';
import {
  Modal,
  Form,
  Input,
  Select,
  Tooltip,
  Icon,
  InputNumber,
  DatePicker,
} from 'antd';
import moment from 'moment';

const { Option } = Select;

@Form.create()
class AddCoupon extends Component{

  state = {
    isFix: false,
  };

  handleFixChange = (values) => {
    let isFix = false;
    if (values === 1) {
      isFix = true
    }
    this.setState({
      isFix,
    })
  };

  handleOk = () => {
    const { form: { validateFieldsAndScroll, resetFields }, onOk } = this.props;
    validateFieldsAndScroll((error, values) => {
      if (!error) {
        const {
          validityTime,
        } = values;
        const obj = {
          ... values
        };
        obj.validityTime = validityTime.toDate().getTime();
        onOk( obj, resetFields);
      }
    });
  };

  handleCancel = () => {
    const { form: { resetFields }, onCancel } = this.props;
    onCancel(resetFields)
  };

  render() {
    const {
      visible,
      form,
      courseCouponName,
      courseCouponType,
      fixedAmountCouponFulfilMoney,
      lessNum,
      validityTime,
    } = this.props;
    const { getFieldDecorator} = form;
    const { isFix } = this.state;
    return (
      <Modal
        title="添加优惠券"
        visible={visible}
        onOk={this.handleOk}
        onCancel={this.handleCancel}
      >
        <Form layout="vertical">

          <Form.Item label="名称">
            {getFieldDecorator('courseCouponName', {
              initialValue: courseCouponName || '',
              rules: [{ required: false }],
            })(
              <Input disabled />
            )}
          </Form.Item>

          <Form.Item label="优惠券类型">
            {getFieldDecorator('courseCouponType', {
              initialValue: courseCouponType || 2,
              rules: [{ required: true, message: '请选择类型' }],
            })(
              <Select onChange={this.handleFixChange}>
                <Option value={2}>比例</Option>
                <Option value={1}>固定数额</Option>
              </Select>
            )}
          </Form.Item>

          <Form.Item
            style={{ display: isFix ? 'block' : 'none' }}
            label={(
              <span>
                额度 / 元&nbsp;
                <Tooltip title="固定数额优惠卷使用需要满足的金额">
                  <Icon type="question-circle-o" />
                </Tooltip>
              </span>
          )}
          >
            {getFieldDecorator('fixedAmountCouponFulfilMoney', {
              initialValue: fixedAmountCouponFulfilMoney || 0,
              rules: [{ required: isFix, message: '请输入金额' }],
            })(
              <InputNumber style={{ width: '100%' }} />
            )}
          </Form.Item>

          <Form.Item
            label={(
              <span>
                优惠金额 / [ 元 | % ]&nbsp;
                <Tooltip title="根据比例，固定金额类型输入正确的 金额或者比例">
                  <Icon type="question-circle-o" />
                </Tooltip>
              </span>
            )}
          >
            {getFieldDecorator('lessNum', {
              initialValue: lessNum || 0,
              rules: [{ required: true, message: '请输入金额或比例' }],
            })(
              <InputNumber style={{ width: '100%' }} />
            )}
          </Form.Item>

          <Form.Item
            label="有效期"
          >
            {getFieldDecorator('validityTime', {
              initialValue: validityTime ? moment(validityTime) : moment(),
              rules: [{ required: true, message: '请选择日期' }],
            })(
              <DatePicker />
            )}
          </Form.Item>
        </Form>
      </Modal>
    )
  }
}

export default AddCoupon;
