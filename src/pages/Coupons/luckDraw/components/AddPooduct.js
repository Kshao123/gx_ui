import React, { Component } from 'react';
import {
  Modal,
  Form,
  Input,
} from 'antd';
import SearchSel from '@/components/SearchSel';

@Form.create()
class AddCoupon extends Component{
  handleOk = () => {
    const { form: { validateFieldsAndScroll, resetFields }, onOk, productId, productName, productPicture } = this.props;
    validateFieldsAndScroll(error => {
      if (!error) {
        onOk({
          productName,
          productId,
          productPicture
        }, resetFields);
      }
    });
  };

  handleCancel = () => {
    const { form: { resetFields }, onCancel } = this.props;
    onCancel(resetFields)
  };

  handleValueChange = (value, id, data) => {
    const { key: productId, label: productName } = value;
    let productPicture = [];
    if (data && data.list) {
      productPicture = data.list.find(item => item.id === productId).productPictureList.slice(0, 1);
    }
    const { changeInfo } = this.props;
    changeInfo('productInfo', {
      productId,
      productName,
      productPicture
    });
  };


  render() {
    const {
      visible,
      form,
      productId,
      url,
      productName,
      currentOrg,
      productPicture,
    } = this.props;
    const { getFieldDecorator} = form;

    return (
      <Modal
        title="产品信息"
        visible={visible}
        onOk={this.handleOk}
        onCancel={this.handleCancel}
      >
        <Form layout="vertical">
          {getFieldDecorator('productId', {
            initialValue: productId || '' })(
              <Input type="hidden" />
          )}

          <Form.Item label="名称">
            {getFieldDecorator('productName', {
              initialValue: {label: productName, key: productId} || '',
              rules: [{ required: true, message: '请输入' }],
            })(
              <SearchSel
                orgId={currentOrg ? currentOrg.id : ''}
                onChange={this.handleValueChange}
                queryUrl={`${url}/inventoryManagement/displayProduct`}
                keyValue="productName"
                showSearch
                attr={{
                  id: 'id',
                  name: 'productName',
                }}
                placeholder="请输入名称"
                allowClear
              />
            )}
          </Form.Item>

          <Form.Item label="图片">
            {
              productPicture.slice(0, 1).map(item => (
                <img
                  key={item}
                  src={item}
                  alt=""
                  style={{
                    width: 100,
                    height: 100
                  }}
                />
              ))
            }
          </Form.Item>
        </Form>
      </Modal>
    )
  }
}

export default AddCoupon;
