import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Table,
  Button,
  Input,
  message,
  Popconfirm,
  Divider,
  InputNumber,
  Select,
} from 'antd';
import isEqual from 'lodash/isEqual';
import styles from '@/styles/public/tableFormMgr.less';
import AddProduct from './components/AddPooduct';
import AddCoupon from './components/AddCoupon';
import { url } from '@/utils/utils';

const TYPE = {
  1: {
    name: 'productName'
  },

  2: {
    name: 'courseCouponName'
  }
};
const optionsType = [_, '产品', '优惠券'];

const { Option } = Select;

class TableForm extends PureComponent {
  constructor(props) {
    super(props);
    const { dataList } = props.value;
    this.state = {
      data: dataList || [],
      loading: false,
      value: props.value,
      ShowBtn: true,
      showProduct: false,
      showCoupon: false,
      productInfo: {
        productId: null,
        productName: null,
        productPicture: [],
      },
      couponInfo: {
        courseCouponName: '',
        courseCouponType: 2,
        fixedAmountCouponFulfilMoney: 0,
        lessNum: 0,
        validityTime: null,
      },
      key: null,
    };
  }

  static getDerivedStateFromProps(nextProps, preState) {
    if (isEqual(nextProps.value, preState.value)) {
      return null;
    }
    return {
      data: nextProps.value.dataList || nextProps.value || [], // 这样写是因为变化时 父组件中的判断有问题 会导致传来一个空数组
      value: nextProps.value,
    };
  }


  getRowByKey(key, newData) {
    const { data } = this.state;
    return (newData || data).filter(item => item.key === key)[0];
  }

  index = 0;

  cacheOriginData = {};

  toggleEditable = (e, key) => {
    e && e.preventDefault();
    const { data } = this.state;
    const newData = data.map(item => ({ ...item }));
    const target = this.getRowByKey(key, newData);
    if (target) {
      // 进入编辑状态时保存原始数据
      if (!target.editable) {
        this.cacheOriginData[key] = { ...target };
      }
      target.editable = !target.editable;
      this.setState({ data: newData });
    }
  };

  newMember = () => {
    const { data } = this.state;
    const newData = data.map(item => ({ ...item }));
    newData.push({
      key: `NEW_TEMP_ID_${this.index}`,
      productName: '',
      productId: '',
      num: 0,
      winningRate: 0,
      productPicture: [],
      editable: true,
      isNew: true,
      new: true,
      prizeType: 1, // 1：产品 2：优惠券
      courseCouponName: '',
      courseCouponType: 2,
      fixedAmountCouponFulfilMoney: 0,
      lessNum: 0,
      validityTime: null,
    });
    this.index += 1;
    this.setState({ data: newData });
  };

  remove(key) {
    const { data } = this.state;
    const { onChange } = this.props;
    const newData = data.filter(item => item.key !== key);
    this.setState({ data: newData });
    onChange(newData);
  }

  handleKeyPress(e, key) {
    if (e.key === 'Enter') {
      this.saveRow(e, key);
    }
  }

  handleFieldChange(e, fieldName, key) {
    const { data } = this.state;
    const newData = data.map(item => ({ ...item }));
    const target = this.getRowByKey(key, newData);
    if (target) {
      target[fieldName] = e.target.value;
      this.setState({ data: newData });
    }
  }

  saveRow(e, key) {
    e.persist();

    this.setState({
      loading: true,
    });

    setTimeout(() => {
      if (this.clickedCancel) {
        this.clickedCancel = false;
        return;
      }
      const target = this.getRowByKey(key) || {};
      const {
        productName,
        num,
        winningRate,
        prizeType,
        productPicture,
        productId,
        courseCouponName,
        courseCouponType,
        fixedAmountCouponFulfilMoney,
        lessNum,
        validityTime,
      } = target;

      if (prizeType === 2 && !courseCouponName) {
        message.error('请填写名称。');
        e.target.focus();
        this.setState({
          loading: false,
        });
        return;
      }

      if (num < 0 || winningRate < 0 ) {
        message.error('请填写完整信息。');
        e.target.focus();
        this.setState({
          loading: false,
        });
        return;
      }
      if (prizeType === 1) {
        this.setState({
          showProduct: true,
          productInfo: {
            showProduct: true,
            productId,
            productName,
            productPicture,
          },
        });
      } else if (prizeType === 2) {
        this.setState({
          showCoupon: true,
          couponInfo: {
            courseCouponName,
            courseCouponType,
            fixedAmountCouponFulfilMoney,
            lessNum,
            validityTime,
          },
        });
      }
      this.setState({
        key,
      });

      // delete target.isNew;
      // this.toggleEditable(e, key);
      // const { data } = this.state;
      // const { onChange } = this.props;
      // onChange(data);
      // this.setState({
      //   loading: false,
      //   data,
      // });
    }, 500);
  }

  cancel(e, key) {
    this.clickedCancel = true;
    e.preventDefault();
    const { data } = this.state;
    const newData = data.map(item => ({ ...item }));
    const target = this.getRowByKey(key, newData);
    if (this.cacheOriginData[key]) {
      Object.assign(target, this.cacheOriginData[key]);
      delete this.cacheOriginData[key];
    }
    target.editable = false;
    this.setState({ data: newData });
    this.clickedCancel = false;
  }

  handlePrice = (value, attr, record) => {
    const { key } = record;
    const { data } = this.state;
    const target = data.map((item) => {
      if (item.key === key) {
        item[attr] = value;
      }
      return item;
    });
    this.setState({
      data: target,
    });
  };

  handleProductOk = ({ productName, productId, productPicture }, reset) => {
    const { key, data } = this.state;
    if (! key) return;
    const target = this.getRowByKey(key) || {};
    target.productId = productId;
    target.productName = productName;
    target.productPicture = productPicture;
    target.editable = false;
    delete target.isNew;
    this.toggleEditable(undefined, key);
    const { onChange } = this.props;
    onChange(data);
    reset();
    this.setState({
      loading: false,
      data,
      showProduct: false,
      productInfo: {
        productId: null,
        productName: null,
        productPicture: [],
      },
    })
  };

  handleCouponOk = (
    {
      courseCouponName,
      courseCouponType,
      fixedAmountCouponFulfilMoney,
      lessNum,
      validityTime,
    }, reset) => {
    const { key, data } = this.state;
    if (! key) return;
    const target = this.getRowByKey(key) || {};
    target.courseCouponName = courseCouponName;
    target.courseCouponType = courseCouponType;
    target.fixedAmountCouponFulfilMoney = fixedAmountCouponFulfilMoney;
    target.lessNum = lessNum;
    target.validityTime = validityTime;
    target.editable = false;
    delete target.isNew;
    this.toggleEditable(undefined, key);
    const { onChange } = this.props;
    onChange(data);
    reset();
    this.setState({
      loading: false,
      data,
      showCoupon: false,
      couponInfo: {
        courseCouponName: '',
        courseCouponType: 2,
        fixedAmountCouponFulfilMoney: 0,
        lessNum: 0,
        validityTime: null,
      },
    })
  };

  handleProductCancel = (reset) => {
    reset();
    this.setState({
      showProduct: false,
      loading: false,
      productInfo: {
        productId: null,
        productName: null,
        productPicture: [],
      },
    })
  };

  handleCouponCancel = (reset) => {
    reset();
    this.setState({
      showCoupon: false,
      loading: false,
      couponInfo: {
        courseCouponName: '',
        courseCouponType: 2,
        fixedAmountCouponFulfilMoney: 0,
        lessNum: 0,
        validityTime: null,
      },
    })
  };

  changeInfo = (name, data) => {
    this.setState({
      [name]: data
    })
  };

  render() {
    const {
      handlePrice,
      handleProductOk,
      handleProductCancel,
      changeInfo,
      handleCouponOk,
      handleCouponCancel
    } = this;
    const {
      showCoupon,
      showProduct,
      productInfo,
      couponInfo
    } = this.state;
    const { currentOrg } = this.props;
    const columns = [
      {
        title: '名称',
        dataIndex: 'name',
        key: 'name',
        render: (text, record) => {
          const { prizeType } = record;
          if (record.editable) {
            return (
              <Input
                disabled={prizeType === 1}
                value={record[TYPE[prizeType].name] || ''}
                onChange={value => handlePrice(value.target.value, TYPE[prizeType].name, record)}
                placeholder="请输入名称"
              />
            );
          }
          return record[TYPE[prizeType].name] || '';
        },
      },
      {
        title: '数量',
        dataIndex: 'num',
        key: 'num',
        render: (text, record) => {
          if (record.editable) {
            return (
              <InputNumber
                onChange={value => handlePrice(value, 'num', record)}
                value={text}
                placeholder="请输入值"
                min={0}
                max={100}
              />
            );
          }
          return text;
        },
      },
      {
        title: '概率/%',
        dataIndex: 'winningRate',
        key: 'winningRate',
        render: (text, record) => {
          if (record.editable) {
            return (
              <InputNumber onChange={value => handlePrice(value, 'winningRate', record)} value={text} placeholder="请输入概率" min={0} />
            );
          }
          return text;
        },
      },
      {
        title: '类型',
        dataIndex: 'prizeType',
        key: 'prizeType',
        render: (text, record) => {
          if (record.editable) {
            return (
              <Select
                value={text}
                onChange={value => handlePrice(value, 'prizeType', record)}
                placeholder="请选择类型"
              >
                <Option value={1}>产品</Option>
                <Option value={2}>优惠券</Option>
              </Select>
            );
          }
          return optionsType[text];
        },
      },
      {
        title: '操作',
        key: 'action',
        render: (text, record) => {
          const { loading } = this.state;
          if (!!record.editable && loading) {
            return null;
          }
          if (record.editable) {
            if (record.isNew) {
              return (
                <span>
                  <a onClick={e => this.saveRow(e, record.key)}>添加</a>
                  <Divider type="vertical" />
                  <Popconfirm title="是否要删除此行？" onConfirm={() => this.remove(record.key)}>
                    <a>删除</a>
                  </Popconfirm>
                </span>
              );
            }
            return (
              <span>
                <a onClick={e => this.saveRow(e, record.key)}>保存</a>
                <Divider type="vertical" />
                <a onClick={e => this.cancel(e, record.key)}>取消</a>
              </span>
            );
          }
          return (
            <span>
              <a onClick={e => this.toggleEditable(e, record.key)}>编辑</a>
              <Divider type="vertical" />
              <Popconfirm title="是否要删除此行？" onConfirm={() => this.remove(record.key)}>
                <a>删除</a>
              </Popconfirm>
            </span>
          );
        },
      },
    ];

    const { loading, data } = this.state;
    return (
      <Fragment>
        <Table
          loading={loading}
          columns={columns}
          dataSource={data}
          pagination={false}
          rowKey={(record) => record.key}
          rowClassName={record => (record.editable ? styles.editable : '')}
        />
        <Button
          style={{ width: '100%', marginTop: 16, marginBottom: 8, display: data.length >= 8 ? 'none' : 'block' }}
          type="dashed"
          onClick={this.newMember}
          icon="plus"
        >
          新增产品
        </Button>

        <AddProduct
          visible={showProduct}
          currentOrg={currentOrg}
          url={url}
          {... productInfo}
          changeInfo={changeInfo}
          onOk={handleProductOk}
          onCancel={handleProductCancel}
        />

        <AddCoupon
          visible={showCoupon}
          {... couponInfo}
          onOk={handleCouponOk}
          onCancel={handleCouponCancel}
        />
      </Fragment>
    );
  }
}

export default connect(({ global: { currentOrg }, loading }) => ({
  currentOrg,
  loading: loading.global,
}))(TableForm);
