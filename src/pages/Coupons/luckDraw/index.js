import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import {
  Card,
  Table,
  Divider,
  Badge,
  Switch, Button,
} from 'antd';
import Hoc from '@/utils/Hoc';
import styles from '@/utils/global.less';
import SearchSel from '@/components/SearchSel';
import Authorized from '@/utils/Authorized';
import StandardTable from '@/components/DelTable';
import { url, parseTime } from '@/utils/utils';

const { check } = Authorized;
const FETCH_URL = 'coupons/getLuckDraw';
const OPEN_URL = 'coupons/openLuckDraw';
const CLOSE_URL = 'coupons/closeLuckDraw';
const statusText = {
  1: {
    status: 'success',
    text: '开启中'
  },
  0: {
    status: 'default',
    text: '关闭中'
  }
};

@connect(({ loading, coupons: { luckDrawInfo }, global: { currentOrg } }) => ({
  loading: loading.effects[FETCH_URL],
  luckDrawInfo,
  currentOrg
}))
@Hoc({ fetchUrl: FETCH_URL })
class LuckDraw extends  PureComponent{
  constructor() {
    super();
    this.state = {
      currentOrg: {
        id: null
      }
    };
  }

  componentDidMount() {
    // console.log(this.props);
  }

  // static getDerivedStateFromProps(nextProps, preState) {
  //   const { dispatch, currentOrg } = nextProps;
  //   if (!currentOrg.id) return null;
  //   if (currentOrg.id === preState.currentOrg.id) return null;
  //   dispatch({
  //     type: FETCH_URL,
  //     payload: {
  //       orgId: currentOrg.id,
  //     }
  //   });
  //   return {
  //     currentOrg
  //   }
  // }

  handleSwitch = (value, record) => {
    const data = {
      ... record
    };
    const { dispatch, currentOrg, pagination  } = this.props;
    if (value) {
      delete data.activitiesState;
      delete data.id;
      dispatch({
        type: OPEN_URL,
        payload: {
          orgId: currentOrg.id,
          pagination,
          values: {
            ... data,
            orgId: currentOrg.id
          },
          flag: true,
        }
      });
    } else {
      dispatch({
        type: CLOSE_URL,
        payload: {
          orgId: currentOrg.id,
          pagination,
        }
      });
    }
  };

  handleAdd = record => {
    const { dispatch } = this.props;
    dispatch(routerRedux.push({
      pathname: '/coupons/SharingLottery/mgr',
      state: record,
    }));
  };

  render() {
    const {
      luckDrawInfo,
      loading,
      handleStandardTableChange
    } = this.props;

    const columns = [
      {
        title: '名称',
        dataIndex: 'activityName',
      },
      {
        title: '状态',
        dataIndex: 'activitiesState',
        render(text) {
          return (
            <Badge status={statusText[text].status} text={statusText[text].text} />
          )
        }
      },
      {
        title: '操作',
        render:(text, record) => {
          // console.log(record);
          const { activitiesState } = record;
          return (
            <Fragment>
              <Switch
                style={{ display: activitiesState ? 'initial' : 'none' }}
                checked={!! activitiesState}
                onChange={e => this.handleSwitch(e, record)}
                checkedChildren="开"
                unCheckedChildren="关"
              />
              <Divider style={{ display: activitiesState ? 'initial' : 'none' }} type="vertical" />
              <a onClick={() =>this.handleAdd(record)}>编辑</a>
            </Fragment>

          )
        }
      }
    ];

    return (
      <section>
        <Card>
          <div className={styles.tableListOperator}>
            {check('活动管理-开启活动', (
              <Button icon="plus" type="primary" onClick={() =>this.handleAdd(undefined)}>
                新建
              </Button>
           ))}
          </div>
          <div className={styles.tableList}>
            <StandardTable
              bordered
              loading={loading}
              data={luckDrawInfo}
              columns={columns}
              onChange={handleStandardTableChange}
              // scroll={{ x: 1360 }}
            />
          </div>
        </Card>
      </section>
    )
  }
}

export default LuckDraw;
