import React, { PureComponent, Fragment } from 'react';
import {
  Card,
  Button,
  Form,
  Icon,
  Col,
  Row,
  Input,
  Popover,
  InputNumber,
  message,
  DatePicker,
  Select,
} from 'antd';
import moment from 'moment';
import { connect } from 'dva';
import FooterToolbar from '@/components/FooterToolbar';
import styles from '@/utils/global.less';
import TableForm from './TableForm';
import { url } from '@/utils/utils';


const { Option } = Select;
const { TextArea } = Input;
const fieldLabels = {
  activityName: '名称',
  endTime: '结束时间',
  shareNum: '分享次数',
};

class One2One extends PureComponent {

  constructor(props) {
    super(props);
    const { location: { state } } = this.props;
    let subList = [];
    // let fileList = [];
    if (state) {
      subList = state.marketingActivitiesPrizeList.map((items, index) => {
        const {
          productName,
          productId,
          num,
          winningRate,
          productPicture,
          prizeType,
          fixedAmountCouponFulfilMoney,
          lessNum,
          courseCouponName,
          courseCouponType,
          validityTime,
        } = items;

        // marketingActivitiesPrizeList
        return {
          key: `NEW_TEMP_ID_${index}112`,
          productName,
          productId,
          num,
          winningRate,
          productPicture,
          prizeType, // 1：产品 2：优惠券
          courseCouponName,
          courseCouponType,
          fixedAmountCouponFulfilMoney,
          lessNum,
          validityTime,
          editable: false,
          isNew: false,
          new: false,
        }
      });
      // const productPictureList = state.productPictureList;
      // if (productPictureList.length) {
      //   fileList = productPictureList.map((item, index) => (
      //     {
      //       url: item,
      //       name: `产品图片${index}`,
      //       uid: `${item}${index}`,
      //     }
      //   ))
      // }
    }
    this.state = {
      subList,
      // fileList,
    };
  }

  componentDidMount() {
    // setTimeout(() => {
    //   const { dispatch, currentOrg: { id } } = this.props;
    //   if (!id) return;
    //   dispatch({
    //     type: 'Inventory/getInventoryType',
    //     payload: {
    //       orgId: id,
    //       pagination: {
    //         pageNum: 1,
    //         pageSize: 1000,
    //       }
    //     },
    //   });
    // },200)
  }

  handleTabChange = (item) => {
    // console.log(item);
    const subList = item.filter(items => !items.editable);
    this.setState({
      subList,
    });
  };

  // handleUpload = (file) => {
  //   const { name, size } = file;
  //   const Rxp = /.+\.(jpg|jpeg|png|bmp)$/;
  //   if (!Rxp.test(name)) {
  //     message.error('请上传正确的图片文件 仅支持 jpg | png | jpeg | bmp ', 3);
  //     return false;
  //   } if ((size / 1024) > 6000) {
  //     message.error('图片文件大于 5M', 3);
  //     return false;
  //   }
  //   return true;
  // };

  // handleRemove = (file) => {
  //   const { form: { resetFields } } = this.props;
  //   resetFields(['subjectPictureUrl']);
  //   const { uid } = file;
  //   const fileList = this.state.fileList.filter(item => item.uid !== uid);
  //   this.setState({
  //     fileList,
  //   });
  //   return true;
  // };

  // handleChange = ({ file, fileList} ) => {
  //   let state = false;
  //   const { name, size, status } = file;
  //   const Rxp = /.+\.(jpg|jpeg|png|bmp)$/;
  //   if (!Rxp.test(name) || (size / 1024) > 3000) {
  //     return;
  //   }
  //   let fileList1 = fileList;
  //   fileList1 = fileList1.map((item) => {
  //     if (item.response) {
  //       item.url = item.response.result;
  //     }
  //     return item;
  //   });
  //   fileList1 = fileList1.filter((item) => {
  //     const { response } = item;
  //     if (response) {
  //       const { status: states, message: msg } = response;
  //       !states ? message.error(msg) : '';
  //       state = states;
  //       return states === true;
  //     }
  //     return true;
  //   });
  //   (status === 'done' && state) ? message.success('上传成功') : '';
  //   this.setState({ fileList: fileList1 });
  // };


  fixCommit = (data) => {
    const { marketingActivitiesPrizeList, endTime } = data;

    const list = marketingActivitiesPrizeList.map((item) => {
      const {
        productId,
        num,
        winningRate,
        prizeType,
        fixedAmountCouponFulfilMoney,
        lessNum,
        courseCouponName,
        courseCouponType,
        validityTime,
      } = item;
      const dataInfo = {
        winningRate,
        num,
        prizeType
      };
      if (prizeType === 1) {
        dataInfo.productId = productId;
      }
      if (prizeType === 2) {
        dataInfo.courseCouponName = courseCouponName;
        dataInfo.courseCouponType = courseCouponType;
        dataInfo.fixedAmountCouponFulfilMoney = fixedAmountCouponFulfilMoney;
        dataInfo.lessNum = lessNum;
        dataInfo.validityTime = validityTime;
      }

      return dataInfo
    });

    data.endTime = endTime.toDate().getTime();

    return {
      ...data,
      marketingActivitiesPrizeList: list,
    };
  };

  validate = () => {
    const { form: { validateFieldsAndScroll }, location: { state }, dispatch } = this.props;
    validateFieldsAndScroll((error, values) => {
      if (!error) {
        const { subList } = this.state;
        values.marketingActivitiesPrizeList = subList;
        values = this.fixCommit(values);

        if (!state) {
          delete values.activitiesState;
          delete values.id;
        }
        dispatch({
          type: 'coupons/openLuckDraw',
          payload: {
            values
          },
        });
      }
    });
  };

  render() {
    const {
      form,
      loading,
      currentOrg,
      location,
      dispatch,
    } = this.props;
    const { getFieldDecorator, getFieldsError } = form;
    const { state } = location;
    const { subList } = this.state;
    const {
      activityName,
      endTime,
      shareNum,
    } = fieldLabels;
    const errors = getFieldsError();
    const getErrorInfo = () => {
      const errorCount = Object.keys(errors).filter(key => errors[key]).length;
      if (!errors || errorCount === 0) {
        return null;
      }
      const scrollToField = (fieldKey) => {
        const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
        if (labelNode) {
          labelNode.scrollIntoView(true);
        }
      };
      const errorList = Object.keys(errors).map((key) => {
        if (!errors[key]) {
          return null;
        }
        return (
          <li key={key} className={styles.errorListItem} onClick={() => scrollToField(key)}>
            <Icon type="cross-circle-o" className={styles.errorIcon} />
            <div className={styles.errorMessage}>{errors[key][0]}</div>
            <div className={styles.errorField}>{fieldLabels[key]}</div>
          </li>
        );
      });
      return (
        <span className={styles.errorIcon}>
          <Popover
            title="表单校验信息"
            content={errorList}
            overlayClassName={styles.errorPopover}
            trigger="click"
            getPopupContainer={trigger => trigger.parentNode}
          >
            <Icon type="exclamation-circle" />
          </Popover>
          {errorCount}
        </span>
      );
    };

    return (
      <Fragment>
        <Card title="产品基础信息" className={styles.Card} bordered={false}>
          <Form layout="vertical">

            {getFieldDecorator('orgId', {
              initialValue: state && state.orgId ? state.orgId : (currentOrg.id || ''),
            })(
              <Input type="hidden" />
            )}

            <Row gutter={16}>
              <Col lg={8} md={8} sm={24}>
                <Form.Item label={activityName}>
                  {getFieldDecorator('activityName', {
                    initialValue: state ? state.activityName : '',
                    rules: [{ required: true, message: `请输入${activityName}` }],
                  })(
                    <Input placeholder={activityName} />
                  )}
                </Form.Item>
              </Col>
              <Col lg={8} md={8} sm={24}>
                <Form.Item label={shareNum}>
                  {getFieldDecorator('shareNum', {
                    initialValue: state ? state.shareNum : 1,
                    rules: [{ required: true, message: `请输入${shareNum}` }],
                  })(
                    <InputNumber placeholder={shareNum} style={{ width: '100%' }} min={0} max={99999} />
                  )}
                </Form.Item>
              </Col>

              <Col lg={8} md={12} sm={24}>
                <Form.Item label={endTime}>
                  {getFieldDecorator('endTime', {
                    initialValue: state ? moment(state.endTime) : moment(),
                    rules: [
                      { required: false, message: 'endTime' }],
                  })(
                    <DatePicker style={{ width: '100%' }} />
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Card>

        <Card title="产品列表" className={styles.Card} bordered={false}>
          <Form.Item label="">
            {getFieldDecorator('marketingActivitiesPrizeList', {
              initialValue: state || subList.length ? { dataList: subList } : [],
              rules: [{ required: true, message: '请选择商品' }],
            })(
              <TableForm dispatch={dispatch} onChange={this.handleTabChange} />
            )}
          </Form.Item>
        </Card>
        <FooterToolbar style={{ width: '100%' }}>
          {getErrorInfo()}
          <Button
            type="primary"
            onClick={this.validate}
            loading={loading}
          >
            提交
          </Button>
        </FooterToolbar>
      </Fragment>
    );
  }
}

export default connect(({ global, loading }) => ({
  currentOrg: global.currentOrg,
  loading: loading.global
}))(Form.create()(One2One));
