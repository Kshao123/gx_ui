import React, { Component } from 'react';
import {
  Modal,
  DatePicker,
  InputNumber,
  Icon,
  Tooltip,
  Form,
  Select,
} from 'antd';
import moment from 'moment';

const { Option } = Select;

@Form.create()
class FormModal extends Component{
  state = {
    isFix: false,
  };

  handleFixChange = (values) => {
    let isFix = false;
    if (values === 1) {
      isFix = true
    }
    this.setState({
      isFix,
    })
  };

  render() {
    const {
      visible,
      handleOk,
      handleCancel,
      form,
      formObj={},
    } = this.props;
    const { getFieldDecorator } = form;
    const { isFix } = this.state;
    return (
      <Modal
        title="添加优惠券"
        visible={visible}
        onOk={() => handleOk(form)}
        onCancel={() => handleCancel(form)}
      >
        <Form layout="vertical">
          <Form.Item label="优惠券类型">
            {getFieldDecorator('courseCouponType', {
              initialValue: formObj.courseCouponType || 2,
              rules: [{ required: true, message: '请选择类型' }],
            })(
              <Select onChange={this.handleFixChange}>
                <Option value={2}>比例</Option>
                <Option value={1}>固定数额</Option>
              </Select>
            )}
          </Form.Item>

          <Form.Item label={(
            <span>
                额度 / 元&nbsp;
              <Tooltip title="固定数额优惠卷使用需要满足的金额">
                <Icon type="question-circle-o" />
              </Tooltip>
            </span>
          )}
          >
            {getFieldDecorator('fixedAmountCouponFulfilMoney', {
              initialValue: formObj.fixedAmountCouponFulfilMoney || 0,
              rules: [{ required: isFix, message: '请输入金额' }],
            })(
              <InputNumber style={{ width: '100%' }} />
            )}
          </Form.Item>

          <Form.Item
            label={(
              <span>
                优惠金额 / [ 元 | % ]&nbsp;
                <Tooltip title="根据比例，固定金额类型输入正确的 金额或者比例">
                  <Icon type="question-circle-o" />
                </Tooltip>
              </span>
            )}
          >
            {getFieldDecorator('lessNum', {
              initialValue: formObj.lessNum || 0,
              rules: [{ required: true, message: '请输入金额或比例' }],
            })(
              <InputNumber style={{ width: '100%' }} />
            )}
          </Form.Item>

          <Form.Item
            label="数量"
          >
            {getFieldDecorator('num', {
              initialValue: formObj.num || 0,
              rules: [{ required: true, message: '请输入数量' }],
            })(
              <InputNumber style={{ width: '100%' }} />
            )}
          </Form.Item>

          <Form.Item
            label="有效期"
          >
            {getFieldDecorator('validityTime', {
              initialValue: formObj.validityTime ? moment(formObj.validityTime) : moment(),
              rules: [{ required: true, message: '请选择日期' }],
            })(
              <DatePicker />
            )}
          </Form.Item>
        </Form>
      </Modal>
    )
  }
}

export default FormModal
