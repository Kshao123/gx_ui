import React, { Fragment, PureComponent } from 'react';
import { connect } from 'dva';
import {
  Card,
  Row,
  Col,
  Form,
  Button,
  DatePicker,
  Select,
  Tooltip,
  Icon,
  message,
} from 'antd';
import moment from 'moment';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import Hoc from '@/utils/Hoc';
import { url, parseTime } from '@/utils/utils';
import SearchSel from '@/components/SearchSel';
import Authorized from '@/utils/Authorized';
import Ellipsis from '@/components/Ellipsis';
import FormModal from './formModal';
import Popcon from '@/components/Popconfirm';

const { check } = Authorized;
const { Option } = Select;
const FormItem = Form.Item;
const TYPE = {
  Gen: {
    1: '固定数额',
    2: '比例',
  },
  course: {
    1: '一对一',
    2: '一对多'
  }
};

@connect(({ coupons: { generates }, global, loading }) => ({
  generates,
  currentOrg: global.currentOrg,
  loading: loading.effects['coupons/getGenerates'],
}))
@Form.create()
@Hoc({ fetchUrl: 'coupons/getGenerates' })

class Generates extends PureComponent {
  constructor() {
    super();
    this.state = {
      formValues: {},
      formObj: {},
      visible: false,
      expandForm: false,
    };
  }

  handleFormReset = () => {
    const { dispatch, pagination, currentOrg, form  } = this.props;
    if (currentOrg.id) {
      form.resetFields();
      this.setState({
        formValues: {},
      });
      dispatch({
        type: 'coupons/getGenerates',
        payload: {
          pagination,
          orgId: currentOrg.id,
        },
      });
    }
  };

  handleSearch = (e) => {
    e.preventDefault();
    const { dispatch, currentOrg, form } = this.props;
    const pagination = {
      pageNum: 1,
      pageSize: 10,
    };
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const { stuName, selectTime, timeType, useType, validityType } = fieldsValue;
      const query = {};
      if (stuName) {
        query.stuName = stuName.label.split('-')[0]
      }
      if (timeType >= 1) {
        query.timeType = timeType;
      }
      if (useType >= 1) {
        query.useType = useType;
      }
      if (validityType >= 1) {
        query.validityType = validityType;
      }
      if (selectTime) {
        query.selectTime = selectTime.toDate().getTime();
      }
      this.setState({
        formValues: query,
      });
      // console.log(query);
      dispatch({
        type: 'coupons/getGenerates',
        payload: {
          pagination,
          orgId: currentOrg.id,
          query,
        },
      })
    });
  };


  footer = (data) => {
    if (!data.length) return '';
    const info = data[0];
    return (
      <div style={{ width: '100%', display: 'flex' }}>
        <div style={{ flex: 1 }}>
          <span>减免总金额：</span>
          <span>{info.allLessMoney || 0} 元</span>
        </div>
        <div style={{ flex: 1 }}>
          <span>
            消费总金额：
          </span>
          <span>
            {info.allActualMoney || 0} 元
          </span>
        </div>
      </div>
    )
  };

  handleOk = (thatForm) => {
    const { form, dispatch, pagination, currentOrg } = this.props;
    const { formValues } = this.state;
    thatForm.validateFields((err, values) => {
      if (err) {
        return;
      }
      const { validityTime, courseCouponType, fixedAmountCouponFulfilMoney } = values;
      if (courseCouponType === 1 && !fixedAmountCouponFulfilMoney) {
        message.error('请输入正确的金额！');
        return;
      }
      dispatch({
        type: 'coupons/addGenerates',
        payload: {
          values: {
            ... values,
            validityTime: validityTime.toDate().getTime(),
            orgId: currentOrg.id,
          },
          query: {
            pagination,
            orgId: currentOrg.id,
            query: formValues,
          }
        }
      }).then((res) => {
        if (!res) return;
        form.resetFields();
        this.setState({
          visible: false,
        })
      })
    })
  };

  handleCancel = () => {
    this.props.form.resetFields();
    this.setState({
      visible: false,
    })
  };

  handleAdd = () => {
    this.setState({
      visible: true,
    })
  };

  toggleForm = () => {
    const { expandForm } = this.state;
    this.setState({
      expandForm: !expandForm,
    });
  };

  handleCopy = (text) => {
    const input = document.createElement('input');
    document.body.appendChild(input);
    input.setAttribute('value', text);
    input.select();
    if (document.execCommand('copy')) {
      document.execCommand('copy');
      message.success('复制成功')
    }
    document.body.removeChild(input);
  };

  handleDelete = (record, flag) => {
    let courseCouponIds = [];
    if (flag) {
      courseCouponIds = record.length ? record : [];
    } else {
      const { id } = record;
      courseCouponIds.push(id);
    }
    const { dispatch, pagination, currentOrg } = this.props;
    const { formValues } = this.state;
    dispatch({
      type: 'coupons/delGenerates',
      payload: {
        courseCouponIds,
        query: {
          query: formValues,
          orgId: currentOrg.id,
          pagination,
        }
      }
    });
  };

  handleSelectRows = (rows) => {
    this.setState({
      selectedRows: rows,
    });
  };

  renderSearchForm() {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    const teaProps = {
      removal: true,
      queryUrl: `${url}/stuConsult/queryStu`,
      keyValue: 'name',
      showSearch: true,
      attr: {
        id: 'stuId',
        name: 'stuName',
      },
      placeholder: '学生姓名',
      allowClear: true,
    };

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="学生">
              {getFieldDecorator('stuName')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...teaProps} />
              )}
            </FormItem>
          </Col>
          <Col md={16} sm={24}>
            <FormItem label="时间">
              {getFieldDecorator('selectTime',{
                initialValue: moment(),
              })(
                <DatePicker />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">查询</Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
                展开 <Icon type="down" />
              </a>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderAdvanceForm() {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    const teaProps = {
      removal: true,
      queryUrl: `${url}/stuConsult/queryStu`,
      keyValue: 'name',
      showSearch: true,
      attr: {
        id: 'stuId',
        name: 'stuName',
      },
      placeholder: '学生姓名',
      allowClear: true,
    };

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="学生">
              {getFieldDecorator('stuName')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...teaProps} />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="使用类型">
              {getFieldDecorator('useType', {
                initialValue: 1,
              })(
                <Select>
                  <Option value={1}>全部</Option>
                  <Option value={2}>未领取</Option>
                  <Option value={3}>未使用</Option>
                  <Option value={4}>已使用</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="有效类型">
              {getFieldDecorator('validityType', {
                initialValue: 1,
              })(
                <Select>
                  <Option value={1}>全部</Option>
                  <Option value={2}>有效</Option>
                  <Option value={3}>失效</Option>
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col md={8} sm={24}>
            <FormItem label="时间">
              {getFieldDecorator('selectTime',{
                initialValue: moment(),
              })(
                <DatePicker />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="时间类型">
              {getFieldDecorator('timeType', {
                initialValue: 2,
              })(
                <Select>
                  <Option value={1}>周</Option>
                  <Option value={2}>月</Option>
                  <Option value={3}>年</Option>
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">查询</Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
                收起 <Icon type="up" />
              </a>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderForm() {
    const { expandForm } = this.state;
    return expandForm ? this.renderAdvanceForm() : this.renderSearchForm();
  }

  render() {
    const {
      forItem,
      generates,
      loading,
      handleStandardTableChange,
    } = this.props;
    const {
      formValues,
      formObj,
      visible,
      selectedRows,
    } = this.state;
    const columns = [
      {
        title: 'id',
        dataIndex: 'id',
        align: 'center',
        render: function(text) {
          return (
            <div onClick={() => this.handleCopy(text)}>
              <Ellipsis length={12} tooltip>
                {text}
              </Ellipsis>
            </div>
          )
        }.bind(this),
      },
      {
        title: '类型',
        dataIndex: 'courseCouponType',
        align: 'center',
        render(text) {
          return text ? forItem('icon-leixing', `${TYPE.Gen[text]}` ) : '';
        },
      },
      {
        title: '生成账户',
        dataIndex: 'createUserName',
        align: 'center',
        render(text) {
          return text ? forItem('icon-icon-', text) : '';
        },
      },
      {
        title: '使用时间',
        dataIndex: 'useTime',
        align: 'center',
        render(text) {
          return forItem('icon-shijian', text ? parseTime(text, '{d}日 {h}:{i}') : '暂无信息' );
        },
      },
      {
        title: '课程',
        dataIndex: 'courseName',
        align: 'center',
        width: 165,
        render(text, record) {
          return (
            <Tooltip title={record.courseCouponType ? `课程类型：${TYPE.course[record.courseCouponType]}` : ''}>
              {  text || '暂无信息' }
            </Tooltip>
          )
        },
      },
      {
        title: '学生名称',
        dataIndex: 'stuName',
        align: 'center',
        render(text) {
          return forItem('icon-mingcheng', text || '暂无信息' );
        },
      },
      {
        title: '课时开始',
        dataIndex: 'participateClassTimeStart',
        align: 'center',
        render(text) {
          return forItem('icon-kemu', text || '暂无信息' );
        },
      },
      {
        title: '课时结束',
        dataIndex: 'participateClassTimeEnd',
        align: 'center',
        render(text) {
          return forItem('icon-kemu', text || '暂无信息' );
        },
      },
      {
        title: '减免金额 / 元',
        dataIndex: 'lessMoney',
        align: 'center',
        render(text) {
          return forItem('icon-tichengshezhi', !text ? '暂无信息' : `${text} 元`);
        }
      },
      {
        title: '实际消费 / 元',
        dataIndex: 'actualMoney',
        align: 'center',
        render(text) {
          return forItem('icon-jiage', !text ? '暂无信息' : `${text} 元`);
        }
      },
      {
        title: (
          <Tooltip title="只能删除未领取的优惠券哦">
            <span>操作 <Icon type="question-circle" /></span>
          </Tooltip>

        ),
        fixed: 'right',
        width: 85,
        render: (text, record) => (
          <Fragment>
            {check('优惠卷:增加', (
              <Popcon
                title="是否确认删除"
                onConfirm={() => this.handleDelete(record)}
              />
            ))}
          </Fragment>
        ),
      },
    ];

    return (
      <section>
        <Card className={styles.tableListForm}>
          {this.renderForm()}
        </Card>
        <Card>
          <div className={styles.tableListOperator}>
            {check('优惠卷:增加', (
              <Button icon="plus" type="primary" onClick={this.handleAdd}>
                新建
              </Button>
            ))}
          </div>
          <div className={styles.tableList}>
            <StandardTable
              showAllSel
              Rowselect
              bordered
              // selectedRows={selectedRows}
              // onSelectRow={this.handleSelectRows}
              loading={loading}
              data={generates}
              columns={columns}
              onChange={e => handleStandardTableChange(e, formValues)}
              footer={this.footer}
              scroll={{ x: 1360 }}
              onDel={keys => this.handleDelete(keys, true)}
            />
          </div>
        </Card>
        <FormModal
          visible={visible}
          handleOk={this.handleOk}
          handleCancel={this.handleCancel}
          formObj={formObj}
        />
      </section>
    );
  }
}
export default Generates;

