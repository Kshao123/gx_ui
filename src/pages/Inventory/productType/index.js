import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Card,
  Form,
  Input,
  Button,
  List,
  Modal,
  Spin, Row, Col,
} from 'antd';
import styles from './index.less';
import Popcon from '@/components/Popconfirm';
import Hoc from '@/utils/Hoc';
import Authorized from '@/utils/Authorized';
import SearchSel from '@/components/SearchSel';
import { url } from '@/utils/utils';

const { check } = Authorized;

@connect(({ global, Inventory, loading }) => ({
  currentOrg: global.currentOrg,
  loading: loading.effects['Inventory/getInventoryType'],
  Inventory,
}))
@Form.create()
@Hoc({ fetchUrl: 'Inventory/getInventoryType' })
class Inventory extends PureComponent {
  constructor() {
    super();
    this.state = {
      visible: false,
      isEdit: false,
      FormObj: {},
      productTypeName: '',
    };
  }

  handleEdit = (item, value) => {
    let str = false;
    let obj = { id: '' };
    if (value) {
      str = true;
      obj = item;
    }
    this.setState({
      visible: true,
      isEdit: str,
    });
    setTimeout(() => {
      this.setState({
        FormObj: obj,
      });
    }, 150);
  };

  handleDelete = ({ id }) => {
    const { productTypeName } = this.state;
    const { dispatch, pagination, currentOrg } = this.props;
    dispatch({
      type: 'Inventory/delInventoryType',
      payload: {
        idList: [id],
        pagination,
        orgId: currentOrg.id,
        productTypeName,
      },
    });
  };

  handleOk = () => {
    const { form, dispatch, pagination, currentOrg } = this.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      if (!this.state.isEdit) {
        delete values.id;
      }
      dispatch({
        type: 'Inventory/addInventoryType',
        payload: {
          data: values,
          pagination,
          orgId: currentOrg.id,
        },
      });

      // 在此处调用的是 重置表单内容， 不重置的话会导致 对话框再次打开不能实时更新内容的bug
      form.resetFields();
      this.setState({
        visible: false,
        isEdit: false,
        FormObj: {},
      });
    });
  };

  handleCancel = () => {
    this.props.form.resetFields();
    this.setState({
      visible: false,
      isEdit: false,
      FormObj: {},
    });
  };

  Info = (text) => (
    <div className={styles.textImg}>
      <p>{text.toString()[0]}</p>
      <p>{text}</p>
    </div>
  );

  handleValueChange = (value) => {
    const { dispatch } = this.props;
    const { currentOrg: { id: orgId }, pagination } = this.props;
    if (!value) {
      dispatch({
        type: 'Inventory/getInventoryType',
        payload: {
          orgId,
          productTypeName: '',
          pagination,
        },
      });
      return;
    }

    const { key, label: productTypeName } = value;
    this.setState({
      productTypeName,
    });
    dispatch({
      type: 'Inventory/getInventoryType',
      payload: {
        orgId,
        productTypeName,
        pagination,
      },
    })
  };

  renderForm() {
    const { currentOrg } = this.props;
    const props = {
      removal: true,
      onChange: this.handleValueChange,
      queryUrl: `${url}/inventoryManagement/displayProductType`,
      keyValue: 'productTypeName',
      showSearch: true,
      attr: {
        id: 'id',
        name: 'productTypeName',
      },
      placeholder: '请输入名称',
      allowClear: true,
    };

    return (
      <Form layout="inline">
        <Row>
          <Col md={8} sm={24}>
            <Form.Item label="名称">
              <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...props} />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const {
      Inventory: { InventoryTypes: { list, pagination } },
      loading,
      form,
      currentOrg,
      handleStandardTableChange,
    } = this.props;
    const { getFieldDecorator } = form;
    const { isEdit, FormObj } = this.state;
    return (
      <section>

        <Card className={styles.tableListForm}>
          {this.renderForm()}
        </Card>

        <Card
          className={styles.listCard}
          bordered={false}
          // title="产品类别"
          style={{ marginTop: 15 }}
          bodyStyle={{ padding: '0 32px 40px 32px' }}
        >
          {check('库存管理-类别管理:增加', (
            <Button
              type="dashed"
              style={{ width: '100%', marginBottom: 8, marginTop: 15 }}
              icon="plus"
              onClick={this.handleEdit}
            >
              添加
            </Button>
          ))}
          <List
            size="large"
            rowKey="id"
            loading={loading}
            pagination={
              { ...pagination, onChange: handleStandardTableChange }
            }
            dataSource={list}
            renderItem={item => (
              <List.Item
                actions={[
                  <div>
                    {check('库存管理-类别管理:增加', (
                      <a
                        onClick={(e) => {
                          e.preventDefault();
                          this.handleEdit(item, true);
                        }}
                      >
                        编辑
                      </a>
                    ))}
                  </div>,
                  <div>
                    {check('库存管理-类别管理:删除', (
                      <Popcon
                        title="是否确认删除"
                        onConfirm={() => this.handleDelete(item)}
                      />
                    ))}
                  </div>,
                ]}
              >
                {this.Info(item.productTypeName)}
              </List.Item>
            )}
          />
        </Card>

        <Modal
          title="添加/修改产品类别"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Spin spinning={!Object.keys(FormObj).length}>
            <Form layout="vertical">
              {getFieldDecorator('id', {
                initialValue: isEdit ? FormObj.id : '' })(
                  <Input type="hidden" />
              )}
              {getFieldDecorator('orgId', {
                initialValue: currentOrg ? currentOrg.id : '' })(
                  <Input type="hidden" />
              )}
              <Form.Item label="产品类别">
                {getFieldDecorator('productTypeName', {
                  initialValue: isEdit ? FormObj.productTypeName : '',
                  rules: [{ required: true, message: '请输入类型' }],
                })(
                  <Input placeholder="产品类别" />
                )}
              </Form.Item>
            </Form>
          </Spin>
        </Modal>
      </section>
    );
  }
}

export default Inventory;
