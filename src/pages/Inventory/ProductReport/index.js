import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Card,
  Row,
  Col,
  Form,
  Button,
} from 'antd';
import monent from 'moment';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import Hoc from '@/utils/Hoc';

import { url, getTimeDistance } from '@/utils/utils';
import SearchSel from '@/components/SearchSel';
import styles1 from './Analysis.less';
import SalesCard from './SalesCard';

const FormItem = Form.Item;
const FetchUrl = 'Inventory/getProductReport';

@connect(({ Inventory: { productReport }, global, loading }) => ({
  productReport,
  currentOrg: global.currentOrg,
  loading: loading.effects[FetchUrl],
}))
@Form.create()
@Hoc({ fetchUrl: FetchUrl })
class ProductReport extends PureComponent {
  constructor() {
    super();
    this.state = {
      name: '',
      rangePickerValue: monent(new Date()),
    };
  }

  handleValueChange = (value, name, data) => {
    const { list } = data;
    const { label } = value;

    if (!list.length) return;
    if (list.length >= 2) {
      data.list = list.filter(item => item.productName === label);
    }
    this.setState({
      [name]: label,
    });
    const { dispatch } = this.props;
    dispatch({
      type: 'Inventory/_searchData',
      payload: {
        type: 'saveProductReport',
        value: data
      },
    });
  };

  handleFormReset = () => {
    const { dispatch, form, currentOrg, pagination, } = this.props;
    const { rangePickerValue } = this.state;
    if (!currentOrg.id) return;
    this.setState({
      name: null,
      rangePickerValue: monent(new Date())
    });
    form.resetFields();
    dispatch({
      type: FetchUrl,
      payload: {
        pagination,
        orgId: currentOrg.id,
        time: rangePickerValue.toDate().getTime(),
      },
    });
  };

  isActive = type => {
    const { rangePickerValue } = this.state;
    const value = getTimeDistance(type);
    if (
      rangePickerValue.isSame(value[0], 'month')
    ) {
      return styles1.currentDate;
    }
    return '';
  };

  handleRangePickerChange = rangePickerValue => {
    const { dispatch, currentOrg, pagination } = this.props;

    const { name } = this.state;
    const query = {
      time: rangePickerValue.toDate().getTime(),
    };
    if (name && name.length) query.productName = name;
    this.setState({
      rangePickerValue,
    });
    dispatch({
      type: FetchUrl,
      payload: {
        pagination,
        orgId: currentOrg.id,
        query
      },
    });
  };

  selectDate = type => {
    const { dispatch, currentOrg, pagination } = this.props;
    const rangePickerValue = monent(getTimeDistance(type)[0]);
    const { name } = this.state;
    const query = {
      time: rangePickerValue.toDate().getTime(),
    };
    if (name && name.length) query.productName = name;
    this.setState({
      rangePickerValue,
    });
    dispatch({
      type: FetchUrl,
      payload: {
        pagination,
        orgId: currentOrg.id,
        query,
      },
    });
  };


  renderSearchForm() {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    return (
      <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="产品">
              {getFieldDecorator('productName')(
                <SearchSel
                  showSearch
                  keyValue="productName"
                  attr={{
                    id: 'name',
                    name: 'productName',
                  }}
                  placeholder="查询产品"
                  queryUrl={`${url}/productSales/displayProductSalesInfoInMonth`}
                  orgId={currentOrg ? currentOrg.id : ''}
                  onChange={this.handleValueChange}
                />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>重置</Button>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const {
      forItem,
      productReport,
      loading,
      handleStandardTableChange
    } = this.props;

    const columns = [
      {
        title: '名称',
        dataIndex: 'productName',
        align: 'left',
        render(text) {
          return forItem('icon-xingming', text);
        },
      },
      {
        title: '种类',
        dataIndex: 'productTypeName',
        align: 'center',
        render(text) {
          return forItem('icon-kemu', text);
        },
      },
      {
        title: '数量',
        dataIndex: 'productSalesNum',
        align: 'center',
        render(text) {
          return forItem('icon-leixing', text);
        }
      },
      {
        title: '总额',
        dataIndex: 'productSalesMoney',
        align: 'center',
        render(text) {
          return forItem('icon-tichengshezhi', text);
        }
      },
    ];

    const { rangePickerValue, salesData, name } = this.state;
    return (
      <section>
        <Card className={styles.tableListForm}>
          {this.renderSearchForm()}
        </Card>
        <Card>
          <SalesCard
            rangePickerValue={rangePickerValue}
            salesData={salesData}
            isActive={this.isActive}
            handleRangePickerChange={this.handleRangePickerChange}
            loading={loading}
            selectDate={this.selectDate}
          />
          <div className={styles.tableList}>
            <StandardTable
              bordered
              loading={loading}
              data={productReport}
              columns={columns}
              onChange={e => handleStandardTableChange(e, { productName: name, time: rangePickerValue.toDate().getTime() })}
              // scroll={{ x: 650 }}
            />
          </div>
        </Card>
      </section>
    );
  }
}
export default ProductReport;
