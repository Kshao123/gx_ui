import React, { memo } from 'react';
import { DatePicker } from 'antd';
import styles from './Analysis.less';

const { MonthPicker } = DatePicker;

const SalesCard = memo(
  ({ rangePickerValue, isActive, handleRangePickerChange, selectDate }) => (
    <div className={styles.salesExtraWrap}>
      <div className={styles.salesExtra}>
        <a className={isActive('month')} onClick={() => selectDate('month')}>
          本月
        </a>
      </div>
      <MonthPicker
        value={rangePickerValue}
        onChange={handleRangePickerChange}
        style={{ width: 256 }}
        format="YYYY/MM"
      />
    </div>
  )
);

export default SalesCard;
