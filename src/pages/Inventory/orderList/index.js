import React, { Fragment, PureComponent } from 'react';
import { connect } from 'dva';
import {
  Card,
  Row,
  Col,
  Form,
  Button,
  DatePicker,
  Select,
  Tooltip,
  Icon,
  message,
  Popconfirm,
  Badge,
  Input,
  Divider,
} from 'antd';
import moment from 'moment';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import Hoc from '@/utils/Hoc';
import { url, parseTime } from '@/utils/utils';
import SearchSel from '@/components/SearchSel';
import Authorized from '@/utils/Authorized';
import Ellipsis from '@/components/Ellipsis';
import BuyProduct from './BuyProduct';

const { check } = Authorized;
const { Option } = Select;
const { RangePicker } = DatePicker;
const FormItem = Form.Item;
const FETCH_URL = 'Inventory/getProductOrder';

@connect(({ Inventory: { orderList }, global, loading }) => ({
  orderList,
  currentOrg: global.currentOrg,
  loading: loading.effects['Inventory/getProductOrder'],
}))
@Form.create()
@Hoc({ fetchUrl: FETCH_URL })

class Generates extends PureComponent {
  constructor() {
    super();
    this.state = {
      formValues: {},
      formObj: {},
      visible: false,
      expandForm: false,
    };
  }

  handleFormReset = () => {
    const { dispatch, currentOrg, form  } = this.props;
    if (currentOrg.id) {
      form.resetFields();
      this.setState({
        formValues: {},
      });
      dispatch({
        type: FETCH_URL,
        payload: {
          pagination: {
            pageSize: 10,
            pageNum: 1
          },
          orgId: currentOrg.id,
        },
      });
    }
  };

  handleSearch = (e) => {
    e.preventDefault();
    const { dispatch, currentOrg, form } = this.props;
    const pagination = {
      pageNum: 1,
      pageSize: 10,
    };
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const { productName, selectTime, productOrderId, isDeliverProduct, isRefund } = fieldsValue;
      const query = {};
      if (productName) {
        query.productName = productName.label.split('-')[0]
      }
      if (productOrderId) {
        query.productOrderId = productOrderId;
      }

      if (isDeliverProduct !== undefined) {
        query.isDeliverProduct = !! isDeliverProduct;
      }

      if (isRefund !== undefined) {
        query.isRefund = !! isRefund;
      }

      if (selectTime) {
        const [ timeS, timeE ] = selectTime;
        query.startTime = timeS.toDate().getTime();
        query.endTime = timeE.toDate().getTime();
      }
      this.setState({
        formValues: query,
      });

      dispatch({
        type: FETCH_URL,
        payload: {
          pagination,
          orgId: currentOrg.id,
          query,
        },
      })
    });
  };

  handleOk = (query, reset) => {
    const { dispatch, pagination, currentOrg: { id: orgId } } = this.props;
    const { formValues } = this.state;
    dispatch({
      type: 'Inventory/buyProduct',
      payload: {
        data: {
          ... query,
          orgId
        },

        query: {
          pagination,
          query: formValues,
          orgId
        }
      },
    })
      .then(res => {
        if (!res) return;
        this.setState({
          visible: false,
        });
        reset()
      })
  };

  handleCancel = reset => {
    reset();
    this.setState({
      visible: false,
    })
  };

  handleAdd = () => {
    this.setState({
      visible: true,
    })
  };

  toggleForm = () => {
    const { expandForm } = this.state;
    this.setState({
      expandForm: !expandForm,
    });
  };

  handleCopy = (text) => {
    const input = document.createElement('input');
    document.body.appendChild(input);
    input.setAttribute('value', text);
    input.select();
    if (document.execCommand('copy')) {
      document.execCommand('copy');
      message.success('复制成功')
    }
    document.body.removeChild(input);
  };

  handleDelete = (record, flag) => {
    let courseCouponIds = [];
    if (flag) {
      courseCouponIds = record.length ? record : [];
    } else {
      const { id } = record;
      courseCouponIds.push(id);
    }
    const { dispatch, pagination, currentOrg } = this.props;
    const { formValues } = this.state;
    dispatch({
      type: 'coupons/delGenerates',
      payload: {
        courseCouponIds,
        query: {
          query: formValues,
          orgId: currentOrg.id,
          pagination,
        }
      }
    });
  };

  handlePay = (productOrderId, type) => {
    const { dispatch, pagination, currentOrg: { id: orgId } } = this.props;
    const { formValues } = this.state;
    dispatch({
      type: `Inventory/${type}`,
      payload: {
        data: {
          orgId,
          productOrderId,
        },

        query: {
          orgId,
          pagination,
          query: formValues
        }
      }
    })
  };

  handleCopy = (text) => {
    const input = document.createElement('input');
    document.body.appendChild(input);
    input.setAttribute('value', text);
    input.select();
    if (document.execCommand('copy')) {
      document.execCommand('copy');
      message.success('复制成功')
    }
    document.body.removeChild(input);
  };

  renderSearchForm() {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    const teaProps = {
      removal: true,
      queryUrl: `${url}/inventoryManagement/displayProduct`,
      keyValue: 'productName',
      showSearch: true,
      attr: {
        id: 'productName',
        name: 'productName',
      },
      placeholder: '产品名称',
      allowClear: true,
    };

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="产品名称">
              {getFieldDecorator('productName')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...teaProps} />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="订单号">
              {getFieldDecorator('productOrderId')(
                <Input placeholder="订单号" />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">查询</Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
                展开 <Icon type="down" />
              </a>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderAdvanceForm() {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    const teaProps = {
      removal: true,
      queryUrl: `${url}/inventoryManagement/displayProduct`,
      keyValue: 'productName',
      showSearch: true,
      attr: {
        id: 'productName',
        name: 'productName',
      },
      placeholder: '产品名称',
      allowClear: true,
    };

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="产品名称">
              {getFieldDecorator('productName')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...teaProps} />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="订单号">
              {getFieldDecorator('productOrderId')(
                <Input placeholder="订单号" />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="交付状态">
              {getFieldDecorator('isDeliverProduct', {
                initialValue: undefined,
              })(
                <Select>
                  <Option value={1}>已交付</Option>
                  <Option value={0}>未交付</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="交易状态">
              {getFieldDecorator('isRefund', {
                initialValue: undefined,
              })(
                <Select>
                  <Option value={1}>产生退款</Option>
                  <Option value={0}>交易成功</Option>
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col md={16} sm={24}>
            <FormItem label="时间">
              {getFieldDecorator('selectTime',{
                initialValue: [moment().day(-15), moment()],
              })(
                <RangePicker />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">查询</Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
                收起 <Icon type="up" />
              </a>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderForm() {
    const { expandForm } = this.state;
    return expandForm ? this.renderAdvanceForm() : this.renderSearchForm();
  }

  render() {
    const {
      forItem,
      orderList,
      loading,
      handleStandardTableChange,
      currentOrg
    } = this.props;
    const {
      formValues,
      visible,
    } = this.state;
    const columns = [
      {
        title: '订单号',
        dataIndex: 'id',
        align: 'center',
        render: function(text) {
          return (
            <div onClick={() => this.handleCopy(text)}>
              <Ellipsis length={12} tooltip>
                {text}
              </Ellipsis>
            </div>
          )
        }.bind(this),
      },
      {
        title: '学生名称',
        dataIndex: 'stuName',
        align: 'center',
        render(text) {
          return forItem('icon-mingcheng', text || '暂无信息' );
        },
      },
      {
        title: '产品名称',
        dataIndex: 'productName',
        align: 'center',
        render(text) {
          return forItem('icon-kemu', text || '暂无信息' );
        },
      },
      {
        title: '产品单价',
        dataIndex: 'productPrice',
        align: 'center',
        render(text) {
          return text ? forItem('icon-jiage', text) : '';
        },
      },
      {
        title: '订单单价',
        dataIndex: 'orderPrice',
        align: 'center',
        render(text) {
          return text ? forItem('icon-jiage', text) : '';
        },
      },
      {
        title: '数量',
        dataIndex: 'buyNum',
        align: 'center',
        render(text) {
          return text;
        },
      },
      {
        title: '交付状态',
        dataIndex: 'isDeliverProduct',
        align: 'center',
        render(text) {
          if (text) return <Badge status="success" text="已交付" />;
          return <Badge status="error" text="未交付" />
        },
      },
      {
        title: '交易状态',
        dataIndex: 'isRefund',
        align: 'center',
        render(text) {
          if (!text) return <Badge status="success" text="交易成功" />;
          return <Badge status="error" text="产生退款" />
        },
      },
      {
        title: '交易时间',
        dataIndex: 'buyTime',
        align: 'center',
        render(text) {
          return text ? forItem('icon-shijian', parseTime(text, '{y}-{m}-{d} {h}:{i}')) : '暂无信息';
        },
      },
      {
        title: '操作',
        fixed: 'right',
        width: 110,
        render: (text, record) => (
          <Fragment>
            {check('库存管理-产品销售记录:确认交付', (
              <a onClick={() => this.handlePay(record.id, 'confirmProductOrder')}>交付</a>
            ))}
            <Divider type="vertical" />
            {check('库存管理-产品销售记录:退款', (
              <Popconfirm title="是否确认退款？" okText="是" cancelText="否" onConfirm={() => this.handlePay(record.id, 'refundProduct')}>
                <a>退款</a>
              </Popconfirm>
            ))}
          </Fragment>
        ),
      },
    ];

    return (
      <section>
        <Card className={styles.tableListForm}>
          {this.renderForm()}
        </Card>
        <Card>
          <div className={styles.tableListOperator}>
            {check('库存管理-产品销售记录:新增', (
              <Button icon="plus" type="primary" onClick={this.handleAdd}>
                新建
              </Button>
            ))}
          </div>
          <div className={styles.tableList}>
            <StandardTable
              bordered
              loading={loading}
              data={orderList}
              columns={columns}
              onChange={e => handleStandardTableChange(e, formValues)}
              footer={data => {
                let allPrice = '';
                if (data.length) allPrice = data[0].allOrderPrice.toFixed(2);
                return (
                  <div>
                    总销售额：{allPrice} 元
                  </div>
                )
              }}
              scroll={{ x: 1150 }}
            />
          </div>
        </Card>
        <BuyProduct
          visible={visible}
          handleOk={this.handleOk}
          handleCancel={this.handleCancel}
          orgId={currentOrg ? currentOrg.id : undefined}
        />
      </section>
    );
  }
}
export default Generates;

