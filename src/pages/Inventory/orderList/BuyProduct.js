import React, { Component } from 'react';
import {
  Modal,
  InputNumber,
  Form,
  Tooltip,
  Icon,
} from 'antd';
import SearchSel from '@/components/SearchSel';
import { url } from '@/utils/utils';

@Form.create()
class BuyProduct extends Component{

  handleOk = () => {
    const { handleOk, form, } = this.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      const query = {};
      const { stuId, productId, buyNum } = values;
      if (stuId) query.stuId = stuId.key;
      if (productId) query.productId = productId.key;
      query.buyNum = buyNum;
      handleOk(query, () => form.resetFields())
    })
  };

  handleCancel = () => {
    const { handleCancel, form, } = this.props;
    handleCancel(() => form.resetFields())
  };

  render() {
    const { visible, form, orgId } = this.props;
    const { getFieldDecorator } = form;

    return (
      <Modal
        title="添加记录"
        visible={visible}
        onOk={this.handleOk}
        onCancel={this.handleCancel}
      >
        <Form layout="vertical">
          <Form.Item
            label={(
              <span>
                学生姓名&nbsp;
                <Tooltip title="不填则为顾客到店购买">
                  <Icon type="question-circle-o" />
                </Tooltip>
              </span>
            )}
          >
            {getFieldDecorator('stuId', {
              rules: [{ required: false }],
            })(
              <SearchSel
                orgId={orgId}
                removal
                queryUrl={`${url}/stuConsult/queryStu`}
                keyValue='name'
                showSearch
                attr={
                  {
                    id: 'stuId',
                    name: 'stuName',
                  }
                }
                placeholder='学生姓名'
                allowClear
              />
            )}
          </Form.Item>

          <Form.Item label="产品名称">
            {getFieldDecorator('productId', {
              rules: [{ required: true, message: '请输入产品名称' }],
            })(
              <SearchSel
                orgId={orgId}
                // removal
                queryUrl={`${url}/inventoryManagement/displayProduct`}
                keyValue='productName'
                showSearch
                attr={
                  {
                    id: 'productName',
                    name: 'productName',
                  }
                }
                placeholder='产品名称'
                allowClear
              />
            )}
          </Form.Item>

          <Form.Item label="数量">
            {getFieldDecorator('buyNum', {
              initialValue: 1,
              rules: [{ required: true, message: '请输入数量' }],
            })(
              <InputNumber style={{ width: '100%' }} min={1} />
            )}
          </Form.Item>

        </Form>
      </Modal>
    )
  }
}

export default BuyProduct;
