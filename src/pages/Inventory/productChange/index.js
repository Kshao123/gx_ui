import React, { Fragment, PureComponent } from 'react';
import { connect } from 'dva';
import {
  Card,
  Row,
  Col,
  Form,
  Button,
  DatePicker,
} from 'antd';
import moment from 'moment';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import Hoc from '@/utils/Hoc';
import { url, parseTime } from '@/utils/utils';
import SearchSel from '@/components/SearchSel';
import Ellipsis from '@/components/Ellipsis';

const FormItem = Form.Item;
const { RangePicker } = DatePicker;
const TYPE = ['', '直接入库', '机构转入', '直接出库', '转出机构', '销售', '退款'];

@connect(({ Inventory: { ProductChange }, global, loading }) => ({
  ProductChange,
  currentOrg: global.currentOrg,
  loading: loading.effects['Inventory/ProductChange'],
}))
@Form.create()
@Hoc({ fetchUrl: 'Inventory/ProductChange' })

class Generates extends PureComponent {
  constructor() {
    super();
    this.state = {
      formValues: {},
    };
  }

  handleFormReset = () => {
    const { dispatch, currentOrg, form  } = this.props;
    if (currentOrg.id) {
      form.resetFields();
      this.setState({
        formValues: {},
      });
      dispatch({
        type: 'Inventory/ProductChange',
        payload: {
          pagination: {
            pageSize: 10,
            pageNum: 1
          },
          orgId: currentOrg.id,
        },
      });
    }
  };

  handleSearch = (e) => {
    e.preventDefault();
    const { dispatch, currentOrg, form } = this.props;
    const pagination = {
      pageNum: 1,
      pageSize: 10,
    };
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const { selectTime, productId, productTypeId } = fieldsValue;
      const query = {};
      if (productId) {
        query.productId = productId.key
      }
      if (productTypeId) {
        query.productTypeId = productTypeId.key
      }
      if (selectTime) {
        query.startTime = selectTime[0].toDate().getTime();
        query.endTime = selectTime[1].toDate().getTime();
      }
      this.setState({
        formValues: query,
      });
      dispatch({
        type: 'Inventory/ProductChange',
        payload: {
          pagination,
          orgId: currentOrg.id,
          query,
        },
      })
    });
  };

  renderAdvanceForm() {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    const productProps = {
      removal: true,
      queryUrl: `${url}/inventoryManagement/displayProduct`,
      keyValue: 'productName',
      showSearch: true,
      attr: {
        id: 'id',
        name: 'productName',
      },
      placeholder: '名称',
      allowClear: true,
    };
    const productTypeProps = {
      removal: true,
      queryUrl: `${url}/inventoryManagement/displayProductType`,
      keyValue: 'productTypeName',
      showSearch: true,
      attr: {
        id: 'id',
        name: 'productTypeName',
      },
      placeholder: '类型',
      allowClear: true,
    };

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="产品名称">
              {getFieldDecorator('productId')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {... productProps} />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="产品类型">
              {getFieldDecorator('productTypeId')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {... productTypeProps} />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col md={16} sm={24}>
            <FormItem label="时间">
              {getFieldDecorator('selectTime',{
                initialValue: [moment().day(-7), moment()],
              })(
                <RangePicker />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">查询</Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const {
      forItem,
      ProductChange,
      loading,
      handleStandardTableChange,
    } = this.props;
    // console.log(ProductChange)
    const {
      formValues,
    } = this.state;
    const columns = [
      {
        title: '类型',
        dataIndex: 'productTypeName',
        align: 'center',
        render(text) {
          return (
            <Ellipsis length={4} tooltip>
              {text}
            </Ellipsis>
          );
        },
      },
      {
        title: '名称',
        dataIndex: 'productName',
        align: 'center',
        render(text) {
          return (
            <Ellipsis length={4} tooltip>
              {text}
            </Ellipsis>
          )
        },
      },
      {
        title: '数量',
        dataIndex: 'changeNum',
        align: 'center',
        render(text) {
          return forItem('icon-zhuankoukuan', text );
        },
      },
      {
        title: '变动方式',
        dataIndex: 'changeType',
        align: 'center',
        render(text) {
          return (
            <Ellipsis length={4} tooltip>
              {TYPE[text]}
            </Ellipsis>
          )
        },
      },
      {
        title: '机构名称',
        dataIndex: 'inOrOutOrgName',
        align: 'center',
        render(text) {
          return (
            <Ellipsis length={4} tooltip>
              {text || '暂无'}
            </Ellipsis>
          )
        },
      },
      {
        title: '使用时间',
        dataIndex: 'createTime',
        align: 'center',
        render(text) {
          return forItem('icon-shijian', text ? parseTime(text, '{d}日 {h}:{i}') : '' );
        },
      },
      // {
      //   title: '操作',
      //   fixed: 'right',
      //   width: 85,
      //   render: (text, record) => (
      //     <Fragment>
      //       {check('优惠卷:增加', (
      //         <Popcon
      //           title="是否确认删除"
      //           onConfirm={() => this.handleDelete(record)}
      //         />
      //       ))}
      //     </Fragment>
      //   ),
      // },
    ];

    return (
      <section>
        <Card className={styles.tableListForm}>
          {this.renderAdvanceForm()}
        </Card>
        <Card>
          <div className={styles.tableList}>
            <StandardTable
              bordered
              loading={loading}
              data={ProductChange}
              columns={columns}
              onChange={e => handleStandardTableChange(e, formValues)}
              expandedRowRender={records => records.remarks}
              scroll={{ x: 850 }}
            />
          </div>
        </Card>
      </section>
    );
  }
}
export default Generates;

