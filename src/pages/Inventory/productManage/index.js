import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Card,
  Form,
  Icon,
  Button,
  Divider,
  Table, Dropdown, Menu, Row, Col,
} from 'antd';
import { routerRedux } from 'dva/router';
import Ellipsis from '@/components/Ellipsis';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import Popcon from '@/components/Popconfirm';
import Authorized from '@/utils/Authorized';
import Hoc from '@/utils/Hoc';
import RollOutProduct from './rollProduct/RollOutProduct';
import RollInProduct from './rollProduct/RollInProduct';
import SearchSel from '@/components/SearchSel';
import { url } from '@/utils/utils';

const { check } = Authorized;

@connect(({ global, Inventory, loading }) => ({
  currentOrg: global.currentOrg,
  loading: loading.effects['Inventory/getInventoryManage'],
  Inventory,
}))
@Form.create()
@Hoc({ fetchUrl: 'Inventory/getInventoryManage' })
class productManage extends PureComponent {
  constructor() {
    super();
    this.state = {
      RollOutVisible: false,
      productId: undefined,
      RollInVisible: false,
      productName: '',
    };
  }

  handleEdit = (record) => {
    const { dispatch } = this.props;
    dispatch(routerRedux.push({
      pathname: '/Inventory/productManage/mgr',
      state: record,
    }));
  };

  handleDelete = (record) => {
    const { id } = record;
    const { dispatch, pagination, currentOrg } = this.props;
    const { productName } = this.state;
    dispatch({
      type: 'Inventory/delInventoryManage',
      payload: {
        productIdList: [id],
        pagination,
        orgId: currentOrg.id,
        query: {
          productName
        }
      },
    });
  };

  handleRollOutProduct = record => {
    const { id: productId } = record;
    this.setState({
      RollOutVisible: true,
      productId,
    })
  };

  handleRollOutOk = (values, clear) => {
    const { pagination, currentOrg, dispatch } = this.props;
    const { productName } = this.state;

    dispatch({
      type: 'Inventory/rollOutProduct',
      payload: {
        data: values,
        pagination,
        orgId: currentOrg.id,
        query: {
          productName
        }
      }
    })
      .then(res => {
        if (res) {
          clear();
          this.setState({
            RollOutVisible: false
          })
        }
      });

  };

  handleRollOutCancel = (clear) => {
    clear();
    this.setState({
      RollOutVisible: false
    })
  };

  handleRollInProduct = record => {
    const { id: productId } = record;
    this.setState({
      RollInVisible: true,
      productId,
    })
  };

  handleRollInOk = (values, clear) => {
    const { pagination, currentOrg, dispatch } = this.props;
    const { productName } = this.state;
    dispatch({
      type: 'Inventory/rollInProduct',
      payload: {
        data: values,
        pagination,
        orgId: currentOrg.id,
        query: {
          productName
        }
      }
    })
      .then(res => {
        if (res) {
          clear();
          this.setState({
            RollInVisible: false
          })
        }
      });
  };

  handleRollInCancel = (clear) => {
    clear();
    this.setState({
      RollInVisible: false
    })
  };

  expandedRowRender = (data) => {
    const { forItem } = this.props;
    const colums = [
      {
        title: '定义名称',
        dataIndex: 'propertiesName',
        align: 'center',
        key: 'propertiesName',
        render(text) {
          return forItem('icon-mingcheng', `${text}`);
        },
      },
      {
        title: '定义属性',
        dataIndex: 'propertiesValue',
        align: 'center',
        key: 'propertiesValue',
        render(text) {
          return text;
        },
      },
    ];
    return (
      <Table
        rowKey={(record) => record.id}
        bordered={false}
        columns={colums}
        dataSource={data.productCustomPropertiesList}
        pagination={false}
      />
    );
  };

  handleValueChange = (value = {}) => {
    const { dispatch } = this.props;
    const { currentOrg: { id: orgId }, pagination } = this.props;

    const { label: productName = '' } = value;
    this.setState({
      productName,
    });
    dispatch({
      type: 'Inventory/getInventoryManage',
      payload: {
        orgId,
        query: {
          productName
        },
        pagination,
      },
    })
  };

  renderForm() {
    const { currentOrg } = this.props;
    const props = {
      removal: true,
      onChange: this.handleValueChange,
      queryUrl: `${url}/inventoryManagement/displayProduct`,
      keyValue: 'productName',
      showSearch: true,
      attr: {
        id: 'id',
        name: 'productName',
      },
      placeholder: '请输入名称',
      allowClear: true,
    };

    return (
      <Form layout="inline">
        <Row>
          <Col md={8} sm={24}>
            <Form.Item label="名称">
              <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...props} />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const {
      forItem,
      Inventory: { InventoryManage },
      loading,
      handleStandardTableChange,
      currentOrg,
    } = this.props;
    const { RollOutVisible, productId, RollInVisible, productName } = this.state;
    const {
      handleRollOutOk,
      handleRollOutCancel,
      handleRollInOk,
      handleRollInCancel,
    } = this;
    const columns = [
      {
        title: '类别',
        dataIndex: 'productTypeName',
        render(text) {
          return forItem('icon-jibie', text);
        },
      },
      {
        title: '名称',
        dataIndex: 'productName',
        render(text) {
          return (
            <Ellipsis length={6} tooltip>
              {text}
            </Ellipsis>
          );
        },
      },
      {
        title: '数量',
        dataIndex: 'productNum',
        render(text) {
          return forItem('icon-dengji1', text);
        },
      },
      {
        title: '价格',
        dataIndex: 'productPrice',
        render(text) {
          return forItem('icon-jiage', text);
        },
      },
      {
        title: '简介',
        dataIndex: 'productBriefIntroduction',
        render(text) {
          return (
            <Ellipsis length={6} tooltip>
              {text}
            </Ellipsis>
          )
        },
      },
      {
        title: '操作',
        align: 'center',
        render: (text, record) => (
          <Fragment>
            {check('小组授课:修改', (
              <Dropdown overlay={(
                <Menu>
                  <Menu.Item>
                    <a onClick={() => this.handleEdit(record)}>修改产品</a>
                  </Menu.Item>
                  <Menu.Item>
                    {check(['库存管理-产品数量变动:转出到其他机构', '库存管理-产品数量变动:出库'], (
                      <a onClick={() => this.handleRollOutProduct(record)}>产品出库</a>
                    ))}
                  </Menu.Item>
                  <Menu.Item>
                    {check('库存管理-产品数量变动:入库', (
                      <a onClick={() => this.handleRollInProduct(record)}>产品入库</a>
                    ))}
                  </Menu.Item>
                </Menu>
              )}
              >
                <a className="ant-dropdown-link">
                  修改 <Icon type="down" />
                </a>
              </Dropdown>
            ))}
            <Divider type="vertical" />
            {check('库存管理-产品管理:删除', (
              <Popcon
                title="是否确认删除"
                onConfirm={() => this.handleDelete(record)}
              />
            ))}
          </Fragment>
        ),
      },
    ];

    return (
      <section>

        <Card className={styles.tableListForm}>
          {this.renderForm()}
        </Card>

        <Card>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              {check('库存管理-产品管理:增加', (
                <Button icon="plus" type="primary" onClick={() => this.handleEdit(undefined)}>
                  新建
                </Button>
              ))}
            </div>
            <StandardTable
              bordered
              loading={loading}
              data={InventoryManage}
              columns={columns}
              onChange={e => handleStandardTableChange(e,{ productName })}
              expandedRowRender={record => (this.expandedRowRender(record))}
              scroll={{ x: 650 }}
            />
          </div>
        </Card>

        <RollOutProduct
          visible={RollOutVisible}
          handleRollOutOk={handleRollOutOk}
          handleRollOutCancel={handleRollOutCancel}
          orgId={currentOrg.id}
          productId={productId}
        />

        <RollInProduct
          visible={RollInVisible}
          handleRollInOk={handleRollInOk}
          handleRollInCancel={handleRollInCancel}
          orgId={currentOrg.id}
          productId={productId}
        />
      </section>
    );
  }
}
export default productManage;
