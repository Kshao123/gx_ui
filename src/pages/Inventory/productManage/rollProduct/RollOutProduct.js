import React, { Component } from 'react';
import {
  Modal,
  Form,
  InputNumber,
  Select,
  Input,
} from 'antd';
import SearchSel from '@/components/SearchSel';
import { url } from '@/utils/utils';

const { TextArea } = Input;
const { Option } = Select;

@Form.create()
class RollOutProduct extends Component{

  state = {
    isSel: false,
  };

  handleOk = () => {
    const { form: { validateFieldsAndScroll, resetFields }, handleRollOutOk } = this.props;
    const { isSel } = this.state;
    validateFieldsAndScroll((error, values) => {
      if (!error) {
        const data = { ... values };
        const { rollInOrgId } = data;
        if (isSel) {
          data.rollInOrgId = parseInt(rollInOrgId.key, 10);
        } else {
          delete data.rollInOrgId
        }

        handleRollOutOk(data, resetFields);
      }
    });
  };

  handleCancel = () => {
    const { form: { resetFields }, handleRollOutCancel } = this.props;
    handleRollOutCancel(resetFields)
  };

  handleSelChange = value => {
    if (value === 4) {
      this.setState({
        isSel: true,
      })
    } else {
      this.setState({
        isSel: false,
      })
    }
  };


  render() {
    const {
      visible,
      form,
      productId,
      orgId,
    } = this.props;
    const { isSel } = this.state;
    const { getFieldDecorator} = form;
    const orgListProps = {
      queryUrl: `${url}/inventoryManagement/getOrgIdList`,
      keyValue: 'orgName',
      placeholder: '产品类型',
      attr: {
        id: 'id',
        name: 'orgName',
      },
    };

    return (
      <Modal
        title="产品出库"
        visible={visible}
        onOk={this.handleOk}
        onCancel={this.handleCancel}
      >
        <Form layout="vertical">
          {getFieldDecorator('productId', {
            initialValue: productId || '' })(
              <Input type="hidden" />
          )}

          {getFieldDecorator('orgId', {
            initialValue: orgId || '' })(
              <Input type="hidden" />
          )}

          <Form.Item label="转出数量">
            {getFieldDecorator('changeNum', {
             initialValue: 1,
             rules: [{ required: true, message: '请输入' }],
           })(
             <InputNumber precision={0} style={{ width: '100%' }} min={1} placeholder="转出数量" />
           )}
          </Form.Item>

          <Form.Item label="类型">
            {getFieldDecorator('changeType', {
              initialValue: 3,
              rules: [{ required: true, message: '请选择类型' }],
            })(
              <Select style={{ width: '100%' }} onChange={this.handleSelChange}>
                <Option value={3}>直接出库</Option>
                <Option value={4}>转出到其他机构</Option>
              </Select>
            )}
          </Form.Item>

          <Form.Item label="目标机构">
            {getFieldDecorator('rollInOrgId', {
              rules: [{ required: isSel, message: '请选择机构' }],
            })(
              <SearchSel disabled={!isSel} style={{ width: 100 }} showSearch orgId={orgId} {... orgListProps} />
           )}
          </Form.Item>

          <Form.Item label="备注">
            {getFieldDecorator('remarks', {
             rules: [{ required: false, message: '请输入备注' }],
            })(
              <TextArea placeholder="备注" autosize={{ minRows: 2, maxRows: 6 }} />
          )}
          </Form.Item>
        </Form>
      </Modal>
    )
  }
}

export default RollOutProduct;
