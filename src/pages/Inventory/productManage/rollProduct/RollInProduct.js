import React, { Component } from 'react';
import {
  Modal,
  Form,
  InputNumber,
  Select,
  Input,
} from 'antd';

const { TextArea } = Input;
const { Option } = Select;

@Form.create()
class RollOutProduct extends Component{

  handleOk = () => {
    const { form: { validateFieldsAndScroll, resetFields }, handleRollInOk } = this.props;
    validateFieldsAndScroll((error, values) => {
      if (!error) {
        const data = { ... values };

        handleRollInOk(data, resetFields);
      }
    });
  };

  handleCancel = () => {
    const { form: { resetFields }, handleRollInCancel } = this.props;
    handleRollInCancel(resetFields)
  };


  render() {
    const {
      visible,
      form,
      productId,
      orgId,
    } = this.props;
    const { getFieldDecorator} = form;

    return (
      <Modal
        title="产品入库"
        visible={visible}
        onOk={this.handleOk}
        onCancel={this.handleCancel}
      >
        <Form layout="vertical">
          {getFieldDecorator('productId', {
            initialValue: productId || '' })(
              <Input type="hidden" />
          )}

          {getFieldDecorator('orgId', {
            initialValue: orgId || '' })(
              <Input type="hidden" />
          )}

          <Form.Item label="转入数量">
            {getFieldDecorator('changeNum', {
              initialValue: 1,
              rules: [{ required: true, message: '请输入' }],
            })(
              <InputNumber precision={0} style={{ width: '100%' }} min={1} placeholder="转出数量" />
            )}
          </Form.Item>

          <Form.Item label="类型">
            {getFieldDecorator('changeType', {
              initialValue: 3,
              rules: [{ required: true, message: '请选择类型' }],
            })(
              <Select disabled style={{ width: '100%' }}>
                <Option value={3}>直接入库</Option>
                <Option value={4}>其他机构转入</Option>
              </Select>
            )}
          </Form.Item>

          <Form.Item label="备注">
            {getFieldDecorator('remarks', {
              rules: [{ required: false, message: '请输入备注' }],
            })(
              <TextArea placeholder="备注" autosize={{ minRows: 2, maxRows: 6 }} />
            )}
          </Form.Item>
        </Form>
      </Modal>
    )
  }
}

export default RollOutProduct;
