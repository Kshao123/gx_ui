import React, { PureComponent, Fragment } from 'react';
import {
  Card,
  Button,
  Form,
  Icon,
  Col,
  Row,
  Input,
  Popover,
  InputNumber,
  message,
  Upload,
  Select,
} from 'antd';
import { connect } from 'dva';
import FooterToolbar from '@/components/FooterToolbar';
import styles from '@/utils/global.less';
import TableForm from './TableForm';
import { url } from '@/utils/utils';

const { Option } = Select;
const { TextArea } = Input;
const fieldLabels = {
  productTypeName: '产品类型',
  productName: '产品名称',
  productPrice: '价格 / 元',
  productBriefIntroduction: '产品简介',
  productPictureList: '产品图片',
};

class One2One extends PureComponent {

  constructor(props) {
    super(props);
    const { location: { state } } = this.props;
    let subList = [];
    let fileList = [];
    if (state) {
      subList = state.productCustomPropertiesList.map((items, index) => ({
            key: `NEW_TEMP_ID_${index}112`,
            propertiesName: items.propertiesName,
            propertiesValue: items.propertiesValue,
            editable: false,
            isNew: false,
            new: false,
      }));
      const productPictureList = state.productPictureList;
      if (productPictureList.length) {
        fileList = productPictureList.map((item, index) => (
          {
            url: item,
            name: `产品图片${index}`,
            uid: `${item}${index}`,
          }
        ))
      }
    }
    this.state = {
      subList,
      fileList,
    };
  }

  componentDidMount() {
    setTimeout(() => {
      const { dispatch, currentOrg: { id } } = this.props;
      if (!id) return;
      dispatch({
        type: 'Inventory/getInventoryType',
        payload: {
          orgId: id,
          pagination: {
            pageNum: 1,
            pageSize: 1000,
          }
        },
      });
    },200)
  }

  handleTabChange = (item) => {
    const subList = item.filter(items => !items.editable);
    this.setState({
      subList,
    });
  };

  handleUpload = (file) => {
    const { name, size } = file;
    const Rxp = /.+\.(jpg|jpeg|png|bmp)$/;
    if (!Rxp.test(name)) {
      message.error('请上传正确的图片文件 仅支持 jpg | png | jpeg | bmp ', 3);
      return false;
    } if ((size / 1024) > 6000) {
      message.error('图片文件大于 5M', 3);
      return false;
    }
    return true;
  };

  handleRemove = (file) => {
    const { form: { resetFields } } = this.props;
    resetFields(['subjectPictureUrl']);
    const { uid } = file;
    const fileList = this.state.fileList.filter(item => item.uid !== uid);
    this.setState({
      fileList,
    });
    return true;
  };

  handleChange = ({ file, fileList} ) => {
    let state = false;
    const { name, size, status } = file;
    const Rxp = /.+\.(jpg|jpeg|png|bmp)$/;
    if (!Rxp.test(name) || (size / 1024) > 3000) {
      return;
    }
    let fileList1 = fileList;
    fileList1 = fileList1.map((item) => {
      if (item.response) {
        item.url = item.response.result;
      }
      return item;
    });
    fileList1 = fileList1.filter((item) => {
      const { response } = item;
      if (response) {
        const { status: states, message: msg } = response;
        !states ? message.error(msg) : '';
        state = states;
        return states === true;
      }
      return true;
    });
    (status === 'done' && state) ? message.success('上传成功') : '';
    this.setState({ fileList: fileList1 });
  };


  fixCommit = (data) => {
    const { Inventory: { InventoryTypes: { list: OptionsTypes } } } = this.props;
    const { fileList } = this.state;
    const { productTypeName, productCustomPropertiesList  } = data;
    let productTypeId = '';
    const list = productCustomPropertiesList.map((item) => ({
      propertiesName: item.propertiesName,
      propertiesValue: item.propertiesValue,
    }));
    if (OptionsTypes.length) {
      productTypeId = OptionsTypes.filter(item => item.productTypeName === productTypeName)[0].id;
    }
    let productPictureList = [];
    if (fileList.length) {
      try {
        productPictureList = fileList.map(item => item.url);
      }catch (e) {
        console.log(e)
      }
    }

    return {
      ...data,
      productTypeId,
      productCustomPropertiesList: list,
      productPictureList,
    };
  };

  validate = () => {
    const { form: { validateFieldsAndScroll }, location: { state }, dispatch } = this.props;
    validateFieldsAndScroll((error, values) => {
      if (!error) {
        const { subList } = this.state;
        values.productCustomPropertiesList = subList;
        values = this.fixCommit(values);
        if (!state) {
          delete values.productId;
        }
        dispatch({
          type: 'Inventory/addInventoryManage',
          payload: values,
        });
      }
    });
  };

  render() {
    const {
      form,
      submitting,
      currentOrg,
      location,
      Inventory: { InventoryTypes: { list: OptionsTypes } },
      dispatch,
    } = this.props;
    const { getFieldDecorator, getFieldsError } = form;
    const { state } = location;
    const { subList, fileList } = this.state;
    const {
      productName,
      productTypeName,
      productPrice,
      productBriefIntroduction,
      productPictureList,
    } = fieldLabels;
    const errors = getFieldsError();
    const getErrorInfo = () => {
      const errorCount = Object.keys(errors).filter(key => errors[key]).length;
      if (!errors || errorCount === 0) {
        return null;
      }
      const scrollToField = (fieldKey) => {
        const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
        if (labelNode) {
          labelNode.scrollIntoView(true);
        }
      };
      const errorList = Object.keys(errors).map((key) => {
        if (!errors[key]) {
          return null;
        }
        return (
          <li key={key} className={styles.errorListItem} onClick={() => scrollToField(key)}>
            <Icon type="cross-circle-o" className={styles.errorIcon} />
            <div className={styles.errorMessage}>{errors[key][0]}</div>
            <div className={styles.errorField}>{fieldLabels[key]}</div>
          </li>
        );
      });
      return (
        <span className={styles.errorIcon}>
          <Popover
            title="表单校验信息"
            content={errorList}
            overlayClassName={styles.errorPopover}
            trigger="click"
            getPopupContainer={trigger => trigger.parentNode}
          >
            <Icon type="exclamation-circle" />
          </Popover>
          {errorCount}
        </span>
      );
    };

    return (
      <Fragment>
        <Card title="产品基础信息" className={styles.Card} bordered={false}>
          <Form layout="vertical">
            {getFieldDecorator('productId', {
              initialValue: state ? state.id : '' })(
                <Input type="hidden" />
            )}
            {getFieldDecorator('orgId', {
              initialValue: state && state.orgId ? state.orgId : (currentOrg.id || ''),
            })(
              <Input type="hidden" />
            )}
            <Row gutter={16}>
              <Col lg={8} md={8} sm={24}>
                <Form.Item label={productTypeName}>
                  {getFieldDecorator('productTypeName', {
                    initialValue: state ? state.productTypeName : '',
                    rules: [{ required: true, message: `请选择${productTypeName}` }],
                  })(
                    <Select placeholder={productTypeName}>
                      {OptionsTypes
                        ? OptionsTypes.map(item => <Option key={item.productTypeName}>{item.productTypeName}</Option>)
                        : null}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col lg={8} md={8} sm={24}>
                <Form.Item label={productName}>
                  {getFieldDecorator('productName', {
                    initialValue: state ? state.productName : '',
                    rules: [{ required: true, message: `请输入${productName}` }],
                  })(
                    <Input placeholder={productName} />
                  )}
                </Form.Item>
              </Col>
              <Col lg={8} md={8} sm={24}>
                <Form.Item label={productPrice}>
                  {getFieldDecorator('productPrice', {
                    initialValue: state ? state.productPrice : 0,
                    rules: [{ required: true, message: `请输入${productPrice}` }],
                  })(
                    <InputNumber placeholder={productPrice} style={{ width: '100%' }} min={0} max={99999} />
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col lg={12} md={12} sm={24}>
                <Form.Item label={productPictureList}>
                  {getFieldDecorator('productPictureList', {
                    initialValue: state ? fileList : [],
                    rules: [
                      { required: false, message: '请重新上传' }],
                  })(
                    <Upload
                      accept="image/gif,image/jpg,image/png"
                      action={`${url}/mechanism/saveImage`}
                      listType="picture-card"
                      name="file1"
                      className='upload-list-inline'
                      beforeUpload={this.handleUpload}
                      onRemove={this.handleRemove}
                      onChange={this.handleChange}
                      fileList={fileList}
                      multiple
                    >
                      <div>
                        <Icon className="ant-icon" type="plus" />
                        <div className="ant-upload-text">{productPictureList}</div>
                      </div>
                    </Upload>
                  )}
                </Form.Item>
              </Col>
            </Row>

          </Form>
        </Card>

        <Card title="简介" className={styles.Card} bordered={false}>
          <Row gutter={16}>
            <Col lg={20} md={20} sm={20}>
              <Form.Item label={productBriefIntroduction}>
                {getFieldDecorator('productBriefIntroduction', {
                  initialValue: state ? state.productBriefIntroduction : '',
                  rules: [{ required: false, message: '请输入内容' }],
                })(
                  <TextArea placeholder={productBriefIntroduction} autosize={{ minRows: 2, maxRows: 6 }} />
                )}
              </Form.Item>
            </Col>
          </Row>
        </Card>

        <Card title="定义属性" className={styles.Card} bordered={false}>
          <Form.Item label="产品属性">
            {getFieldDecorator('productCustomPropertiesList', {
              initialValue: state || subList.length ? { dataList: subList } : [],
              rules: [{ required: false, message: '' }],
            })(
              <TableForm dispatch={dispatch} onChange={this.handleTabChange} />
            )}
          </Form.Item>
        </Card>
        <FooterToolbar style={{ width: '100%' }}>
          {getErrorInfo()}
          <Button type="primary" onClick={this.validate} loading={submitting}>
            提交
          </Button>
        </FooterToolbar>
      </Fragment>
    );
  }
}

export default connect(({ global, Inventory }) => ({
  Inventory,
  currentOrg: global.currentOrg,
}))(Form.create()(One2One));
