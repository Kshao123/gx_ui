import React, { Fragment } from 'react';
import { connect } from 'dva';
import {
  Form,
  Input,
  Button,
  Select,
  Divider,
  Radio,
  Upload,
  Icon,
  message,
  Popover,
  Progress,
} from 'antd';
import { routerRedux } from 'dva/router';
import { formatMessage, FormattedMessage } from 'umi/locale';
import styles from './style.less';
import { url } from '@/utils/utils';

const { Option } = Select;
const RadioGroup = Radio.Group;
const formItemLayout = {
  labelCol: {
    span: 5,
  },
  wrapperCol: {
    span: 19,
  },
};
const orgClassMap = [
  {
    value: 1,
    text: '个体工商户',
  },
  {
    value: 2,
    text: '企业',
  },
];

const passwordProgressMap = {
  ok: 'success',
  pass: 'normal',
  poor: 'exception',
};

const passwordStatusMap = {
  ok: (
    <div className={styles.success}>
      <FormattedMessage id="validation.password.strength.strong" />
    </div>
  ),
  pass: (
    <div className={styles.warning}>
      <FormattedMessage id="validation.password.strength.medium" />
    </div>
  ),
  poor: (
    <div className={styles.error}>
      <FormattedMessage id="validation.password.strength.short" />
    </div>
  ),
};


@connect(({ layoutloading: { currentUser }, loading }) => ({
  currentUser,
  submiting: loading.effects['user/Register']
}))
@Form.create()
class Step1 extends React.PureComponent {
  constructor(props) {
    super(props);
    const { location: { state } } = props;
    let Org = [];
    let Show = [];
    let CardFrontList = [];
    let CardVersoList = [];
    if (state) {
      const { orgLicense, orgShow, idCardFront, idCardVerso } = state;
      Org = orgLicense ? this.handleMapPic(JSON.parse(orgLicense), '营业执照') : [];
      Show = orgShow ? this.handleMapPic(JSON.parse(orgShow), '展示') : [];
      CardFrontList = idCardFront ? this.handleMapPic(JSON.parse(idCardFront), '身份证人像面') : [];
      CardVersoList = idCardVerso ? this.handleMapPic(JSON.parse(idCardVerso), '身份证国徽面') : [];
    }
    this.state = {
      fileListOrg: Org,
      fileListShow: Show,
      CardFrontList,
      CardVersoList,
      confirmDirty: false,
      picVal: '',
      picVisible: false,
      PROPS: {
        action: `${url}/mechanism/saveImage`,
        listType: 'picture-card',
        name: 'file1',
        className: 'upload-list-inline',
        beforeUpload: this.handleUpload,
      },
      visible: false,
      help: '',
    }
  }

  componentDidMount() {

  }

  handleMapPic = (item, type) => item.map((items, index) => ({
    name: `${type} ${index + 1}`,
    uid: index,
    url: items,
  }));

  handleUpload = (file) => {
    // /1024 即可得到实际大小 file.type 为格式
    const { name, size } = file;
    const Rxp = /.+\.(jpg|jpeg|png|bmp)$/;
    if (!Rxp.test(name)) {
      message.error('请上传正确的图片文件 仅支持 jpg | png | jpeg | bmp ', 3);
      return false;
    } if ((size / 1024) > 6000) {
      message.error('图片文件大于 5M', 3);
      return false;
    }
    return true;
  };

  handleRemovePic = (file, name, restName) => {
    const { form: { resetFields } } = this.props;
    if (restName) resetFields([restName]);
    const { uid } = file;
    const fileList = this.state[name].filter(item => item.uid !== uid);
    this.setState({
      [name]: fileList,
    });
    return true;
  };

  handleChangePic = ({ file, fileList}, stateName) => {
    const { name, size } = file;
    const Rxp = /.+\.(jpg|jpeg|png|bmp)$/;
    if (!Rxp.test(name) || (size / 1024) > 6000) {
      return;
    }
    let fileListPic = fileList;
    fileListPic = fileListPic.map((item) => {
      if (item.response) {
        item.url = item.response.result;
      }
      return item;
    });
    fileListPic = fileListPic.filter((item) => {
      if (item.response) {
        return item.response.status === true;
      }
      return true;
    });
    file.status === 'done' ? (message.success('上传成功', 3)) : '';
    this.setState({ [stateName]: fileListPic });
  };

  checkConfirm = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('passwd')) {
      callback(formatMessage({ id: 'validation.password.twice' }));
    } else {
      callback();
    }
  };

  getPasswordStatus = (name, max, min, isTrue) => {
    const { form } = this.props;
    let value;
    if (isTrue) {
      const length = form.getFieldValue(name);
      if (length) value = length.fileList;
    } else {
      value = form.getFieldValue(name);
    }
    if (value && value.length >= max) {
      return 'ok';
    }
    if (value && value.length > min) {
      return 'pass';
    }
    return 'poor';
  };

  renderPasswordProgress = (name, isTrue, max, min) => {
    const { form } = this.props;
    let value;
    let num = 10;
    if (isTrue) {
      const length = form.getFieldValue(name);
      if (length) value = length.fileList;
      num = 35;
    } else {
     value = form.getFieldValue(name);
    }
    const passwordStatus = this.getPasswordStatus(name, max, min, isTrue);
    return value && value.length ? (
      <div className={styles[`progress-${passwordStatus}`]}>
        <Progress
          status={passwordProgressMap[passwordStatus]}
          className={styles.progress}
          strokeWidth={6}
          percent={value.length * num > 100 ? 100 : value.length * num}
          showInfo={false}
        />
      </div>
    ) : null;
  };


  checkPassword = (rule, value, callback) => {
    const { visible, confirmDirty } = this.state;
    if (!value) {
      this.setState({
        help: formatMessage({ id: 'validation.password.required' }),
        visible: !!value,
      });
      callback('error');
    } else {
      this.setState({
        help: '',
      });
      if (!visible) {
        this.setState({
          visible: !!value,
        });
      }
      if (value.length < 6) {
        callback('error');
      } else {
        const { form } = this.props;
        if (value && confirmDirty) {
          form.validateFields(['confirm'], { force: true });
        }
        callback();
      }
    }
  };

  checkShow = (rule, value, callback) => {
    const { fileListShow: { length } } = this.state;
    if (!length) {
      this.setState({
        picVal: '请上传展示图片',
        picVisible: !!length,
      });
      callback('error');
    } else {
      const { picVal, confirmDirty } = this.state;
      this.setState({
        picVal: '',
      });
      if (!picVal) {
        this.setState({
          picVisible: !!length,
        });
      }
      if (length < 3) {
        callback('error');
      }
      else {
        const { form } = this.props;
        if (length && confirmDirty) {
          form.validateFields(['orgShow'], { force: true });
        }
        setTimeout(() => {
          this.setState({
            picVisible: false,
          })
        }, 1500);
        callback();
      }
    }
  };

  confirmPic = (fileList) => fileList.map(item => item.url);

  render() {
    const { form, dispatch, location: { state }, disabled, submiting } = this.props;
    const { getFieldDecorator, validateFields } = form;
    const {
      fileListOrg,
      help,
      visible,
      fileListShow,
      picVal,
      picVisible,
      PROPS,
      CardFrontList,
      CardVersoList,
    } = this.state;
    const { handleChangePic, handleRemovePic } = this;
    const onValidateForm = (e) => {
      e.preventDefault();
      validateFields((err, values) => {
        if (!err) {
          const data = values;
          if (!state) {
            delete data.userId;
            delete data.orgId;
          }
          if (!data.orgCode || !data.orgCode.length) {
            message.warn('请填写正确的统一社会信用代码',2);
            return;
          }
          const { orgLicense, idCardVerso, idCardFront } = data;
          data.orgLicense = orgLicense ? JSON.stringify(this.confirmPic(fileListOrg)) : '';
          data.idCardVerso = idCardVerso ? JSON.stringify(this.confirmPic(CardVersoList)) : '';
          data.idCardFront = idCardFront ? JSON.stringify(this.confirmPic(CardFrontList)) : '';
          if (fileListShow && fileListShow.length >= 3) {
            data.orgShow = JSON.stringify(this.confirmPic(fileListShow));
          }else {
            data.orgShow = '';
            message.warn('请根据要求 上传相应的机构展图片',2);
            return;
          }
          delete data.confirm;
          dispatch({
            type: 'user/Register',
            payload: data,
          }).then((res)=> {
            if (res) {
              dispatch(routerRedux.push({
                pathname: '/user/registerStep/result',
                state: {
                  ...data,
                  auditResult: 2,
                },
              }));
            }
          })
        }
      });
    };
    return (
      <Fragment>
        <Form layout="horizontal" className={styles.stepForm}>

          {getFieldDecorator('userId', {
            initialValue: state ? state.userId : '' })(
              <Input type="hidden" />
          )}

          {getFieldDecorator('orgId', {
            initialValue: state ? state.orgId : '' })(
              <Input type="hidden" />
          )}

          <Form.Item {...formItemLayout} className={styles.stepFormText} label="姓名">
            {getFieldDecorator('name', {
              initialValue: state && state.name ? state.name : '',
              rules: [
                { required: true, message: '请输入姓名' }],
            })(
              <Input size="large" placeholder="请输入姓名" />
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} className={styles.stepFormText} label="身份证">
            {getFieldDecorator('idCard', {
              initialValue: state && state.idCard ? state.idCard : '',
              rules: [
                { pattern: /^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/, required: true, message: '请输入身份证' }],
            })(
              <Input size="large" placeholder="请输入正确的证件号码" />
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} className={styles.stepFormText} label="用户名">
            {getFieldDecorator('username', {
              initialValue: state && state.username ? state.username : '',
              rules: [
                { required: true, message: '请输入用户名' }],
            })(
              <Input size="large" placeholder="请输入用户名" />
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} className={styles.stepFormText} help={help} label="密码">
            <Popover
              getPopupContainer={node => node.parentNode}
              content={
                <div style={{ padding: '4px 0' }}>
                  {passwordStatusMap[this.getPasswordStatus('passwd', 9, 5)]}
                  {this.renderPasswordProgress('passwd', undefined,9, 5)}
                  <div style={{ marginTop: 10 }}>
                    <FormattedMessage id="validation.password.strength.msg" />
                  </div>
                </div>
              }
              overlayStyle={{ width: 240 }}
              placement="right"
              visible={visible}
            >
              {getFieldDecorator('passwd', {
                initialValue: state && state.passwd ? state.passwd : '',
                rules: [
                  {
                    required: true,
                  },
                  {
                    validator: this.checkPassword,
                  },
                ],
              })(
                <Input
                  size="large"
                  type="password"
                  placeholder={formatMessage({ id: 'form.password.placeholder' })}
                />
              )}
            </Popover>
          </Form.Item>

          <Form.Item {...formItemLayout} className={styles.stepFormText} label="确认密码">
            {getFieldDecorator('confirm', {
              initialValue: state && state.confirm ? state.confirm : '',
              rules: [
                {
                  required: true,
                  message: formatMessage({ id: 'validation.confirm-password.required' }),
                },
                {
                  validator: this.checkConfirm,
                },
              ],
            })(
              <Input
                size="large"
                type="password"
                placeholder={formatMessage({ id: 'form.confirm-password.placeholder' })}
              />
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} label="手机">
            {getFieldDecorator('mobile', {
              initialValue: state && state.mobilePhone ? state.mobilePhone : '',
              rules: [
                { pattern: /^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$/, required: true, message: '格式错误，请重新输入' }],
            })(
              <Input size="large" disabled={disabled} placeholder="请输入手机号码" />
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} label="邮箱">
            {getFieldDecorator('orgEmails', {
              initialValue: state && state.orgEmails ? state.orgEmails : '',
              rules: [
                { pattern: /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/, required: true, message: '邮箱格式有误，请重新输入' }],
            })(
              <Input size="large" disabled={disabled} placeholder="请输入邮箱地址" />
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} label="机构名称">
            {getFieldDecorator('orgName', {
              initialValue: state && state.orgName ? state.orgName : '',
              rules: [
                { pattern: /^[\u4E00-\u9FA5-a-zA-Z0-9_]{2,16}$/, required: true, message: '请输入中、英文、数字或下划线' }],
            })(
              <Input size="large" disabled={disabled} placeholder="请输入机构名称" />
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} className={styles.stepFormText} label="社会信用代码">
            {getFieldDecorator('orgCode', {
              initialValue: state && state.orgCode ? state.orgCode : '',
              rules: [
                { required: false, message: '格式有误，请重新输入' }],
            })(
              <Input size="large" disabled={disabled} placeholder="请输入" />
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} className={styles.stepFormText} label="身份证人像面">
            {getFieldDecorator('idCardFront', {
              initialValue: state && state.idCardFront ? CardFrontList : [],
              rules: [
                { required: true, message: '请重新上传' }],
            })(
              <Upload
                disabled={disabled}
                {...PROPS}
                fileList={CardFrontList}
                onChange={file => handleChangePic(file, 'CardFrontList')}
                onRemove={file => handleRemovePic(file, 'CardFrontList', 'idCardFront')}
              >
                {CardFrontList.length >= 1 ? null : (
                  <div>
                    <Icon className="ant-icon" type="plus" />
                    <div className="ant-upload-text">身份证人像面</div>
                  </div>
                )}
              </Upload>
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} className={styles.stepFormText} label="身份证国徽面">
            {getFieldDecorator('idCardVerso', {
              initialValue: state && state.idCardVerso ? CardVersoList : [],
              rules: [
                { required: true, message: '请重新上传' }],
            })(
              <Upload
                disabled={disabled}
                {...PROPS}
                fileList={CardVersoList}
                onChange={file => handleChangePic(file, 'CardVersoList')}
                onRemove={file => handleRemovePic(file, 'CardVersoList', 'idCardVerso')}
              >
                {CardVersoList.length >= 1 ? null : (
                  <div>
                    <Icon className="ant-icon" type="plus" />
                    <div className="ant-upload-text">身份证国徽面</div>
                  </div>
                )}
              </Upload>
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} className={styles.stepFormText} label="营业执照">
            {getFieldDecorator('orgLicense', {
              initialValue: state && state.orgLicense ? fileListOrg : [],
              rules: [
                { required: true, message: '请重新上传' }],
            })(
              <Upload
                disabled={disabled}
                {...PROPS}
                fileList={fileListOrg}
                onChange={file => handleChangePic(file, 'fileListOrg')}
                onRemove={file => handleRemovePic(file, 'fileListOrg', 'orgLicense')}
              >
                {fileListOrg.length >= 1 ? null : (
                  <div>
                    <Icon className="ant-icon" type="plus" />
                    <div className="ant-upload-text">营业执照</div>
                  </div>
                )}
              </Upload>
            )}
          </Form.Item>

          <Popover
            content={
              <div style={{ padding: '4px 0' }}>
                {this.renderPasswordProgress('orgShow', true, 3, 5)}
                <div style={{ marginTop: 10 }}>
                  请上传至少三张，关于本机构店面展示图片，图片当中必须有包含此机构前台、教室、教学，等相关信息
                </div>
              </div>
            }
            overlayStyle={{ width: 240 }}
            placement="rightTop"
            visible={picVisible}
          >
            <Form.Item {...formItemLayout} className={styles.stepFormText} help={picVal} label="机构展示">
              {getFieldDecorator('orgShow', {
                initialValue: state && state.orgShow ? fileListShow : [],
                rules: [
                  { required: true },
                  { validator: this.checkShow }],
              })(

                <Upload
                  disabled={disabled}
                  {...PROPS}
                  fileList={fileListShow}
                  onChange={file => handleChangePic(file, 'fileListShow')}
                  onRemove={file => handleRemovePic(file, 'fileListShow')}
                  multiple
                >
                  {disabled ? null : (
                    <div>
                      <Icon className="ant-icon" type="plus" />
                      <div className="ant-upload-text">机构展示</div>
                    </div>
                  )}
                </Upload>
              )}
            </Form.Item>
          </Popover>



          <Form.Item {...formItemLayout} label="分类">
            {getFieldDecorator('orgClass', {
              initialValue: state ? state.orgClass : '',
              rules: [
                { required: true, message: '请选择机构分类' }],
            })(
              <RadioGroup disabled={disabled}>
                {orgClassMap.map(item => <Radio key={item.value} value={item.value}>{item.text}</Radio>)}
              </RadioGroup>
            )}
          </Form.Item>

          <Form.Item
            wrapperCol={{
              xs: { span: 24, offset: 0 },
              sm: {
                span: formItemLayout.wrapperCol.span,
                offset: formItemLayout.labelCol.span,
              },
            }}
            label=""
          >
            <Button loading={submiting} type="primary" onClick={onValidateForm}>
              下一步
            </Button>
          </Form.Item>
        </Form>
        <Divider style={{ margin: '40px 0 24px' }} />
        <div className={styles.desc}>
          <h3>说明</h3>
          <p>
            联系方式请填写正确，审核的结果我们将优先使用短信通知！也可以在登陆界面的账号查询，输入统一社会信用代码进行查看
          </p>
        </div>
      </Fragment>
    );
  }
}

export default Step1;
