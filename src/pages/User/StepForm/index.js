import React, { PureComponent, Fragment } from 'react';
import { Card, Steps } from 'antd';
import styles from '../style.less';

const { Step } = Steps;

window.onbeforeunload = function() {
  return ''
};



export default class StepForm extends PureComponent {
  getCurrentStep() {
    const { location } = this.props;
    const { pathname } = location;
    const pathList = pathname.split('/');
    switch (pathList[pathList.length - 1]) {
      case 'info':
        return 0;
      case 'result':
        return 1;
      case 'pay':
        return 2;
      default:
        return 0;
    }
  }

  render() {
    const { children } = this.props;
    return (
      <Fragment>
        <Steps current={this.getCurrentStep()} className={styles.steps}>
          <Step title="填写信息" />
          <Step title="提交审核" />
          <Step title="支付" />
        </Steps>
        {children}
      </Fragment>
    );
  }
}
