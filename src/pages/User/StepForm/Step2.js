import React, { Fragment } from 'react';
import { connect } from 'dva';
import { Button, Row, Col, Steps, Card } from 'antd';
import { routerRedux } from 'dva/router';
import Result from '@/components/Result';
import styles from './style.less';


const { Step } = Steps;
const caseStatus = (state) => {
  if (!state) return;
  const { auditResult } = state;
  if (!auditResult) return;
  switch (auditResult) {
    case 0:
      return (
        <div>
          <p>审核未通过</p>
          <p>{state.falseCause}</p>
        </div>
      );
    case 1:
      return '审核通过';
    default:
      break;
  }
};

const caseTitle = (state) => {
  if (!state) return;
  const { auditResult } = state;
  if (!auditResult) return;
  switch (auditResult) {
    case 0:
      return '审核未通过';
    case 1:
      return '审核通过';
    case 2:
      return '预计审核时间为2~3个工作日';
    default:
      break;
  }
};
const orgClassMap = {
  '1': '个体工商户',
  '2': '企业',
};

const caseMsg = (state) => {
  if (!state) return;
  const { auditResult } = state;
  if (!auditResult && auditResult !== 0) return;
  switch (auditResult) {
    case 0:
      return `拒绝原因：${state.falseCause}`;
    case 1:
      return '审核通过';
    case 2:
      return '';
    default:
      break;
  }
};

const changeNum = (state) => {
  if (!state) return;
  const { auditResult } = state;
  if (!auditResult && auditResult !== 0) return;
  switch (auditResult) {
    case 0:
      return 2;
    case 1:
      return 2;
    case 2:
      return 1;
    default:
      break;
  }
};

@connect(({ form }) => ({
  // data: form.step,
}))
class Step3 extends React.PureComponent {

  render() {
    const { location: { state }, dispatch } = this.props;
    const onFinish = () => {
      dispatch(routerRedux.push({
        pathname: '/user/registerStep/pay',
        state: state ? {
          ...state,
        } : undefined,
      }));
    };
    const onAfresh = (num) => {
      switch (num) {
        case 0:
          dispatch(routerRedux.push({
            pathname: '/user/registerStep/info',
            state: state ? {
              ...state,
            } : undefined,
          }));
          break;
        case 2:
          dispatch(routerRedux.push({
            pathname: '/user/login',
          }));
          break;
        default:
          break;
      }
    };
    const information = (
      <div className={styles.information}>
        <Row>
          <Col xs={24} sm={8} className={styles.label}>
            姓名：
          </Col>
          <Col xs={24} sm={16}>
            {state ? state.name : 'loding...'}
          </Col>
        </Row>
        <Row>
          <Col xs={24} sm={8} className={styles.label}>
            用户名：
          </Col>
          <Col xs={24} sm={16}>
            {state ? state.name : 'loding...'}
          </Col>
        </Row>
        <Row>
          <Col xs={24} sm={8} className={styles.label}>
          联系电话：
          </Col>
          <Col xs={24} sm={16}>
            {state ? state.mobile : 'loding...'}
          </Col>
        </Row>
        <Row>
          <Col xs={24} sm={8} className={styles.label}>
            机构名称：
          </Col>
          <Col xs={24} sm={16}>
            {state ? state.orgName : 'loding...'}
          </Col>
        </Row>

        <Row>
          <Col xs={24} sm={8} className={styles.label}>
            社会信用代码：
          </Col>
          <Col xs={24} sm={16}>
            {state ? state.orgCode : 'loding...'}
          </Col>
        </Row>
        <Row>
          <Col xs={24} sm={8} className={styles.label}>
            分类：
          </Col>
          <Col xs={24} sm={16}>
            {state && state.orgClass ? orgClassMap[state.orgClass] : 'loding...'}
          </Col>
        </Row>
        <Row>
          <Col xs={24} sm={8} className={styles.label}>
            营业执照：
          </Col>
          <Col xs={24} sm={16}>
            <img style={{ width: 120, height: 120 }} src={state && state.orgLicense ? JSON.parse(state.orgLicense)[0] : ''} alt="" />
          </Col>
        </Row>
      </div>
    );
    const actions = (
      <Fragment>
        <Button type="primary" style={{ display: state && state.auditResult === 1 ? 'inline-block' : 'none' }} onClick={onFinish}>
          立即支付
        </Button>
        <Button type="primary" onClick={() => onAfresh(0)} style={{ display: state && state.auditResult === 0 ? 'inline-block' : 'none' }}>重新修改</Button>
        <Button type="primary" onClick={() => onAfresh(2)} style={{ display: state && state.auditResult === 2 ? 'inline-block' : 'none' }}>返回登陆</Button>
      </Fragment>
    );

    return (
      <Card>
        <Result
          type={state && state.auditResult ? 'success' : 'error'}
          title={caseTitle(state)}
          description={caseMsg(state)}
          extra={information}
          actions={actions}
          className={styles.result}
        />
        <Card title="流程进度" style={{ marginTop: 24 }} bordered={false}>
          <Steps status={state && state.auditResult === 0 ? 'error' : ''} current={changeNum(state)}>
            <Step title="申请账号" />
            <Step title="审批中" />
            <Step description={caseMsg(state)} title="结果" />
          </Steps>
        </Card>
      </Card>

    );
  }
}

export default Step3;
