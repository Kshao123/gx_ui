import React, { Fragment } from 'react';
import { connect } from 'dva';
import {
  Button,
  Row,
  Col,
  Steps,
  Card,
  Popover,
  Badge,
  Radio,
  Spin,
  Modal,
  message,
} from 'antd';
import ClassNames from 'classnames';
import { routerRedux } from 'dva/router';
import styles from './style.less';
import Result from '@/components/Result';

const checkPay = {
  '1': '微信支付(ZLF)',
  '2': '支付宝支付（SD）'
};
let time = '';

@connect(({ user, loading }) => ({
  Price: user.Price,
  getEwm: loading.effects['user/getEwm'],
  Paying: loading.effects['user/getPayRes']
}))
class Step3 extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      currentPrice: 0,
      avgPrice: 0,
      id: undefined, // 价格id
      value: undefined, // 支付方式
      checked: false, // 是否选择支付
      visible: false,
      down: true,
      paySrc: '',
      orderStr: '',
    }
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'user/getPrice'
    })
  }

  radioChange = (e) => {
    const now = e.target.value;
    const { Price } = this.props;
    if (!Price.length) return;
    const filterPrice = Price.filter(item => item.monthNum === now)[0];
    const { avgPrice, totalPrice, id } = filterPrice;
    this.setState({
      avgPrice,
      currentPrice: totalPrice,
      id,
    })
  };

  handleChange = (e) => {
    const inputs = document.getElementsByTagName('input');
    const value = e.target.value;
    for (let i = 0; i< inputs.length; i += 1) {
      const attr = inputs[i];
      attr.parentNode.className = ClassNames(styles.lab);
      if (attr.value === value) {
        attr.parentNode.className = ClassNames(styles.lab, styles.checked);
      }
    }
    this.setState({
      checked: true,
      value,
    })
  };

  handlePay = () => {
    const { checked, id, value } = this.state;
    const { location: { state }, dispatch } = this.props;
    if (!checked) {
      message.warn('请选择支付方式');
      return;
    }
    if (!state || !id) {
      message.error('订单获取错误 请重新查询');
      return;
    }
    const { username } = state;
    dispatch({
      type: 'user/getEwm',
      payload:{
        priceId: id,
        username,
        flag: value,
      }
    })
    .then(res => {
      if (!res || !Object.keys(res).length) {
        this.setState({
          visible: false,
        });
        return;
      };
      const { out_trade_no, resultImage } = res;
      this.setState({
        paySrc: resultImage,
        orderStr: out_trade_no,
      });
      time = setInterval(() => {
        dispatch({
          type: 'user/getPayRes',
          payload: {
            out_trade_no
          },
        })
          .then((Res)=> {
            if (!Res) return;
            // 0：失败，1：成功，2：支付中
            switch (Res) {
              case 1:
                clearInterval(time);
                this.setState({
                  down: {
                    status: 'success',
                    tset: '支付成功！',
                    introduce: '即将跳转至登陆界面！'
                  },
                });
                setTimeout(()=>{
                  dispatch(routerRedux.push({
                    pathname: '/user/login',
                  }));
                },5000);
                break;
              case 2:

                break;
              case 0:
                clearInterval(time);
                this.setState({
                  down: {
                    status: 'error',
                    tset: '支付失败！',
                    introduce: '请重新支付！'
                  },
                  visible: false,
                });
                break;
              default:
                break;
            }
          })
      },1800)
    });
    this.setState({
      visible: true,
    });
  };

  handleOk = () => {
    this.setState({
      visible: false,
    })
  };

  handleCancel = () => {
    if (time) {
      clearInterval(time);
    }
    this.setState({
      visible: false,
    })
  };

  handleDone = () => {
    const { dispatch } = this.props;
    dispatch(routerRedux.push({
      pathname: '/user/login',
    }));
  };

  render() {
    const { location: { state }, Price, getEwm, Paying } = this.props;
    const { currentPrice, avgPrice, visible, down, paySrc, value, orderStr } = this.state;
    const { handleChange, handlePay, handleOk, handleCancel, handleDone } = this;
    const information = (
      <div style={{ margin: 'auto', maxWidth: 550 }} className={styles.information}>
        <Row>
          <Col xs={24} sm={8} className={styles.label}>
            姓名：
          </Col>
          <Col xs={24} sm={16}>
            {state ? state.name : 'loding...'}
          </Col>
        </Row>
        <Row>
          <Col xs={24} sm={8} className={styles.label}>
          联系电话：
          </Col>
          <Col xs={24} sm={16}>
            {state ? state.mobile : 'loading...'}
          </Col>
        </Row>
        <Row>
          <Col xs={24} sm={8} className={styles.label}>
            机构名称：
          </Col>
          <Col xs={24} sm={16}>
            {state ? state.orgName : 'loading...'}
          </Col>
        </Row>

      </div>
    );

    const priceCheck = (
      <div style={{ marginTop: 25 }} className={styles.information}>
        <Row>
          <Col xs={24} sm={8} className={styles.label}>
            购买时长：
          </Col>
          <Col xs={24} sm={16}>
            <Radio.Group onChange={this.radioChange} buttonStyle="solid">
              {Price && Price.length ? Price.map((item) => {
                if (item.monthNum >= 6) {
                  // className={styles.radios} 优惠价样式 暂时去掉
                  return <Radio.Button key={item.monthNum} value={item.monthNum}>{item.monthNum}月</Radio.Button>
                }
                return  <Radio.Button key={item.monthNum} value={item.monthNum}>{item.monthNum}月</Radio.Button>
              }) : <Spin />}
            </Radio.Group>
          </Col>
        </Row>
        <Row>
          <Col xs={24} sm={8} className={styles.label}>
            优惠价：
          </Col>
          <Col style={{ color: '#ff7a22' }} xs={24} sm={16}>
            <span style={{ fontSize: '25px' }}>
              {currentPrice}
            </span> 元
            <span style={{ fontSize: 12, marginLeft: 10, color: '#a2a2a2', textDecoration: 'line-through' }}>每月：{avgPrice} 元</span>
          </Col>
        </Row>
      </div>
    );

    const pay = (
      <section className={styles.payList}>
        <div className={styles.payMenu}>
          <div className={styles.payEwm}>
            <img src="https://www.vipkm.com/static/app/images/pay1.png" alt="" />
              扫码支付
          </div>
        </div>
        <div className={styles.payRadio}>
          <label className={styles.lab}>
            <input onChange={handleChange} name="pid" value="1" type="radio" />
            <img src="https://www.vipkm.com/static/app/images/icon_wx.jpg" alt="" />
          </label>
          <label className={styles.lab}>
            <input onChange={handleChange} name="pid" value="2" type="radio" />
            <img src="https://www.vipkm.com/static/app/images/icon_zfb.jpg" alt="" />
          </label>
        </div>
        <div className={styles.submit}>
          <button onClick={handlePay} className={styles.check_pay}>确认支付</button>
        </div>
      </section>
    );

    const payModal = (
      <div>
        <Modal
          visible={visible}
          onOk={handleOk}
          onCancel={handleCancel}
          maskClosable={false}
          confirmLoading={!!Paying}
        >
          <Spin spinning={!!getEwm}>
            {down.status ? (
              <Result
                type={down.status}
                title={down.text}
                description={down.introduce}
                actions={
                  <Button type="primary" onClick={handleDone}>
                    知道了
                  </Button>
                }
                className={styles.formResult}
              />
            ) : (
              <div>
                <div className={styles.order}>
                  <img src="https://www.vipkm.com/static/app/images/order_pay.png" alt="" />
                  <p>
                    支付金额：
                    <span>{currentPrice}元 </span>
                    <br />
                    支付方式：
                    <span>{checkPay[value]}</span>
                    <br />
                    支付订单号：
                    <span>
                      {orderStr}
                    </span>
                  </p>
                </div>
                <img style={{ display: 'block', margin: 'auto' }} src={paySrc} alt="" />
              </div>

            )}
          </Spin>
        </Modal>
      </div>
    );

    return (
      <Card>
        {information}
        {priceCheck}
        {pay}
        {payModal}
      </Card>

    );
  }
}

export default Step3;
