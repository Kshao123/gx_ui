import React, { Component, Fragment } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Checkbox, Alert, Button, Input, Spin, Icon, message } from 'antd';
import Login from '../../components/Login';
import styles from './Login.less';
import { fakeAccountLogin } from '../../services/api';

const { Tab, UserName, Password, Submit } = Login;
const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;

@connect(({ login, loading }) => ({
  login,
  submitting: loading.effects['login/_login'],
  loading: loading.effects['user/getRegister'],
}))
class LoginPage extends Component {
  state = {
    type: 'account',
    autoLogin: true,
    value: '',
  };

  onTabChange = (type) => { // 传给子级方法
    this.setState({ type });
  };

  handleSubmit = (err, values) => { // 传给子级的方法
    const { type, autoLogin } = this.state;
    if (!err) {
      this.props.dispatch({
        type: 'login/_login',
        payload: {
          ...values,
          type,
          autoLogin,
        },
      });
    }
  };

// CheckBox 发生或改变是 改变state中的状态
  changeAutoLogin = (e) => {
    this.setState({
      autoLogin: e.target.checked,
    });
  };

  renderMessage = (content) =>  // alert 为antd 弹出报错信息 message 为报错信息
     (
       <Alert style={{ marginBottom: 24 }} message={content} type="error" showIcon />
    );

  onChange = (e) => {
    const value = e.target.value;
    this.setState({
      value,
    })
  };

  handleSearch = () => {
    const { value } = this.state;
    if (!value || !value.length) {
      message.warn('请输入正确的代码');
      return;
    }
    const { dispatch } = this.props;
    dispatch({
      type: 'user/getRegister',
      payload: {
        orgLicense: value,
      },
    }).then((res) => {
      if (!res) return;
      if (Object.keys(res).length) {
        dispatch(routerRedux.push({
          pathname: '/user/registerStep/result',
          state: res,
        }));
      }
    })
  };

  render() {
    const { login, submitting, loading } = this.props;
    const { type, value } = this.state;
    return (
      <div className={styles.main}>

        <Login
          defaultActiveKey={type}
          onTabChange={this.onTabChange}
          onSubmit={this.handleSubmit}
          ref={form => {
            this.loginForm = form;
          }}
        >
          <Tab key="account" tab="账户密码登录">
            {
              login.status === false &&
              !login.submitting &&
              this.renderMessage('账户或密码错误')
            }
            <UserName name="userName" placeholder="用户名" />
            <Password
              name="password"
              placeholder="密码"
              onPressEnter={e => {
                e.preventDefault();
                this.loginForm.validateFields(this.handleSubmit);
              }}
            />
            <div>
              <Checkbox name="remember-me" checked={this.state.autoLogin} onChange={this.changeAutoLogin}>自动登录</Checkbox>
              <a style={{ float: 'right' }} href="#/user/registerStep/info">申请账号</a>
            </div>
            <Submit loading={submitting}>登录</Submit>
          </Tab>
          <Tab key="query" tab="账号查询">
            <Spin spinning={!!loading} indicator={antIcon}>
              <Input onChange={this.onChange} value={value} style={{ marginBottom: 25 }} size="large" placeholder="请输入统一社会信用代码" />
              <Button
                block
                type="primary"
                size="large"
                onClick={this.handleSearch}
                disabled={!value}
              >
                查询
              </Button>
            </Spin>

            <div style={{ marginTop: 35 }} className={styles.desc}>
              <h3>说明</h3>
              <p>
                根据统一社会信用代码，查询账号的审核进度。
              </p>
            </div>
          </Tab>

        </Login>
      </div>
    );
  }
}

export default LoginPage;
