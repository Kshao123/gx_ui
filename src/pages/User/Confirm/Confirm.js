import React, { Component, Fragment } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import {
  Checkbox,
  Alert,
  Button,
  Input,
  Spin,
  Icon,
  message,
  Form,
} from 'antd';
import styles from '../Login.less';

const formItemLayout = {
  labelCol: {
    span: 5,
  },
  wrapperCol: {
    span: 19,
  },
};

@connect(({ loading }) => ({
  submitting: loading.effects['login/_login'],
  loading: loading.effects['user/getRegister'],
}))
@Form.create()
class Confirm extends Component {
  state = {
  };

  render() {
    const { form } = this.props;
    const { getFieldDecorator } = form;
    return (
      <div style={{ marginTop: 25 }} className={styles.main}>
        <Form.Item {...formItemLayout} label="姓名">
          {getFieldDecorator('name', {
            rules: [
              { required: true, message: '请输入姓名' }],
          })(
            <Input size="large" placeholder="请输入姓名" />
          )}
        </Form.Item>

        <Form.Item {...formItemLayout} className={styles.stepFormText} label="姓名">
          {getFieldDecorator('na1me', {
            rules: [
              { required: true, message: '请输入姓名' }],
          })(
            <Input size="large" placeholder="请输入姓名" />
          )}
        </Form.Item>
      </div>
    );
  }
}

export default Confirm;
