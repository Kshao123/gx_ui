import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Card,
  Form,
  Button,
  Input,
  Divider,
  Modal,
  InputNumber,
  Icon,
  Table,
} from 'antd';
import styles from '@/utils/global.less';
import { scriptUrl } from '@/utils/utils';
import Popcon from '@/components/Popconfirm';
import Authorized from '@/utils/Authorized';

const { check } = Authorized;
const IconFont = Icon.createFromIconfontCN({
  scriptUrl,
});


@connect(({ loading, system }) => ({
  loading: loading.effects['system/_getSystemPrice'],
  data: system.data
}))
@Form.create()
class payList extends PureComponent {
  constructor() {
    super();
    this.state = {
      visible: false,
      isEdit: false,
      FormObj: {},
    };
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'system/_getSystemPrice'
    })
  }

  handleValueChange = ({ key }, id) => {
    this.setState({
      [id]: Number(key),
    });
  };

  handleOk = () => {
    const { form, dispatch } = this.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      if (!this.state.isEdit) {
        delete (values.id);
      }
      dispatch({
        type: 'system/_setSystemPrice',
        payload: values,
      });

      form.resetFields();
      this.setState({
        visible: false,
        isEdit: false,
      });
    });
  };

  handleCancel = () => {
    this.props.form.resetFields();
    this.setState({
      visible: false,
      isEdit: false,
    });
  };

  handleAdd = (item, value) => {
    let obj = {};
    let str = false;
    if (value) {
      obj = item;
      str = true;
    }
    this.setState({
      visible: true,
      FormObj: obj,
      isEdit: str,
    });
  };

  handleConfirm = (record) => {
    const { id } = record;
    const { dispatch } = this.props;
    dispatch({
      type: 'system/_delSystemPrice',
      payload: [id]
    });
  };


  forItem = (type, text) => (
    <div>
      <IconFont type={type} />
      <span>  {text}</span>
    </div>
  );

  render() {
    const {
      loading,
      data,
      form: { getFieldDecorator },
    } = this.props;
    const { forItem } = this;
    const columns = [
      {
        title: '月数',
        dataIndex: 'monthNum',
        key: 'monthNum',
        render(text) {
          return forItem('icon-data', text);
        },
      },
      {
        title: '平均价/月',
        dataIndex: 'avgPrice',
        key: 'avgPrice',
        render(text) {
          return forItem('icon-jiage', text && text > 0 ? `${text}元` : '暂无');
        },
      },
      {
        title: '总金额/元',
        dataIndex: 'totalPrice',
        key: 'totalPrice',
        render(text) {
          return forItem('icon-jiage', text && text > 0 ? `${text}元` : '暂无');
        },
      },
      {
        title: '操作',
        key: 'operation',
        fixed: 'right',
        width: 110,
        render: (text, record) => (
          <Fragment>
            {check('系统管理-价格:修改', <a onClick={() => this.handleAdd(record, true)}>修改</a>)}
            <Divider type="vertical" />
            {check('系统管理-价格:删除', (
              <Popcon
                title="是否确认删除"
                onConfirm={() => this.handleConfirm(record)}
              />
            ))}
          </Fragment>
          ),
      },
    ];
    const { isEdit, FormObj, visible } = this.state;
    return (
      <section>
        <Card>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              {check('系统管理-价格:增加', (
                <Button icon="plus" type="primary" onClick={() => this.handleAdd(undefined)}>
                  新建
                </Button>
              ))}
            </div>
            <Table loading={loading} bordered dataSource={data} columns={columns} />
          </div>
          <Modal
            title="编辑/修改信息"
            visible={visible}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
          >
            <Form layout="vertical">
              {getFieldDecorator('id', {
                initialValue: isEdit ? FormObj.id : '' })(
                  <Input type="hidden" />
              )}
              <Form.Item label="月数">
                {getFieldDecorator('monthNum', {
                  initialValue: isEdit ? FormObj.monthNum : '',
                  rules: [{ required: true, message: '请输入' }],
                })(
                  <InputNumber style={{ width: '100%' }} min={1} max={999999} />
                )}
              </Form.Item>

              <Form.Item label="平均价">
                {getFieldDecorator('avgPrice', {
                  initialValue: isEdit ? FormObj.avgPrice : '',
                  rules: [{ required: true, message: '请输入' }],
                })(
                  <InputNumber style={{ width: '100%' }} min={0} max={999999} precision={2} />
                )}
              </Form.Item>

            </Form>
          </Modal>
        </Card>
      </section>
    );
  }
}
export default payList;
