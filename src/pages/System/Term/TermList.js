import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Card,
  Form,
  Button,
  Input,
  Divider,
  Modal,
  InputNumber,
  Icon,
  Row,
  Col,
  DatePicker,
} from 'antd';
import styles from '@/utils/global.less';
import { parseTime, url, scriptUrl } from '@/utils/utils';
import StandardTable from '@/components/DelTable';
import Popcon from '@/components/Popconfirm';
import SearchSel from "@/components/SearchSel";
import Authorized from '@/utils/Authorized';

const { check } = Authorized;

const IconFont = Icon.createFromIconfontCN({
  scriptUrl,
});
const FormItem = Form.Item;

@connect(({ loading, system }) => ({
  loading: loading.effects['system/_getSystemTerm'],
  data: system.Term
}))
@Form.create()
class payList extends PureComponent {
  constructor() {
    super();
    this.state = {
      visible: false,
      isEdit: false,
      FormObj: {},
      pagination: {
        pageSize: 10,
        pageNum: 1,
      },
      props: {
        showSearch: true,
        onChange: this.handleValueChange,
        queryUrl: `${url}/userOrder/queryTenancyTerm`,
        keyValue: 'username',
        placeholder: '搜索账户',
        attr: {
          id: 'userId',
          name: 'username',
        },
      },
    };
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'system/_getSystemTerm',
      payload: {
        pageSize: 10,
        pageNum: 1,
      }
    })
  }

  handleStandardTableChange = (pagination) => {
    const { dispatch } = this.props;
    const { pageSize, current: pageNum } = pagination;
    this.setState({
      pagination: {
        pageNum,
        pageSize,
      },
    });
    dispatch({
      type: 'system/_getSystemTerm',
      payload: {
        pageNum,
        pageSize,
      },
    });
  };

  handleValueChange = (value, id, data) => {
    const { list } = data;
    const { label } = value;
    if (!list.length) return;
    if (list.length >= 2) {
      data.list = list.filter(item => item.username === label);
    }
    const { dispatch } = this.props;
    dispatch({
      type: 'system/_Updata',
      payload: {
        value: 'saveTerm',
        data,
      }
    });
  };

  handleOk = () => {
    const { form, dispatch } = this.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      if (!this.state.isEdit) {
        delete (values.id);
        delete values.userId
      }
      values.startTime = new Date(values.startTime.toDate()).getTime();
      console.log(values);
      const { pagination } = this.state;
      dispatch({
        type: 'system/_setSystemTerm',
        payload: {
          data: values,
          pagination,
        },
      }).then((res) => {
        if (res) {
          form.resetFields();
          this.setState({
            visible: false,
            isEdit: false,
          });
          dispatch({
            type: 'system/_getSystemTerm',
            payload: pagination,
          });
        }
      });
    });
  };

  handleCancel = () => {
    this.props.form.resetFields();
    this.setState({
      visible: false,
      isEdit: false,
    });
  };

  handleAdd = (item, value) => {
    let obj = {};
    let str = false;
    if (value) {
      obj = item;
      str = true;
    }
    this.setState({
      visible: true,
      FormObj: obj,
      isEdit: str,
    });
  };

  handleConfirm = (record) => {
    const { id } = record;
    const { dispatch } = this.props;
    const { pagination } = this.state;
    dispatch({
      type: 'system/_delSystemTerm',
      payload: {
        ids: [id],
        pagination,
      }
    });
  };

  handleFormReset = () => {
    const { dispatch } = this.props;
    const { pagination } = this.state;
    dispatch({
      type: 'system/_getSystemTerm',
      payload: {
        ...pagination,
      }
    })
  };

  renderSearchForm() {
    const { currentOrg  } = this.props;
    return (
      <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="用户名">
              <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...this.state.props} />
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>重置</Button>
          </Col>
        </Row>
      </Form>
    );
  }


  forItem = (type, text) => (
    <div>
      <IconFont type={type} />
      <span>  {text}</span>
    </div>
  );

  render() {
    const {
      loading,
      data,
      form: { getFieldDecorator },
    } = this.props;
    const { forItem } = this;
    const columns = [
      {
        title: '姓名',
        dataIndex: 'name',
        key: 'name',
        render(text) {
          return forItem('icon-data', text);
        },
      },
      {
        title: '账号',
        dataIndex: 'username',
        key: 'username',
        render(text) {
          return forItem('icon-data', text);
        },
      },
      {
        title: '天数',
        dataIndex: 'dayNum',
        key: 'dayNum',
        render(text) {
          return forItem('icon-data', text);
        },
      },
      {
        title: '开始时间',
        dataIndex: 'startTime',
        align: 'center',
        render(text) {
          return forItem('icon-shijian', (text ? parseTime(text) : '暂无'));
        },
      },
      {
        title: '到期时间',
        dataIndex: 'endTime',
        align: 'center',
        render(text) {
          return forItem('icon-shijian', (text ? parseTime(text) : '暂无'));
        },
      },
      {
        title: '操作',
        key: 'operation',
        fixed: 'right',
        width: 110,
        render: (text, record) => (
          <Fragment>
            {check('账号管理-租期:修改', <a onClick={() => this.handleAdd(record, true)}>修改</a>)}
            <Divider type="vertical" />
            {check('账号管理-租期:删除', (
              <Popcon
                title="是否确认删除"
                onConfirm={() => this.handleConfirm(record)}
              />
            ))}
          </Fragment>
        ),
      },
    ];
    const { isEdit, FormObj, visible } = this.state;
    return (
      <section>
        <Card className={styles.tableListForm}>
          {this.renderSearchForm()}
        </Card>
        <Card>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              {check('账号管理-租期:增加', (
                <Button icon="plus" type="primary" onClick={() => this.handleAdd(undefined)}>
                  新建
                </Button>
              ))}
            </div>
            <StandardTable
              bordered
              loading={loading}
              data={data}
              columns={columns}
              scroll={{ x: 950 }}
              onChange={this.handleStandardTableChange}
            />
          </div>
          <Modal
            title="编辑/修改信息"
            visible={visible}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
          >
            <Form layout="vertical">
              {getFieldDecorator('id', {
                initialValue: isEdit ? FormObj.id : '' })(
                  <Input type="hidden" />
              )}
              {getFieldDecorator('userId', {
                initialValue: isEdit ? FormObj.userId : '' })(
                  <Input type="hidden" />
              )}

              <Form.Item label="账号">
                {getFieldDecorator('username', {
                  initialValue: isEdit ? FormObj.username : '',
                  rules: [{ required: true, message: '请输入账号' }],
                })(
                  <Input disabled={!!isEdit} placeholder="请输入账号" />
                )}
              </Form.Item>

              <Form.Item label="天数">
                {getFieldDecorator('dayNum', {
                  initialValue: isEdit ? FormObj.dayNum : '',
                  rules: [{ required: true, message: '请输入' }],
                })(
                  <InputNumber style={{ width: '100%' }} min={0} max={999999} />
                )}
              </Form.Item>

              {/*<Form.Item label="总金额/元">*/}
                {/*{getFieldDecorator('totalPrice', {*/}
                  {/*initialValue: isEdit ? FormObj.totalPrice : '',*/}
                  {/*rules: [{ required: true, message: '请输入' }],*/}
                {/*})(*/}
                  {/*<InputNumber style={{ width: '100%' }} min={1} max={999999} />*/}
                {/*)}*/}
              {/*</Form.Item>*/}

              <Form.Item label="开始时间">
                {getFieldDecorator('startTime', {
                  initialValue: isEdit ? moment(parseTime(FormObj.startTime)) : moment('2019'),
                  rules: [{ required: true, message: '请输入' }],
                })(
                  <DatePicker disabled={!!isEdit} />
                )}
              </Form.Item>

              {/* <Form.Item label="结束时间"> */}
              {/* {getFieldDecorator('endTime', { */}
              {/* initialValue: isEdit ? moment(parseTime(FormObj.endTime)) : moment('2019'), */}
              {/* rules: [{ required: true, message: '请输入' }], */}
              {/* })( */}
              {/* <DatePicker /> */}
              {/* )} */}
              {/* </Form.Item> */}

            </Form>
          </Modal>
        </Card>
      </section>
    );
  }
}
export default payList;
