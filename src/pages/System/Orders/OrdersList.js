import React, { Fragment, PureComponent } from 'react';
import { connect } from 'dva';
import {
  Card,
  Form,
  Icon,
  Row,
  Col,
  Button,
} from 'antd';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import { scriptUrl, parseTime, url } from '../../../utils/utils';
import Ellipsis from '@/components/Ellipsis';
import Popcon from '@/components/Popconfirm';
import SearchSel from '@/components/SearchSel';
import Authorized from '@/utils/Authorized';

const { check } = Authorized;
const IconFont = Icon.createFromIconfontCN({
  scriptUrl,
});
const FormItem = Form.Item;

@connect(({ system, loading }) => ({
  system,
  loading: loading.effects['system/_getSystemOrders'],
}))
@Form.create()
class ClassroomleaseOpenList extends PureComponent {
  constructor() {
    super();
    this.state = {
      pagination: {
        pageSize: 10,
        pageNum: 1,
      },
      props: {
        removal: true,
        onChange: this.handleValueChange,
        queryUrl: `${url}/userOrder/queryAllOrder`,
        keyValue: 'username',
        showSearch: true,
        attr: {
          id: 'userId',
          name: 'username',
        },
        placeholder: '搜索用户',
      },
    };
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'system/_getSystemOrders',
      payload: {
        pageSize: 10,
        pageNum: 1,
      }
    })
  }

  forItem = (type, text) => (
    <div>
      <IconFont type={type} />
      <span>  {text}</span>
    </div>
  );

  handleStandardTableChange = (pagination) => {
    const { dispatch } = this.props;
    const { pageSize, current: pageNum } = pagination;
    this.setState({
      pagination: {
        pageNum,
        pageSize,
      },
    });
    dispatch({
      type: 'system/_getSystemOrders',
      payload: {
        pageNum,
        pageSize,
      },
    });
  };

  handleConfirm = (record) => {
    const { id } = record;
    const { dispatch } = this.props;
    const { pagination } = this.state;
    dispatch({
      type: 'system/_delSystemOrders',
      payload: {
        payload: pagination,
        ids: [id]
      }
    });
  };

  handleFormReset = () => {
    const { dispatch } = this.props;
    const { pagination } = this.state;
    dispatch({
      type: 'system/_getSystemOrders',
      payload: {
        ...pagination,
      }
    })
  };

  handleValueChange = (value, id, data) => {
    const { list } = data;
    const { label } = value;
    if (!list.length) return;
    if (list.length >= 2) {
      data.list = list.filter(item => item.username === label);
    }
    const { dispatch } = this.props;
    dispatch({
      type: 'system/_Updata',
      payload: {
        value: 'saveOrders',
        data,
      }
    });
  };

  renderSearchForm() {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="用户名">
              {getFieldDecorator('stuId')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...this.state.props} />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>重置</Button>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const { forItem } = this;
    const columns = [
      {
        title: '姓名',
        dataIndex: 'name',
        align: 'center',
        render(text) {
          return forItem('icon-mingcheng', text);
        },
      },
      {
        title: '账号',
        dataIndex: 'username',
        align: 'center',
        render(text) {
          return forItem('icon-icon-', text);
        },
      },
      {
        title: '总金额/元',
        dataIndex: 'totalFee',
        align: 'center',
        render(text) {
          return forItem('icon-jiage', text);
        },
      },
      {
        title: '商品描述',
        dataIndex: 'tradeBody',
        align: 'center',
        render(text) {
          return (
            <div className={styles.TableEllipsis}>
              <IconFont style={{ marginRight: 4 }} type="icon-fankui" />
              <Ellipsis length={6} tooltip>
                {text}
              </Ellipsis>
            </div>
          );
        },
      },
      {
        title: '订单号',
        dataIndex: 'outTradeNo',
        align: 'center',
        render(text) {
          return (
            <div className={styles.TableEllipsis}>
              <IconFont style={{ marginRight: 4 }} type="icon-gongzidan" />
              <Ellipsis length={18} tooltip>
                {text}
              </Ellipsis>
            </div>
          );
        },
      },
      {
        title: '交易时间',
        dataIndex: 'payTime',
        align: 'center',
        render(text) {
          return forItem('icon-shijian', (text ? parseTime(text) : '暂无'));
        },
      },
      {
        title: '操作',
        key: 'operation',
        fixed: 'right',
        width: 75,
        render: (text, record) => (
          <Fragment>
            {check('账号管理-订单:删除', (
              <Popcon
                title="是否确认删除"
                onConfirm={() => this.handleConfirm(record)}
              />
            ))}
          </Fragment>
        ),
      },
    ];

    const { system: { Orders: data }, loading } = this.props;
    return (
      <div>
        <Card className={styles.tableListForm}>
          {this.renderSearchForm()}
        </Card>
        <Card>
          <div className={styles.tableList}>
            <StandardTable
              bordered
              loading={loading}
              data={data}
              columns={columns}
              scroll={{ x: 950 }}
              onChange={this.handleStandardTableChange}
            />
          </div>
        </Card>
      </div>

    );
  }
}

export default ClassroomleaseOpenList;
