import React, { memo } from 'react';
import { Row, Col, Card, Tabs, DatePicker } from 'antd';
import { FormattedMessage, formatMessage } from 'umi/locale';
import numeral from 'numeral';
import styles from './Analysis.less';
import { Bar, CourseTable, TeaRate } from '@/components/Charts';
// import CourseTable from './couresTable'

const { RangePicker } = DatePicker;
const { TabPane } = Tabs;

const rankingListData = [];
for (let i = 0; i < 7; i += 1) {
  rankingListData.push({
    title: formatMessage({ id: 'app.analysis.test' }, { no: i }),
    total: 323234,
  });
}

const SalesCard = memo(
  ({
     rangePickerValue,
     isActive,
     handleRangePickerChange,
     loading,
     selectDate,
     courseInfo,
     handleTabChange,
     StudentChangeInfo,
     classRoomData,
     TeaClassRateInfo,
   }) => (
     <Card loading={loading} bordered={false} bodyStyle={{ padding: 0 }}>
       <div className={styles.salesCard}>
         <Tabs
           onChange={handleTabChange}
           tabBarExtraContent={
             <div className={styles.salesExtraWrap}>
               <div className={styles.salesExtra}>
                 <a className={isActive('today')} onClick={() => selectDate('today')}>
                   <FormattedMessage id="app.analysis.all-day" defaultMessage="All Day" />
                 </a>
                 {/* <a className={isActive('week')} onClick={() => selectDate('week')}> */}
                 {/*  <FormattedMessage id="app.analysis.all-week" defaultMessage="All Week" /> */}
                 {/* </a> */}
                 <a className={isActive('month')} onClick={() => selectDate('month')}>
                   <FormattedMessage id="app.analysis.all-month" defaultMessage="All Month" />
                 </a>
                 <a className={isActive('year')} onClick={() => selectDate('year')}>
                   <FormattedMessage id="app.analysis.all-year" defaultMessage="All Year" />
                 </a>
               </div>
               <RangePicker
                 value={rangePickerValue}
                 onChange={handleRangePickerChange}
                 style={{ width: 256 }}
               />
             </div>
          }
           size="large"
           tabBarStyle={{ marginBottom: 24 }}
         >
           <TabPane
             tab="课时变动"
             key="course"
           >
             <Row>
               <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                 <div className={styles.salesBar}>
                   <CourseTable
                     title="课时数/节"
                     data={courseInfo}
                   />
                 </div>
               </Col>
             </Row>
           </TabPane>
           <TabPane
             tab="学员涨幅"
             key="stuNum"
           >
             <Row>
               <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                 <div className={styles.salesBar}>
                   <CourseTable
                     title="学员人数"
                     data={StudentChangeInfo}
                   />
                 </div>
               </Col>
             </Row>
           </TabPane>
           <TabPane
             tab="教室订单"
             key="classRoom"
           >
             <Row>
               <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                 <div className={styles.salesBar}>
                   <Bar
                     height={295}
                     title="教室订单信息 / 元"
                     data={classRoomData}
                   />
                 </div>
               </Col>
             </Row>
           </TabPane>
           <TabPane
             tab="上课率"
             key="teaRate"
           >
             <Row>
               <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                 <div className={styles.salesBar}>
                   <TeaRate
                     height={295}
                     data={TeaClassRateInfo}
                   />
                 </div>
               </Col>
             </Row>
           </TabPane>
         </Tabs>
       </div>
     </Card>
  )
);

export default SalesCard;
