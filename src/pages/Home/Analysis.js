import React, { Component, Suspense } from 'react';
import { connect } from 'dva';
import { Row, Col, Icon, Tooltip, Avatar } from 'antd';
import moment from 'moment';
import numeral from 'numeral';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import { getTimeDistance, parseTime } from '@/utils/utils';
import Yuan from '@/utils/Yuan';
import Trend from '@/components/Trend';
import styles from './Analysis.less';
import {
  ChartCard,
  MiniArea,
  MiniBar,
  MiniProgress,
  Field,
} from '@/components/Charts';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import BirthDayInfo from './BirthDayInfo';

const visitData = [];
const beginDay = new Date().getTime();
const fakeY2 = [1, 6, 4, 12, 33, 4, 3, 7, 2, 2, 3];
for (let i = 0; i < fakeY2.length; i += 1) {
  visitData.push({
    x: moment(new Date(beginDay + 1000 * 60 * 60 * 24 * i)).format('YYYY-MM-DD'),
    y: fakeY2[i],
  });
}
// tabs 里的柱状图数据
document.body.onselectstart = () => false;
const date = new Date();
const endTimeN = date.getTime();
const startTimeN = endTimeN - (1296000000);
const TYPE = {
  'today': 1,
  'month': 2,
  'year': 3
};

const SalesCard = React.lazy(() => import('./SalesCard'));
@connect(({
  layoutloading: { userMsg, currentUser },
  global: { currentOrg },
  loading,
  home: {
    data,
    payData,
    courseInfo,
    StudentInfo,
    StudentChangeInfo,
    UserBirthdayInfo,
    ClassMoneyInfo,
    TeaClassRateInfo,
  },
}) => ({
  loading: loading.global,
  payDataLod: loading.effects['home/getPayData'],
  userMsg,
  currentUser,
  currentOrg,
  data,
  payData,
  courseInfo,
  StudentInfo,
  StudentChangeInfo,
  UserBirthdayInfo,
  ClassMoneyInfo,
  TeaClassRateInfo,
  loadingLine: loading.effects['home/_getOnline'],
  loadUser: loading.effects['layoutloading/getUserMsg'],
  loadStu: loading.effects['home/getStudentNum'],
  loadStuNum: loading.effects['home/getStudentChange'],
  loadBirthDay: loading.effects['home/getUserBirthday']
}))

class Analysis extends Component {
  constructor(props) {
    super(props);
    const { currentOrg } = props;
    this.state = {
      currentOrg: {
        id: currentOrg.id || '',
      },
      salesData: [],
      rangePickerValue: getTimeDistance('year'),
      timeType: 'year',
      currentTab: 'course',
      dateValue: moment(),
      selValue: 1,
    };
  }

  componentDidMount() {
    const { dispatch, currentUser, currentOrg } = this.props;
    const { userid } = currentUser;
    const { id } = currentOrg;
    if (id) {
      if (userid) {
        dispatch({
          type: 'layoutloading/getUserMsg',
          payload: userid,
        });
      }
      dispatch({
        type: 'home/getPayData',
        payload: {
          orgId: id,
          startTime: startTimeN,
          endTime: endTimeN,
        }
      });
      dispatch({
        type: 'home/getStudentNum',
        payload: {
          orgId: id,
        }
      });
      const { rangePickerValue, timeType } = this.state;
      if (rangePickerValue.length) {
        dispatch({
          type: 'home/getCourseNumChange',
          payload: {
            orgId: id,
            startTime: rangePickerValue[0].toDate().getTime(),
            endTime: rangePickerValue[1].toDate().getTime(),
            timeType: TYPE[timeType],
          }
        });
      }
      dispatch({
        type: 'home/getUserBirthday',
        payload: {
          orgId: currentOrg.id,
          startTime: startTimeN,
          type: 1,
        },
      });
      this.setState({
        currentOrg,
      })
    }
    dispatch({
      type: 'home/_getOnline',
      payload: {
        dayNumber: 14,
      },
    });
  }

  static getDerivedStateFromProps(nextProps, preState) {
    const { dispatch } = nextProps;
    if (!nextProps.currentOrg.id) return null;
    if (nextProps.currentOrg.id === preState.currentOrg.id) {
      return null;
    }
    const { rangePickerValue, timeType: type, dateValue, selValue = 1 } = preState;
    if (rangePickerValue.length) {
      const startTime = rangePickerValue[0].toDate().getTime();
      const endTime = rangePickerValue[1].toDate().getTime();
      const timeType = TYPE[type];
      switch (preState.currentTab) {
        case 'course':
          dispatch({
            type: 'home/getCourseNumChange',
            payload: {
              orgId: nextProps.currentOrg.id,
              startTime,
              endTime,
              timeType,
            }
          });
          break;
        case 'stuNum':
          dispatch({
            type: 'home/getStudentChange',
            payload: {
              orgId: nextProps.currentOrg.id,
              startTime,
              endTime,
              timeType,
            }
          });
          break;
        case 'classRoom':
          dispatch({
            type: 'home/getClassMoney',
            payload: {
              orgId: nextProps.currentOrg.id,
              startTime,
              endTime,
              timeType,
            }
          });
          break;
        case 'teaRate':
          dispatch({
            type: 'home/getTeaClassRate',
            payload: {
              orgId: nextProps.currentOrg.id,
              startTime,
              endTime,
              timeType,
            }
          });
          break;
        default:
          break;
      }
    }
    dispatch({
      type: 'home/getPayData',
      payload: {
        orgId: nextProps.currentOrg.id,
        startTime: startTimeN,
        endTime: endTimeN,
      }
    });
    dispatch({
      type: 'home/getStudentNum',
      payload: {
        orgId: nextProps.currentOrg.id,
      }
    });
    dispatch({
      type: 'home/getUserBirthday',
      payload: {
        orgId: nextProps.currentOrg.id,
        startTime: dateValue.toDate().getTime(),
        type: selValue,
      },
    });
    if (nextProps.currentUser.userid) {
      dispatch({
        type: 'layoutloading/getUserMsg',
        payload: nextProps.currentUser.userid,
      });
    }
    return {
      currentOrg: nextProps.currentOrg,
    };
  }

  queryData = (type, { orgId, time = [] }, timeType) => {
    const { dispatch } = this.props;
    let disTime = time;
    if (!time.length) {
      disTime = getTimeDistance( timeType || 'year');
    }
    // today 1 month 2 year 3
    const Type = TYPE[timeType];
    const startTime = disTime[0].toDate().getTime();
    const endTime = disTime[1].toDate().getTime();
    return new Promise((resolve, reject) => {
      dispatch({
        type,
        payload: {
          orgId,
          startTime,
          endTime,
          timeType: Type
        },
      })
        .then(res => {
          if (res) {
            resolve();
          }
        })
    })
  };

  handleTabChange = activeKey => {
    const { currentOrg, StudentChangeInfo, ClassMoneyInfo } = this.props;
    const { timeType, rangePickerValue } = this.state;
    switch (activeKey) {
      case 'course':
        // if (!StudentChangeInfo.length) {
        this.queryData(
          'home/getCourseNumChange',
          {
            orgId: currentOrg.id,
            time: rangePickerValue
          },
          timeType
        );
        // }
        break;
      case 'stuNum':
        // if (!StudentChangeInfo.length) {
          this.queryData(
            'home/getStudentChange',
            {
              orgId: currentOrg.id,
              time: rangePickerValue
            },
            timeType
          );
        // }
        break;
      case 'classRoom':
        // if (!ClassMoneyInfo.length) {
          this.queryData(
            'home/getClassMoney',
            {
              orgId: currentOrg.id,
              time: rangePickerValue
            },
            timeType
          );
        // }
          break;
      case 'teaRate':
          this.queryData(
            'home/getTeaClassRate',
            {
              orgId: currentOrg.id,
              time: rangePickerValue
            },
            timeType
          );
          break;
      default:
        break;
    }
    this.setState({
      currentTab: activeKey,
    })
  };

  handleRangePickerChange = rangePickerValue => {
    const { dispatch, currentOrg } = this.props;
    const { timeType, currentTab } = this.state;
    switch (currentTab) {
      case 'course':
        this.queryData(
          'home/getCourseNumChange',
          {
            orgId: currentOrg.id,
            time: rangePickerValue,
          },
          timeType,
        );
        break;
      case 'stuNum':
        this.queryData(
          'home/getStudentChange',
          {
            orgId: currentOrg.id,
            time: rangePickerValue,
          },
          timeType,
        );
        break;
      case 'classRoom':
        this.queryData(
          'home/getClassMoney',
          {
            orgId: currentOrg.id,
            time: rangePickerValue,
          },
          timeType,
        );
        break;
      case 'teaRate':
        this.queryData(
          'home/getTeaClassRate',
          {
            orgId: currentOrg.id,
            time: rangePickerValue,
          },
          timeType,
        );
        break;
      default:
        break;
    }

    this.setState({
      rangePickerValue,
    });

    dispatch({
      type: 'chart/fetchSalesData',
    });
  };

  selectDate = type => {
    const { currentOrg } = this.props;
    const rangePickerValue = getTimeDistance(type);
    const { currentTab } = this.state;
    switch (currentTab) {
      case 'course':
        this.queryData(
          'home/getCourseNumChange',
          {
            orgId: currentOrg.id,
            time: rangePickerValue
          },
          type
        );
        break;
      case 'stuNum':
        this.queryData(
          'home/getStudentChange',
          {
            orgId: currentOrg.id,
            time: rangePickerValue
          },
          type
        );
        break;
      case 'classRoom':
        this.queryData(
          'home/getClassMoney',
          {
            orgId: currentOrg.id,
            time: rangePickerValue
          },
          type
        );
        break;
      case 'teaRate':
        this.queryData(
          'home/getTeaClassRate',
          {
            orgId: currentOrg.id,
            time: rangePickerValue
          },
          type
        );
        break;
      default:
        break;
    }
    this.setState({
      timeType: type,
      rangePickerValue,
    });
  };

  isActive = type => {
    const { rangePickerValue } = this.state;
    const value = getTimeDistance(type);
    if (!rangePickerValue[0] || !rangePickerValue[1]) {
      return '';
    }
    if (
      rangePickerValue[0].isSame(value[0], 'day') &&
      rangePickerValue[1].isSame(value[1], 'day')
    ) {
      return styles.currentDate;
    }
    return '';
  };

  getTime = type => {
    const date = new Date();
    switch (type) {
      case 'years':
        return date.getFullYear();
      case 'month':
        return date.getMonth() + 1;
      default:
        break;
    }
  };

  // 待优化。。。
  renderData = (data= [], keys= ['plusCourseNumChange', 'lessCourseNumChange']) => {
    // today 1 month 2 year 3
    if (!data.length) return [];
    const { timeType } = this.state;
    const [ plus, less ] = keys;
    switch (timeType) {
      case 'today':
        return data.map(item => (
          {
            date: parseTime(item.time, '{d}'),
            '新增': item[plus],
            '减少': item[less],
          }
        ));
      case 'month':
        return data.map(item => (
          {
            date: parseTime(item.time, '{m}') + '月',
            '新增': item[plus],
            '减少': item[less],
          }
        ));
      case 'year':
        return data.map(item => (
          {
            date: parseTime(item.time, '{y}') + '年',
            '新增': item[plus],
            '减少': item[less],
          }
        ));
      default:
        return []
    }
  };

  renderCLassRoomData = (data= []) => {
    // today 1 month 2 year 3
    if (!data.length) return [];
    const { timeType } = this.state;
    switch (timeType) {
      case 'today':
        return data.map(item => (
          {
            x: parseTime(item.time, '{d}'),
            y: item.money
          }
        ));
      case 'month':
        return data.map(item => (
          {
            x: parseTime(item.time, '{m}') + '月',
            y: item.money
          }
        ));
      case 'year':
        return data.map(item => (
          {
            x: parseTime(item.time, '{y}') + '年',
            y: item.money
          }
        ));
      default:
        return []
    }
  };

  renderTeaRate = (data= []) => {
    // today 1 month 2 year 3
    if (!data.length) return [];
    const { timeType } = this.state;
    switch (timeType) {
      case 'today':
        return data.map(item => (
          {
            x: parseTime(item.time, '{d}'),
            y: item.dayRealityCourseNum,
            z: item.dayShouldCourseNum,
          }
        ));
      case 'month':
        return data.map(item => (
          {
            x: `${parseTime(item.time, '{m}')}月`,
            y: item.dayRealityCourseNum,
            z: item.dayShouldCourseNum,
          }
        ));
      case 'year':
        return data.map(item => (
          {
            x: parseTime(item.time, '{y}') + '年',
            y: item.dayRealityCourseNum,
            z: item.dayShouldCourseNum,
          }
        ));
      default:
        return []
    }
  };

  handleDateChange = (dateValue) => {
    const { selValue } = this.state;
    const { dispatch, currentOrg } = this.props;
    dispatch({
      type: 'home/getUserBirthday',
      payload: {
        orgId: currentOrg.id,
        startTime: dateValue.toDate().getTime(),
        type: selValue
      }
    });
    this.setState({
      dateValue
    })
  };

  handleSelChange = selValue => {
    const { dateValue } = this.state;
    const { dispatch, currentOrg } = this.props;
    dispatch({
      type: 'home/getUserBirthday',
      payload: {
        orgId: currentOrg.id,
        startTime: dateValue.toDate().getTime(),
        type: selValue
      }
    });
    this.setState({
      selValue
    })
  };

  render() {
    const {
      loading,
      rangePickerValue,
      dateValue,
      selValue,
    } = this.state;
    const {
      currentUser,
      currentOrg,
      data,
      userMsg,
      payData: { refund, dayAllMoney, recharge },
      payDataLod,
      courseInfo,
      loadingLine,
      loadUser,
      loadStu,
      StudentInfo: { allStudentNum, InactiveStudentNum },
      StudentChangeInfo,
      loadBirthDay,
      UserBirthdayInfo,
      ClassMoneyInfo,
      TeaClassRateInfo,
    } = this.props;
    let timeData = [];
    if (data.everyDayOnlinePeopleNumber) {
      timeData = data.everyDayOnlinePeopleNumber.map((item) => ({
          y: item.number,
          x: parseTime(item.time).split(' ')[0],
        }));
    }

    const topColResponsiveProps = {
      xs: 24,
      sm: 12,
      md: 12,
      lg: 12,
      xl: 6,
      style: { marginBottom: 24 },
    };

    const pageHeaderContent =
      currentUser && Object.keys(currentUser).length ? (
        <div className={styles.pageHeaderContent}>
          <div className={styles.avatar}>
            <Avatar size="large" src={userMsg.picture ? userMsg.picture[0] : ''} />
          </div>
          <div className={styles.content}>
            <div className={styles.contentTitle}>
              早安，
              {currentUser.name}
              ，祝你开心每一天！
            </div>
            <div>
              当前机构：{currentOrg ? currentOrg.orgName : 'loading...'}
            </div>
          </div>
        </div>
      ) : null;

    return (
      <PageHeaderWrapper
        loading={loadUser}
        content={pageHeaderContent}
      >
        <GridContent>
          <Row style={{ marginTop: 20 }} gutter={24}>
            <Col {...topColResponsiveProps}>
              <ChartCard
                bordered={false}
                title="总销售额"
                action={
                  <Tooltip
                    title="近15日销售额"
                  >
                    <Icon type="info-circle-o" />
                  </Tooltip>
                }
                loading={payDataLod}
                total={() => <Yuan>{recharge + refund}</Yuan>}
                footer={
                  <Field
                    label="日销售额"
                    value={`￥${numeral(dayAllMoney).format('0,0')}`}
                  />
                }
                contentHeight={46}
              >
                <Trend flag="up" style={{ marginRight: 16 }}>
                  营收
                  <span className={styles.trendText}>{recharge}</span>
                </Trend>
                <Trend flag="down">
                  退款
                  <span className={styles.trendText}>{refund}</span>
                </Trend>
              </ChartCard>
            </Col>
            <Col {...topColResponsiveProps}>
              <ChartCard
                bordered={false}
                loading={loadingLine}
                title="当前在线人数"
                action={
                  <Tooltip
                    title="在线人数"
                  >
                    <Icon type="info-circle-o" />
                  </Tooltip>
                }
                total={numeral(data.onlinePeopleNumber).format('0,0')}
                footer={
                  <Field
                    label={
                      <span>
                        最近14日在线情况
                      </span>
                    }
                  />
                }
                contentHeight={46}
              >
                <MiniArea color="#975FE4" data={timeData} />
              </ChartCard>
            </Col>
            <Col {...topColResponsiveProps}>
              <ChartCard
                bordered={false}
                loading={loadStu}
                title="学员人数"
                action={
                  <Tooltip
                    title="学员概况"
                  >
                    <Icon type="info-circle-o" />
                  </Tooltip>
              }
                total={numeral(allStudentNum).format('0,0')}
                footer={
                  <Field
                    label={
                      <span>
                      僵尸学员:
                      </span>
                  }
                    value={numeral(InactiveStudentNum).format('0,0')}
                  />
              }
                contentHeight={46}
              >
                <MiniProgress percent={InactiveStudentNum / allStudentNum * 100} strokeWidth={8} target={100} color="#13C2C2" />
              </ChartCard>
            </Col>
          </Row>
          <Suspense fallback={null}>
            <SalesCard
              rangePickerValue={rangePickerValue}
              classRoomData={this.renderCLassRoomData(ClassMoneyInfo) || []}
              isActive={this.isActive}
              handleRangePickerChange={this.handleRangePickerChange}
              loading={loading}
              selectDate={this.selectDate}
              courseInfo={this.renderData(courseInfo)}
              handleTabChange={this.handleTabChange}
              StudentChangeInfo={this.renderData(StudentChangeInfo, ['plusStudentNum', 'lessStudentNum'])}
              TeaClassRateInfo={this.renderTeaRate(TeaClassRateInfo)}
            />
          </Suspense>
          <div className={styles.twoColLayout}>
            <Row gutter={24}>
              <Col xl={12} lg={24} md={24} sm={24} xs={24}>
                <Suspense fallback={null}>
                  <BirthDayInfo
                    dateValue={dateValue}
                    loading={loadBirthDay}
                    handleDateChange={this.handleDateChange}
                    selValue={selValue}
                    handleSelChange={this.handleSelChange}
                    data={UserBirthdayInfo.map(item => (
                      {
                        amount: !item.amount ? 0.5 : -(3 + item.amount),
                        date: parseTime(item.date, '{y}-{m}-{d}'),
                        name: item.name,
                      }
                    ))}
                  />
                </Suspense>
              </Col>
            </Row>
          </div>
        </GridContent>
      </PageHeaderWrapper>
    );
  }
}

export default Analysis;
