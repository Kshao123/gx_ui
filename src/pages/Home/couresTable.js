import React from 'react';
import { Chart, Axis, Tooltip, Geom, Legend } from 'bizcharts';
import DataSet from '@antv/data-set';


// 下面的代码会被作为 cdn script 注入 注释勿删
// CDN START
const data = [
  { label: '0.1', '放款应还本金': 2800, '价格': 2800 },
  { label: '0.2', '放款应还本金': 1800, '价格': 1800 },
];
const ds = new DataSet();
const dv = ds.createView().source(data);
dv.transform({
  type: 'fold',
  fields: ['放款应还本金', '价格', '收益'], // 展开字段集
  key: 'type', // key字段
  value: 'value', // value字段
});
const scale = {
  '总收益率': {
    type: 'cat',
    min: 0,
  },
};

let chartIns = null;

const getG2Instance = (chart) => {
  chartIns = chart;
};

class CouresTable extends React.Component {
  render() {
    return (
      <Chart height={400} width={500} forceFit data={dv} scale={scale} padding="auto" onGetG2Instance={getG2Instance}>
        <Legend
          custom
          allowAllCanceled
          items={[
            { value: '放款应还本金', marker: { symbol: 'square', fill: '#3182bd', radius: 5 } },
            { value: '价格', marker: { symbol: 'square', fill: '#41a2fc', radius: 5 } },
            { value: '收益', marker: { symbol: 'square', fill: '#54ca76', radius: 5 } },
            { value: '总收益率', marker: { symbol: 'hyphen', stroke: '#fad248', radius: 5, lineWidth: 3 } },
          ]}
          onClick={(ev) => {
            const item = ev.item;
            const value = item.value;
            const checked = ev.checked;
            const geoms = chartIns.getAllGeoms();
            for (let i = 0; i < geoms.length; i++) {
              const geom = geoms[i];
              if (geom.getYScale().field === value && value === '总收益率') {
                if (checked) {
                  geom.show();
                } else {
                  geom.hide();
                }
              } else if (geom.getYScale().field === 'value' && value !== '总收益率') {
                geom.getShapes().map((shape) => {
                  if (shape._cfg.origin._origin.type == value) {
                    shape._cfg.visible = !shape._cfg.visible;
                  }
                  shape.get('canvas').draw();
                  return shape;
                });
              }
            }
          }}
        />
        <Axis name="label" />
        <Axis name="value" position={'left'} />
        <Tooltip />
        <Geom
          type="interval"
          position="label*value"
          color={['type', (value) => {
            if (value === '放款应还本金') {
              return '#2b6cbb';
            }
            if (value === '价格') {
              return '#41a2fc';
            }
            if (value === '收益') {
              return '#54ca76';
            }
          }]}
          adjust={[{
            type: 'dodge',
            marginRatio: 1 / 32,
          }]}
        />
        <Geom type="line" position="label*总收益率" color="#fad248" size={3} />
      </Chart>
    );
  }
}

export default CouresTable
