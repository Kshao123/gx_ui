import React, { memo } from 'react';
// import { FormattedMessage } from 'umi-plugin-react/locale';
import { Card, DatePicker, Select } from 'antd';
import styles from './Analysis.less';
import { UserBirsday } from '@/components/Charts';
import Yuan from '@/utils/Yuan';

const { MonthPicker } = DatePicker;
const { Option } = Select;

const ProportionSales = memo(
  ({ dateValue, loading, handleDateChange, selValue, handleSelChange, data }) => (
    <Card
      loading={loading}
      className={styles.salesCard}
      bordered={false}
      title={`${selValue === 1 ? '学生' : '老师'}生日近况`}
      bodyStyle={{ padding: 24 }}
      extra={
        <div>
          <Select
            style={{ width: 120, marginRight: 15 }}
            value={selValue}
            onChange={handleSelChange}
          >
            <Option value={1}>学生</Option>
            <Option value={2}>老师</Option>
          </Select>
          <MonthPicker
            value={dateValue}
            onChange={handleDateChange}
          />
        </div>
      }
      style={{ marginTop: 24 }}
    >
      <h4 style={{ marginTop: 10, marginBottom: 32 }}>
        {`${selValue === 1 ? '学生' : '老师'}生日`}
      </h4>
      <UserBirsday data={data} />
    </Card>
  )
);

export default ProportionSales;
