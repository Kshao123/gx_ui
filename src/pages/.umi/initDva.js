import dva from 'dva';
import createLoading from 'dva-loading';

const runtimeDva = window.g_plugins.mergeConfig('dva');
let app = dva({
  history: window.g_history,
  
  ...(runtimeDva.config || {}),
});

window.g_app = app;
app.use(createLoading());
(runtimeDva.plugins || []).forEach(plugin => {
  app.use(plugin);
});

app.model({ namespace: 'api', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/api.js').default) });
app.model({ namespace: 'Business', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/Business.js').default) });
app.model({ namespace: 'classRoom', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/classRoom.js').default) });
app.model({ namespace: 'coupons', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/coupons.js').default) });
app.model({ namespace: 'global', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/global.js').default) });
app.model({ namespace: 'home', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/home.js').default) });
app.model({ namespace: 'Inventory', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/Inventory.js').default) });
app.model({ namespace: 'layoutloading', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/layoutloading.js').default) });
app.model({ namespace: 'list', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/list.js').default) });
app.model({ namespace: 'login', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/login.js').default) });
app.model({ namespace: 'menu', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/menu.js').default) });
app.model({ namespace: 'mine', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/mine.js').default) });
app.model({ namespace: 'one22one', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/one22one.js').default) });
app.model({ namespace: 'one2one', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/one2one.js').default) });
app.model({ namespace: 'project', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/project.js').default) });
app.model({ namespace: 'role', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/role.js').default) });
app.model({ namespace: 'schedule', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/schedule.js').default) });
app.model({ namespace: 'school', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/school.js').default) });
app.model({ namespace: 'schoolControl', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/schoolControl.js').default) });
app.model({ namespace: 'setting', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/setting.js').default) });
app.model({ namespace: 'stuApi', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/stuApi.js').default) });
app.model({ namespace: 'studentDatas', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/studentDatas.js').default) });
app.model({ namespace: 'stuLeave', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/stuLeave.js').default) });
app.model({ namespace: 'system', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/system.js').default) });
app.model({ namespace: 'teacher', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/teacher.js').default) });
app.model({ namespace: 'user', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/user.js').default) });
app.model({ namespace: 'userMgr', ...(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/models/userMgr.js').default) });
