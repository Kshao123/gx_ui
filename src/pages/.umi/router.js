import React from 'react';
import { Router as DefaultRouter, Route, Switch } from 'react-router-dom';
import dynamic from 'umi/dynamic';
import renderRoutes from 'umi/_renderRoutes';
import RendererWrapper0 from 'C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/pages/.umi/LocaleWrapper.jsx'
import _dvaDynamic from 'dva/dynamic'

let Router = require('dva/router').routerRedux.ConnectedRouter;

let routes = [
  {
    "path": "/user",
    "component": _dvaDynamic({
  
  component: () => import('../../layouts/UserLayout'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
    "routes": [
      {
        "path": "/user",
        "redirect": "/user/login",
        "exact": true
      },
      {
        "path": "/user/login",
        "component": _dvaDynamic({
  app: window.g_app,
models: () => [
  import('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/pages/User/models/register.js').then(m => { return { namespace: 'register',...m.default}})
],
  component: () => import('../User/Login'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
        "exact": true
      },
      {
        "path": "/user/register",
        "component": _dvaDynamic({
  app: window.g_app,
models: () => [
  import('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/pages/User/models/register.js').then(m => { return { namespace: 'register',...m.default}})
],
  component: () => import('../User/Register'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
        "exact": true
      },
      {
        "path": "/user/register-result",
        "component": _dvaDynamic({
  app: window.g_app,
models: () => [
  import('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/pages/User/models/register.js').then(m => { return { namespace: 'register',...m.default}})
],
  component: () => import('../User/RegisterResult'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
        "exact": true
      },
      {
        "path": "/user/registerStep",
        "component": _dvaDynamic({
  app: window.g_app,
models: () => [
  import('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/pages/User/models/register.js').then(m => { return { namespace: 'register',...m.default}})
],
  component: () => import('../User/StepForm'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
        "routes": [
          {
            "path": "/user/registerStep/info",
            "component": _dvaDynamic({
  app: window.g_app,
models: () => [
  import('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/pages/User/models/register.js').then(m => { return { namespace: 'register',...m.default}})
],
  component: () => import('../User/StepForm/Step1'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "path": "/user/registerStep/result",
            "component": _dvaDynamic({
  app: window.g_app,
models: () => [
  import('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/pages/User/models/register.js').then(m => { return { namespace: 'register',...m.default}})
],
  component: () => import('../User/StepForm/Step2'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "path": "/user/registerStep/pay",
            "component": _dvaDynamic({
  app: window.g_app,
models: () => [
  import('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/pages/User/models/register.js').then(m => { return { namespace: 'register',...m.default}})
],
  component: () => import('../User/StepForm/Step3'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "component": () => React.createElement(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
          }
        ]
      },
      {
        "path": "/user/confirm",
        "component": _dvaDynamic({
  app: window.g_app,
models: () => [
  import('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/pages/User/models/register.js').then(m => { return { namespace: 'register',...m.default}})
],
  component: () => import('../User/Confirm/Confirm'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
        "exact": true
      },
      {
        "component": () => React.createElement(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
      }
    ]
  },
  {
    "path": "/",
    "component": _dvaDynamic({
  
  component: () => import('../../layouts/BasicLayout'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
    "Routes": [require('../Authorized').default],
    "routes": [
      {
        "path": "/",
        "redirect": "/Home/Analysis",
        "exact": true
      },
      {
        "path": "/Home/Analysis",
        "name": "home",
        "icon": "dashboard",
        "component": _dvaDynamic({
  
  component: () => import('../Home/Analysis'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
        "exact": true
      },
      {
        "name": "schedules",
        "icon": "table",
        "path": "/schedules",
        "component": _dvaDynamic({
  
  component: () => import('../Schedule/schedulesDemo'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
        "authority": "查询课程表",
        "exact": true
      },
      {
        "name": "curriculum",
        "icon": "book",
        "path": "/teaching",
        "authority": [
          "一对一:查询",
          "小组授课:查询",
          "科目类型:查询",
          "科目管理:查询"
        ],
        "routes": [
          {
            "name": "subjectType",
            "path": "/teaching/SubjectType",
            "component": _dvaDynamic({
  
  component: () => import('../Teaching/SubjectType/SubjectTypeList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "authority": "科目类型:查询",
            "exact": true
          },
          {
            "name": "classification",
            "path": "/teaching/classification",
            "component": _dvaDynamic({
  
  component: () => import('../Teaching/Classification/classFicationList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "authority": "科目管理:查询",
            "exact": true
          },
          {
            "path": "/teaching/classification/mgr",
            "component": _dvaDynamic({
  
  component: () => import('../Teaching/Classification/mgr'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "authority": "科目管理:查询",
            "exact": true
          },
          {
            "name": "one2one",
            "path": "/teaching/one2one",
            "component": _dvaDynamic({
  
  component: () => import('../Teaching/One2One/One2OneList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "authority": "一对一:查询",
            "exact": true
          },
          {
            "path": "/teaching/one2one/mgr",
            "component": _dvaDynamic({
  
  component: () => import('../Teaching/One2One/One2One'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "name": "one22one",
            "path": "/teaching/one22one",
            "component": _dvaDynamic({
  
  component: () => import('../Teaching/One22One/One22OneList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "authority": "小组授课:查询",
            "exact": true
          },
          {
            "path": "/teaching/one22one/mgr",
            "component": _dvaDynamic({
  
  component: () => import('../Teaching/One22One/One22One'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "component": () => React.createElement(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
          }
        ]
      },
      {
        "name": "student",
        "icon": "user-add",
        "path": "/student",
        "authority": [
          "学生管理-正式学生:查询",
          "请假管理-请假记录:查询",
          "学生管理-咨询学生:查询",
          "学生交费功能:查询",
          "学生管理-试听记录:查询",
          "学生管理-潜在学生信息"
        ],
        "routes": [
          {
            "name": "data",
            "path": "/student/data",
            "authority": "学生管理-正式学生:查询",
            "component": _dvaDynamic({
  
  component: () => import('../student/Data/StudentDataList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "path": "/student/data/Mgr",
            "component": _dvaDynamic({
  
  component: () => import('../student/Data/StudentDataMgr'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "name": "Audition",
            "path": "/student/Audition",
            "authority": "学生管理-咨询学生:查询",
            "component": _dvaDynamic({
  
  component: () => import('../student/Audition/AuditionList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "name": "Intention",
            "path": "/student/Intention",
            "authority": "学生管理-潜在学生信息",
            "component": _dvaDynamic({
  
  component: () => import('../student/Intention/IntentionList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "name": "AudRecord",
            "path": "/student/AudRecord",
            "authority": "学生管理-试听记录:查询",
            "component": _dvaDynamic({
  
  component: () => import('../student/AudRecord/AudRecordList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "name": "leave",
            "path": "/student/stu-leave",
            "authority": "请假管理-请假记录:查询",
            "component": _dvaDynamic({
  
  component: () => import('../student/StuToLeave/studentLeave'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "path": "/student/Audition/Mgr",
            "component": _dvaDynamic({
  
  component: () => import('../student/Audition/AuditionMgr'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "component": () => React.createElement(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
          }
        ]
      },
      {
        "name": "teacher",
        "icon": "team",
        "path": "/teacher",
        "authority": [
          "教师管理-老师:查询",
          "教师管理-上课反馈记录:查询",
          "教师管理-上课打卡记录:查询"
        ],
        "routes": [
          {
            "name": "teachData",
            "path": "/teacher/teach-data",
            "authority": [
              "教师管理-老师:查询"
            ],
            "component": _dvaDynamic({
  
  component: () => import('../Teacher/teachers/teacherList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "name": "feedback",
            "path": "/teacher/feedback",
            "authority": "教师管理-上课反馈记录:查询",
            "component": _dvaDynamic({
  
  component: () => import('../Teacher/feedback/feedbackList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "name": "signIn",
            "path": "/teacher/signIn",
            "authority": "教师管理-老师补充打卡",
            "component": _dvaDynamic({
  
  component: () => import('../Teacher/signIn'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "path": "/teacher/teach-data/Mgr",
            "component": _dvaDynamic({
  
  component: () => import('../Teacher/teachers/teacherMgr'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "component": () => React.createElement(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
          }
        ]
      },
      {
        "name": "classroom",
        "icon": "folder",
        "path": "/classroom",
        "authority": [
          "教室管理-教室:查询",
          "教室管理-租赁教室订单:查询"
        ],
        "routes": [
          {
            "name": "Maintain",
            "path": "/classroom/list",
            "authority": "教室管理-教室:查询",
            "component": _dvaDynamic({
  
  component: () => import('../Classroom/roomList/list'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "name": "classroomleaseOpen",
            "path": "/classroom/classroomleaseOpen",
            "authority": "教室管理-租赁教室订单:查询",
            "component": _dvaDynamic({
  
  component: () => import('../Classroom/classroomleaseOpen/classroomleaseOpenList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "component": () => React.createElement(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
          }
        ]
      },
      {
        "name": "payManagement",
        "icon": "dollar",
        "path": "/payManagement",
        "routes": [
          {
            "name": "pay",
            "path": "/payManagement/pay",
            "authority": "学生交费功能:查询",
            "component": _dvaDynamic({
  
  component: () => import('../school/pay/payList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "name": "salaryDetails",
            "path": "/payManagement/SalaryList",
            "authority": [
              "教师管理-老师:查询"
            ],
            "component": _dvaDynamic({
  
  component: () => import('../Teacher/salaryDetails'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "name": "salary",
            "path": "/payManagement/salary",
            "authority": [
              "教师管理-老师:查询"
            ],
            "component": _dvaDynamic({
  
  component: () => import('../Teacher/salary'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "component": () => React.createElement(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
          }
        ]
      },
      {
        "name": "school",
        "icon": "switcher",
        "path": "/school",
        "authority": [
          "消息管理-系统消息:查询"
        ],
        "routes": [
          {
            "name": "news",
            "path": "/school/news",
            "authority": "消息管理-系统消息:查询",
            "component": _dvaDynamic({
  
  component: () => import('../school/MsgList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "component": () => React.createElement(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
          }
        ]
      },
      {
        "name": "personnel",
        "icon": "usergroup-add",
        "path": "/personnel",
        "authority": [
          "人员管理-校区负责人:查询",
          "请假管理-请假记录:查询"
        ],
        "routes": [
          {
            "name": "schoolControl",
            "path": "/personnel/schoolControl",
            "authority": "人员管理-校区负责人:查询",
            "component": _dvaDynamic({
  
  component: () => import('../Peoples/SchoolControl/controlList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "name": "peopleLeave",
            "path": "/personnel/leave",
            "authority": "请假管理-请假记录:查询",
            "component": _dvaDynamic({
  
  component: () => import('../Peoples/Leave/LeaveList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "path": "/personnel/schoolControl/mgr",
            "authority": "人员管理-校区负责人:查询",
            "component": _dvaDynamic({
  
  component: () => import('../Peoples/SchoolControl/controlListMgr'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "path": "/personnel/director/mgr",
            "authority": "人员管理-教务主管:查询",
            "component": _dvaDynamic({
  
  component: () => import('../Peoples/Director/directorMgr'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "component": () => React.createElement(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
          }
        ]
      },
      {
        "name": "Inventory",
        "icon": "shop",
        "path": "/Inventory",
        "authority": [
          "库存管理-类别管理:查询",
          "库存管理-产品销售记录:查看"
        ],
        "routes": [
          {
            "name": "productType",
            "path": "/Inventory/productType",
            "component": _dvaDynamic({
  
  component: () => import('../Inventory/productType'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "name": "productManage",
            "path": "/Inventory/productManage",
            "component": _dvaDynamic({
  
  component: () => import('../Inventory/productManage'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "name": "productChange",
            "path": "/Inventory/productChange",
            "component": _dvaDynamic({
  
  component: () => import('../Inventory/productChange'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "name": "orderList",
            "path": "/Inventory/orderList",
            "authority": "库存管理-产品销售记录:查看",
            "component": _dvaDynamic({
  
  component: () => import('../Inventory/orderList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "name": "ProductReport",
            "path": "/Inventory/ProductReport",
            "authority": "库存管理-产品销售记录:查看",
            "component": _dvaDynamic({
  
  component: () => import('../Inventory/ProductReport'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "path": "/Inventory/productManage/mgr",
            "component": _dvaDynamic({
  
  component: () => import('../Inventory/productManage/mgr'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "component": () => React.createElement(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
          }
        ]
      },
      {
        "name": "Coupons",
        "icon": "credit-card",
        "path": "/Coupons",
        "authority": [
          "优惠卷:增加",
          "优惠卷:查询",
          "活动管理-显示活动信息"
        ],
        "routes": [
          {
            "name": "Generates",
            "path": "/Coupons/Generates",
            "authority": "优惠卷:增加",
            "component": _dvaDynamic({
  
  component: () => import('../Coupons/Generates'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "name": "SharingLottery",
            "path": "/Coupons/SharingLottery",
            "authority": "活动管理-显示活动信息",
            "component": _dvaDynamic({
  
  component: () => import('../Coupons/luckDraw'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "path": "/Coupons/SharingLottery/mgr",
            "component": _dvaDynamic({
  
  component: () => import('../Coupons/luckDraw/DrawMgr'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "component": () => React.createElement(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
          }
        ]
      },
      {
        "name": "mechanism",
        "icon": "appstore-o",
        "path": "/mechanism",
        "authority": [
          "机构管理-我的机构:查询",
          "机构管理-机构法人:查询",
          "机构管理-机构审核:查询"
        ],
        "routes": [
          {
            "name": "mine",
            "path": "/mechanism/mine",
            "authority": "机构管理-我的机构:查询",
            "component": _dvaDynamic({
  
  component: () => import('../Mechanism/Mine/MineList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "path": "/mechanism/mine/mgr",
            "authority": "机构管理-我的机构:查询",
            "component": _dvaDynamic({
  
  component: () => import('../Mechanism/examine/examineMgr'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "path": "/mechanism/mine/stepForm",
            "component": _dvaDynamic({
  
  component: () => import('../Mechanism/Mine/StepForm/index'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "hideChildrenInMenu": true,
            "routes": [
              {
                "path": "/mechanism/mine/stepForm",
                "redirect": "/mechanism/mine/StepForm/info",
                "exact": true
              },
              {
                "path": "/mechanism/mine/StepForm/info",
                "component": _dvaDynamic({
  
  component: () => import('../Mechanism/Mine/StepForm/Step1'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
                "exact": true
              },
              {
                "path": "/mechanism/mine/StepForm/mine",
                "component": _dvaDynamic({
  
  component: () => import('../Mechanism/Mine/StepForm/Step2'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
                "exact": true
              },
              {
                "path": "/mechanism/mine/StepForm/result",
                "component": _dvaDynamic({
  
  component: () => import('../Mechanism/Mine/StepForm/Step3'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
                "exact": true
              },
              {
                "component": () => React.createElement(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
              }
            ]
          },
          {
            "name": "examine",
            "path": "/mechanism/examine",
            "authority": "机构管理-机构审核:查询",
            "component": _dvaDynamic({
  
  component: () => import('../Mechanism/examine/examineList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "path": "/mechanism/examine/mgr",
            "authority": "机构管理-机构审核:查询",
            "component": _dvaDynamic({
  
  component: () => import('../Mechanism/examine/examineMgr'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "name": "manager",
            "path": "/mechanism/manager",
            "authority": "机构管理-机构法人:查询",
            "component": _dvaDynamic({
  
  component: () => import('../Mechanism/Manager/ManagerList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "path": "/mechanism/mine/mgr",
            "authority": "机构管理-我的机构:查询",
            "component": _dvaDynamic({
  
  component: () => import('../Mechanism/Mine/MineMgr'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "component": () => React.createElement(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
          }
        ]
      },
      {
        "name": "admin",
        "icon": "desktop",
        "path": "/admin",
        "authority": [
          "角色:查询",
          "用户:查询"
        ],
        "routes": [
          {
            "name": "user",
            "path": "/admin/user",
            "authority": "用户:查询",
            "component": _dvaDynamic({
  
  component: () => import('../Admin/User/UserList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "name": "role",
            "path": "/admin/role",
            "authority": "角色:查询",
            "component": _dvaDynamic({
  
  component: () => import('../Admin/Role/RoleList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "path": "/admin/user/mgr",
            "authority": "机构管理-我的机构:查询",
            "component": _dvaDynamic({
  
  component: () => import('../Admin/User/UserMgr'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "path": "/admin/Role/mgr",
            "authority": "角色:查询",
            "component": _dvaDynamic({
  
  component: () => import('../Admin/Role/RoleMgr'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "component": () => React.createElement(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
          }
        ]
      },
      {
        "name": "system",
        "icon": "laptop",
        "path": "/system",
        "authority": [
          "系统管理-价格:查询",
          "账号管理-订单:查询",
          "账号管理-租期:查询"
        ],
        "routes": [
          {
            "name": "Price",
            "path": "/system/Price",
            "authority": "系统管理-价格:查询",
            "component": _dvaDynamic({
  
  component: () => import('../System/Price/PriceList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "name": "Orders",
            "path": "/system/Orders",
            "authority": "账号管理-订单:查询",
            "component": _dvaDynamic({
  
  component: () => import('../System/Orders/OrdersList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "name": "Term",
            "path": "/system/Term",
            "authority": "账号管理-租期:查询",
            "component": _dvaDynamic({
  
  component: () => import('../System/Term/TermList'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "exact": true
          },
          {
            "component": () => React.createElement(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
          }
        ]
      },
      {
        "name": "account",
        "icon": "user",
        "path": "/account",
        "authority": [
          "个人资料-修改",
          "系统设置-账号:充值"
        ],
        "routes": [
          {
            "name": "base",
            "path": "/account/base",
            "component": _dvaDynamic({
  
  component: () => import('../Account/index'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "authority": "个人资料-修改",
            "routes": [
              {
                "path": "/account/base",
                "redirect": "/account/base/info",
                "exact": true
              },
              {
                "path": "/account/base/setting",
                "component": _dvaDynamic({
  
  component: () => import('../Account/SecurityView'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
                "exact": true
              },
              {
                "path": "/account/base/info",
                "component": _dvaDynamic({
  
  component: () => import('../Account/base'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
                "exact": true
              },
              {
                "component": () => React.createElement(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
              }
            ]
          },
          {
            "name": "settings",
            "path": "/account/settings",
            "component": _dvaDynamic({
  app: window.g_app,
models: () => [
  import('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/pages/Account/Settings/models/geographic.js').then(m => { return { namespace: 'geographic',...m.default}})
],
  component: () => import('../Account/Settings/Info'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "authority": "系统设置-账号:充值",
            "routes": [
              {
                "path": "/account/settings",
                "redirect": "/account/settings/binding",
                "exact": true
              },
              {
                "path": "/account/settings/base",
                "component": _dvaDynamic({
  app: window.g_app,
models: () => [
  import('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/pages/Account/Settings/models/geographic.js').then(m => { return { namespace: 'geographic',...m.default}})
],
  component: () => import('../Account/Settings/BaseView'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
                "exact": true
              },
              {
                "path": "/account/settings/security",
                "component": _dvaDynamic({
  app: window.g_app,
models: () => [
  import('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/pages/Account/Settings/models/geographic.js').then(m => { return { namespace: 'geographic',...m.default}})
],
  component: () => import('../Account/Settings/SecurityView'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
                "exact": true
              },
              {
                "path": "/account/settings/binding",
                "component": _dvaDynamic({
  app: window.g_app,
models: () => [
  import('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/pages/Account/Settings/models/geographic.js').then(m => { return { namespace: 'geographic',...m.default}})
],
  component: () => import('../Account/Settings/BindingView'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
                "exact": true
              },
              {
                "path": "/account/settings/notification",
                "component": _dvaDynamic({
  app: window.g_app,
models: () => [
  import('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/pages/Account/Settings/models/geographic.js').then(m => { return { namespace: 'geographic',...m.default}})
],
  component: () => import('../Account/Settings/NotificationView'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
                "exact": true
              },
              {
                "component": () => React.createElement(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
              }
            ]
          },
          {
            "name": "BusinessSetting",
            "path": "/account/BusinessSetting",
            "component": _dvaDynamic({
  
  component: () => import('../Account/BusinessSetting/Info'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
            "authority": "机构系统设置:查询",
            "routes": [
              {
                "path": "/account/BusinessSetting",
                "redirect": "/account/BusinessSetting/BaseView",
                "exact": true
              },
              {
                "path": "/account/BusinessSetting/HolidaySet",
                "component": _dvaDynamic({
  
  component: () => import('../Account/BusinessSetting/HolidaySet'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
                "exact": true
              },
              {
                "path": "/account/BusinessSetting/Information",
                "component": _dvaDynamic({
  
  component: () => import('../Account/BusinessSetting/Information'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
                "exact": true
              },
              {
                "path": "/account/BusinessSetting/WeChatPublicSet",
                "component": _dvaDynamic({
  
  component: () => import('../Account/BusinessSetting/WeChatPublicSet'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
                "exact": true
              },
              {
                "path": "/account/BusinessSetting/OtherSettings",
                "component": _dvaDynamic({
  
  component: () => import('../Account/BusinessSetting/OtherSettings'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
                "exact": true
              },
              {
                "path": "/account/BusinessSetting/BaseView",
                "component": _dvaDynamic({
  
  component: () => import('../Account/BusinessSetting/BaseSet'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
                "exact": true
              },
              {
                "component": () => React.createElement(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
              }
            ]
          },
          {
            "component": () => React.createElement(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
          }
        ]
      },
      {
        "component": _dvaDynamic({
  
  component: () => import('../404'),
  LoadingComponent: require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/src/components/PageLoading/index').default,
}),
        "exact": true
      },
      {
        "component": () => React.createElement(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
      }
    ]
  },
  {
    "component": () => React.createElement(require('C:/Users/yuchen/Desktop/新建文件夹 (2)/migration_gx_ui/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
  }
];
window.g_plugins.applyForEach('patchRoutes', { initialValue: routes });

export default function() {
  return (
<RendererWrapper0>
          <Router history={window.g_history}>
      { renderRoutes(routes, {}) }
    </Router>
        </RendererWrapper0>
  );
}
