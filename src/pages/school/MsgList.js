import React, { PureComponent } from 'react';
import { connect } from 'dva';
import isEqual from 'lodash/isEqual';
import {
  Card,
  Form,
  Badge,
  Icon,
  List,
  Button,
  Modal,
  Input,
  Select,
  Pagination,
} from 'antd';
import Ellipsis from '@/components/Ellipsis';
import styles from './msgList.less';
import { parseTime } from '@/utils/utils';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import Authorized from '@/utils/Authorized';

const { check } = Authorized;
const { TextArea } = Input;
const { Option } = Select;

const MsgForm = {
  msgReleaseName: '张三',
  msgReleaseType: 1,
  msgStatus: '状态',
  msgTitle: '标题',
  msgReleaseTime:	'2018*10*13',
  receiveUserType: '指定对象',
  msgType: 1,
  isRead: true,
  msgDetails: '消息详情',
};

const status = [
  {},
  {
    text: '未发布',
    value: 'default',
  },
  {
    text: '已发布',
    value: 'success',
  },
];
@connect(({ layoutloading, global, loading }) => ({
  userId: layoutloading.currentUser.userid,
  loading: loading.effects['global/fetchNotices'],
  global,
  currentOrg: global.currentOrg,
}))
@Form.create()
class MsgList extends PureComponent {
  constructor() {
    super();
    this.state = {
      visible: false,
      isEdit: false,
      FormObj: {},
      pagination: {
        pageSize: 10,
        pageNum: 1,
      },
      currentOrg: {
        id: '',
      },
    };
  }

  componentDidMount() {
    const { dispatch, currentOrg } = this.props;
    if (!currentOrg.id) return;
    const { pagination } = this.state;
    dispatch({
      type: 'global/fetchNotices',
      payload: {
        pagination,
        orgId: currentOrg.id,
      },
    });
  }

  static getDerivedStateFromProps(nextProps, preState) {
    const { dispatch } = nextProps;
    if (!nextProps.currentOrg.id) return null;
    if (isEqual(nextProps.currentOrg.id, preState.currentOrg.id)) {
      return null;
    }
    const { pagination } = preState;
    dispatch({
      type: 'global/fetchNotices',
      payload: {
        pagination,
        orgId: nextProps.currentOrg.id,
      },
    });
    return {
      currentOrg: nextProps.currentOrg,
    };
  }

  onChange = (pageNum, pageSize) => {
    const { dispatch } = this.props;
    this.setState({
      pagination: {
        pageNum,
        pageSize,
      },
    });
    dispatch({
      type: 'global/fetchNotices',
      payload: {
        pagination: {
          pageNum,
          pageSize,
        },
        orgId: this.state.currentOrg.id,
      },
    });
  };

  handleEdit = (item, value) => {
    let str = false;
    let obj = {};
    if (value) {
      str = true;
      obj = item;
    }
    this.setState({
      visible: true,
      isEdit: str,
      FormObj: obj,
    });
  };

  handleOk = () => {
    const { form, dispatch } = this.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      if (!this.state.isEdit) {
        delete (values.id);
      }
      const { currentOrg, pagination } = this.state;
      dispatch({
        type: 'global/_changeNotices',
        payload: {
          data: values,
          pagination,
          orgId: currentOrg.id,
        },
      });

      // 在此处调用的是 重置表单内容， 不重置的话会导致 对话框再次打开不能实时更新内容的bug
      form.resetFields();
      this.setState({
        visible: false,
        isEdit: false,
      });
    });
  };

  handleCancel = () => {
    this.props.form.resetFields();
    this.setState({
      visible: false,
      isEdit: false,
    });
  };

  handleDel = (item) => {
    const { id, msgReleaseTime } = item;
    const { pagination, currentOrg } = this.state;
    this.props.dispatch({
      type: 'global/_delNotices',
      payload: {
        data: {
          ids: id,
          msgReleaseTime,
        },
        pagination,
        orgId: currentOrg.id,
      },
    });
  };


  handleParseTime = (time) => {
    if (!time) return;
    let str = parseTime(time).split(' ')[0].split('-');
    str.shift();
    str = str.join('-');
    return `${str} ${parseTime(time).split(' ')[1]}`;
  };

  render() {
    const {
      loading,
      form: { getFieldDecorator },
      userId,
      global: { oldMsg, pagination },
      currentOrg,
    } = this.props;
    const { isEdit, FormObj, visible } = this.state;

    const content = (
      <div className={styles.pageHeaderContent}>
        <p>
          根据人员 精准发布通知
        </p>
        <div className={styles.contentLink}>
          <a>
            <img alt="" src="https://gw.alipayobjects.com/zos/rmsportal/MjEImQtenlyueSmVEfUD.svg" />{' '}
            快速开始
          </a>
          <a>
            <img alt="" src="https://gw.alipayobjects.com/zos/rmsportal/NbuDUAuBlIApFuDvWiND.svg" />{' '}
            简洁易懂
          </a>
          <a>
            <img alt="" src="https://gw.alipayobjects.com/zos/rmsportal/ohOEPSYdDTNnyMbGuyLb.svg" />{' '}
            一键发布
          </a>
        </div>
      </div>
    );

    const extraContent = (
      <div className={styles.extraImg}>
        <img
          alt="消息列表"
          src="https://gw.alipayobjects.com/zos/rmsportal/RzwpdLnhmvDJToTdfDPe.png"
        />
      </div>
    );

    return (
      <PageHeaderWrapper title="消息列表" content={content} extraContent={extraContent}>
        <div className={styles.cardList}>
          <List
            rowKey="id"
            loading={loading}
            grid={{ gutter: 24, lg: 3, md: 2, sm: 1, xs: 1 }}
            dataSource={['', ...oldMsg]}
            renderItem={item =>
              (item ? (
                <List.Item key={item.id}>
                  <Card
                    hoverable
                    className={styles.card}
                    actions={
                      [
                        <div>
                          {check('消息管理-系统消息:删除', <a onClick={() => { this.handleDel(item); }}>删除</a>)}
                        </div>,
                        (item.msgStatus !== 2 ?
                          <div>
                            {check('消息管理-系统消息:修改', <a onClick={() => this.handleEdit(item, true)}>编辑</a>)}
                          </div>
                        : '')
                        ,
                      ]
                    }
                  >
                    <Card.Meta
                      title={<a>{item.msgTitle}</a>}
                      description={
                        <div>
                          <Ellipsis className={styles.item} lines={3}>
                            {item.msgDetails}
                          </Ellipsis>
                          <p style={{ color: '#bfbfbf', marginTop: 10, marginBottom: 0 }}>
                            <span>
                              {this.handleParseTime(item.msgReleaseTime)}
                            </span>
                            <span style={{ marginLeft: 5 }}>
                              {item.msgReleaseName}
                            </span>
                          </p>
                          <p style={{ color: '#bfbfbf', marginTop: 5, marginBottom: 0 }}>
                            <Badge style={{ float: 'right' }} status={status[item.msgStatus].value} text={status[item.msgStatus].text} />
                          </p>
                        </div>
                      }
                    />
                  </Card>
                </List.Item>
              ) : (
                <List.Item>
                  {check('消息管理-系统消息:增加', (
                    <Button onClick={this.handleEdit} type="dashed" className={styles.newButton}>
                      <Icon type="plus" /> 新增通知
                    </Button>
                  ))}
                </List.Item>
              ))
            }
          />
        </div>
        <Pagination
          style={{ marginTop: 30 }}
          onChange={this.onChange}
          defaultCurrent={1}
          {...pagination}
        />
        <Modal
          title="编辑/修改信息"
          visible={visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Form layout="vertical">
            {getFieldDecorator('id', {
              initialValue: isEdit ? FormObj.id : '' })(
                <Input type="hidden" />
            )}
            {getFieldDecorator('msgReleaseId', {
              initialValue: userId })(
                <Input type="hidden" />
            )}
            {getFieldDecorator('orgId', {
              initialValue: currentOrg ? currentOrg.id : '' })(
                <Input type="hidden" />
            )}
            <Form.Item label={MsgForm.msgTitle}>
              {getFieldDecorator('msgTitle', {
                initialValue: isEdit ? FormObj.msgTitle : '',
                rules: [{ required: true, message: '请输入' }],
              })(
                <Input placeholder="请输入" />
              )}
            </Form.Item>

            <Form.Item label={MsgForm.msgStatus}>
              {getFieldDecorator('msgStatus', {
                initialValue: isEdit ? FormObj.msgStatus : '',
                rules: [{ required: true, message: '请输入' }],
              })(
                <Select style={{ width: 120 }}>
                  <Option value={2}>已发布</Option>
                  <Option value={1}>未发布</Option>
                </Select>
              )}
            </Form.Item>

            <Form.Item label={MsgForm.receiveUserType}>
              {getFieldDecorator('releaseUserType', {
                initialValue: isEdit ? FormObj.releaseUserType : '',
                rules: [{ required: true, message: '请输入' }],
              })(
                <Select style={{ width: 120 }}>
                  <Option value={1}>学生</Option>
                  <Option value={2}>老师</Option>
                  <Option value={3}>登陆用户</Option>
                  <Option value={4}>所有人</Option>
                </Select>
              )}
            </Form.Item>

            <Form.Item label={MsgForm.msgDetails}>
              {getFieldDecorator('msgDetails', {
                initialValue: isEdit ? FormObj.msgDetails : '',
                rules: [{ required: true, message: '请输入' }],
              })(
                <TextArea placeholder="请输入" autosize={{ minRows: 2, maxRows: 6 }} />
              )}
            </Form.Item>

          </Form>
        </Modal>
      </PageHeaderWrapper>
    );
  }
}

export default MsgList;
