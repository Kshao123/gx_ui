import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Card,
  Form,
  Row,
  Col,
  Button,
  DatePicker,
} from 'antd';
import moment from 'moment';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import { parseTime, url } from '@/utils/utils';
import Popcon from '@/components/Popconfirm';
import Hoc from '@/utils/Hoc';
import Authorized from '@/utils/Authorized';
import SearchSel from '@/components/SearchSel';

const { check } = Authorized;
const FormItem = Form.Item;
const { RangePicker } = DatePicker;
const PAYTYPE = {
  1: '网上微信支付',
  2: '网上支付宝支付',
  3: '线下微信支付',
  4: '线下支付宝支付',
  5: '现金支付',
  6: 'pos机支付',
};

@connect(({ global, loading, school, layoutloading }) => ({
  userId: layoutloading.currentUser.userid,
  school,
  currentOrg: global.currentOrg,
  loading: loading.effects['school/_getStuPayList'],
}))
@Form.create()
@Hoc({ fetchUrl: 'school/_getStuPayList' })

class PayList extends PureComponent {

  state = {
    formValues: {},
  };

  handleConfirm = (record) => {
    const { id } = record;
    const { pagination, currentOrg, dispatch } = this.props;
    const { formValues } = this.state;
    dispatch({
      type: 'school/_delStuPayList',
      payload: {
        id,
        pagination,
        orgId: currentOrg.id,
        query: formValues,
      },
    });
  };

  handleSearch = (e) => {
    e.preventDefault();

    const { dispatch, pagination, currentOrg, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const { stuName, time, payRecordUserName } = fieldsValue;
      const query = {};
      if (stuName) {
        query.stuName = stuName.label.split('-')[0]
      }
      if (payRecordUserName) {
        query.payRecordUserName = payRecordUserName.label.split('-')[0]
      }
      const [ startTime, endTime ] = time;
      query.startTime = startTime.toDate().getTime();
      query.endTime = endTime.toDate().getTime();
      this.setState({
        formValues: query,
      });
      // console.log(query)
      dispatch({
        type: 'school/_getStuPayList',
        payload: {
          pagination,
          orgId: currentOrg.id,
          query,
        },
      })
    });
  };

  handleFormReset = () => {
    const { dispatch, pagination, currentOrg, form  } = this.props;
    if (currentOrg.id) {
      form.resetFields();
      this.setState({
        formValues: {},
      });
      dispatch({
        type: 'school/_getStuPayList',
        payload: {
          pagination,
          orgId: currentOrg.id,
        },
      });
    }
  };

  renderSearchForm = () => {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    const stuProps = {
      removal: true,
      queryUrl: `${url}/stuPay/query`,
      keyValue: 'stuName',
      showSearch: true,
      attr: {
        name: 'stuName',
      },
      placeholder: '学生姓名',
    };
    const payRecord = {
      removal: true,
      queryUrl: `${url}/stuPay/query`,
      keyValue: 'payRecordUserName',
      showSearch: true,
      hidePhone: true,
      attr: {
        name: 'payRecordUserName',
      },
      placeholder: '录入人员',
    };

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="学生">
              {getFieldDecorator('stuName')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...stuProps} />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="录入人员">
              {getFieldDecorator('payRecordUserName')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...payRecord} />
              )}
            </FormItem>
          </Col>
          <Col md={14} sm={24}>
            <FormItem label="时间">
              {getFieldDecorator('time',{
                initialValue: [
                  moment(),
                  moment().add(7,'day')
                ],
              })(
                <RangePicker
                  placeholder="选择时间"
                  showTime={{ format: 'HH:mm' }}
                  format="YYYY-MM-DD HH:mm"
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">查询</Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  };

  render() {
    const {
      loading,
      school: { payList: data },
      handleStandardTableChange,
      forItem
    } = this.props;
    const { formValues } = this.state;
    const columns = [
      {
        title: '学生姓名',
        dataIndex: 'stuName',
        render(text) {
          return forItem('icon-mingcheng', text);
        },
      },
      {
        title: '交费金额',
        dataIndex: 'totalAmount',
        render(text) {
          return forItem('icon-jiage', text);
        },
      },
      {
        title: '交费日期',
        dataIndex: 'payDate',
        render(text) {
          return forItem('icon-shijian', parseTime(text));
        },
      },
      {
        title: '缴费方式',
        dataIndex: 'payWay',
        render(text) {
          return forItem('icon-leixing', PAYTYPE[text]);
        },
      },
      {
        title: '录入人员',
        dataIndex: 'payRecordUserName',
        render(text) {
          return forItem('icon-icon-', text);
        },
      },
      {
        title: '操作',
        dataIndex: 'operation',
        fixed: 'right',
        width: 70,
        render: (text, record) => (
          <Fragment>
            {check('学生交费功能:删除', (
              <Popcon
                title="是否确认删除"
                onConfirm={() => this.handleConfirm(record)}
              />
            ))}
          </Fragment>
          ),
      },
    ];
    return (
      <section>
        <Card className={styles.tableListForm}>
          {this.renderSearchForm()}
        </Card>
        <Card>
          <div className={styles.tableList}>
            <StandardTable
              bordered
              loading={loading}
              data={data}
              columns={columns}
              onChange={e => handleStandardTableChange(e, formValues)}
            />
          </div>
        </Card>
      </section>
    );
  }
}
export default PayList;
