import React, { Component } from 'react';
import { connect } from 'dva';
import { Card, Divider } from 'antd';
import DescriptionList from '@/components/DescriptionList';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import { parseTime } from '../../../utils/utils';
import styles from './mgr.less'

const { Description } = DescriptionList;
const ORGSTATUS = [
  '',
  '个体工商户',
  '企业'
];
const STATUSMAP = {
  true: '营业中',
  false: '未营业'
};

@connect(({ profile, loading }) => ({
  profile,
  loading: loading.effects['profile/fetchBasic'],
}))
class BasicProfile extends Component {
  render() {
    const { location: { state } } = this.props;
    return (
      <PageHeaderWrapper title={`${state ? state.orgName : ''} 基础详情`}>
        <Card className={styles.cardImg} bordered={false}>
          <DescriptionList size="large" title="法人信息" style={{ marginBottom: 32 }}>
            <Description term="姓名">{state ? state.userName : ''}</Description>
            <Description term="证件号码">{state ? state.idCard : ''}</Description>
            <Description term="手机">{state ? state.mobilePhone : ''}</Description>
            <Description term="邮箱">{state ? state.orgEmails : ''}</Description>
            <Description term="固定电话">{state ? state.orgTelephone : ''}</Description>
            <Divider style={{ marginBottom: 32 }} />
            <Description term="身份证人像面">
              <img src={state && state.idCardFronts ? state.idCardFronts.length ? state.idCardFronts[0] : '' : ''} alt="" />
            </Description>
            <Description term="身份证国徽面">
              <img src={state && state.idCardVersos ? state.idCardVersos.length ? state.idCardVersos[0] : '' : ''} alt="" />
            </Description>
          </DescriptionList>
          <Divider style={{ marginBottom: 32 }} />
          <DescriptionList size="large" title="机构信息" style={{ marginBottom: 32 }}>
            <Description term="名称">{state ? state.orgName : ''}</Description>
            <Description term="状态">{state && state.orgStatus ? STATUSMAP[state.orgStatus] : ''}</Description>
            <Description term="类型">{state && state.orgClass ? ORGSTATUS[state.orgClass] : ''}</Description>
            <Description term="成立日期">{state && state.foundTime ? parseTime(state.foundTime).split(' ')[0] : ''}</Description>
            <Description term="社会信用代码">{state ? state.orgCode : ''}</Description>
            <Description term="经营范围">{state ? state.scopeManage : ''}</Description>
            <Description term="注册地址">{state ? state.registerAddr : ''}</Description>
            <Description term="经营地址">{state ? state.manageAddr : ''}</Description>
          </DescriptionList>
          <DescriptionList size="large" style={{ marginBottom: 32 }}>
            <Description term="营业执照">
              <img src={state && state.orgLicenses.length ? state.orgLicenses[0] : ''} alt="" />
            </Description>
            <Description term="Logo">
              <img src={state && state.orgLogos.length ? state.orgLogos[0] : ''} alt="" />
            </Description>
            <Divider style={{ marginBottom: 32 }} />
            <Description term="机构展示">
              {state && state.orgShows.length ? state.orgShows.map(item => (
                <img style={{ marginBottom: 15 }} key={item} src={item} alt="" />
              )) : ''}
            </Description>
            <Divider style={{ marginBottom: 32 }} />
            <Description term="详细介绍">{state ? state.orgDetails : ''}</Description>
          </DescriptionList>
          <Divider style={{ marginBottom: 32 }} />

        </Card>
      </PageHeaderWrapper>
    );
  }
}

export default BasicProfile;
