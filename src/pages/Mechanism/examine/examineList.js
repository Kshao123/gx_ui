import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import {
  Tooltip,
  Card,
  Form,
  Icon,
  Button,
  Input,
  Divider,
  Modal,
} from 'antd';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import { scriptUrl, parseTime } from '@/utils/utils';
import Ellipsis from '@/components/Ellipsis';

const { TextArea } = Input;

const IconFont = Icon.createFromIconfontCN({
  scriptUrl,
});

@connect(({ global, mine, layoutloading: { currentUser }, loading }) => ({
  currentOrg: global.currentOrg,
  mine,
  currentUser,
  loading: loading.global,
}))
@Form.create()
class MineList extends PureComponent {
  constructor() {
    super();
    this.state = {
      visible: false,
      id: undefined,
      pagination: {
        pageNum: 1,
        pageSize: 10,
      },
    };
  }

  componentDidMount() {
    const { dispatch, currentUser: { userid: userId } } = this.props;
    if (!userId) return;
    dispatch({
      type: 'mine/_getExamine',
      payload: {
        pageNum: 1,
        pageSize: 10,
        userId,
      },
    });
  }

  componentWillUpdate(nextProps) {
    const { dispatch, currentUser: { userid: preId } } = this.props;
    const { currentUser: { userid } } = nextProps;
    if (preId === userid) return;
    dispatch({
      type: 'mine/_getExamine',
      payload: {
        pageNum: 1,
        pageSize: 10,
        userId: userid,
      },
    });
  }

  handleEdit = (record, value) => { // do 点击修改回调
    const { dispatch } = this.props;
    if (value) record.disabled = true;

    dispatch(routerRedux.push({
      pathname: '/mechanism/examine/mgr',
      state: record,
    }));
  };

  handleDelete = ({ id }, value) => {
    if (value) {
      this.setState({
        visible: true,
        id,
      });
      return;
    }
    const { dispatch, currentUser: { userid: userId } } = this.props;
    const { pagination } = this.state;
    dispatch({
      type: 'mine/_ToAudit',
      payload: {
        data: {
          id,
          auditResult: 1,
        },
        pagination,
        userId,
      },
    });
  };

  forItem = (type, text) => (
    <div>
      <IconFont type={type} />
      <span>  {text}</span>
    </div>
  );

  handleStandardTableChange = (pagination) => {
    const { dispatch, currentUser: { userid: userId } } = this.props;
    const { pageSize, current: pageNum } = pagination;
    this.setState({
      pagination: {
        pageNum,
        pageSize,
      },
    });
    dispatch({
      type: 'mine/fetch',
      payload: {
        pageNum,
        pageSize,
        userId,
      },
    });
  };

  handleOk = () => {
    const { form, dispatch, currentUser: { userid: userId } } = this.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      const { id, pagination } = this.state;
      dispatch({
        type: 'mine/_ToAudit',
        payload: {
          data: {
            id,
            auditResult: 0,
            ...values
          },
          pagination,
          userId,
        },
      });

      form.resetFields();this.setState({
        visible: false,
        id: undefined,
      });
    });
  };

  handleCancel = () => {
    this.setState({
      visible: false,
      id: undefined,
    })
  };

  render() {
    const { forItem, handleEdit } = this;
    const columns = [
      {
        title: '机构名称',
        dataIndex: 'orgName',
        render(text, record) {
          return (
            <Tooltip title={`点击查看${text}机构的详细信息`}>
              <div className={styles.TableEllipsis} style={{ cursor: 'pointer' }} onClick={() => handleEdit(record, true)}>
                <IconFont type="icon-zuzhi" style={{ marginRight: 5 }} />
                <Ellipsis style={{ color: '#1890ff' }} length={10}>
                  {text}
                </Ellipsis>
              </div>
            </Tooltip>
          );
        },
      },
      {
        title: '手机号',
        dataIndex: 'mobilePhone',
        render(text) {
          return forItem('icon-shouji', text);
        },
      },
      {
        title: '邮箱',
        dataIndex: 'orgEmails',
        render(text) {
          return forItem('icon-youxiang', text);
        },
      },
      {
        title: '成立时间',
        dataIndex: 'foundTime',
        align: 'center',
        render(text) {
          return forItem('icon-shijian1', text ? parseTime(text).split(' ')[0] : '');
        },
      },
      {
        title: '操作',
        fixed: 'right',
        width: 120,
        render: (text, record) => (
          <Fragment>
            <a onClick={() => this.handleDelete(record)}>通过</a>
            <Divider type="vertical" />
            <a onClick={() => this.handleDelete(record, true)}>拒绝</a>
          </Fragment>
        ),
      },
    ];

    const { mine: { mangerList: data }, loading, form: { getFieldDecorator }  } = this.props;
    const { visible } = this.state;
    return (
      <Card>
        <div className={styles.tableList}>
          <StandardTable
            bordered
            loading={loading}
            data={data}
            columns={columns}
            scroll={{ x: 950 }}
            onChange={this.handleStandardTableChange}
          />
        </div>
        <Modal
          title="拒绝原因"
          visible={visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Form layout="vertical">
            <Form.Item label="拒绝原因">
              {getFieldDecorator('falseCause', {
                rules: [{ required: true, message: '请填写拒绝原因' }],
              })(
                <TextArea placeholder="原因" autosize={{ minRows: 2, maxRows: 6 }} />
              )}
            </Form.Item>
          </Form>
        </Modal>
      </Card>
    );
  }
}
export default MineList;
