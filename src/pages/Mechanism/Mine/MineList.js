import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import {
  Tooltip,
  Card,
  Form,
  Icon,
  Button,
  Divider,
  Badge,
} from 'antd';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import { scriptUrl, parseTime } from '@/utils/utils';
import Ellipsis from '@/components/Ellipsis';
import Popcon from '@/components/Popconfirm';
import Authorized from '@/utils/Authorized';

const { check } = Authorized;
const IconFont = Icon.createFromIconfontCN({
  scriptUrl,
});

const typeMap = [
  {
    value: 'error',
    text: '未通过',
  },
  {
    value: 'success',
    text: '通过',
  },
  {
    value: 'warning',
    text: '审核中',
  },
];

@connect(({ global, mine, layoutloading: { currentUser }, loading }) => ({
  currentOrg: global.currentOrg,
  mine,
  currentUser,
  loading: loading.global,
}))
@Form.create()
class MineList extends PureComponent {
  constructor() {
    super();
    this.state = {
      typeMap: {
        true: {
          value: 'success',
          text: '营业中',
        },
        false: {
          value: 'default',
          text: '未营业',
        },
      },
      pagination: {
        pageNum: 1,
        pageSize: 10,
      },
    };
  }

  componentDidMount() {
    const { dispatch, currentUser: { userid: userId } } = this.props;
    if (!userId) return;
    dispatch({
      type: 'mine/fetch',
      payload: {
        pageNum: 1,
        pageSize: 10,
        userId,
      },
    });
  }

  componentWillUpdate(nextProps) {
    const { dispatch, currentUser: { userid: preId } } = this.props;
    const { currentUser: { userid } } = nextProps;
    if (preId === userid) return;
    dispatch({
      type: 'mine/fetch',
      payload: {
        pageNum: 1,
        pageSize: 10,
        userId: userid,
      },
    });
  }

  handleEdit = (record, value) => { // do 点击修改回调
    const { dispatch } = this.props;
    if (value) {
      dispatch(routerRedux.push({
        pathname: '/mechanism/mine/mgr',
        state: record,
      }));
      return;
    }
    dispatch(routerRedux.push({
      pathname: '/mechanism/mine/stepForm/info',
      state: record,
    }));
  };

  handleDelete = ({ id }) => {
    const { dispatch, currentUser: { userid: userId } } = this.props;
    const { pagination } = this.state;
    dispatch({
      type: 'mine/delete',
      payload: {
        ids: id,
        pagination,
        userId,
      },
    });
  };

  forItem = (type, text) => (
    <div>
      <IconFont type={type} />
      <span>  {text}</span>
    </div>
  );

  handleStandardTableChange = (pagination) => {
    const { dispatch, currentUser: { userid: userId } } = this.props;
    const { pageSize, current: pageNum } = pagination;
    this.setState({
      pagination: {
        pageNum,
        pageSize,
      },
    });
    dispatch({
      type: 'mine/fetch',
      payload: {
        pageNum,
        pageSize,
        userId,
      },
    });
  };

  render() {
    const { forItem, handleEdit } = this;
    const columns = [
      {
        title: '机构名称',
        dataIndex: 'orgName',
        render(text, record) {
          return (
            <Tooltip title={`点击查看${text}机构的详细信息`}>
              <div className={styles.TableEllipsis} style={{ cursor: 'pointer' }} onClick={() => handleEdit(record, true)}>
                <IconFont type="icon-zuzhi" style={{ marginRight: 5 }} />
                <Ellipsis style={{ color: '#1890ff' }} length={10}>
                  {text}
                </Ellipsis>
              </div>
            </Tooltip>
          );
        },
      },
      {
        title: '手机号',
        dataIndex: 'mobilePhone',
        render(text) {
          return forItem('icon-shouji', text);
        },
      },
      {
        title: '邮箱',
        dataIndex: 'orgEmails',
        render(text) {
          return forItem('icon-youxiang', text);
        },
      },
      {
        title: '成立时间',
        dataIndex: 'foundTime',
        align: 'center',
        render(text) {
          return forItem('icon-shijian1', text ? parseTime(text).split(' ')[0] : '');
        },
      },
      {
        title: '状态',
        align: 'center',
        dataIndex: 'auditResult',
        render: (text) => (
          <Badge status={typeMap[text].value} text={typeMap[text].text} />
          ),
      },
      {
        title: '操作',
        fixed: 'right',
        width: 120,
        render: (text, record) => (
          <Fragment>
            {check('机构管理-我的机构:修改', <a onClick={() => this.handleEdit(record)}>修改</a>)}
            <Divider type="vertical" />
            {check('机构管理-我的机构:删除', (
              <Popcon
                title="是否确认删除"
                onConfirm={() => this.handleDelete(record)} // 点击确认时
              />
            ))}
          </Fragment>
        ),
      },
    ];

    const { mine: { data }, loading } = this.props;
    return (
      <Card>
        <div className={styles.tableList}>
          <div className={styles.tableListOperator}>
            {check('机构管理-我的机构:添加', (
              <Button icon="plus" type="primary" onClick={() => this.handleEdit(undefined)}>
                新建
              </Button>
            ))}
          </div>
          <StandardTable
            bordered
            loading={loading}
            data={data}
            columns={columns}
            scroll={{ x: 950 }}
            onChange={this.handleStandardTableChange}
          />
        </div>
      </Card>
    );
  }
}
export default MineList;
