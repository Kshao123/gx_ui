import React, { PureComponent } from 'react';
import {
  Card,
  Button,
  Form,
  message,
  Upload,
  Icon,
  Col,
  Row,
  DatePicker,
  Input,
  Select,
  Popover,
  Radio,
} from 'antd';
import moment from 'moment';
import { connect } from 'dva';
import FooterToolbar from '../../../components/FooterToolbar';
import styles from '../../../utils/global.less';

import { parseTime, url } from '../../../utils/utils';

const { TextArea } = Input;
const RadioGroup = Radio.Group;
const { Option } = Select;
const dateFormat = 'YYYY/MM/DD'; // 默认日期个格式
const fieldLabels = {
  orgName: '机构名称',
  mobilePhone: '手机',
  orgTelephone: '固定电话',
  registerAddr: '注册地址',
  manageAddr: '经营地址',
  orgDetails: '详情介绍',
  orgEmails: 'email',
  orgAbstract: '简介',
  orgCode: '统一社会信用代码',
  orgClass: '机构分类',
  orgStatus: '机构状态',
  scopeManage: '经营范围',
  foundTime: '成立日期',
};

const orgClassMap = [
  {
    value: 1,
    text: '个体工商户',
  },
  {
    value: 2,
    text: '企业',
  },
];

const Rxp = /.+\.(jpg|jpeg|png|bmp)$/;

const grid = {
  xl: 12,
  lg: 12,
  ma: 12,
  sm: 24,
};
class MineMgr extends PureComponent {
  constructor(props) {
    super(props);
    const { location: { state } } = props;
    let Logo = [];
    let Org = [];
    let Show = [];
    let status = false;
    if (state) {
      const { orgLogos, orgLicenses, orgShows, disabled } = state;
      Logo = this.handleMapPic(orgLogos, 'Logo');
      Org = this.handleMapPic(orgLicenses, '营业执照');
      Show = orgShows.length ? this.handleMapPic(orgShows, '展示') : [];
      status = !!disabled;
    }
    this.state = {
      width: '100%',
      propsLogo: {
        action: `${url}/mechanism/saveImage`,
        listType: 'picture-card',
        name: 'file1',
        className: 'upload-list-inline',
        beforeUpload: this.handleUpload,
        onRemove: this.handleRemoveLogo,
        onChange: this.handleChangeLogo,
      },
      propsOrg: {
        action: `${url}/mechanism/saveImage`,
        listType: 'picture-card',
        name: 'file1',
        className: 'upload-list-inline',
        beforeUpload: this.handleUpload,
        onRemove: this.handleRemoveOrg,
        onChange: this.handleChangeOrg,
      },
      propsShow: {
        action: `${url}/mechanism/saveImage`,
        listType: 'picture-card',
        name: 'file1',
        className: 'upload-list-inline',
        beforeUpload: this.handleUpload,
        onRemove: this.handleRemove,
        multiple: true,
        onChange: this.handleChangeShow,
      },
      fileListLogo: Logo,
      fileListOrg: Org,
      fileListShow: Show,
      disabled: status,
    };
  }

  handleUpload = (file) => {
    // /1024 即可得到实际大小 file.type 为格式
    const { name, size } = file;
    if (!Rxp.test(name)) {
      message.error('请上传正确的图片文件 仅支持 jpg | png | jpeg | bmp ', 3);
      this.setState({
        fileListLogo: [],
      });
      return false;
    } else if ((size / 1024) > 6000) {
      message.error('图片文件大于 5M', 3);
      return false;
    }
    return true;
  };

  handleRemoveLogo = (file) => {
    const { uid } = file;
    const fileListLogo = this.state.fileListLogo.filter(item => item.uid !== uid);
    this.setState({
      fileListLogo,
    });
    return true;
  };

  handleRemoveOrg = (file) => {
    const { uid } = file;
    const fileListOrg = this.state.fileListOrg.filter(item => item.uid !== uid);
    this.setState({
      fileListOrg,
    });
    return true;
  };

  handleRemoveShow = ({ file, fileList }) => {
    this.setState({
      fileListShow: fileList,
    });
    return true;
  };

  handleChangeLogo = ({ file, fileList, event }, b) => {
    let fileListLogo = fileList;
    fileListLogo = fileListLogo.map((item) => {
      if (item.response) {
        item.url = item.response.result;
      }
      return item;
    });
    fileListLogo = fileListLogo.filter((item) => {
      if (item.response) {
        return item.response.status === true;
      }
      return true;
    });
    file.status === 'done' ? (message.success('上传成功', 3)) : '';
    this.setState({ fileListLogo });
  };

  handleChangeOrg = ({ file, fileList, event }, b) => {
    let fileListOrg = fileList;
    fileListOrg = fileListOrg.map((item) => {
      if (item.response) {
        item.url = item.response.result;
      }
      return item;
    });
    fileListOrg = fileListOrg.filter((item) => {
      if (item.response) {
        return item.response.status === true;
      }
      return true;
    });
    file.status === 'done' ? (message.success('上传成功', 3)) : '';
    this.setState({ fileListOrg });
  };

  handleChangeShow = ({ file, fileList, event }, b) => {
    let fileListShow = fileList;
    fileListShow = fileListShow.map((item) => {
      if (item.response) {
        item.url = item.response.result;
      }
      return item;
    });
    fileListShow = fileListShow.filter((item) => {
      if (item.response) {
        return item.response.status === true;
      }
      return true;
    });
    file.status === 'done' ? (message.success('上传成功', 3)) : '';
    this.setState({ fileListShow });
  };

  handleMapPic = (item, type) => item.map((items, index) => ({
        name: `${type} ${index + 1}`,
        uid: index,
        url: items,
      }));

  confirmPic = (fileList) => fileList.map(item => item.url);

  render() {
    const { form, dispatch, submitting, location, currentUser } = this.props;
    const { getFieldDecorator, validateFieldsAndScroll, getFieldsError } = form;
    const { state } = location;
    const {
      propsLogo,
      propsOrg,
      propsShow,
      fileListLogo,
      fileListOrg,
      fileListShow,
      disabled,
    } = this.state;
    const validate = () => {
      validateFieldsAndScroll((error, values) => {
        if (error) {
          return;
        }
        const { props: { location: { state } } } = this;
        // 如果state为真 则为修改状态
        if (!state) {
          delete (values.id);
        }
        values.foundTime = new Date(values.foundTime.toDate()).getTime();
        values.orgLogo = JSON.stringify(this.confirmPic(this.state.fileListLogo));
        values.orgLicense = JSON.stringify(this.confirmPic(this.state.fileListOrg));
        if (this.state.fileListShow && this.state.fileListShow.length) {
          values.orgShow = JSON.stringify(this.confirmPic(this.state.fileListShow));
        }else {
          values.orgShow = '';
        }
        dispatch({
          type: 'mine/submit',
          payload: values,
        });
      });
    };
    const errors = getFieldsError();
    const getErrorInfo = () => { // 报错时的提示消息
      const errorCount = Object.keys(errors).filter(key => errors[key]).length; // 计算错误个数
      if (!errors || errorCount === 0) {
        return null;
      }
      const scrollToField = (fieldKey) => {
        const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
        if (labelNode) {
          labelNode.scrollIntoView(true);
        }
      };
      const errorList = Object.keys(errors).map((key) => {
        if (!errors[key]) {
          return null;
        }
        return (
          <li key={key} className={styles.errorListItem} onClick={() => scrollToField(key)}>
            <Icon type="cross-circle-o" className={styles.errorIcon} />
            <div className={styles.errorMessage}>{errors[key][0]}</div>
            <div className={styles.errorField}>{fieldLabels[key]}</div>
          </li>
        );
      });
      return (
        <span className={styles.errorIcon}>
          <Popover
            title="表单校验信息"
            content={errorList}
            overlayClassName={styles.errorPopover}
            trigger="click"
            getPopupContainer={trigger => trigger.parentNode}
          >
            <Icon type="exclamation-circle" />
          </Popover>
          {errorCount}
        </span>
      );
    };
    return (
      <div>
        <Card title="机构信息" className={styles.card} bordered={false}>
          <Form layout="vertical">
            {getFieldDecorator('id', {
              initialValue: state ? state.id : '' })(
                <Input type="hidden" />
            )}
            {getFieldDecorator('userId', {
              initialValue: state ? state.userId : currentUser.userid })(
                <Input type="hidden" />
            )}
            {getFieldDecorator('userName', {
              initialValue: state ? state.userName : currentUser.name })(
                <Input type="hidden" />
            )}
            <Row gutter={16}>
              <Col {...grid}>
                <Form.Item label={fieldLabels.orgName}>
                  {getFieldDecorator('orgName', {
                    initialValue: state ? state.orgName : '',
                    rules: [
                      { pattern: /^[\u4E00-\u9FA5-a-zA-Z0-9_]{2,16}$/, required: true, message: '请输入中、英文、数字或下划线' }],
                  })(
                    <Input disabled={disabled} placeholder="请输入机构名称" />
                  )}
                </Form.Item>
              </Col>
              <Col {...grid}>
                <Form.Item label={fieldLabels.orgTelephone}>
                  {getFieldDecorator('orgTelephone', {
                    initialValue: state ? state.orgTelephone : '',
                    rules: [
                      { pattern: /0\d{2,3}-\d{7,8}/, required: false, message: '格式错误，请重新输入， tip：0511-66****8' }],
                  })(
                    <Input disabled={disabled} placeholder="请输入固定电话" />
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col {...grid}>
                <Form.Item label={fieldLabels.mobilePhone}>
                  {getFieldDecorator('mobilePhone', {
                    initialValue: state ? state.mobilePhone : '',
                    rules: [
                      { pattern: /^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$/, required: true, message: '格式错误，请重新输入' }],
                  })(
                    <Input disabled={disabled} placeholder="请输入手机号码" />
                  )}
                </Form.Item>
              </Col>
              <Col {...grid}>
                <Form.Item label={fieldLabels.orgEmails}>
                  {getFieldDecorator('orgEmails', {
                    initialValue: state ? state.orgEmails : '',
                    rules: [
                      { pattern: /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/, required: false, message: '邮箱格式有误，请重新输入' }],
                  })(
                    <Input disabled={disabled} placeholder="请输入邮箱地址" />
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col {...grid}>
                <Form.Item label={fieldLabels.orgClass}>
                  {getFieldDecorator('orgClass', {
                    initialValue: state ? state.orgClass : '',
                    rules: [
                      { required: true, message: '请选择机构分类' }],
                  })(
                    <RadioGroup disabled={disabled}>
                      {orgClassMap.map(item => <Radio key={item.value} value={item.value}>{item.text}</Radio>)}
                    </RadioGroup>
                  )}
                </Form.Item>
              </Col>
              <Col {...grid}>
                <Form.Item label={fieldLabels.orgStatus}>
                  {getFieldDecorator('orgStatus', {
                    initialValue: state ? state.orgStatus : '',
                    rules: [
                      { required: true, message: '请选择机构状态' }],
                  })(
                    <Select
                      disabled={disabled}
                      style={{ width: '100%' }}
                      placeholder="请确认状态"
                    >
                      <Option value>营业</Option>
                      <Option value={false}>未营业</Option>
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Card>
        <Card title="详细信息" className={styles.card} bordered={false}>
          <Form layout="vertical">
            <Row gutter={16}>
              <Col {...grid}>
                <Form.Item label={fieldLabels.foundTime}>
                  {getFieldDecorator('foundTime', {
                  initialValue: state ? moment(`${parseTime(state.foundTime)}`) : moment('2018-11'),
                  rules: [
                    { required: true, message: '请重新输入' }],
                })(
                  <DatePicker
                    disabled={disabled}
                    style={{ width: '100%' }}
                    placeholder="请选择日期"
                    format={dateFormat}
                  />
                )}
                </Form.Item>
              </Col>
              <Col {...grid}>
                <Form.Item label={fieldLabels.scopeManage}>
                  {getFieldDecorator('scopeManage', {
                    initialValue: state ? state.scopeManage : '',
                    rules: [
                      { pattern: /^[\u4E00-\u9FA5A-Za-z0-9]|['“”"{}()*&.?!,…:;。《》、\\/@` ]+$/, required: true, message: '请重新输入' }],
                  })(
                    <Input disabled={disabled} placeholder="请输入" />
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col {...grid}>
                <Form.Item label={fieldLabels.orgCode}>
                  {getFieldDecorator('orgCode', {
                    initialValue: state ? state.orgCode : '',
                    rules: [
                      { pattern: /^([159Y]{1})([1239]{1})([0-9ABCDEFGHJKLMNPQRTUWXY]{6})([0-9ABCDEFGHJKLMNPQRTUWXY]{9})([0-90-9ABCDEFGHJKLMNPQRTUWXY])$/, required: true, message: '格式有误，请重新输入' }],
                  })(
                    <Input disabled={disabled} placeholder="请输入" />
                  )}
                </Form.Item>
              </Col>
              <Col lg={12} md={12} sm={24} xl={8}>
                <Form.Item label="机构Logo">
                  {getFieldDecorator('orgLogo', {
                    initialValue: state ? this.state.fileListLogo : [],
                    rules: [
                      { required: true, message: '请重新上传' }],
                  })(
                    <Upload disabled={disabled} {...propsLogo} fileList={this.state.fileListLogo}>
                      { fileListLogo.length >= 1 ? null : (
                        <div>
                          <Icon className="ant-icon" type="plus" />
                          <div className="ant-upload-text">机构Logo</div>
                        </div>
                      ) }
                    </Upload>
                  )}
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={16}>
              <Col {...grid}>
                <Form.Item label="营业执照">
                  {getFieldDecorator('orgLicense', {
                    initialValue: state ? this.state.fileListOrg : [],
                    rules: [
                      { required: true, message: '请重新上传' }],
                  })(
                    <Upload disabled={disabled} {...propsOrg} fileList={this.state.fileListOrg}>
                      {fileListOrg.length >= 1 ? null : (
                        <div>
                          <Icon className="ant-icon" type="plus" />
                          <div className="ant-upload-text">营业执照</div>
                        </div>
                      )}
                    </Upload>
                  )}
                </Form.Item>
              </Col>
              <Col {...grid}>
                <Form.Item label="机构展示">
                  {getFieldDecorator('orgShow', {
                    initialValue: state ? this.state.fileListShow : [],
                    rules: [
                      { required: false, message: '请重新上传' }],
                  })(
                    <Upload disabled={disabled} {...propsShow} fileList={this.state.fileListShow}>
                      {disabled ? null : (
                        <div>
                          <Icon className="ant-icon" type="plus" />
                          <div className="ant-upload-text">机构展示</div>
                        </div>
                      )}
                    </Upload>
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col {...grid}>
                <Form.Item label={fieldLabels.registerAddr}>
                  {getFieldDecorator('registerAddr', {
                    initialValue: state ? state.registerAddr : '',
                    rules: [
                      { pattern: /^[\u4E00-\u9FA5A-Za-z0-9]+$/, required: true, message: '地址格式不正确' }],
                  })(
                    <Input disabled={disabled} placeholder="请填写详细地址" />
                  )}
                </Form.Item>
              </Col>
              <Col {...grid}>
                <Form.Item label={fieldLabels.manageAddr}>
                  {getFieldDecorator('manageAddr', {
                    initialValue: state ? state.manageAddr : '',
                    rules: [
                      { pattern: /^[\u4E00-\u9FA5A-Za-z0-9]+$/, required: true, message: '地址格式不正确' }],
                  })(
                    <Input disabled={disabled} placeholder="请填写详细地址" />
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col lg={12} md={12} sm={24} xl={22}>
                <Form.Item label={fieldLabels.orgAbstract}>
                  {getFieldDecorator('orgAbstract', {
                    initialValue: state ? state.orgAbstract : '',
                    rules: [
                      { pattern: /^[\u4E00-\u9FA5A-Za-z0-9]|['“”"{}()*&.?!,…:;。《》、\\/@` ]+$/, required: false, message: '请重新输入' }],
                  })(
                    <TextArea disabled={disabled} placeholder="请填写简介" autosize={{ minRows: 2, maxRows: 4 }} />
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col lg={12} md={12} sm={24} xl={22}>
                <Form.Item label={fieldLabels.orgDetails}>
                  {getFieldDecorator('orgDetails', {
                    initialValue: state ? state.orgDetails : '',
                    rules: [
                      { pattern: /^[\u4E00-\u9FA5A-Za-z0-9]|['“”"{}()*&.?!,…:;。《》、\\/@` ]+$/, required: false, message: '请重新输入' }],
                  })(
                    <TextArea disabled={disabled} placeholder="请填写详细介绍" autosize={{ minRows: 2, maxRows: 100 }} />
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Card>
        <FooterToolbar style={{ width: this.state.width, display: disabled ? 'none' : 'block' }}>
          {getErrorInfo()}
          <Button type="primary" onClick={validate} loading={submitting}>
            提交
          </Button>
        </FooterToolbar>
      </div>
    );
  }
}

export default connect(({ mine, global, loading, layoutloading: { currentUser } }) => ({
  mine,
  currentOrg: global.currentOrg,
  submitting: loading.effects['mine/submit'],
  currentUser,
}))(Form.create()(MineMgr));
