import React from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Form,
  Input,
  Button,
  DatePicker,
  message,
  Upload,
  Icon,
} from 'antd';
import { routerRedux } from 'dva/router';
import styles from './style.less';
import { parseTime } from '../../../../utils/utils';
import { url } from '@/utils/utils';

const { TextArea } = Input;
const formItemLayout = {
  labelCol: {
    span: 5,
  },
  wrapperCol: {
    span: 15,
  },
};

const data = {
  payAccount: 'ant-design@alipay.com',
  receiverAccount: 'test@example.com',
  receiverName: 'Alex',
  amount: '500',
};

@connect(({ loading }) => ({
  submitting: loading.effects['mine/submit'],
  // data: form.step,
}))
@Form.create()
class Step2 extends React.PureComponent {
  constructor(props) {
    super(props);
    const { location: { state } } = props;
    let Logo = [];
    let Org = [];
    let Show = [];
    if (state) {
      const { orgLogos, orgLicenses, orgShows } = state;
      Logo = orgLogos ? this.handleMapPic(orgLogos, 'Logo') : [];
      Org = orgLicenses ? this.handleMapPic(orgLicenses, '营业执照') : [];
      Show = orgShows ? this.handleMapPic(orgShows, '展示') : [];
    }
    this.state = {
      PROPS: {
        action: `${url}/mechanism/saveImage`,
        listType: 'picture-card',
        name: 'file1',
        className: 'upload-list-inline',
        beforeUpload: this.handleUpload,
      },
      fileListLogo: Logo,
      fileListOrg: Org,
      fileListShow: Show,
    };
  }

  handleUpload = (file) => {
    // /1024 即可得到实际大小 file.type 为格式
    const { name, size } = file;
    const Rxp = /.+\.(jpg|jpeg|png|bmp)$/;
    if (!Rxp.test(name)) {
      message.error('请上传正确的图片文件 仅支持 jpg | png | jpeg | bmp ', 3);
      return false;
    } else if ((size / 1024) > 6000) {
      message.error('图片文件大于 5M', 3);
      return false;
    }
    return true;
  };

  handleRemovePic = (file, name, restName) => {
    const { form: { resetFields } } = this.props;
    if (restName) resetFields([restName]);
    const { uid } = file;
    const fileList = this.state[name].filter(item => item.uid !== uid);
    this.setState({
      [name]: fileList,
    });
    return true;
  };

  handleChangePic = ({ file, fileList}, stateName) => {
    const { name, size } = file;
    const Rxp = /.+\.(jpg|jpeg|png|bmp)$/;
    if (!Rxp.test(name) || (size / 1024) > 6000) {
      return;
    }
    let fileListPic = fileList;
    fileListPic = fileListPic.map((item) => {
      if (item.response) {
        item.url = item.response.result;
      }
      return item;
    });
    fileListPic = fileListPic.filter((item) => {
      if (item.response) {
        return item.response.status === true;
      }
      return true;
    });
    file.status === 'done' ? (message.success('上传成功', 3)) : '';
    this.setState({ [stateName]: fileListPic });
  };

  handleMapPic = (item, type) => item.map((items, index) => ({
    name: `${type} ${index + 1}`,
    uid: index,
    url: items,
  }));

  confirmPic = (fileList) => fileList.map(item => item.url);

  render() {

    const { form, dispatch, submitting, location: { state }, } = this.props;
    const { getFieldDecorator, validateFields } = form;
    const {
      fileListLogo,
      fileListOrg,
      fileListShow,
      PROPS,
    } = this.state;
    const { handleRemovePic, handleChangePic } = this;
    const onPrev = e => {
      e.preventDefault();
      validateFields((err, values) => {
        values.foundTime = values.foundTime ? new Date(values.foundTime.toDate()).getTime() : '';
        values.orgLogo = values.orgLogo ? JSON.stringify(this.confirmPic(this.state.fileListLogo)) : [];
        values.orgLicense = values.orgLicense ? JSON.stringify(this.confirmPic(this.state.fileListOrg)) : [];
        if (this.state.fileListShow && this.state.fileListShow.length) {
          values.orgShow = JSON.stringify(this.confirmPic(this.state.fileListShow));
        }else {
          values.orgShow = [];
        }
        dispatch(routerRedux.push({
          pathname: '/mechanism/mine/StepForm/info',
          state: state ? {
            ...state,
            ...values,
          } : {
            ...values,
          },
        }));
      });
    };
    const onValidateForm = e => {
      e.preventDefault();
      validateFields((err, values) => {
        if (!err) {
          if (!state) {
            message.error('信息填写不完整');
            return;
          }
          if (!values.orgCode || !values.orgCode.length) {
            message.warn('请填写正确的统一社会信用代码',2);
            return;
          }
          if (!fileListLogo.length || !fileListOrg.length) {
            message.error('请上传 相关证件');
            return;
          }else if (!state.step || state.step !== 'down') {
            message.error('信息填写不完整');
            return;
          }
          values.foundTime = new Date(values.foundTime.toDate()).getTime();
          values.orgLogo = JSON.stringify(this.confirmPic(this.state.fileListLogo));
          values.orgLicense = JSON.stringify(this.confirmPic(this.state.fileListOrg));
          if (this.state.fileListShow && this.state.fileListShow.length) {
            values.orgShow = JSON.stringify(this.confirmPic(this.state.fileListShow));
          }else {
            values.orgShow = '';
          }
          values = {
            ...values,
            ...state,
          };
          if (values.Afresh) delete values.Afresh;
          if (values.step) delete values.step;
          dispatch({
            type: 'mine/submit',
            payload: values,
          }).then(() => {
            values.auditResult = 2;
            dispatch(routerRedux.push({
              pathname: '/mechanism/mine/StepForm/result',
              state: values,
            }));
          })
        }
      });
    };
    return (
      <Form layout="horizontal" className={styles.stepForm}>
        <Form.Item {...formItemLayout} className={styles.stepFormText} label="成立日期">
          {getFieldDecorator('foundTime', {
            initialValue: state && state.foundTime ? moment(`${parseTime(state.foundTime)}`) : moment('2018'),
            rules: [
              { required: true, message: '请重新输入' }],
          })(
            <DatePicker
              style={{ width: '100%' }}
              placeholder="请选择日期"
              format="YYYY/MM/DD"
            />
          )}
        </Form.Item>
        <Form.Item {...formItemLayout} className={styles.stepFormText} label="社会信用代码">
          {getFieldDecorator('orgCode', {
            initialValue: state && state.orgCode ? state.orgCode : '',
            rules: [
              { required: false, message: '格式有误，请重新输入' }],
          })(
            <Input placeholder="请输入" />
          )}
        </Form.Item>
        <Form.Item {...formItemLayout} className={styles.stepFormText} label="经营范围">
          {getFieldDecorator('scopeManage', {
            initialValue: state && state.scopeManage ? state.scopeManage : '',
            rules: [
              { pattern: /^[\u4E00-\u9FA5A-Za-z0-9]|['“”"{}()*&.?!,…:;。《》、\\/@` ]+$/, required: true, message: '请重新输入' }],
          })(
            <Input placeholder="请输入" />
          )}
        </Form.Item>
        <Form.Item {...formItemLayout} className={styles.stepFormText} label="注册地址">
          {getFieldDecorator('registerAddr', {
            initialValue: state && state.registerAddr ? state.registerAddr : '',
            rules: [
              { pattern: /^[\u4E00-\u9FA5A-Za-z0-9]+$/, required: true, message: '地址格式不正确' }],
          })(
            <Input placeholder="请填写详细地址" />
          )}
        </Form.Item>
        <Form.Item {...formItemLayout} className={styles.stepFormText} label="经营地址">
          {getFieldDecorator('manageAddr', {
            initialValue: state && state.manageAddr ? state.manageAddr : '',
            rules: [
              { pattern: /^[\u4E00-\u9FA5A-Za-z0-9]+$/, required: true, message: '地址格式不正确' }],
          })(
            <Input placeholder="请填写详细地址" />
          )}
        </Form.Item>
        <Form.Item {...formItemLayout} className={styles.stepFormText} label="机构Logo">
          {getFieldDecorator('orgLogo', {
            initialValue: state && state.orgLogo ? fileListLogo : [],
            rules: [
              { required: true, message: '请重新上传' }],
          })(
            <Upload
              {...PROPS}
              fileList={fileListLogo}
              onChange={file => handleChangePic(file, 'fileListLogo')}
              onRemove={file => handleRemovePic(file, 'fileListLogo', 'orgLogo')}
            >
              { fileListLogo.length >= 1 ? null : (
                <div>
                  <Icon className="ant-icon" type="plus" />
                  <div className="ant-upload-text">机构Logo</div>
                </div>
              ) }
            </Upload>
          )}
        </Form.Item>
        <Form.Item {...formItemLayout} className={styles.stepFormText} label="营业执照">
          {getFieldDecorator('orgLicense', {
            initialValue: state && state.orgLicense ? fileListOrg : [],
            rules: [
              { required: true, message: '请重新上传' }],
          })(
            <Upload
              {...PROPS}
              fileList={fileListOrg}
              onChange={file => handleChangePic(file, 'fileListOrg')}
              onRemove={file => handleRemovePic(file, 'fileListOrg', 'orgLicense')}
            >
              {fileListOrg.length >= 1 ? null : (
                <div>
                  <Icon className="ant-icon" type="plus" />
                  <div className="ant-upload-text">营业执照</div>
                </div>
              )}
            </Upload>
          )}
        </Form.Item>
        <Form.Item {...formItemLayout} className={styles.stepFormText} label="机构展示">
          {getFieldDecorator('orgShow', {
            initialValue: state && state.orgShow ? fileListShow : [],
            rules: [
              { required: false, message: '请重新上传' }],
          })(
            <Upload
              {...PROPS}
              multiple
              fileList={fileListShow}
              onChange={file => handleChangePic(file, 'fileListShow')}
              onRemove={file => handleRemovePic(file, 'fileListShow', 'orgShow')}
            >
              <div>
                <Icon className="ant-icon" type="plus" />
                <div className="ant-upload-text">机构展示</div>
              </div>
            </Upload>
          )}
        </Form.Item>
        <Form.Item {...formItemLayout} className={styles.stepFormText} label="详细介绍">
          {getFieldDecorator('orgDetails', {
            initialValue: state ? state.orgDetails : '',
            rules: [
              { pattern: /^[\u4E00-\u9FA5A-Za-z0-9]|['“”"{}()*&.?!,…:;。《》、\\/@` ]+$/, required: false, message: '请重新输入' }],
          })(
            <TextArea placeholder="请填写详细介绍" autosize={{ minRows: 2, maxRows: 100 }} />
          )}
        </Form.Item>
        <Form.Item
          style={{ marginBottom: 8 }}
          wrapperCol={{
            xs: { span: 24, offset: 0 },
            sm: {
              span: formItemLayout.wrapperCol.span,
              offset: formItemLayout.labelCol.span,
            },
          }}
          label=""
        >
          <Button onClick={onPrev}>
          上一步
          </Button>
          <Button type="primary" style={{ marginLeft: 8 }} onClick={onValidateForm} loading={submitting}>
            提交
          </Button>

        </Form.Item>
      </Form>
    );
  }
}

export default Step2;
