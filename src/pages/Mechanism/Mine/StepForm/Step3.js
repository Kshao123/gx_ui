import React, { Fragment } from 'react';
import { connect } from 'dva';
import { Button, Row, Col, Steps, Card, Popover, Badge } from 'antd';
import { routerRedux } from 'dva/router';
import Result from '@/components/Result';
import styles from './style.less';


const { Step } = Steps;
const caseStatus = (state) => {
  if (!state) return;
  const { auditResult } = state;
  if (!auditResult) return;
  switch (auditResult) {
    case 0:
      return (
        <div>
          <p>审核未通过</p>
          <p>{state.falseCause}</p>
        </div>
      );
    case 1:
      return '审核通过';
    default:
      break;
  }
};

const caseTitle = (state) => {
  if (!state) return;
  const { auditResult } = state;
  if (!auditResult) return;
  switch (auditResult) {
    case 0:
      return '审核未通过';
    case 1:
      return '审核通过';
    case 2:
      return '预计审核时间为2~3个工作日';
    default:
      break;
  }
};

const caseMsg = (state) => {
  if (!state) return;
  const { auditResult } = state;
  if (!auditResult && auditResult !== 0) return;
  switch (auditResult) {
    case 0:
      return `拒绝原因：${state.falseCause}`;
    case 1:
      return '审核通过';
    case 2:
      return '';
    default:
      break;
  }
};

const changeNum = (state) => {
  if (!state) return;
  const { auditResult } = state;
  if (!auditResult && auditResult !== 0) return;
  switch (auditResult) {
    case 0:
      return 2;
    case 1:
      return 2;
    case 2:
      return 1;
    default:
      break;
  }
};

@connect(({ form }) => ({
  // data: form.step,
}))
class Step3 extends React.PureComponent {

  render() {
    const { location: { state }, dispatch } = this.props;
    const onFinish = () => {
      dispatch(routerRedux.push({
        pathname: '/mechanism/mine',
      }));
    };
    const onAfresh = () => {
      dispatch(routerRedux.push({
        pathname: '/mechanism/mine/stepForm/info',
        state: state ? {
          ...state,
          Afresh: true,
        } : undefined,
      }));
    };
    const information = (
      <div className={styles.information}>
        <Row>
          <Col xs={24} sm={8} className={styles.label}>
            机构名称：
          </Col>
          <Col xs={24} sm={16}>
            {state ? state.orgName : ''}
          </Col>
        </Row>
        <Row>
          <Col xs={24} sm={8} className={styles.label}>
            联系电话：
          </Col>
          <Col xs={24} sm={16}>
            {state ? state.mobilePhone : ''}
          </Col>
        </Row>
        <Row>
          <Col xs={24} sm={8} className={styles.label}>
            社会信用代码：
          </Col>
          <Col xs={24} sm={16}>
            {state ? state.orgCode : ''}
          </Col>
        </Row>
        <Row>
          <Col xs={24} sm={8} className={styles.label}>
            营业执照：
          </Col>
          <Col xs={24} sm={16}>
            <img style={{ width: 120, height: 120 }} src={state && state.orgLicense ? JSON.parse(state.orgLicense)[0] : ''} alt="" />
          </Col>
        </Row>
      </div>
    );
    const actions = (
      <Fragment>
        <Button type="primary" onClick={onFinish}>
          返回机构
        </Button>
        <Button onClick={onAfresh} style={{ display: state && state.auditResult !== 2 ? 'inline-block' : 'none' }}>重新修改</Button>
      </Fragment>
    );

    return (
      <Card>
        <Result
          type={state && state.auditResult ? 'success' : 'error'}
          title={caseTitle(state)}
          description={caseMsg(state)}
          extra={information}
          actions={actions}
          className={styles.result}
        />
        <Card title="流程进度" style={{ marginTop: 24 }} bordered={false}>
          <Steps status={state && state.auditResult === 0 ? 'error' : ''} current={changeNum(state)}>
            <Step title="创建机构" />
            <Step title="审批中" />
            <Step description={caseMsg(state)} title="结果" />
          </Steps>
        </Card>
      </Card>

    );
  }
}

export default Step3;
