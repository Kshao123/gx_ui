import React, { Fragment } from 'react';
import { connect } from 'dva';
import {
  Form,
  Input,
  Button,
  Select,
  Divider,
  Radio,
} from 'antd';
import { routerRedux } from 'dva/router';
import styles from './style.less';

const { Option } = Select;
const RadioGroup = Radio.Group;
const formItemLayout = {
  labelCol: {
    span: 5,
  },
  wrapperCol: {
    span: 19,
  },
};
const orgClassMap = [
  {
    value: 1,
    text: '个体工商户',
  },
  {
    value: 2,
    text: '企业',
  },
];

@connect(({ layoutloading: { currentUser } }) => ({
  currentUser,
}))
@Form.create()
class Step1 extends React.PureComponent {
  constructor(props) {
    super(props);
    const { location: { state } } = props;
    if (state) {
      console.log(state)
      if ((state.auditResult || state.auditResult === 0) && !state.Afresh) {
        props.dispatch(routerRedux.push({
          pathname: '/mechanism/mine/StepForm/result',
          state,
        }));
      }
    }
    this.state = {
      // data: state,
    }
  }

  componentDidMount() {

  }

  render() {
    const { form, dispatch, location: { state }, disabled, currentUser} = this.props;
    const { getFieldDecorator, validateFields } = form;
    const onValidateForm = () => {
      validateFields((err, values) => {

        if (!err) {
          if (!state) {
            delete values.id;
          }
          dispatch(routerRedux.push({
            pathname: '/mechanism/mine/StepForm/mine',
            state: state ? {
              ...state,
              ...values,
              step: 'down',
            } : {
              ...values,
              step: 'down',
            },
          }));
        }
      });
    };
    return (
      <Fragment>
        <Form layout="horizontal" className={styles.stepForm}>
          {getFieldDecorator('id', {
            initialValue: state ? state.id : '' })(
              <Input type="hidden" />
          )}
          {getFieldDecorator('userId', {
            initialValue: state ? state.userId : currentUser.userid })(
            <Input type="hidden" />
          )}
          <Form.Item {...formItemLayout} label="机构名称">
            {getFieldDecorator('orgName', {
              initialValue: state ? state.orgName : '',
              rules: [
                { pattern: /^[\u4E00-\u9FA5-a-zA-Z0-9_]{2,16}$/, required: true, message: '请输入中、英文、数字或下划线' }],
            })(
              <Input disabled={disabled} placeholder="请输入机构名称" />
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label="固定电话">
            {getFieldDecorator('orgTelephone', {
              initialValue: state ? state.orgTelephone : '',
              rules: [
                { pattern: /0\d{2,3}-\d{7,8}/, required: false, message: '格式错误，请重新输入， tip：0511-66****8' }],
            })(
              <Input disabled={disabled} placeholder="请输入固定电话" />
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label="手机">
            {getFieldDecorator('mobilePhone', {
              initialValue: state ? state.mobilePhone : '',
              rules: [
                { pattern: /^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$/, required: true, message: '格式错误，请重新输入' }],
            })(
              <Input disabled={disabled} placeholder="请输入手机号码" />
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label="邮箱">
            {getFieldDecorator('orgEmails', {
              initialValue: state ? state.orgEmails : '',
              rules: [
                { pattern: /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/, required: false, message: '邮箱格式有误，请重新输入' }],
            })(
              <Input disabled={disabled} placeholder="请输入邮箱地址" />
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label="分类">
            {getFieldDecorator('orgClass', {
              initialValue: state ? state.orgClass : '',
              rules: [
                { required: true, message: '请选择机构分类' }],
            })(
              <RadioGroup disabled={disabled}>
                {orgClassMap.map(item => <Radio key={item.value} value={item.value}>{item.text}</Radio>)}
              </RadioGroup>
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label="状态">
            {getFieldDecorator('orgStatus', {
              initialValue: state ? state.orgStatus : '',
              rules: [
                { required: true, message: '请选择机构状态' }],
            })(
              <Select
                disabled={disabled}
                placeholder="请确认状态"
              >
                <Option value>营业</Option>
                <Option value={false}>未营业</Option>
              </Select>
            )}
          </Form.Item>

          <Form.Item
            wrapperCol={{
              xs: { span: 24, offset: 0 },
              sm: {
                span: formItemLayout.wrapperCol.span,
                offset: formItemLayout.labelCol.span,
              },
            }}
            label=""
          >
            <Button type="primary" onClick={onValidateForm}>
              下一步
            </Button>
          </Form.Item>
        </Form>
        <Divider style={{ margin: '40px 0 24px' }} />
        <div className={styles.desc}>
          <h3>说明</h3>
          <p>
            联系方式请填写正确，审核的结果我们将优先使用短信通知！
          </p>
        </div>
      </Fragment>
    );
  }
}

export default Step1;
