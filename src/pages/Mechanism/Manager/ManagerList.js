import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Button, Divider, Modal } from 'antd';
import StandardTable from '../../../components/DelTable';
import styles from '../../../utils/global.less';
import Ellipsis from '../../../components/Ellipsis';
import Hoc from '../../../utils/Hoc';
import Popcon from '../../../components/Popconfirm';
import Authorized from '@/utils/Authorized';

const { check } = Authorized;
const { TextArea } = Input;


const MsgForm = {
  id: 1,
  username: '账号',
  mobile: '手机',
  email: '邮箱',
  name: '姓名',
  address: '地址',
};

@connect(({ global, mine, loading }) => ({
  currentOrg: global.currentOrg,
  mine,
  loading: loading.global,
}))
@Form.create()
@Hoc({ fetchUrl: 'mine/_getManager' })
class ManagerList extends PureComponent {
  constructor() {
    super();
    this.state = {
     selectedRows: [],
     visible: false,
     isEdit: false,
     FormObj: {},
    };
   }

  handleEdit = (item, value) => {
     let str = false;
     let obj = {};
     if (value) {
       str = true;
       obj = item;
     }
    this.setState({
      visible: true,
      isEdit: str,
      FormObj: obj,
    });
 };

 handleDelete = ({ id }) => {
   const { dispatch, pagination, currentOrg: { id: orgId } } = this.props;
   dispatch({
     type: 'mine/_deleteManager',
     payload: {
       id,
       pagination,
       orgId,
     },
   });
 };

 handleOk = () => {
  const { form, dispatch, pagination, currentOrg: { id: orgId } } = this.props;
  form.validateFields((err, values) => {
    if (err) {
       return;
    }

    if (!this.state.isEdit) {
     delete (values.id);
    }
    dispatch({
       type: 'mine/_setManager',
       payload: {
         values,
         pagination,
         orgId,
       },
    });

     // 在此处调用的是 重置表单内容， 不重置的话会导致 对话框再次打开不能实时更新内容的bug
    form.resetFields();this.setState({
       visible: false,
       isEdit: false,
    });
  });
 };

 handleCancel = () => {
   this.props.form.resetFields();
   this.setState({
       visible: false,
       isEdit: false,
   });
 };

  render() {
    const columns = [
      {
        title: '账号',
        dataIndex: 'username',
      },
      {
        title: '姓名',
        dataIndex: 'name',
      },
      {
        title: '手机号',
        dataIndex: 'mobile',
      },
      {
        title: '邮箱',
        dataIndex: 'email',
      },
      {
        title: '地址',
        dataIndex: 'address',
        render(text) {
          return (
            <Ellipsis length={12} tooltip>
              {text || '暂无信息'}
            </Ellipsis>
          );
        },
      },
      {
        title: '操作',
        render: (text, record) => (
          <Fragment>
            {check('机构管理-机构法人:修改', <a onClick={() => this.handleEdit(record, true)}>修改</a>)}
            <Divider type="vertical" />
            {check('机构管理-机构法人:删除', (<Popcon
              title="是否确认删除"
              onConfirm={() => this.handleDelete(record)}
            />))}
          </Fragment>
         ),
      },
    ];
    const {
      mine: { mangerList: data },
      loading,
      handleStandardTableChange,
      form: { getFieldDecorator }
    } = this.props;
    const { isEdit, FormObj } = this.state;

    return (
      <Card>
        <div className={styles.tableListOperator}>
          {check('机构管理-机构法人:增加', (
            <Button icon="plus" type="primary" onClick={this.handleEdit}>
              新建
            </Button>
          ))}
        </div>
        <div className={styles.tableList}>
          <StandardTable
            bordered
            loading={loading}
            data={data}
            columns={columns}
            onChange={handleStandardTableChange}
          />
        </div>
        <Modal
          title="编辑/修改信息"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Form layout="vertical">
            {getFieldDecorator('id', {
                    initialValue: isEdit ? FormObj.id : '' })(
                      <Input type="hidden" />
                  )}
            <Form.Item label={MsgForm.username}>
              {getFieldDecorator('username', {
                      initialValue: isEdit ? FormObj.username : '',
                      rules: [{ required: true, message: '请输入账号' }],
                    })(
                      <Input placeholder="账号" />
                    )}
            </Form.Item>


            <Form.Item label={MsgForm.name}>
              {getFieldDecorator('name', {
                initialValue: isEdit ? FormObj.name : '',
                rules: [{ required: true, message: '请输入姓名' }],
              })(
                <Input placeholder="姓名" />
              )}
            </Form.Item>

            <Form.Item label={MsgForm.mobile}>
              {getFieldDecorator('mobile', {
                      initialValue: isEdit ? FormObj.mobile : '',
                      rules: [{ pattern: /^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$/, required: true, message: '格式错误，请重新输入' }],
                    })(
                      <Input placeholder="手机" />
                    )}
            </Form.Item>

            <Form.Item label={MsgForm.email}>
              {getFieldDecorator('email', {
                      initialValue: isEdit ? FormObj.email : '',
                      rules: [{ pattern: /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/, required: false, message: '邮箱格式有误，请重新输入' }],
                    })(
                      <Input placeholder="邮箱" />
                    )}
            </Form.Item>

            <Form.Item label={MsgForm.address}>
              {getFieldDecorator('address', {
                      initialValue: isEdit ? FormObj.address : '',
                      rules: [{ required: false, message: '请输入地址' }],
                    })(
                      <TextArea placeholder="地址" autosize={{ minRows: 2, maxRows: 6 }} />
                    )}
            </Form.Item>

          </Form>
        </Modal>
      </Card>
    );
  }
}

export default ManagerList;
