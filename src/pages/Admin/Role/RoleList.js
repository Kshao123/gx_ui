import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import isEqual from 'lodash/isEqual';
import { Form, Button, Divider, Card } from 'antd';
import StandardTable from '@/components/DelTable';
import RoleMgr from './RoleMgr';// 引入对话框
import styles from '@/utils/global.less';
import RoleFunction from './RoleFunction';
import Popcon from '@/components/Popconfirm';
import Authorized from '@/utils/Authorized';

const { check } = Authorized;

@connect(({ role, global, loading }) => ({
  role,
  currentOrg: global.currentOrg,
  loading: loading.global,
}))
@Form.create()
class RoleList extends PureComponent {
  constructor() {
    super();
    this.state = {
      pagination: {
        pageSize: 10,
        pageNum: 1,
      },
      currentOrg: {
        id: '',
      },
      selectedRows: [],
    };
  }

  componentDidMount() { // 加载dom时 获取数据
    const { dispatch, currentOrg } = this.props;
    if (!currentOrg.id) return;
    const { pagination } = this.state;
    dispatch({
      type: 'role/fetch',
      payload: {
        pagination,
        orgId: currentOrg.id,
      },
    });
  }

  static getDerivedStateFromProps(nextProps, preState) {
    const { dispatch } = nextProps;
    if (!nextProps.currentOrg.id) return null;
    if (isEqual(nextProps.currentOrg.id, preState.currentOrg.id)) {
      return null;
    }
    const { pagination } = preState;
    dispatch({
      type: 'role/fetch',
      payload: {
        pagination,
        orgId: nextProps.currentOrg.id,
      },
    });
    return {
      currentOrg: nextProps.currentOrg,
    };
  }

  handleStandardTableChange = (pagination) => {
    const { dispatch } = this.props;
    const { pageSize, current: pageNum } = pagination;
    this.setState({
      pagination: {
        pageNum,
        pageSize,
      },
    });
    dispatch({
      type: 'role/fetch',
      payload: {
        pagination: {
          pageNum,
          pageSize,
        },
        orgId: this.state.currentOrg.id,
      },
    });
  };


  handleEdit = (record) => { // 点击 角色对应权限触发的事件 参数为当前点击的数据
    const { dispatch } = this.props;
    dispatch({
      type: 'role/queryFunction',
      payload: record,
    }).then((res) => { // 请求成功时的回调 此处的res 为 请求的返回结果 // 此处触发修改权限对话框 并把当前点击权限的数据保存到modal里
      dispatch({
        type: 'role/handleFunctionRight',
        data: {
          ...record,
          ...res.result,
        },
      });
    });
  };

  handleDelete = (record, value) => {
    let arr = [];
    if (value) {
      arr = record;
    } else {
      arr.push(record.id);
    }
    const { dispatch } = this.props;
    const { pagination, currentOrg } = this.state;
    dispatch({
      type: 'role/delete',
      payload: {
        ids: arr,
        pagination,
        orgId: currentOrg.id,
      },
    });
  };

  handleSelectRows = (rows) => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleAdd = () => {
    this.props.dispatch({
      type: 'role/handleModalVisible',
    });
  };

  handleJump = () => {
    const { dispatch } = this.props;
    const { pagination: { pageNum, pageSize }, currentOrg } = this.state;
    dispatch({
      type: 'role/fetch',
      payload: {
        pagination: {
          pageNum,
          pageSize,
        },
        orgId: this.state.currentOrg.id,
      },
    });
  };

  render() {
    const columns = [
      {
        title: '角色编码',
        dataIndex: 'roleCode',
      },
      {
        title: '角色名称',
        dataIndex: 'roleName',
      },
      {
        title: '操作',
        render: (text, record) => (
          <Fragment>
            {check('角色:修改', <a onClick={() => this.handleEdit(record)}>角色对应权限</a>)}
            <Divider type="vertical" />
            {check('角色:删除', (
              <Popcon
                title="是否确认删除"
                onConfirm={() => this.handleDelete(record)} // 点击确认时
              />
            ))}
          </Fragment>
        ),
      },
    ];
    const { role: { data }, loading } = this.props;
    const { selectedRows } = this.state;

    return (
      <Card>
        <div className={styles.tableList}>
          <div className={styles.tableListForm} />
          <div className={styles.tableListOperator}>
            {check('角色:增加', (
              <Button icon="plus" type="primary" onClick={this.handleAdd}>
                新建
              </Button>
            ))}
          </div>
          <StandardTable
            showAllSel
            Rowselect
            selectedRows={selectedRows}
            loading={loading}
            data={data}
            SelKey={['id', 'roleName']} // 传过去的
            columns={columns}
            onSelectRow={this.handleSelectRows}
            onChange={this.handleStandardTableChange}
            onDel={e => this.handleDelete(e, true)}
          />
          <RoleMgr handleJump={this.handleJump} />
          <RoleFunction />
        </div>
      </Card>
    );
  }
}

export default RoleList;
