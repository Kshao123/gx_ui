/* eslint-disable no-unused-vars,linebreak-style,import/first */
import React, { PureComponent } from 'react';
import { Form, Input, Modal, message, notification } from 'antd';
import { connect } from 'dva';

const fieldLabels = {
  roleCode: '角色编码',
  roleName: '角色名称',
};

class RoleMgr extends PureComponent {
  onCreate = () => {
    const { form, dispatch } = this.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      dispatch({
        type: 'role/submit',
        payload: values,
      }).then((res) => {
        const { message: msg, status } = res;
        if (!status) {
          message.error(msg);
          return;
        }
        notification.success({
          message: '成功',
          description: '保存用户角色成功',
        });
        form.resetFields();
        dispatch({
          type: 'role/handleModalVisible',
        });
        this.props.handleJump();
      });
    });
  }

  onCancel = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'role/handleModalVisible',
    });
  }

  render() {
    const { form, role: { modal: { data, visible } }, currentOrg } = this.props;
    const { getFieldDecorator } = form;
    return (
      <Modal
        visible={visible}
        title="新增/编辑角色"
        okText="保存"
        onCancel={this.onCancel}
        onOk={this.onCreate}
      >
        <Form layout="vertical">
          {getFieldDecorator('orgId', {
            initialValue: currentOrg && currentOrg.id ? currentOrg.id : '' })(
              <Input type="hidden" />
          )}
          <Form.Item label={fieldLabels.roleCode}>
            {getFieldDecorator('roleCode', {
                    initialValue: data ? data.roleCode : '',
                                        rules: [{ required: true, message: '请输入' }],
                                    })(
                                      <Input placeholder="请输入" />
                                    )}
          </Form.Item>
          <Form.Item label={fieldLabels.roleName}>
            {getFieldDecorator('roleName', {
                    initialValue: data ? data.roleName : '',
                                        rules: [{ required: true, message: '请输入' }],
                                    })(
                                      <Input placeholder="请输入" />
                                    )}
          </Form.Item>
        </Form>
      </Modal>
    );
  }
}

export default connect(({ role, loading, global }) => ({
  role,
  currentOrg: global.currentOrg,
  // submitting: loading.effects['role/submit'],
}))(Form.create()(RoleMgr));
