import React, { PureComponent } from 'react';
import { Form, Input, Transfer, Modal, message, notification } from 'antd';
import { connect } from 'dva';


const fieldLabels = {
  roleCode: '角色编码',
  roleName: '角色名称',
  rights: '权限列表',
};

const list = ['ROLE_LEADER', 'SCH_ADMIN', 'EDU_ADMIN'];

class RoleFunction extends PureComponent {
  onCreate = () => {
    const { form, dispatch } = this.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      dispatch({
        type: 'role/saveFunction',
        payload: values,
      }).then((res) => {
        const { message: msg, status } = res;
        if (!status) {
          message.error(msg);
          return;
        }
        notification.success({
          message: '成功',
          description: '保存用户角色成功',
        });
        form.resetFields();
        dispatch({
          type: 'role/handleFunctionRight',
        });
      });
    });
  }

  onCancel = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'role/handleFunctionRight',
    });
  }

  handleChange = (targetKeys, direction, moveKeys) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'role/handleTransfer',
      payload: targetKeys,
    });
  }

  render() {
    const { form, role: { modal }, currentOrg } = this.props;
    const { data, functionVisible } = modal;
    const { getFieldDecorator } = form;
    let ds;
    if (data && data.all) { // data.all 为所有权限 数组
      ds = data.all.map((v) => {
        return {
          key: v.id,
          title: v.functionName,
        };
      });
    }
    let disabled = false;
    if (data && list.indexOf(data.roleCode) >= 0) disabled = true;
    return (
      <Modal
        visible={functionVisible}
        title="新增/编辑角色权限"
        okText="保存"
        onCancel={this.onCancel}
        onOk={this.onCreate}
      >
        <Form layout="vertical">
          {getFieldDecorator('id', {
                            initialValue: data ? data.id : '' })(
                              <Input type="hidden" />
                                  )}
          {getFieldDecorator('orgId', {
                            initialValue: currentOrg && currentOrg.id ? currentOrg.id : '' })(
                              <Input type="hidden" />
                                  )}
          {/* label 标签的文本 小标题 getFieldDecorator 接受两个参数 第一个 ID 必填输入控件唯一标志 */}
          <Form.Item label={fieldLabels.roleCode}>
            {getFieldDecorator('roleCode', {
                    initialValue: data ? data.roleCode : '',
                                        rules: [{ required: true, message: '请输入' }], // rules:是否必选 message:校验文案
                                    })(
                                      <Input disabled={disabled} placeholder="请输入" />// placeholder 同用
                                    )}
          </Form.Item>
          <Form.Item label={fieldLabels.roleName}>
            {getFieldDecorator('roleName', {
                    initialValue: data ? data.roleName : '',
                                        rules: [{ required: true, message: '请输入' }],
                                    })(
                                      <Input disabled={disabled} placeholder="请输入" />
                                    )}
          </Form.Item>
          <Form.Item label={fieldLabels.rights}>
            {getFieldDecorator('rights', {
                    initialValue: data ? data.functions : '',
                                        rules: [{ required: true, message: '请输入' }],
                                    })(
                                      <Transfer
                                        dataSource={ds}
                                        showSearch
                                        listStyle={{
                                                    width: '40%',
                                                    height: 300,
                                                  }}
                                        operations={['选择', '撤销']}
                                        targetKeys={data && data.functions ? data.functions : []}
                                        onChange={this.handleChange}
                                        render={(item) => { return `${item.title}`; }}
                                      />
                                    // targetKeys:显示在右侧框数据的key集合 onChange:选项在两栏之间转移时的回调函数
                                    // dataSource:数据源，其中的数据将会被渲染到左边一栏中，targetKeys 中指定的除外。
                                    )}
          </Form.Item>
        </Form>
      </Modal>
    );
  }
}

export default connect(({ role, loading, global }) => ({
  role,
  submitting: loading.effects['role/submit'],
  currentOrg: global.currentOrg,
}))(Form.create()(RoleFunction));
