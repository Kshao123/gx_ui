import React, { PureComponent } from 'react';
import { Card, Button, Form, Icon, Col, Row, Tooltip, Input, Select, Popover } from 'antd';
import { connect } from 'dva';
import FooterToolbar from '../../../components/FooterToolbar';
import styles from '../../../utils/global.less';

const fieldLabels = {
  username: '用户名',
  mobile: '联系方式',
  email: 'email',
  name: '姓名',
};

class UserMgr extends PureComponent {
  state = {
    width: '100%',
  };
  render() {
    const { form, dispatch, submitting, location, currentOrg } = this.props;
    const { getFieldDecorator, validateFieldsAndScroll, getFieldsError } = form;
    const { state } = location;
    const validate = () => {
      validateFieldsAndScroll((error, values) => {
        if (!error) {
          if (!state) delete values.id;
          dispatch({
            type: 'userMgr/submit',
            payload: values,
          });
        }
      });
    };
    const errors = getFieldsError();
    const getErrorInfo = () => {
      const errorCount = Object.keys(errors).filter(key => errors[key]).length;
      if (!errors || errorCount === 0) {
        return null;
      }
      const scrollToField = (fieldKey) => {
        const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
        if (labelNode) {
          labelNode.scrollIntoView(true);
        }
      };
      const errorList = Object.keys(errors).map((key) => {
        if (!errors[key]) {
          return null;
        }
        return (
          <li key={key} className={styles.errorListItem} onClick={() => scrollToField(key)}>
            <Icon type="cross-circle-o" className={styles.errorIcon} />
            <div className={styles.errorMessage}>{errors[key][0]}</div>
            <div className={styles.errorField}>{fieldLabels[key]}</div>
          </li>
        );
      });
      return (
        <span className={styles.errorIcon}>
          <Popover
            title="表单校验信息"
            content={errorList}
            overlayClassName={styles.errorPopover}
            trigger="click"
            getPopupContainer={trigger => trigger.parentNode}
          >
            <Icon type="exclamation-circle" />
          </Popover>
          {errorCount}
        </span>
      );
    };
    return (
      <div>
        <Card title="用户信息" className={styles.card} bordered={false}>
          <Form layout="vertical" hideRequiredMark>
            {getFieldDecorator('id', {
                            initialValue: state ? state.id : '' })(
                              <Input type="hidden" />
                                    )}
            {getFieldDecorator('orgId', {
                            initialValue: currentOrg ? currentOrg.id : '' })(
                              <Input type="hidden" />
                                    )}
            <Row gutter={16}>
              <Col xl={{ span: 6, offset: 2 }} lg={{ span: 10 }} md={{ span: 24 }} sm={24}>
                <Form.Item label={fieldLabels.username}>
                  <Tooltip title="4到16位 可由字母，数字，下划线 组成">
                    {getFieldDecorator('username', {
                      initialValue: state ? state.username : '',
                                          rules: [{ pattern: /^[a-zA-Z0-9_]{4,16}$/, required: true, message: '请输入4到16位 可由字母，数字，下划线 组成' }],
                                      })(
                                        <Input placeholder="请输入" />
                                      )}
                  </Tooltip>
                </Form.Item>
              </Col>
              <Col xl={{ span: 6, offset: 4 }} lg={{ span: 10 }} md={{ span: 24 }} sm={24}>
                <Form.Item label={fieldLabels.name}>
                  <Tooltip title="请输入姓名">
                    {getFieldDecorator('name', {
                      initialValue: state ? state.name : '',
                                          rules: [{ pattern: /^[\u4E00-\u9FA5A-Za-z_]+$/, required: true, message: '请输入姓名' }],
                                          })(
                                            <Input placeholder="请输入" />
                                          )}
                  </Tooltip>
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col xl={{ span: 6, offset: 2 }} lg={{ span: 10 }} md={{ span: 24 }} sm={24}>
                <Form.Item label={fieldLabels.mobile}>
                  <Tooltip title="请输入手机号">
                    {getFieldDecorator('mobile', {
                    initialValue: state ? state.mobile : '',
                                        rules: [{ pattern: /^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$/, required: true, message: '请输入正确的联系方式' }],
                                         })(
                                           <Input placeholder="请输入" />
                                         )}
                  </Tooltip>
                </Form.Item>
              </Col>
              <Col xl={{ span: 6, offset: 4 }} lg={{ span: 10 }} md={{ span: 24 }} sm={24}>
                <Form.Item label={fieldLabels.email}>
                  <Tooltip title="请输入邮箱">
                    {getFieldDecorator('email', {
                      initialValue: state ? state.email : '',
                                          rules: [{ type: 'email', required: false, message: '请输入格式正确email' }],
                                          })(
                                            <Input placeholder="请输入" />
                                          )}
                  </Tooltip>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Card>
        <FooterToolbar style={{ width: this.state.width }}>
          {getErrorInfo()}
          <Button type="primary" onClick={validate} loading={submitting}>
                        提交
          </Button>
        </FooterToolbar>
      </div>
    );
  }
}

export default connect(({ usermgr, global, loading }) => ({
  usermgr,
  currentOrg: global.currentOrg,
  submitting: loading.effects['userMgr/submit'],
}))(Form.create()(UserMgr));
