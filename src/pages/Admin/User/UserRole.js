import React, { PureComponent } from 'react';
import { Form, Input, Transfer, Modal, notification, message, Spin } from 'antd';
import { connect } from 'dva';


const fieldLabels = {
  roles: '角色',
};

class UserRole extends PureComponent {
  onCreate = () => {
    const { form, dispatch } = this.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      dispatch({
        type: 'userMgr/saveRoles',
        payload: values,
      }).then((res) => {
        const { message: msg, status } = res;
        if (!status) {
          message.error(msg);
          return;
        }
        notification.success({
          message: '成功',
          description: '保存用户角色成功',
        });
        form.resetFields();
        dispatch({
          type: 'userMgr/changeVisible',
        });
      });
    });
  }

  onCancel = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'userMgr/changeVisible',
    });
  }

  handleChange = (targetKeys) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'userMgr/handleTransfer',
      payload: targetKeys,
    });
  }

  render() {
    const { form, userMgr: { modal }, currentOrg, loading } = this.props;
    const { data, visible } = modal;
    const { getFieldDecorator } = form;
    let ds;
    if (data && data.all) {
      ds = data.all.map((v) => {
        return {
          key: v.id,
          title: v.roleName,
        };
      });
    }
    return (
      <Modal
        visible={visible}
        title="新增/编辑角色权限"
        okText="保存"
        onCancel={this.onCancel}
        onOk={this.onCreate}
      >
        <Spin spinning={loading}>
          <Form layout="vertical">
            {getFieldDecorator('id', {
              initialValue: data ? data.id : '' })(
                <Input type="hidden" />
            )}
            {getFieldDecorator('orgId', {
              initialValue: currentOrg ? currentOrg.id : '' })(
                <Input type="hidden" />
            )}
            <Form.Item label={fieldLabels.roles}>
              {getFieldDecorator('roles', {
                initialValue: data ? data.roles : '',
                rules: [{ required: true, message: '请输入' }],
              })(
                <Transfer
                  dataSource={ds}
                  showSearch
                  listStyle={{
                    width: '40%',
                    height: 300,
                  }}
                  operations={['选择', '撤销']}
                  targetKeys={data && data.roles ? data.roles : []}
                  onChange={this.handleChange}
                  render={item => `${item.title}`}
                />
              )}
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    );
  }
}

export default connect(({ userMgr, global, loading }) => ({
  userMgr,
  currentOrg: global.currentOrg,
  loading: loading.global,
}))(Form.create()(UserRole));
