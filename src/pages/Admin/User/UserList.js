import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import isEqual from 'lodash/isEqual';
import { routerRedux } from 'dva/router';
import { Form, Button, Divider, Card } from 'antd';
import StandardTable from '@/components/DelTable';
import Popcon from '@/components/Popconfirm';
import styles from '@/utils/global.less';
import UserRole from './UserRole';
import Authorized from '@/utils/Authorized';

const { check } = Authorized;
const getValue = obj => Object.keys(obj).map(key => obj[key]).join(',');


@connect(({ userMgr, global, loading }) => ({
  userMgr,
  currentOrg: global.currentOrg,
  loading: loading.global,
}))
@Form.create()
class UserList extends PureComponent {
  constructor() {
    super();
    this.state = {
      pagination: {
        pageSize: 10,
        pageNum: 1,
      },
      currentOrg: {
        id: '',
      },
      selectedRows: [], // do 已选中的数据
      formValues: {}, // do 储存查询表单里的内容
    };
  }

  componentDidMount() { // do dom 加载完成时 向服务器请求数据
    const { dispatch, currentOrg } = this.props;
    if (!currentOrg.id) return;
    const { pagination: { pageNum, pageSize } } = this.state;
    dispatch({
      type: 'userMgr/fetch',
      payload: {
        pageNum,
        pageSize,
        orgId: currentOrg.id,
      },
    });
  }

  static getDerivedStateFromProps(nextProps, preState) {
    const { dispatch } = nextProps;
    if (!nextProps.currentOrg.id) return null;
    if (isEqual(nextProps.currentOrg.id, preState.currentOrg.id)) {
      return null;
    }
    const { pagination: { pageNum, pageSize } } = preState;
    dispatch({
      type: 'userMgr/fetch',
      payload: {
        pageNum,
        pageSize,
        orgId: nextProps.currentOrg.id,
      },
    });
    return {
      currentOrg: nextProps.currentOrg,
    };
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => { // do 页码改变的回调
    const { dispatch } = this.props;
    const { formValues } = this.state;
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});
    const { pageSize, current: pageNum } = pagination;
    this.setState({
      pagination: {
        pageNum,
        pageSize,
      },
    });
    const params = {
      pageNum, // 当前的页数
      pageSize, //  当前数据条数
      ...formValues,
      ...filters,
      orgId: this.state.currentOrg.id,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }
    dispatch({
      type: 'userMgr/fetch',
      payload: params,
    });
  };


  handleEdit = (record) => { // do 点击修改时得回调
    const { dispatch } = this.props;
    dispatch(routerRedux.push({
      pathname: '/admin/user/mgr',
      state: record,
    }));
  };

  handleDelete = (record) => { // do 当点击全部删除时 触发的回调
    const { dispatch } = this.props;
    const { pagination, currentOrg } = this.state;
    dispatch({
      type: 'userMgr/delete',
      payload: {
        data: record,
        pagination,
        orgId: currentOrg.id,
      },
    });
  };

  handleConfirm = (record) => { // 点击确认的回调
    let { selectedRows } = this.state;
    const { dispatch } = this.props;
    if (selectedRows.length) { // do 解决删除 选择不消失
      selectedRows = selectedRows.filter(item => item !== record.id);
      this.setState({ selectedRows });
    }
    const { pagination, currentOrg } = this.state;
    dispatch({
      type: 'userMgr/delete',
      payload: {
        data: record,
        pagination,
        orgId: currentOrg.id,
      },
    });
  };

  handleSelectRows = (rows) => { // do 选择行
    this.setState({
      selectedRows: rows,
    });
  };

  handleAdd = () => {
    this.props.dispatch(routerRedux.push('/admin/user/mgr'));
  };

  handleRole = (record) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'userMgr/queryRolesByUser',
      payload: record,
    }).then((res) => {
      dispatch({
        type: 'userMgr/changeVisible',
        data: {
          ...record,
          ...res.result,
        },
      });
    });
  };

  render() {
    const columns = [
      {
        title: '用户名',
        dataIndex: 'username',
      },
      {
        title: '姓名',
        dataIndex: 'name',
      },
      {
        title: '手机号',
        dataIndex: 'mobile',
      },
      {
        title: '邮箱',
        dataIndex: 'email',
      },
      {
        title: '操作',
        render: (text, record) => (
          <Fragment>
            {check('用户:修改', <a onClick={() => this.handleEdit(record)}>修改</a>)}
            <Divider type="vertical" />
            {check('用户:修改', <a onClick={() => this.handleRole(record)}>角色变更</a>)}
            <Divider type="vertical" />
            {check('用户:删除', (
              <Popcon
                title="是否确认删除"
                onConfirm={() => this.handleConfirm(record)} // 点击确认时
              />
            ))}
          </Fragment>
        ),
      },
    ];
    const { userMgr: { data }, loading } = this.props;
    const { selectedRows } = this.state;
    return (
      <Card>
        <div className={styles.tableList}>
          <div style={{ display: 'inline-block' }} className={styles.tableListOperator}>
            {check('用户:增加', (
              <Button icon="plus" type="primary" onClick={this.handleAdd}>
                新建
              </Button>
            ))}
          </div>
          <StandardTable
            showAllSel
            Rowselect
            selectedRows={selectedRows}
            loading={loading}
            data={data}
            SelKey={['id', 'username']}
            columns={columns}
            onSelectRow={this.handleSelectRows}
            onChange={this.handleStandardTableChange}
            onDel={this.handleDelete}
          />
          <UserRole />
        </div>
      </Card>
    );
  }
}
export default UserList;
