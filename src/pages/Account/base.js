import React, { PureComponent } from 'react';
import {
  List,
  Card,
  Button,
  Form,
  Avatar,
  Modal,
  message,
  Spin,
  Badge,
  Pagination,
  Input,
  Divider,
  DatePicker,
} from 'antd';
import isEqual from 'lodash/isEqual';
import moment from 'moment';
import { connect } from 'dva';
import stylesApplications from './BaseView.less';
import { parseTime } from '../../utils/utils';

const { TextArea } = Input;

const status = [
  {
    text: '未审批',
    value: 'warning',
  },
  {
    text: '通过',
    value: 'success',
  },
  {
    text: '未通过',
    value: 'error',
  },
];
const { RangePicker } = DatePicker;

const msgForm = {
  userId: 1,
  leaveReason: '请假缘由',
  leaveStartTime: '开始时间',
  leaveEndTime: '结束时间',
};

function range(start, end) {
  const result = [];
  for (let i = start; i < end; i += 1) {
    result.push(i);
  }
  // console.log(result.splice(4, 20));
  return result;
}

function disabledDate(current) {
  // Can not select days before today and today
  return current && current < moment().endOf('day');
}

@connect(({ global, loading, layoutloading }) => ({
  currentOrg: global.currentOrg,
  layoutloading,
  currentUser: layoutloading.currentUser,
  loading: loading.effects['layoutloading/_getLeaveList'],
}))
@Form.create()
class Center extends PureComponent {
  constructor() {
    super();
    this.state = {
      visible: false,
      FormObj: {},
      isEdit: false,
      pagination: {
        pageSize: 10,
        pageNum: 1,
      },
      currentOrg: {
        id: '',
      },
    };
  }

  componentDidMount() {
    const { dispatch, currentOrg: { id }, currentUser } = this.props;
    if (!id) return;
    const { pagination } = this.state;
    dispatch({
      type: 'layoutloading/_getLeaveList',
      payload: {
        pagination,
        orgId: id,
        userId: currentUser.userid,
      },
    });
  }

  static getDerivedStateFromProps(nextProps, preState) {
    const { dispatch } = nextProps;
    if (!nextProps.currentOrg.id) return null;
    if (isEqual(nextProps.currentOrg.id, preState.currentOrg.id)) {
      return null;
    }
    const { pagination } = preState;
    dispatch({
      type: 'layoutloading/_getLeaveList',
      payload: {
        pagination,
        orgId: nextProps.currentOrg.id,
        userId: nextProps.currentUser.userid,
      },
    });
    return {
      currentOrg: nextProps.currentOrg,
    };
  }

  onChange = (pageNum, pageSize) => {
    const { currentOrg } = this.state;
    const { dispatch, currentUser } = this.props;
    this.setState({
      pagination: {
        pageNum,
        pageSize,
      },
    });
    dispatch({
      type: 'layoutloading/_getLeaveList',
      payload: {
        pagination: {
          pageNum,
          pageSize,
        },
        orgId: currentOrg.id,
        userId: currentUser.userid,
      },
    });
  };

  handleAdd = () => {
    this.setState({
      visible: true,
    });
  };

  handleCancel = () => {
    this.props.form.resetFields();
    this.setState({
      visible: false,
      isEdit: false,
    });
  };

  fixTime = (values) => {
    const { leaveStartTime: timeArray, leaveReason } = values;
    const leaveStartTime = new Date(timeArray[0]._d).getTime();
    const leaveEndTime = new Date(timeArray[1]._d).getTime();
    if (leaveStartTime >= leaveEndTime) {
      return false;
    }
    return {
      leaveEndTime,
      leaveStartTime,
      leaveReason,
    };
  };

  handleOk = () => {
    const { form, dispatch, currentUser, currentOrg } = this.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      const data = this.fixTime(values);
      if (!data) {
        message.error('时间选择有误 请重新选择');
        return;
      }
      const { pagination } = this.state;
      const { userid } = currentUser;
      const { id } = currentOrg;
      data.userId = userid;
      data.orgId = id;
      values = data;
      dispatch({
        type: 'layoutloading/_getLeave',
        payload: {
          data: values,
          pagination,
          orgId: id,
          userId: userid,
        },
      });
    });
    form.resetFields();
    this.setState({
      visible: false,
    });
  };
  render() {
    const {
      form,
      layoutloading: { currentUser, leaveList: { list, pagination: page } },
      loading,
    } = this.props;
    const { getFieldDecorator } = form;
    const { isEdit, FormObj, visible } = this.state;
    let avatar = 'https://t1.picb.cc/uploads/2018/11/07/JpPQeR.jpg';
    if (currentUser) {
      avatar = currentUser.avatar || avatar;
    }
    return (
      <section>
        <Button style={{ marginBottom: '20px' }} icon="plus" type="primary" onClick={this.handleAdd}>
          请假
        </Button>
        <Spin spinning={loading}>
          <List
            rowKey="id"
            className={stylesApplications.filterCardList}
            grid={{ gutter: 24, xxl: 3, xl: 2, lg: 2, md: 2, sm: 2, xs: 1 }}
            dataSource={list}
            renderItem={item => (
              <List.Item key={item.id}>
                <Card
                  hoverable
                  bodyStyle={{ paddingBottom: 20 }}
                >
                  <Card.Meta avatar={<Avatar size="small" src={avatar} />} title={item.name} />
                  <div style={{ marginTop: '20px' }}>
                    <div>
                      <p style={{ marginBottom: '0' }}>
                        请假原因:
                      </p>
                      <p style={{ textIndent: '1em', marginBottom: '0' }}>
                        {item.leaveReason}
                      </p>
                    </div>
                    <Divider dashed />
                    <div>
                      <p style={{ marginBottom: '0' }}>
                        开始时间:
                      </p>
                      <p style={{ textIndent: '1em' }}>
                        {parseTime(item.leaveStartTime)}
                      </p>
                    </div>
                    <div>
                      <p style={{ marginBottom: '0' }}>
                        结束时间:
                      </p>
                      <p style={{ textIndent: '1em' }}>
                        {parseTime(item.leaveEndTime)}
                      </p>
                    </div>
                    <p>
                      状态：
                      <Badge
                        status={status[item.leaveStatus].value}
                        text={status[item.leaveStatus].text}
                      />
                    </p>
                  </div>
                </Card>
              </List.Item>
            )}
          />
          { page.total >= 10 ? (
            <Pagination
              style={{ marginTop: 30 }}
              onChange={this.onChange}
              defaultCurrent={1}
              {...page}
            />
          ) : null }
        </Spin>
        <Modal
          title="编辑/修改信息"
          visible={visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Form layout="vertical">
            <Form.Item label={msgForm.leaveReason}>
              {getFieldDecorator('leaveReason', {
                initialValue: isEdit ? FormObj.leaveReason : '',
                rules: [{ required: true, message: '请输入' }],
              })(
                <TextArea rows={4} />
              )}
            </Form.Item>

            <Form.Item label={msgForm.leaveStartTime}>
              {getFieldDecorator('leaveStartTime', {
                initialValue: isEdit ? FormObj.leaveStartTime : '',
                rules: [{ required: true, message: '请输入' }],
              })(
                <RangePicker
                  disabledDate={disabledDate}
                  showTime={{
                    hideDisabledOptions: true,
                    defaultValue: [moment('00:00:00', 'HH:mm:ss'), moment('11:59:59', 'HH:mm:ss')],
                  }}
                  format="YYYY-MM-DD HH:mm:ss"
                />
              )}
            </Form.Item>

          </Form>
        </Modal>
      </section>
    );
  }
}

export default Center;
