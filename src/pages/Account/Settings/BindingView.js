import React, { Component, Fragment } from 'react';
import { connect } from 'dva'
import { formatMessage, FormattedMessage } from 'umi/locale';
import {
  Icon,
  List,
  Modal,
  Row,
  Col,
  Radio,
  Spin,
  message,
  Button,
} from 'antd';
import ClassNames from 'classnames';
import { parseTime } from '@/utils/utils';
import styles from '../../User/StepForm/style.less';
import Result from '@/components/Result';

const data = {
  time:1546400022,
};
const checkPay = {
  '1': '微信支付(ZLF)',
  '2': '支付宝支付（SD）'
};
let time = undefined;

@connect(({ login, layoutloading, user, loading }) => ({
  endTime: login.endTime,
  currentUser: layoutloading.currentUser,
  Price: user.Price,
  getEwm: loading.effects['user/getEwm'],
  Paying: loading.effects['user/getPayRes']
}))
class BindingView extends Component {
  constructor(props) {
    super(props);
    let visible = false;
    const { endTime } = props;
    if (endTime) {
      endTime < 72 && (visible = true)
    }
    this.state = {
      visible,
      currentPrice: 0,
      avgPrice: 0,
      id: undefined, // 价格id
      value: undefined, // 支付方式
      checked: false, // 是否选择支付
      down: true,
      paySrc: '',
      orderStr: '',
      resultVisible: false,
    }
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'user/getPrice'
    })
  }

  getData = () => [
    {
      title: formatMessage({ id: 'app.settings.binding.taobao' }, {}),
      description: formatMessage({ id: 'app.settings.binding.taobao-description' }, {}),
      actions: [
        <a>
          <FormattedMessage id="app.settings.binding.bind" defaultMessage="Bind" />
        </a>,
      ],
      avatar: <Icon type="taobao" className="taobao" />,
    },
    {
      title: formatMessage({ id: 'app.settings.binding.alipay' }, {}),
      description: formatMessage({ id: 'app.settings.binding.alipay-description' }, {}),
      actions: [
        <a>
          <FormattedMessage id="app.settings.binding.bind" defaultMessage="Bind" />
        </a>,
      ],
      avatar: <Icon type="alipay" className="alipay" />,
    },
    {
      title: formatMessage({ id: 'app.settings.binding.dingding' }, {}),
      description: formatMessage({ id: 'app.settings.binding.dingding-description' }, {}),
      actions: [
        <a>
          <FormattedMessage id="app.settings.binding.bind" defaultMessage="Bind" />
        </a>,
      ],
      avatar: <Icon type="dingding" className="dingding" />,
    },
  ];

  handleOk = () => {
    if (time) {
      clearInterval(time);
    }
    this.setState({
      visible: false,
    })
  };

  handleCancel = () => {
    if (time) {
      clearInterval(time);
    }
    this.setState({
      visible: false,
    })
  };

  handleResultOk = () => {
    if (time) {
      clearInterval(time);
    }
    this.setState({
      resultVisible: false,
    })
  };

  handleResultCancel = () => {
    if (time) {
      clearInterval(time);
    }
    this.setState({
      resultVisible: false,
    })
  };

  radioChange = (e) => {
    const now = e.target.value;
    const { Price } = this.props;
    if (!Price.length) return;
    const filterPrice = Price.filter(item => item.monthNum === now)[0];
    const { avgPrice, totalPrice, id } = filterPrice;
    this.setState({
      avgPrice,
      currentPrice: totalPrice,
      id,
    })
  };

  handleChange = (e) => {
    const inputs = document.getElementsByClassName(ClassNames(styles.inp));
    const value = e.target.value;
    for (let i = 0; i< inputs.length; i += 1) {
      const attr = inputs[i];
      attr.parentNode.className = ClassNames(styles.lab);
      if (attr.value === value) {
        attr.parentNode.className = ClassNames(styles.lab, styles.checked);
      }
    }
    this.setState({
      checked: true,
      value,
    })
  };

  handlePay = () => {
    const { checked, id, value } = this.state;
    const { dispatch, currentUser } = this.props;

    if (!checked) {
      message.warn('请选择支付方式');
      return;
    }
    if (!id) {
      message.error('订单获取错误 请重新查询');
      return;
    }
    const { name: username, userid: userId } = currentUser;
    dispatch({
      type: 'user/getEwmXf',
      payload:{
        priceId: id,
        username,
        flag: value,
      }
    })
      .then(res => {
        if (!res || !Object.keys(res).length) {
          this.setState({
            resultVisible: false,
          });
          return;
        }
        const { out_trade_no, resultImage } = res;
        this.setState({
          paySrc: resultImage,
          orderStr: out_trade_no,
        });
       time = setInterval(() => {
          dispatch({
            type: 'user/getPayRes',
            payload: {
              out_trade_no,
              userId,
            },
          })
            .then((Res)=> {
              if (!Res && Res !== 0) return;
              // 0：失败，1：成功，2：支付中
              switch (Res) {
                case 1:
                  clearInterval(time);
                  this.setState({
                    down: {
                      status: 'success',
                      tset: '充值/续费 成功！',
                      introduce: '将在5秒后 重新获取对应权限！'
                    },
                  });
                  setTimeout(()=>{
                    window.location.reload();
                  },5000);
                  break;
                case 2:

                  break;
                case 0:
                  clearInterval(time);
                  this.setState({
                    down: {
                      status: 'error',
                      tset: '支付失败！',
                      introduce: '请重新支付！'
                    },
                    resultVisible: true,
                  });
                  break;
                default:
                  break;
              }
            })
        },1800)
      });
    this.setState({
      resultVisible: true,
    });
  };

  handleDone = () => {
    window.location.reload();
    // const { dispatch } = this.props;
    // dispatch(routerRedux.push({
    //   pathname: '/user/login',
    // }));
  };

  render() {
    const { currentPrice, avgPrice, visible, down, paySrc, value, orderStr, resultVisible } = this.state;
    const { currentUser, Price, getEwm, Paying } = this.props;
    const { handleChange, handlePay, handleOk, handleCancel, handleDone, handleResultOk, handleResultCancel} = this;

    const priceCheck = (
      <div style={{ marginTop: 25 }} className={styles.information}>
        <Row>
          <Col xs={24} sm={8} className={styles.label}>
            购买时长：
          </Col>
          <Col xs={24} sm={16}>
            <Radio.Group onChange={this.radioChange} buttonStyle="solid">
              {Price && Price.length ? Price.map((item) => {
                if (item.monthNum >= 6) {
                  return <Radio.Button style={{ marginBottom: 15 }} key={item.monthNum} value={item.monthNum}>{item.monthNum}月</Radio.Button>
                }
                return  <Radio.Button style={{ marginBottom: 15 }} key={item.monthNum} value={item.monthNum}>{item.monthNum}月</Radio.Button>
              }) : <Spin />}
            </Radio.Group>
          </Col>
        </Row>
        <Row>
          <Col xs={24} sm={8} className={styles.label}>
            优惠价：
          </Col>
          <Col style={{ color: '#ff7a22' }} xs={24} sm={16}>
            <span style={{ fontSize: '25px' }}>
              {currentPrice}
            </span> 元
            <span style={{ fontSize: 12, marginLeft: 10, color: '#a2a2a2', textDecoration: 'line-through' }}>每月：{avgPrice} 元</span>
          </Col>
        </Row>
      </div>
    );

    const pay = (
      <section className={styles.payList}>
        <div className={styles.payMenu}>
          <div className={styles.payEwm}>
            <img src="https://www.vipkm.com/static/app/images/pay1.png" alt="" />
            扫码支付
          </div>
        </div>
        <div className={styles.payRadio}>
          <label className={styles.lab}>
            <input className={styles.inp} onChange={handleChange} name="pid" value="1" type="radio" />
            <img src="https://www.vipkm.com/static/app/images/icon_wx.jpg" alt="" />
          </label>
          <label className={styles.lab}>
            <input className={styles.inp} onChange={handleChange} name="pid" value="2" type="radio" />
            <img src="https://www.vipkm.com/static/app/images/icon_zfb.jpg" alt="" />
          </label>
        </div>
        <div className={styles.submit}>
          <button onClick={handlePay} className={styles.check_pay}>确认支付</button>
        </div>
      </section>
    );

    const payModal = (
      <div>
        <Modal
          visible={resultVisible}
          onOk={handleResultOk}
          onCancel={handleResultCancel}
          maskClosable={false}
          confirmLoading={!!Paying}
        >
          <Spin spinning={!!getEwm}>
            {down.status ? (
              <Result
                type={down.status}
                title={down.text}
                description={down.introduce}
                actions={
                  <Button type="primary" onClick={handleDone}>
                    知道了
                  </Button>
                }
                className={styles.formResult}
              />
            ) : (
              <div>
                <div className={styles.order}>
                  <img src="https://www.vipkm.com/static/app/images/order_pay.png" alt="" />
                  <p>
                    支付金额：
                    <span>{currentPrice}元 </span>
                    <br />
                    支付方式：
                    <span>{checkPay[value]}</span>
                    <br />
                    支付订单号：
                    <span>
                      {orderStr}
                    </span>
                  </p>
                </div>
                <img style={{ display: 'block', margin: 'auto' }} src={paySrc} alt="" />
              </div>

            )}
          </Spin>
        </Modal>
      </div>
    );

    return (
      <Fragment>
        <List
          itemLayout="horizontal"
          dataSource={[data]}
          renderItem={item => (
            <div>
              <List.Item actions={[<a onClick={() => this.setState({ visible: true })}>充值</a>]}>
                <List.Item.Meta
                  avatar={<img style={{ width: 48, height: 45 }} src="https://s1.ax2x.com/2019/01/05/5d9HjG.png" alt="" />}
                  title="可用时间"
                  description={currentUser && currentUser.endTime ? parseTime(currentUser.endTime) : 'loading'}
                />
              </List.Item>
              <List.Item>
                <List.Item.Meta
                  avatar={<img src="http://guanxukeji-files.oss-cn-hangzhou.aliyuncs.com/image/10109_1_1482374324889.png" alt="" />}
                  title="版本信息"
                  description="当前版本 V 1.6"
                />
              </List.Item>
            </div>
          )}
        />
        <Modal
          title="充值续费"
          visible={visible}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <Row>
            <Col xs={24} sm={8} style={{ textAlign: 'right' }} className={styles.label}>
              充值账户：
            </Col>
            <Col xs={24} sm={16}>
              {currentUser && currentUser.name? currentUser.name : 'loading...' }
            </Col>
          </Row>
          <div>
            {priceCheck}
          </div>
          <div>
            {pay}
          </div>
          {payModal}
        </Modal>
      </Fragment>
    );
  }
}

export default BindingView;
