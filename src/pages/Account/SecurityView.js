import React, { Component, Fragment } from 'react';
import {
  List,
  Form,
  Avatar,
  Button,
  Modal,
  Input,
  Upload,
  Select,
  Icon,
  Spin,
  message,
  InputNumber,
  DatePicker,
} from 'antd';
import { connect } from 'dva';
import moment from 'moment';
import { parseTime, url } from '../../utils/utils';

const { TextArea } = Input;
const { Option } = Select;
const list = {
  mobile: '手机',
  email: '邮箱',
  name: '姓名',
  age: '年龄',
  sex: '性别',
  address: '地址',
  birthday: '生日',
  picture: '头像',
};

@connect(({ global, loading, layoutloading }) => ({
  currentOrg: global.currentOrg,
  layoutloading,
  currentUser: layoutloading.currentUser,
  loading: loading.effects['layoutloading/_getOneselfInfo'],
}))
@Form.create()
class SecurityView extends Component {
  constructor() {
    super();
    this.state = {
      visible: false,
      msgForm: {},
      fileList: [],
      currentOrg: {
        id: '',
      },
    };
  }

  getDataList = (name, item) => (
    <div>
      <span style={{ color: '#595959' }}>{list[name]}：</span>
      <span style={{ color: '#8c8c8c' }}>{item}</span>
    </div>
    );

  handleAdd = () => {
    const { layoutloading: { userMsg } } = this.props;
    const fileList = [{
      name: '头像',
      uid: 1,
      url: userMsg.picture[0],
    }];
    this.setState({
      visible: true,
    });
    setTimeout(() => {
      this.setState({
        msgForm: userMsg,
        fileList,
      });
    }, 120);
  };

  handleOk = () => {
    const { form, dispatch } = this.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      console.log(values);
      values.picture = [this.state.fileList[0].url];
      values.birthday = new Date(values.birthday._i).getTime();

      dispatch({
        type: 'layoutloading/_setUserMsg',
        payload: values,
      });
      form.resetFields();
      this.setState({
        visible: false,
        fileList: [],
        msgForm: {},
      });
    });
  };

  handleCancel = () => {
    this.props.form.resetFields();
    this.setState({
      visible: false,
      fileList: [],
      msgForm: {},
    });
  };

  handleChange = ({ fileList, file }) => {
    let fileList1 = fileList;
    fileList1 = fileList1.map((item) => {
      if (item.response) {
        item.url = item.response.result;
      }
      return item;
    });
    fileList1 = fileList1.filter((item) => {
      if (item.response) {
        return item.response.status === true;
      }
      return true;
    });
    file.status === 'done' ? (message.success('上传成功', 3)) : '';
    this.setState({ fileList: fileList1 });
  };

  handleRemove = (file) => {
    const { uid } = file;
    const fileList = this.state.fileList.filter(item => item.uid !== uid);
    this.setState({
      fileList,
    });
    return true;
  };

  beforeUpload = (file) => {
    const Rxp = /.+\.(jpg|jpeg|png|bmp)$/;
    const { name, size } = file;
    if (!Rxp.test(name)) {
      message.error('请上传正确的图片文件 仅支持 jpg | png | jpeg | bmp ', 3);
      this.setState({
        fileList: [],
      });
      return false;
    } if ((size / 1024) > 6000) {
      message.error('图片文件大于 5M', 3);
      return false;
    }
    return true;
  };

  render() {
    const {
      form,
      layoutloading: { userMsg },
    } = this.props;
    const { getFieldDecorator } = form;
    const { getDataList } = this;
    const { visible, msgForm } = this.state;
    return (
      <Fragment>
        <div>
          <Button style={{ marginBottom: '20px' }} icon="reload" type="primary" onClick={this.handleAdd}>
            修改信息
          </Button>
        </div>
        <List
          itemLayout="horizontal"
          dataSource={[userMsg]}
          renderItem={item => (
            Object.keys(userMsg).length ? (
              <div>
                <List.Item>
                  <List.Item.Meta avatar={<Avatar size="small" src={item.picture[0]} />} />
                </List.Item>
                <List.Item>
                  {getDataList('name', item.name)}
                </List.Item>
                <List.Item>
                  {getDataList('age', item.age)}
                </List.Item>
                <List.Item>
                  {getDataList('sex', (item.age ? '男' : '女'))}
                </List.Item>
                <List.Item>
                  {getDataList('mobile', item.mobile)}
                </List.Item>
                <List.Item>
                  {getDataList('email', item.email)}
                </List.Item>
                <List.Item>
                  {getDataList('birthday', item.birthday ? parseTime(item.birthday) : '')}
                </List.Item>
                <List.Item>
                  {getDataList('address', item.address)}
                </List.Item>
              </div>
            ) : (
              <div>loading...</div>
            )
          )}
        />
        <Modal
          title="编辑/修改信息"
          visible={visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Spin spinning={!Object.keys(msgForm).length}>
            <Form layout="vertical">
              <Form.Item label="头像">
                {getFieldDecorator('picture', {
              initialValue: msgForm.picture ? this.state.fileList : '',
              rules: [{ required: true, message: '请选择' }],
              })(
                <Upload
                  action={`${url}/mechanism/saveImage`}
                  listType="picture"
                  name="file1"
                  onChange={this.handleChange}
                  onRemove={this.handleRemove}
                  fileList={this.state.fileList}
                  beforeUpload={this.beforeUpload}
                >
                  {this.state.fileList.length >= 1 ? null : (
                    <Button>
                      <Icon type="upload" />上传相片
                    </Button>
              )}
                </Upload>
              )}
              </Form.Item>

              <Form.Item label="姓名">
                {getFieldDecorator('name', {
                  initialValue: msgForm.name || '',
                  rules: [{ required: true, message: '请输入姓名' }],
                })(
                  <Input disabled placeholder="姓名" />
                )}
              </Form.Item>

              {getFieldDecorator('id', {
                  initialValue: msgForm.id || '',
                })(
                  <Input hidden />
                )}

              <Form.Item label="手机">
                {getFieldDecorator('mobile', {
                  initialValue: msgForm.mobile || '',
                  rules: [{ required: true, message: '请输入手机' }],
                })(
                  <Input disabled placeholder="手机" />
                )}
              </Form.Item>

              <Form.Item label="邮箱">
                {getFieldDecorator('email', {
                  initialValue: msgForm.email || '',
                  rules: [{ pattern: /^(\w-*\.*)+@(\w-?)+(\.\w{2,})+$/, required: false, message: '请输入邮箱' }],
                })(
                  <Input placeholder="邮箱" />
                )}
              </Form.Item>

              <Form.Item label="年龄">
                {getFieldDecorator('age', {
                  initialValue: msgForm.age || '',
                  rules: [{ required: true, message: '请输入年龄' }],
                })(
                  <InputNumber placeholder="年龄" />
                )}
              </Form.Item>

              <Form.Item label="性别">
                {getFieldDecorator('sex', {
                  initialValue: msgForm.sex || '',
                  rules: [{ required: true, message: '请选择性别' }],
                })(
                  <Select>
                    <Option value>男</Option>
                    <Option value={false}>女</Option>
                  </Select>
                )}
              </Form.Item>

              <Form.Item label="生日">
                {getFieldDecorator('birthday', {
                  initialValue: msgForm.birthday ? moment(msgForm.birthday) : moment('2018'),
                  rules: [{ required: true, message: '请选择日期' }],
                })(
                  <DatePicker
                    style={{ width: '100%' }}
                  />
                )}
              </Form.Item>

              <Form.Item label="住址">
                {getFieldDecorator('address', {
                  initialValue: msgForm.address || '',
                  rules: [{ required: false, message: '请选择住址' }],
                })(
                  <TextArea placeholder="地址" autosize={{ minRows: 2, maxRows: 6 }} />
                )}
              </Form.Item>
            </Form>
          </Spin>
        </Modal>
      </Fragment>
    );
  }
}

export default SecurityView;
