import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Card, Row, Col } from 'antd';
import GridContent from '../../components/PageHeaderWrapper/GridContent';
import styles from './Info.less';
import WxCode from '@/components/WxCode';

@connect(({ loading, layoutloading }) => ({
  layoutloading,
  currentUser: layoutloading.currentUser,
  loading: loading.effects['layoutloading/getUserMsg'],
}))
class Center extends PureComponent {

  componentDidMount() {
    const { dispatch, currentUser: { userid } } = this.props;
    if (!userid) return;
    dispatch({
      type: 'layoutloading/getUserMsg',
      payload: userid,
    });
  }

  componentWillUpdate(nextProps) {
    const { dispatch, currentUser: { userid: preId } } = this.props;
    const { currentUser: { userid } } = nextProps;
    if (preId === userid) return;
    dispatch({
      type: 'layoutloading/getUserMsg',
      payload: userid || '',
    });
  }

  onTabChange = (key) => {
    const { match, dispatch } = this.props;
    switch (key) {
      case 'info':
        dispatch(routerRedux.push({
          pathname: `${match.url}/info`,
        }));
        break;
      case 'setting':
        dispatch(routerRedux.push({
          pathname: `${match.url}/setting`,
        }));
        break;
      default:
        break;
    }
  };

  render() {
    const {
      listLoading,
      // currentUser: { userid: id, name },
      layoutloading: { userMsg },
      currentUserLoading,
      match,
      location,
      children,
    } = this.props;
    const operationTabList = [
      {
        key: 'info',
        tab: (
          <span>
            请假 <span style={{ fontSize: 14 }} />
          </span>
        ),
      },
      {
        key: 'setting',
        tab: (
          <span>
            资料 <span style={{ fontSize: 14 }} />
          </span>
        ),
      },
    ];
    // console.log(location.pathname.replace(`${match.path}/`, ''))
    return (
      <GridContent className={styles.userCenter}>
        <Row gutter={24}>
          <Col lg={7} md={24}>
            <Card bordered={false} style={{ marginBottom: 24 }} loading={currentUserLoading}>
              {Object.keys(userMsg).length ? (
                <div>
                  <div className={styles.avatarHolder}>
                    <img alt="" src={userMsg.picture[0]} />
                    <div className={styles.name}>
                      <WxCode data={userMsg} userType={5} />
                    </div>
                    {/* <div>{userMsg.signature}</div> */}
                  </div>
                  <div className={styles.detail}>
                    <p>
                      <i className={styles.title} />
                      {userMsg.mobile}
                    </p>
                    <p>
                      <i className={styles.group} />
                      {userMsg.email}
                    </p>
                    <p>
                      <i className={styles.address} />
                      {userMsg.address}
                    </p>
                  </div>
                </div>
              ) : (
                'loading...'
              )}
            </Card>
          </Col>
          <Col lg={17} md={24}>
            <Card
              className={styles.tabsCard}
              bordered={false}
              tabList={operationTabList}
              activeTabKey={location.pathname.replace(`${match.path}/`, '')}
              onTabChange={this.onTabChange}
              loading={listLoading}
            >
              {children}
            </Card>
          </Col>
        </Row>
      </GridContent>
    );
  }
}

export default Center;
