import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Input,
  Form,
  Spin,
  List,
  Divider,
  Button,
  message,
  Upload,
  Icon,
  Tooltip,
} from 'antd';
import Authorized from '@/utils/Authorized';
import isEqual from 'lodash/isEqual';
import Ellipsis from '@/components/Ellipsis';
import { url } from '../../../../utils/utils';

const { check } = Authorized;

@connect(({ global, loading, Business })=> ({
  loading: loading.global,
  currentOrg: global.currentOrg,
  Business
}))
@Form.create()
class WeChatPublicSet extends PureComponent{

  constructor(props) {
    super(props);
    this.state = {
      // currentOrg: {
      //   id: '',
      // },
      appid: '',
      oldappid: '',
      appsecret: '',
      oldappsecret: '',
      showAppid: false,
      showAppsecret: false,
      showBtn: false,
      fileList: [],
    };
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'Business/getWeChatPublic',
    }).then(res => {
      if (res && Object.keys(res).length) {
        this.setState({
          ...res.weChatPublicAccountConfig
        })
      }
    })
  }

  // componentWillReceiveProps(nextProps, nextContext) {
  //   const { dispatch } = nextProps;
  //   if (!nextProps.currentOrg.id) return null;
  //   if (isEqual(nextProps.currentOrg.id, this.state.currentOrg.id)) {
  //     return null;
  //   }
  //   dispatch({
  //     type: 'Business/getWeChatPublic',
  //   }).then(res => {
  //     if (res && Object.keys(res).length) {
  //       this.setState({
  //         ...res.weChatPublicAccountConfig
  //       })
  //     }
  //   });
  //   this.setState({
  //     currentOrg: nextProps.currentOrg,
  //   })
  // }

  handleChangeValue = (value, state) => {
    this.setState({
      [state]: value,
    })
  };

  handleEdit = (flag, state, value) => {
    this.setState({
      [flag]: true,
      [state]: value,
      [`old${state}`]: value,
    })
  };

  handleConfirm = (flag, state) => {
    const { [state]:newStr, [`old${state}`]:oldStr } = this.state;
    if (newStr && newStr !== oldStr) {
      this.setState({
        showBtn: true,
      })
    }
    this.setState({
      [flag]: false,
    })
  };

  handleCancel = (flag, state) => {
    const { [`old${state}`]: value } = this.state;
    this.setState({
      [state]: value,
      [flag]: false,
    })
  };

  handleUpdate = () => {
    const { appid, appsecret, showAppid, showAppsecret } = this.state;
    const { dispatch } = this.props;
    if (showAppsecret || showAppid ) {
      message.error('请完成所有编辑 方可更新', 1.5);
      return;
    }
    dispatch({
      type: 'Business/upDateWeChatPublic',
      payload: {
        appsecret,
        appid,
      }
    }).then((res)=>{
      if (res) {
        this.setState({
          showBtn: false,
        })
      }
    })
  };

  beforeUpload = (file) => {
    const Rxp = /.+\.(txt|TXT)$/;
    const { name, size } = file;
    if (!Rxp.test(name)) {
      message.error('请上传正确的文件', 3);
      this.setState({
        fileList: [],
      });
      return false;
    } if ((size / 1024) > 3000) {
      message.error('文件大于 4M', 3);
      return false;
    }
    return true;
  };

  handleUpload = ({file, fileList}) => {
    let state = false;
    const { name, size, status } = file;
    const Rxp = /.+\.(txt|TXT)$/;
    if (!Rxp.test(name) || (size / 1024) > 3000) {
      return;
    }
    let fileList1 = fileList;
    fileList1 = fileList1.map((item) => {
      if (item.response) {
        item.url = item.response.result;
      }
      return item;
    });
    fileList1 = fileList1.filter((item) => {
      const { response } = item;
      if (response) {
        const { status: states, message: msg } = response;
        // if (this.num === 0) {
          !states ? message.error(msg) : '';
        //   this.num++;
        // } else {
        //   status === 'done' ? message.success('上传成功') : '';
        // }
        state = states;
        return states === true;
      }
      return true;
    });
    (status === 'done' && state) ? message.success('上传成功') : '';
    this.setState({ fileList: fileList1.splice(-1, 1) });
  };

  render() {
    const { Business: { WeChatPublic }, loading } = this.props;
    const {
      showAppid,
      appid,
      showBtn,
      showAppsecret,
      appsecret,
      fileList,
    } = this.state;
    const props = {
      action: `${url}/orgSystemSettings/weChatCertificateLoad`,
      onChange: this.handleUpload,
      fileList,
      beforeUpload: this.beforeUpload,
      accept: '.txt',
    };
    return (
      <section>
        <Spin spinning={!!loading}>
          <List
            itemLayout="horizontal"
            dataSource={[WeChatPublic]}
            renderItem={() => (
              Object.keys(WeChatPublic).length ? (
                <div>
                  <List.Item>
                    <div style={{ display: 'flex', justifyContent: 'space-between',  width: '100%' }}>
                      <div style={{ display: 'flex' }}>
                        <span style={{ color: '#595959', fontWeight: 'bold' }}>appId：</span>
                        <span style={{ display: !showAppid ? 'inline' : 'none', color: '#8c8c8c' }}>{appid}</span>
                        <Input
                          value={appid}
                          onChange={e => this.handleChangeValue(e.target.value, 'appid')}
                          style={{ display: showAppid ? 'initial' : 'none' }}
                          size="small"
                        />
                      </div>
                      {
                        check('机构系统设置:修改', (
                          <div>
                            <a
                              style={{ display: !showAppid ? 'initial' : 'none' }}
                              onClick={() => this.handleEdit('showAppid', 'appid', appid)}
                            >
                              编辑
                            </a>
                            <a
                              style={{ display: showAppid ? 'initial' : 'none' }}
                              onClick={() =>this.handleConfirm('showAppid', 'appid')}
                            >
                              确认
                            </a>
                            <Divider type="vertical" style={{ display: showAppid ? 'initial' : 'none' }} />
                            <a
                              style={{ display: showAppid ? 'initial' : 'none' }}
                              onClick={() => {this.handleCancel('showAppid', 'appid')}}
                            >
                              取消
                            </a>
                          </div>
                        ))
                      }
                    </div>
                  </List.Item>
                  <List.Item>
                    <div style={{ display: 'flex', justifyContent: 'space-between',  width: '100%' }}>
                      <div style={{ display: 'flex' }}>
                        <span style={{ color: '#595959', fontWeight: 'bold' }}>appsecret：</span>
                        <span style={{ display: !showAppsecret ? 'inline' : 'none', color: '#8c8c8c' }}>{appsecret}</span>
                        <Input
                          value={appsecret}
                          onChange={e => this.handleChangeValue(e.target.value, 'appsecret')}
                          style={{ display: showAppsecret ? 'initial' : 'none' }}
                          size="small"
                        />
                      </div>
                      {
                        check('机构系统设置:修改', (
                          <div>
                            <a
                              style={{ display: !showAppsecret ? 'initial' : 'none' }}
                              onClick={() => this.handleEdit('showAppsecret', 'appsecret', appsecret)}
                            >
                              编辑
                            </a>
                            <a
                              style={{ display: showAppsecret ? 'initial' : 'none' }}
                              onClick={() =>this.handleConfirm('showAppsecret', 'appsecret')}
                            >
                              确认
                            </a>
                            <Divider type="vertical" style={{ display: showAppsecret ? 'initial' : 'none' }} />
                            <a
                              style={{ display: showAppsecret ? 'initial' : 'none' }}
                              onClick={() => {this.handleCancel('showAppsecret', 'appsecret')}}
                            >
                              取消
                            </a>
                          </div>
                        ))
                      }
                    </div>
                  </List.Item>
                  <List.Item>
                    <div style={{ display: 'flex', justifyContent: 'space-between',  width: '100%' }}>
                      <div style={{ display: 'flex' }}>
                        <Tooltip title="上传的证书文件请确保内容真实准确！">
                          <span style={{ color: '#595959', fontWeight: 'bold' }}>证书 <Icon type="question-circle" />
                          ：
                          </span>
                        </Tooltip>
                        {/* <Ellipsis style={{ flex: 1 }} length={15} tooltip> */}
                        {/*  {fileList.length ? fileList[fileList.length - 1].name : '上传证书文件'} */}
                        {/* </Ellipsis> */}
                      </div>
                      <Upload {...props}>
                        <Button>
                          <Icon type="upload" /> 上传
                        </Button>
                      </Upload>
                    </div>
                  </List.Item>
                  <Button
                    style={{ marginTop: 25, display: showBtn ? 'initial' : 'none' }}
                    type="primary"
                    onClick={this.handleUpdate}
                  >
                    更新信息
                  </Button>
                </div>
              ) : (
                <div>loading...</div>
              )
            )}
          />
        </Spin>
      </section>
    )
  }
}

export default WeChatPublicSet;
