import React, { Fragment, PureComponent } from 'react';
import { connect } from 'dva';
import {
  Form,
  Button,
  Input,
  Modal,
  message,
  DatePicker,
  Radio,
  Spin,
} from 'antd';
import Popcon from '@/components/Popconfirm';
import Hoc from '@/utils/Hoc';
import Authorized from '@/utils/Authorized';
import StandardTable from '@/components/DelTable';
import { parseTime } from '../../../../utils/utils';
import Ellipsis from '@/components/Ellipsis';

const RadioGroup = Radio.Group;
const { TextArea } = Input;
const { check } = Authorized;
const { RangePicker } = DatePicker;
const TYPE = ['单次', '每周', '每月' ,'每年'];

@connect(({ global, Business, loading }) => ({
  currentOrg: global.currentOrg,
  loading: loading.effects['Business/getHolidayInfo'],
  fetchLoading: loading.effects['Business/setHolidayInfo'],
  Business,
}))
@Form.create()
@Hoc({ fetchUrl: 'Business/getHolidayInfo' })
class HolidaySet extends PureComponent{
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    }
  }

  handleEdit = () => {
    this.setState({
      visible: true,
    })
  };

  handleOk = () => {
    const { form: { validateFieldsAndScroll, resetFields }, dispatch, pagination, currentOrg } = this.props;
    validateFieldsAndScroll((error, value) => {
      if (error) return;
      const { startTime: time, holidayName, isNotice, noticeInfo, orgId, type } = value;
      if (isNotice === 1) {
        if (!noticeInfo || !noticeInfo.length) {
          message.error('请填写需要通知的内容哦');
          return;
        }
      }
      const startTime = time[0].toDate().getTime();
      const endTime = time[1].toDate().getTime();
      dispatch({
        type: 'Business/setHolidayInfo',
        payload: {
          value: {
            startTime,
            endTime,
            orgId,
            isNotice,
            noticeInfo,
            holidayName,
            type,
          },
          data: {
            pagination,
            orgId: currentOrg.id
          }
        }
      }).then(res=> {
        if (res) {
          resetFields();
          this.setState({
            visible: false,
          })
        }
      })
    })
  };

  handleCancel = () => {
    const { form: { resetFields } } = this.props;
    resetFields();
    this.setState({
      visible: false,
    })
  };

  handleDelete = (record) => {
    const { id } = record;
    const { dispatch, currentOrg, pagination } = this.props;
    dispatch({
      type: 'Business/delHolidayInfo',
      payload: {
        data: {
          pagination,
          orgId: currentOrg.id,
        },
        ids: [id]
      }
    })
  };

  render() {
    const {
      handleStandardTableChange,
      loading,
      Business: { HolidayInfo: data },
      form: { getFieldDecorator },
      currentOrg,
      fetchLoading,
    } = this.props;
    const columns = [
      {
        title: '名称',
        key: 'holidayName',
        dataIndex: 'holidayName',
        render(text) {
          return (
            <Ellipsis length={6} tooltip>
              {text}
            </Ellipsis>
          )
        },
      },
      {
        title: '时间',
        key: 'starttime',
        dataIndex: 'starttime',
        render(text, record) {
          const { starttime, endtime, type } = record;
          switch (type) {
            case 0:
              return `${parseTime(starttime, '{y}-{m}-{d} {h}:{i}')} - ${parseTime(endtime, '{y}-{m}-{d} {h}:{i}')}`
            case 1:
              return (
                `${TYPE[type]} ${parseTime(starttime, '{a}')}`
              );
            default:
              return (
                `${TYPE[type]} ${parseTime(starttime, '{m}-{d}')}`
              );
          }
        },
      },
      {
        title: '操作',
        fixed: 'right',
        width: 80,
        render: (text, record) => (
          check('机构系统设置:修改', (
            <Popcon
              title="是否确认删除"
              onConfirm={() => this.handleDelete(record)}
            />
          ))
        ),
      },
    ];
    const { visible } = this.state;
    return (
      <section>
        {check('机构系统设置:修改', (
          <Button icon="plus" type="primary" onClick={this.handleEdit}>
            新建
          </Button>
        ))}
        <StandardTable
          bordered
          loading={loading}
          data={data}
          scroll={{ x: 600 }}
          columns={columns}
          expandedRowRender={record => (
            <p style={{ color: 'rgba(0,0,0,0.43)' }}>
              通知内容：{record.noticeinfo}
            </p>
          )}
          onChange={handleStandardTableChange}
        />
        <Modal
          title="添加课时"
          visible={visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Spin spinning={!!fetchLoading}>
            <Form layout="vertical">
              {
                getFieldDecorator('orgId', {
                  initialValue: currentOrg && currentOrg.id ? currentOrg.id : '',
                })(
                  <Input type="hidden" />
                )
              }
              <Form.Item label="名称">
                {getFieldDecorator('holidayName', {
                  rules: [{ required: true, message: '请输入名称' }],
                })(
                  <Input />
                )}
              </Form.Item>
              <Form.Item label="时间">
                {getFieldDecorator('startTime', {
                  rules: [{ required: true, message: '请选择时间' }],
                })(
                  <RangePicker
                    showTime={{ format: 'HH:mm' }}
                    format="YYYY-MM-DD HH:mm"
                    placeholder={['开始时间', '结束时间']}
                  />
                )}
              </Form.Item>
              <Form.Item label="类型">
                {getFieldDecorator('type', {
                  rules: [{ required: true, message: '请选择状态' }],
                  initialValue: 0,
                })(
                  <RadioGroup>
                    <Radio value={0}>临时</Radio>
                    <Radio value={1}>每周</Radio>
                  </RadioGroup>
                )}
              </Form.Item>
              <Form.Item label="通知">
                {getFieldDecorator('isNotice', {
                  rules: [{ required: true, message: '请选择状态' }],
                  initialValue: 1,
                })(
                  <RadioGroup>
                    <Radio value={1}>通知</Radio>
                    <Radio value={0}>暂不通知</Radio>
                  </RadioGroup>
                )}
              </Form.Item>
              <Form.Item label="通知内容">
                {getFieldDecorator('noticeInfo', {
                  rules: [{ required: false, message: '请选择状态' }],
                })(
                  <TextArea placeholder="请输入内容" autosize={{ minRows: 2, maxRows: 6 }} />
                )}
              </Form.Item>
            </Form>
          </Spin>
        </Modal>
      </section>
    )
  }
}

export default HolidaySet;
