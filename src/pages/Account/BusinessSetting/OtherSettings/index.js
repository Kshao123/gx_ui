import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Input,
  Form,
  Spin,
  List,
  Divider,
  Button,
  message,
  Upload,
  Icon,
  Tooltip,
  InputNumber,
  Switch,
} from 'antd';
import throttle from 'lodash/throttle';
import Authorized from '@/utils/Authorized';
import { url } from '../../../../utils/utils';

const { check } = Authorized;

@connect(({ global, loading, Business })=> ({
  loading: loading.effects['Business/getOtherInfo'],
  currentOrg: global.currentOrg,
  Business
}))
@Form.create()
class OtherSettings extends PureComponent{

  constructor(props) {
    super(props);
    this.state = {
      currentOrg: {
        id: '',
      },
      CanLeaveNums: 0,
      CanSignDay: 0,
      showAppid: false,
      showAppsecret: false,
      showBtn: false,
    };
    this.setValue = throttle(this.setValue, 800);
  }

  componentDidMount() {
    const { dispatch, currentOrg } = this.props;
    if (!currentOrg.id) return;
    dispatch({
      type: 'Business/getOtherInfo',
      payload: {
        type: 'getCanSignTime',
        orgId: currentOrg.id,
      }
    })
      .then(res => {
        this.setState({
          CanSignDay: res || 0
        })
      });
    dispatch({
      type: 'Business/getOtherInfo',
      payload: {
        type: 'getCanLeaveNum',
        orgId: currentOrg.id,
      }
    })
      .then(res => {
        this.setState({
          CanLeaveNums: res || 0
        })
    })
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const { dispatch } = nextProps;
    if (!nextProps.currentOrg.id) return null;
    if ( nextProps.currentOrg.id === this.state.currentOrg.id) {
      return null;
    }
    dispatch({
      type: 'Business/getOtherInfo',
      payload: {
        type: 'getCanSignTime',
        orgId: nextProps.currentOrg.id,
      }
    })
      .then(res => {
        this.setState({
          CanSignDay: res || 0
        })
      });
    dispatch({
      type: 'Business/getOtherInfo',
      payload: {
        type: 'getCanLeaveNum',
        orgId: nextProps.currentOrg.id,
      }
    })
      .then(res => {
        this.setState({
          CanLeaveNums: res || 0
        })
      });
    this.setState({
      currentOrg: nextProps.currentOrg,
    })
  }

  handleChangeValue = (value, state, type, getValue) => {
    if (!Number(value) && value !== 0) return;
    this.setValue(value, getValue, type);
    this.setState({
      [state]: value,
    })
  };

  setValue = (value, getValue, type) => {
    const { dispatch, currentOrg, form } = this.props;
    form.resetFields();
    dispatch({
      type: 'Business/setOtherInfo',
      payload: {
        type,
        orgId: currentOrg.id,
        data: {
          [getValue]: value,
        }
      }
    })
  };

  handleSwitchChange = (value, state, type, getValue) => {
    const { [state]: name } = this.state;
    if (value && name === 0) {
      this.handleChangeValue(1, state, type, getValue);
    } else if (  !value && name > 0) {
      this.handleChangeValue(0, state, type, getValue);
    }
  };

  render() {
    const { loading } = this.props;
    const {
      CanLeaveNums,
      CanSignDay,
    } = this.state;

    return (
      <section>
        <Spin spinning={!!loading}>
          <List
            itemLayout="horizontal"
            dataSource={[CanLeaveNums]}
            renderItem={() => (
              <div>
                <List.Item>
                  <div style={{ display: 'flex', justifyContent: 'space-between',  width: '100%' }}>
                    <Tooltip title="上课时忘记打卡之后允许打卡的时间，单位：天">
                      <span style={{ color: '#595959', fontWeight: 'bold' }}>补打卡天数 <Icon type="question-circle" />
                        ：
                      </span>
                    </Tooltip>
                    <InputNumber
                      disabled={CanSignDay === 0}
                      min={0}
                      style={{ width: '60%' }}
                      value={CanSignDay}
                      size="small"
                      onChange={e => this.handleChangeValue(e, 'CanSignDay', 'getCanSignTime', 'allowClassSignDayNum')}
                    />
                    <Switch
                      checkedChildren="开"
                      unCheckedChildren="关"
                      checked={CanSignDay !== 0}
                      onChange={e => this.handleSwitchChange(e, 'CanSignDay', 'getCanSignTime', 'allowClassSignDayNum')}
                    />
                  </div>
                </List.Item>
                <List.Item>
                  <div style={{ display: 'flex', justifyContent: 'space-between',  width: '100%' }}>
                    <Tooltip title="课时数与允许请假的次数的比值（0：不开启请假功能） / %">
                      <span style={{ color: '#595959', fontWeight: 'bold' }}>请假次数比值 <Icon type="question-circle" />
                        ：
                      </span>
                    </Tooltip>
                    <InputNumber
                      disabled={CanLeaveNums === 0}
                      min={0}
                      style={{ width: '60%' }}
                      value={CanLeaveNums}
                      size="small"
                      onChange={e => this.handleChangeValue(e, 'CanLeaveNums', 'getCanLeaveNum', 'allowLeaveNum')}
                    />
                    <Switch
                      checkedChildren="开"
                      unCheckedChildren="关"
                      checked={CanLeaveNums !== 0}
                      onChange={e => this.handleSwitchChange(e, 'CanLeaveNums', 'getCanLeaveNum', 'allowLeaveNum')}
                    />
                  </div>
                </List.Item>
              </div>
            )}
          />
        </Spin>
      </section>
    )
  }
}

export default OtherSettings;
