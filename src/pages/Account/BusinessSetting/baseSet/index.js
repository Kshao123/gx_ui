import React, { Component, Fragment } from 'react';
import isEqual from 'lodash/isEqual';
import {
  Form,
  Input,
  Upload,
  Button,
  message,
  Icon,
  Spin
} from 'antd';
import { connect } from 'dva';
import styles from '../index.less';
import { url } from '../../../../utils/utils';

const { TextArea } = Input;
const FormItem = Form.Item;
const renderImgList = list => list.map((item, index) => ({
  name: `${item}${index}`,
  url: item,
  uid: `${index}`,
}));

// 头像组件 方便以后独立，增加裁剪之类的功能
const AvatarView = ({ avatar, handleChange, beforeUpload, fileList }) => (
  <Fragment>
    <div className={styles.avatar_title}>
      头像
    </div>
    <div className={styles.avatar}>
      <img src={avatar} alt="avatar" />
    </div>
    <Upload
      action={`${url}/mechanism/saveImage`}
      name="file1"
      onChange={handleChange}
      fileList={fileList}
      beforeUpload={beforeUpload}
    >
      <div className={styles.button_view}>
        <Button icon="upload">
          上传Logo
        </Button>
      </div>
    </Upload>
  </Fragment>
);

@connect(({ global, Business, loading }) => ({
  currentOrg: global.currentOrg,
  loading: loading.effects['Business/getBaseInfo'],
  Business,
}))
@Form.create()
class BaseView extends Component {

  state = {
    currentOrg: {
      id: '',
    },
    fileList: [
      {
        url: 'https://dwz.cn/6RneHNlf',
        uid: 'https://dwz.cn/ttRJaLccasas',
      }
    ],
    orgShow: [],
    orgDetails: '',
  };

  componentDidMount() {
    const { dispatch, currentOrg: { id } } = this.props;
    if (!id) return;
    dispatch({
      type: 'Business/getBaseInfo',
      payload: {
        orgId: id,
      },
    }).then(res => {
      if (res && Object.keys(res).length) {
        this.setState({
          orgShow: renderImgList(res.imgUrlList),
          fileList: renderImgList([res.logoUrl])
        })
      }
    })
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const { dispatch } = nextProps;
    if (!nextProps.currentOrg.id) return null;
    if (isEqual(nextProps.currentOrg.id, this.state.currentOrg.id)) {
      return null;
    }
    dispatch({
      type: 'Business/getBaseInfo',
      payload: {
        orgId: nextProps.currentOrg.id,
      },
    }).then(res => {
      if (res && Object.keys(res).length) {
        this.setState({
          orgShow: renderImgList(res.imgUrlList),
          fileList: renderImgList([res.logoUrl])
        })
      }
    });
    this.setState({
      currentOrg: nextProps.currentOrg,
    })
  }

  handleChange = ({ fileList, file }, stateName) => {
    const { name, size, status } = file;
    const Rxp = /.+\.(jpg|jpeg|png|bmp)$/;
    if (!Rxp.test(name) || (size / 1024) > 6000) {
      return;
    }
    let fileList1 = fileList;
    fileList1 = fileList1.map((item) => {
      if (item.response) {
        item.url = item.response.result;
      }
      return item;
    });
    fileList1 = fileList1.filter((item) => {
      if (item.response) {
        return item.response.status === true;
      }
      return true;
    });
    status === 'done' ? (message.success('上传成功', 3)) : '失败';
    this.setState({ [stateName]: fileList1 });
  };

  handleRemove = (file, name, restName) => {
    const { form: { resetFields } } = this.props;
    const { uid } = file;
    if (restName) resetFields([restName]);
    const fileList = this.state[name].filter(item => item.uid !== uid);
    this.setState({
      [name]: fileList,
    });
    return true;
  };

  beforeUpload = (file) => {
    const Rxp = /.+\.(jpg|jpeg|png|bmp)$/;
    const { name, size } = file;
    if (!Rxp.test(name)) {
      message.error('请上传正确的图片文件 仅支持 jpg | png | jpeg | bmp ', 3);
      this.setState({
        fileList: [],
      });
      return false;
    } if ((size / 1024) > 6000) {
      message.error('图片文件大于 5M', 3);
      return false;
    }
    return true;
  };

  getAvatarURL() {
    const { fileList } = this.state;
    let url = 'https://dwz.cn/6RneHNlf';
    if (fileList.length) {
      url = fileList[fileList.length - 1].url;
    }
    return url;
  }

  getViewDom = ref => {
    this.view = ref;
  };

  handleSubmit = () => {
    const { form, dispatch, currentOrg } = this.props;
    const { fileList, orgShow } = this.state;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      let data = {
        orgId: currentOrg.id,
        logoUrl: '',
        imgUrlList: [],
        orgDetails: values.orgDetails,
      };
      if (fileList.length) {
        data.logoUrl = fileList[fileList.length - 1].url;
      }
      if (orgShow.length) {
        data.imgUrlList = orgShow.map(item => item.url)
      }
      form.resetFields();
      dispatch({
        type: 'Business/setBaseInfo',
        payload: {
          ...data
        },
      });

    });
  };

  render() {
    const {
      form: { getFieldDecorator },
      currentOrg,
      Business: { BaseInfo },
      loading
    } = this.props;
    const { orgShow, fileList } = this.state;
    const { handleChange, handleRemove, beforeUpload, handleSubmit } = this;
    return (
      <Spin spinning={loading}>
        <div className={styles.baseView} ref={this.getViewDom}>
          <div className={styles.left}>
            <Form layout="vertical" hideRequiredMark>
              <FormItem label='机构'>
                {getFieldDecorator('orgName', {
                  initialValue: currentOrg ? currentOrg.orgName : '' ,
                  rules: [
                    {
                      required: false,
                      message: '',
                    },
                  ],
                })(<Input disabled />)}
              </FormItem>
              <FormItem label='简介'>
                {getFieldDecorator('orgDetails', {
                  initialValue: BaseInfo.orgDetails || '' ,
                  rules: [
                    {
                      required: false,
                      message: '请输入简介',
                    },
                  ],
                })(
                  <TextArea placeholder="机构简介" autosize={{ minRows: 2 }} />
                )}
              </FormItem>
              <FormItem label='机构展示'>
                {getFieldDecorator('orgShow', {
                  initialValue: renderImgList(BaseInfo.imgUrlList) ,
                  rules: [
                    {
                      required: false,
                      message: '请输入简介',
                    },
                  ],
                })(
                  <Upload
                    action={`${url}/mechanism/saveImage`}
                    listType="picture-card"
                    name="file1"
                    multiple
                    onChange={e => handleChange(e, 'orgShow')}
                    onRemove={e => handleRemove(e, 'orgShow')}
                    fileList={this.state.orgShow}
                    beforeUpload={beforeUpload}
                  >
                    {orgShow.length > 8 ? null : (
                      <Button>
                        <Icon type="upload" />上传相片
                      </Button>
                    )}
                  </Upload>
                )}
              </FormItem>
              <Button onClick={handleSubmit} type="primary">
                更新信息
              </Button>
            </Form>
          </div>
          <div className={styles.right}>
            <AvatarView
              fileList={fileList}
              avatar={this.getAvatarURL()}
              handleChange={e => handleChange(e, 'fileList')}
              beforeUpload={beforeUpload}
            />
          </div>
        </div>
      </Spin>
    );
  }
}

export default BaseView;
