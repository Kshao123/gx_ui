import React, { Component, Fragment } from 'react';
import isEqual from 'lodash/isEqual';
import {
  Form,
  Input,
  Upload,
  Button,
  message,
  InputNumber,
  Spin
} from 'antd';
import { connect } from 'dva';
import styles from '../index.less';
import { url } from '@/utils/utils';

const FormItem = Form.Item;

const AvatarView = ({ avatar, handleChange, beforeUpload, fileList }) => (
  <Fragment>
    <div className={styles.avatar_title}>
      二维码
    </div>
    <div className={styles.avatar}>
      <img src={avatar} alt="avatar" />
    </div>
    <Upload
      accept="image/ipg, image/png"
      action={`${url}/mechanism/saveImage`}
      name="file1"
      onChange={handleChange}
      fileList={fileList}
      beforeUpload={beforeUpload}
    >
      <div className={styles.button_view}>
        <Button icon="upload">
          上传二维码
        </Button>
      </div>
    </Upload>
  </Fragment>
);

@connect(({ global, Business, loading }) => ({
  currentOrg: global.currentOrg,
  loading: loading.effects['Business/getInformation'],
  Business,
}))
@Form.create()
class Information extends Component {

  state = {
    currentOrg: {
      id: '',
    },
    fileList: [
      {
          url: 'https://dwz.cn/Yexw0XZA',
        uid: 'https://dwz.cn/ttRJaLccasas',
      }
    ],
    orgShow: [],
    orgDetails: '',
  };

  componentDidMount() {
    const { dispatch, currentOrg: { id } } = this.props;
    if (!id) return;
    dispatch({
      type: 'Business/getInformation',
      payload: {
        orgId: id,
      },
    }).then(res => {
      // if (res && Object.keys(res).length) {
      //   this.setState({
      //     orgShow: renderImgList(res.imgUrlList),
      //     fileList: renderImgList([res.logoUrl])
      //   })
      // }
    })
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const { dispatch } = nextProps;
    if (!nextProps.currentOrg.id) return null;
    if (isEqual(nextProps.currentOrg.id, this.state.currentOrg.id)) {
      return null;
    }
    dispatch({
      type: 'Business/getInformation',
      payload: {
        orgId: nextProps.currentOrg.id,
      },
    }).then(res => {
      // if (res && Object.keys(res).length) {
      //   this.setState({
      //     orgShow: renderImgList(res.imgUrlList),
      //     fileList: renderImgList([res.logoUrl])
      //   })
      // }
    });
    this.setState({
      currentOrg: nextProps.currentOrg,
    })
  }

  handleChange = ({ fileList, file }, stateName) => {
    let state = false;
    const { name, size, status } = file;
    const Rxp = /.+\.(jpg|jpeg|png|bmp)$/;
    if (!Rxp.test(name) || (size / 1024) > 6000) {
      return;
    }
    let fileList1 = fileList;
    fileList1 = fileList1.map((item) => {
      if (item.response) {
        item.url = item.response.result;
      }
      return item;
    });
    fileList1 = fileList1.filter((item) => {
      const { response } = item;
      if (response) {
        const { status: states, message: msg } = response;
        !states ? message.error(msg) : '';
        state = states;
        return states === true;
      }
      return true;
    });
    (status === 'done' && state) ? message.success('上传成功') : '';
    this.setState({ [stateName]: fileList1 });
  };

  beforeUpload = (file) => {
    const Rxp = /.+\.(jpg|jpeg|png|bmp)$/;
    const { name, size } = file;
    if (!Rxp.test(name)) {
      message.error('请上传正确的图片文件 仅支持 jpg | png | jpeg | bmp ', 3);
      this.setState({
        fileList: [],
      });
      return false;
    } if ((size / 1024) > 6000) {
      message.error('图片文件大于 5M', 3);
      return false;
    }
    return true;
  };

  getAvatarURL() {
    const { fileList } = this.state;
    let url = 'https://dwz.cn/Yexw0XZA';
    if (fileList.length) {
      url = fileList[fileList.length - 1].url;
    }
    return url;
  }

  getViewDom = ref => {
    this.view = ref;
  };

  handleSubmit = () => {
    const { form, dispatch, currentOrg } = this.props;
    const { fileList } = this.state;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      const data = {};
      for (let i = 1; i < 4; i+=1) {
        const phone = values[`contactTeacherInfo${i}`];
         if (!!phone) {
           data[`contactTeacherInfo${i}`] = `${values[`contactTeacherInfo${i}Name`]}_${phone}`
         }else {
           data[`contactTeacherInfo${i}`] = phone;
         }
      }
      if (fileList.length) {
        data.contactWeChatQrCode = fileList[fileList.length - 1].url;
      }
      data.contactWeChatNo = values.contactWeChatNo;
      form.resetFields();
      dispatch({
        type: 'Business/setInformation',
        payload: {
          orgId: currentOrg.id,
          data
        },
      });
    });
  };

  render() {
    const {
      form: { getFieldDecorator },
      Business: { Information },
      loading
    } = this.props;
    const { contactTeacherInfo1, contactTeacherInfo2, contactTeacherInfo3 } = Information;
    const { fileList } = this.state;
    const { handleChange, beforeUpload, handleSubmit } = this;
    return (
      <Spin spinning={loading}>
        <div className={styles.baseView} ref={this.getViewDom}>
          <div className={styles.left}>
            <Form layout="vertical" hideRequiredMark>
              <FormItem label='老师1姓名'>
                {getFieldDecorator('contactTeacherInfo1Name', {
                  initialValue: contactTeacherInfo1 ? contactTeacherInfo1.split('_')[0] : '' ,
                  rules: [
                    {
                      required: true,
                      message: '请输入号码',
                    },
                  ],
                })(
                  <Input placeholder="老师姓名" />
                )}
              </FormItem>
              <FormItem label='老师1号码'>
                {getFieldDecorator('contactTeacherInfo1', {
                  initialValue: contactTeacherInfo1 ? contactTeacherInfo1.split('_')[1] : '' ,
                  rules: [
                    {
                      pattern: /^(1[3-8][0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$/,
                      required: true,
                      message: '请输入号码',
                    },
                  ],
                })(
                  <InputNumber style={{ width: '100%' }} placeholder="手机号码" />
                )}
              </FormItem>
              <FormItem label='老师2姓名'>
                {getFieldDecorator('contactTeacherInfo2Name', {
                  initialValue: contactTeacherInfo2 ? contactTeacherInfo2.split('_')[0] : '' ,
                  rules: [
                    {
                      required: false,
                      message: '老师姓名',
                    },
                  ],
                })(
                  <Input placeholder="老师姓名" />
                )}
              </FormItem>
              <FormItem label='老师2号码'>
                {getFieldDecorator('contactTeacherInfo2', {
                  initialValue: contactTeacherInfo2 ? contactTeacherInfo2.split('_')[1] : '' ,
                  rules: [
                    {
                      pattern: /^(1[3-8][0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$/,
                      required: false,
                      message: '请输入号码',
                    },
                  ],
                })(
                  <InputNumber style={{ width: '100%' }} placeholder="手机号码" />
                )}
              </FormItem>
              <FormItem label='老师3姓名'>
                {getFieldDecorator('contactTeacherInfo3Name', {
                  initialValue: contactTeacherInfo3 ? contactTeacherInfo3.split('_')[0] : '' ,
                  rules: [
                    {
                      required: false,
                      message: '老师姓名',
                    },
                  ],
                })(
                  <Input placeholder="老师姓名" />
                )}
              </FormItem>
              <FormItem label='老师3号码'>
                {getFieldDecorator('contactTeacherInfo3', {
                  initialValue: contactTeacherInfo3 ? contactTeacherInfo3.split('_')[1] : '' ,
                  rules: [
                    {
                      pattern: /^(1[3-8][0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$/,
                      required: false,
                      message: '请输入号码',
                    },
                  ],
                })(
                  <InputNumber style={{ width: '100%' }} placeholder="手机号码" />
                )}
              </FormItem>
              <FormItem label='微信'>
                {getFieldDecorator('contactWeChatNo', {
                  initialValue: Information.contactWeChatNo || '' ,
                  rules: [
                    {
                      required: false,
                      message: '请输入号码',
                    },
                  ],
                })(
                  <Input placeholder="微信号" />
                )}
              </FormItem>
              <Button onClick={handleSubmit} type="primary">
                更新信息
              </Button>
            </Form>
          </div>
          <div className={styles.right}>
            <AvatarView
              fileList={fileList}
              avatar={this.getAvatarURL()}
              handleChange={e => handleChange(e, 'fileList')}
              beforeUpload={beforeUpload}
            />
          </div>
        </div>
      </Spin>
    );
  }
}

export default Information;
