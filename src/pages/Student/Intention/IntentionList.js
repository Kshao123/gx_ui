import React, { Fragment, PureComponent } from 'react';
import { connect } from 'dva';
import {
  Card,
  Row,
  Col,
  Form,
  Button,
  DatePicker,
  Select,
  Tooltip,
  Icon,
  message,
  Input,
  Menu,
  Dropdown,
  Divider,
} from 'antd';
import moment from 'moment';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import Hoc from '@/utils/Hoc';
import { url, parseTime } from '@/utils/utils';
import SearchSel from '@/components/SearchSel';
import Authorized from '@/utils/Authorized';
import Ellipsis from '@/components/Ellipsis';
import Popcon from '@/components/Popconfirm';

const { check } = Authorized;
const { RangePicker } = DatePicker;
const { Option } = Select;
const FormItem = Form.Item;
const sexType = {
  true: '男',
  false: '女',
};

@connect(({ studentDatas: { StuIntentionInfo }, global, loading }) => ({
  StuIntentionInfo,
  currentOrg: global.currentOrg,
  loading: loading.effects['studentDatas/getStuIntention'],
}))
@Form.create()
@Hoc({ fetchUrl: 'studentDatas/getStuIntention' })

class Intention extends PureComponent {
  constructor() {
    super();
    this.state = {
      formValues: {},
      expandForm: false,
    };
  }

  handleFormReset = () => {
    const { dispatch, currentOrg, form  } = this.props;
    if (currentOrg.id) {
      form.resetFields();
      this.setState({
        formValues: {},
      });
      dispatch({
        type: 'studentDatas/getStuIntention',
        payload: {
          pagination: {
            pageSize: 10,
            pageNum: 1,
          },
          orgId: currentOrg.id,
        },
      });
    }
  };

  handleSearch = (e) => {
    e.preventDefault();
    const { dispatch, currentOrg, form } = this.props;
    const pagination = {
      pageNum: 1,
      pageSize: 10,
    };
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      // console.log(fieldsValue);
      // return;
      const { stuName, selectTime, phone, weChatName, subjectName } = fieldsValue;
      const query = {};
      if (stuName) {
        query.stuName = stuName.label.split('-')[0]
      }
      if (phone) {
        query.phone = phone;
      }
      if (weChatName) {
        query.weChatName = weChatName.label;
      }
      if (subjectName) {
        query.subjectName = subjectName.label;
      }
      if (selectTime && selectTime.length) {
        const [ startTime, endTime ] = selectTime;
        query.startTime = startTime.toDate().getTime();
        query.endTime = endTime.toDate().getTime();
      }
      this.setState({
        formValues: query,
      });
      dispatch({
        type: 'studentDatas/getStuIntention',
        payload: {
          pagination,
          orgId: currentOrg.id,
          query,
        },
      })
    });
  };

  toggleForm = () => {
    const { expandForm } = this.state;
    this.setState({
      expandForm: !expandForm,
    });
  };

  handleDelete = (record, flag) => {
    let courseCouponIds = [];
    if (flag) {
      courseCouponIds = record.length ? record : [];
    } else {
      const { id } = record;
      courseCouponIds.push(id);
    }
    const { dispatch, pagination, currentOrg } = this.props;
    const { formValues } = this.state;
    dispatch({
      type: 'coupons/delGenerates',
      payload: {
        courseCouponIds,
        query: {
          query: formValues,
          orgId: currentOrg.id,
          pagination,
        }
      }
    });
  };

  renderSearchForm() {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    const teaProps = {
      removal: true,
      queryUrl: `${url}/potentialStudent/displayPotentialStudentInfo`,
      keyValue: 'stuName',
      showSearch: true,
      attr: {
        id: 'stuId',
        name: 'stuName',
      },
      placeholder: '姓名',
      allowClear: true,
    };

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="姓名">
              {getFieldDecorator('stuName')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...teaProps} />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="手机号">
              {getFieldDecorator('phone',{
                initialValue: '',
                rules: [{ pattern: /^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$/, message: '请输入正确的联系方式' }],
              })(
                <Input placeholder='联系方式' />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">查询</Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
                展开 <Icon type="down" />
              </a>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderAdvanceForm() {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    const teaProps = {
      removal: true,
      queryUrl: `${url}/potentialStudent/displayPotentialStudentInfo`,
      keyValue: 'stuName',
      showSearch: true,
      attr: {
        id: 'stuId',
        name: 'stuName',
      },
      placeholder: '学生姓名',
      allowClear: true,
    };
    const weChatProps = {
      removal: true,
      queryUrl: `${url}/potentialStudent/displayPotentialStudentInfo`,
      keyValue: 'weChatName',
      showSearch: true,
      attr: {
        id: 'stuId',
        name: 'weChatName',
      },
      placeholder: '微信昵称',
      allowClear: true,
    };
    const subProps = {
      removal: true,
      queryUrl: `${url}/potentialStudent/displayPotentialStudentInfo`,
      keyValue: 'subjectName',
      showSearch: true,
      attr: {
        id: 'stuId',
        name: 'subjectName',
      },
      placeholder: '科目名称',
      allowClear: true,
    };

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="姓名">
              {getFieldDecorator('stuName')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...teaProps} />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="手机号">
              {getFieldDecorator('phone',{
                initialValue: '',
                rules: [{ pattern: /^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$/, message: '请输入正确的联系方式' }],
              })(
                <Input placeholder='联系方式' />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="微信昵称">
              {getFieldDecorator('weChatName',{
                initialValue: '',
              })(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...weChatProps} />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 12, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="科目名称">
              {getFieldDecorator('subjectName',{
                initialValue: '',
              })(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...subProps} />
              )}
            </FormItem>
          </Col>
          <Col md={12} sm={24}>
            <FormItem label="时间">
              {getFieldDecorator('selectTime',{
                // initialValue: [moment()],
              })(
                <RangePicker />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">查询</Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
                收起 <Icon type="up" />
              </a>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderForm() {
    const { expandForm } = this.state;
    return expandForm ? this.renderAdvanceForm() : this.renderSearchForm();
  }

  handleChangeState = (record, type) => {
    const { id } = record;
    const { dispatch, pagination, currentOrg } = this.props;
    const { formValues } = this.state;
    dispatch({
      type: 'studentDatas/changeAuditionStu',
      payload: {
        ids: [id],
        query: formValues,
        orgId: currentOrg.id,
        pagination,
        type,
      }
    });
  };

  render() {
    const {
      forItem,
      StuIntentionInfo,
      loading,
      handleStandardTableChange,
    } = this.props;
    const {
      formValues,
    } = this.state;
    const columns = [
      {
        title: '姓名',
        dataIndex: 'stuName',
        align: 'center',
        render(text) {
          return forItem('icon-mingcheng', text || '暂无信息' );
        },
      },
      {
        title: '微信昵称',
        dataIndex: 'weChatName',
        align: 'center',
        render(text) {
          return (
            <Ellipsis length={6} tooltip>
              {text}
            </Ellipsis>
          );
        },
      },
      {
        title: '性别',
        dataIndex: 'stuSex',
        align: 'center',
        render(text) {

          return forItem('icon-xingbie', sexType[text] );
        },
      },
      {
        title: '生日',
        dataIndex: 'stuBirthday',
        align: 'center',
        render(text) {
          return forItem('icon-shijian1', text ? parseTime(text, '{y}-{m}-{d}') : '暂无信息' );
        },
      },
      {
        title: '手机号',
        dataIndex: 'stuPhone',
        align: 'center',
        render(text) {
          return forItem('icon-shouji', text || '暂无信息');
        },
      },
      {
        title: '父亲手机',
        dataIndex: 'fatherPhone',
        align: 'center',
        render(text) {
          return forItem('icon-shouji', text || '暂无信息');
        },
      },
      {
        title: '母亲手机',
        dataIndex: 'motherPhone',
        align: 'center',
        render(text) {
          return forItem('icon-shouji', text || '暂无信息');
        },
      },
      {
        title: '相关课程',
        dataIndex: 'subjectName',
        align: 'center',
        render(text) {
          return (
            forItem('icon-mingcheng', text || '暂无信息' )
          )
        },
      },
      {
        title: '推荐人姓名',
        dataIndex: 'recommendStuName',
        align: 'center',
        render(text) {
          return forItem('icon-mingcheng', text || '暂无信息' );
        },
      },
      {
        title: '操作',
        fixed: 'right',
        width: 125,
        render: (text, record) => (
          <Fragment>
            <Dropdown overlay={(
              <Menu>
                <Menu.Item>
                  <a onClick={() => this.handleChangeState(record, 0)}>变更试听</a>
                </Menu.Item>
                <Menu.Item>
                  <a onClick={() => this.handleChangeState(record, 1)}>变更学生</a>
                </Menu.Item>
              </Menu>
            )}
            >
              <a className="ant-dropdown-link">
                修改 <Icon type="down" />
              </a>
            </Dropdown>
            <Divider type="vertical" />
            <Popcon
              title="是否确认删除"
              onConfirm={() => this.handleChangeState(record, 2)}
            />
          </Fragment>
        ),
      },
    ];

    return (
      <section>
        <Card className={styles.tableListForm}>
          {this.renderForm()}
        </Card>
        <Card>
          <div className={styles.tableList}>
            <StandardTable
              bordered
              loading={loading}
              data={StuIntentionInfo}
              columns={columns}
              onChange={e => handleStandardTableChange(e, formValues)}
              scroll={{ x: 1250 }}
            />
          </div>
        </Card>
      </section>
    );
  }
}
export default Intention;

