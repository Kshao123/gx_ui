import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Form,
  Button,
  Badge,
  Popconfirm,
  Row,
  Col,
  Card,
} from 'antd';
import StandardTable from '../../../components/DelTable';
import styles from '../../../utils/global.less';
import { parseTime, url } from '../../../utils/utils';
import SearchSel from '../../../components/SearchSel';
import Hoc from '../../../utils/Hoc';
import Authorized from '@/utils/Authorized';

const { check } = Authorized;
const FormItem = Form.Item;

const status = [
  {
    text: '未审批',
    value: 'warning',
  },
  {
    text: '通过',
    value: 'success',
  },
  {
    text: '未通过',
    value: 'error',
  },
];

@connect(({ stuLeave, global, loading }) => ({
  stuLeave,
  loading: loading.effects['stuLeave/_queryLeave'],
  currentOrg: global.currentOrg,
}))
@Form.create()
@Hoc({ fetchUrl: 'stuLeave/_queryLeave' })
class studentLeave extends PureComponent {
  constructor() {
    super();
    this.state = {
      props: {
        removal: true,
        onChange: this.handleValueChange,
        queryUrl: `${url}/stuConsult/queryLeave`,
        keyValue: 'name',
        showSearch: true,
        attr: {
          id: 'stuId',
          name: 'name',
        },
        placeholder: '搜索学生',
      },
    };
  }

  handleConfirm = (record, leaveStatus) => { // do 点击确认的回调
    const { id, stuCourseId } = record;
    const { pagination, currentOrg, dispatch } = this.props;
    dispatch({
      type: 'stuLeave/_leaveApproval',
      payload: {
        data: {
          leaveStatus,
          id,
          stuCourseId,
        },
        pagination,
        orgId: currentOrg.id,
      },
    });
  };

  ConfirmTime = (record) => {
    const { leaveStartTime, leaveEndTime } = record;
    if ((leaveEndTime / 3600000 - leaveStartTime / 3600000) <= 24) {
      return `${parseTime(leaveStartTime, '{y}-{m}-{d} {h}:{i}')} - ${parseTime(leaveEndTime, '{h}:{i}')}`
    }
    return `${parseTime(leaveStartTime, '{y}-{m}-{d} {h}:{i}')} - ${parseTime(leaveEndTime, '{y}-{m}-{d} {h}:{i}')}`
  };

  handleValueChange = (value, id, data) => {
    const { list } = data;
    const { label } = value;
    if (!list.length) return;
    if (list.length >= 2) {
      data.list = list.filter(item => item.name === label);
    }
    const { dispatch } = this.props;
    dispatch({
      type: 'stuLeave/_searchData',
      payload: data,
    });
  };

  handleFormReset = () => {
    const { dispatch, currentOrg, pagination } = this.props;
    if (currentOrg) {
      dispatch({
        type: 'stuLeave/_queryLeave',
        payload: {
          pagination,
          orgId: currentOrg.id,
        },
      });
    }
  };

  renderSearchForm() {
    const { currentOrg, form: { getFieldDecorator } } = this.props;
    return (
      <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="学生">
              {getFieldDecorator('stuId')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...this.state.props} />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>重置</Button>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const columns = [
      {
        title: '姓名',
        dataIndex: 'name',
      },
      {
        title: '请假时间',
        dataIndex: 'leaveStartTime',
        render: (text, record) => (
          this.ConfirmTime(record)
        ),
      },
      {
        title: '涉及课程',
        dataIndex: 'courseName',
      },
      {
        title: '状态',
        dataIndex: 'leaveStatus',
        render(val) {
          return <Badge status={status[val].value} text={status[val].text} />;
        },
      },
      {
        title: '操作',
        render: (text, record) => (
          record.leaveStatus > 0 ? (
            <div>已拒绝</div>
          ) : (
            <Fragment>
              {check('请假管理-请假审批', (
                <Button style={{ marginBottom: 10 }} type="primary" size="small" onClick={() => this.handleConfirm(record, 1)}>通过</Button>
              ))}
              <br />
              {check('请假管理-请假审批', (
                <Popconfirm
                  onConfirm={() => this.handleConfirm(record, 2)}
                  title="是否拒绝请假？"
                >
                  <Button type="danger" size="small">拒绝</Button>
                </Popconfirm>
              ))}
            </Fragment>
          )
        ),
      },
    ];
    const { stuLeave: { data }, loading, handleStandardTableChange } = this.props;
    return (
      <Card>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>
            {this.renderSearchForm()}
          </div>
          <StandardTable
            loading={loading}
            data={data}
            columns={columns}
            onChange={handleStandardTableChange}
            expandedRowRender={record => <p style={{ marginLeft: -46 }}>请假原因：{record.leaveReason}</p>}
          />
        </div>
      </Card>
    );
  }
}
export default studentLeave;
