import React, { PureComponent } from 'react';
import {
  Card,
  Button,
  Form,
  Icon,
  Col,
  Row,
  DatePicker,
  Input,
  Select,
  Popover,
  InputNumber,
} from 'antd';
import moment from 'moment';
import { connect } from 'dva';
import FooterToolbar from '@/components/FooterToolbar';
import styles from '@/utils/global.less';
import { parseTime } from '@/utils/utils';

const { Option } = Select;
const fieldLabels = {
  stuName: '姓名',
  age: '年龄',
  birthday: '生日',
  phone: '电话',
  sex: '性别',
  belongToschool: '所在校区',
  chooseSubject: '科目选择',
  fatherPhone: '父亲手机',
  motherPhone: '母亲手机',
  address: '家庭住址',
  stuStatus: '当前状态',
  startSchoolDate: '入学时间',
};
class studentDataMgr extends PureComponent {
  constructor(props) {
    super(props);
    const { state } = props.location;
    let disabled = false;
    if (state) {
      disabled = state.disabled;
    }
    this.state = {
      state,
      disabled,
    };
  }

  render() {
    const { form, dispatch, submitting, location, currentOrg } = this.props;
    const { getFieldDecorator, validateFieldsAndScroll, getFieldsError } = form;
    const { state } = location;
    const { disabled } = this.state;
    const validate = () => {
      validateFieldsAndScroll((error, values) => {
        if (!error) {
          values.birthday = parseTime(new Date(values.birthday.toDate()).getTime()).split(' ')[0];
          if (!values.id) {
            delete (values.id);
          }
          dispatch({
            type: 'studentDatas/_AddAuditionStu',
            payload: values,
          });
        }
      });
    };
    const errors = getFieldsError();
    const getErrorInfo = () => {
      const errorCount = Object.keys(errors).filter(key => errors[key]).length;
      if (!errors || errorCount === 0) {
        return null;
      }
      const scrollToField = (fieldKey) => {
        const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
        if (labelNode) {
          labelNode.scrollIntoView(true);
        }
      };
      const errorList = Object.keys(errors).map((key) => {
        if (!errors[key]) {
          return null;
        }
        return (
          <li key={key} className={styles.errorListItem} onClick={() => scrollToField(key)}>
            <Icon type="cross-circle-o" className={styles.errorIcon} />
            <div className={styles.errorMessage}>{errors[key][0]}</div>
            <div className={styles.errorField}>{fieldLabels[key]}</div>
          </li>
        );
      });
      return (
        <span className={styles.errorIcon}>
          <Popover
            title="表单校验信息"
            content={errorList}
            overlayClassName={styles.errorPopover}
            trigger="click"
            getPopupContainer={trigger => trigger.parentNode}
          >
            <Icon type="exclamation-circle" />
          </Popover>
          {errorCount}
        </span>
      );
    };
    return (
      <div>
        <Card title="学生信息" className={styles.card} bordered={false}>
          <Form layout="vertical">
            {getFieldDecorator('id', {
              initialValue: state ? state.id : '' })(
                <Input type="hidden" />
            )}
            {getFieldDecorator('orgId', {
              initialValue: currentOrg ? currentOrg.id : '' })(
                <Input type="hidden" />
            )}
            <Row gutter={16}>
              <Col xl={{ span: 8 }} lg={{ span: 8 }} md={{ span: 24 }} sm={24}>
                <Form.Item label={fieldLabels.stuName}>
                  {getFieldDecorator('stuName', {
                    initialValue: state ? state.stuName : '',
                    rules: [{ required: true, message: '请输入' }],
                  })(
                    <Input disabled={disabled} placeholder="请输入" />
                  )}
                </Form.Item>
              </Col>
              <Col xl={{ span: 8 }} lg={{ span: 8 }} md={{ span: 24 }} sm={24}>
                <Form.Item label={fieldLabels.age}>
                  {getFieldDecorator('age', {
                    initialValue: state ? state.age : '',
                    rules: [{ required: true, message: '请输入' }],
                  })(
                    <InputNumber disabled={disabled} style={{ width: '100%' }} placeholder="年龄" />
                  )}
                </Form.Item>
              </Col>
              <Col xl={{ span: 8 }} lg={{ span: 8 }} md={{ span: 24 }} sm={24}>
                <Form.Item label={fieldLabels.sex}>
                  {getFieldDecorator('sex', {
                    initialValue: state ? state.sex === true ? 'true' : 'false' : '',
                    rules: [{ required: true, message: '请选择性别' }],
                  })(
                    <Select disabled={disabled}>
                      <Option value="true">男</Option>
                      <Option value="false">女</Option>
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 24 }} sm={24}>
                <Form.Item label={fieldLabels.birthday}>
                  {getFieldDecorator('birthday', {
                    initialValue: state ? moment(state.birthday) : moment(new Date()),
                    rules: [{ required: true, message: '请输入' }],
                  })(
                    <DatePicker
                      disabled={disabled}
                      style={{ width: '100%' }}
                    />
                  )}
                </Form.Item>
              </Col>
              <Col xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 24 }} sm={24}>
                <Form.Item label={fieldLabels.phone}>
                  {getFieldDecorator('phone', {
                    initialValue: state ? state.phone : '',
                    rules: [{ pattern: /^((13[0-9])|(16[0-9])|(17[0-9])|(19[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(18[0,5-9]))\d{8}$/, required: true, message: '请输入正确的联系方式' }],
                  })(
                    <Input disabled={disabled} placeholder="请输入" />
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 24 }} sm={24}>
                <Form.Item label={fieldLabels.motherPhone}>
                  {getFieldDecorator('motherPhone', {
                    initialValue: state ? state.motherPhone : '',
                    rules: [{ pattern: /^((13[0-9])|(16[0-9])|(17[0-9])|(19[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(18[0,5-9]))\d{8}$/, required: false, message: '请输入正确的联系方式' }],
                  })(
                    <Input disabled={disabled} placeholder="请输入" />
                  )}
                </Form.Item>
              </Col>
              <Col xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 24 }} sm={24}>
                <Form.Item label={fieldLabels.fatherPhone}>
                  {getFieldDecorator('fatherPhone', {
                    initialValue: state ? state.fatherPhone : '',
                    rules: [{ pattern: /^((13[0-9])|(16[0-9])|(17[0-9])|(19[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(18[0,5-9]))\d{8}$/, required: false, message: '请输入正确的联系方式' }],
                  })(
                    <Input disabled={disabled} placeholder="请输入" />
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col xl={{ span: 12 }} lg={{ span: 12 }} md={{ span: 24 }} sm={24}>
                <Form.Item label={fieldLabels.address}>
                  {getFieldDecorator('address', {
                    initialValue: state ? state.address : '',
                    rules: [{ required: true, message: '请输入地址' }],
                  })(
                    <Input disabled={disabled} placeholder="请输入地址" />
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Card>
        <FooterToolbar style={{ width: this.state.width }}>
          {getErrorInfo()}
          {disabled ? null : (
            <Button type="primary" onClick={validate} loading={submitting}>
              提交
            </Button>
          )}
        </FooterToolbar>
      </div>
    );
  }
}

export default connect(({ studentDatas, loading, global }) => ({
  studentDatas,
  loading: loading.global,
  submitting: loading.effects['studentDatas/_AddAuditionStu'],
  currentOrg: global.currentOrg,
}))(Form.create()(studentDataMgr));
