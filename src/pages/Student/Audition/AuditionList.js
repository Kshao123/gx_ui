import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import {
  Card,
  Form,
  Button,
  Divider,
  Tooltip,
  Row,
  Col,
  Modal,
} from 'antd';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import Popcon from '@/components/Popconfirm';
import { parseTime, url } from '@/utils/utils';
import SearchSel from '@/components/SearchSel';
import Hoc from '@/utils/Hoc';
import Authorized from '@/utils/Authorized';

const { check } = Authorized;
const FormItem = Form.Item;
const confirm = Modal.confirm;

@connect(({ studentDatas, global, loading }) => ({
  studentDatas,
  loading: loading.global,
  currentOrg: global.currentOrg,
}))
@Form.create()
@Hoc({ fetchUrl: 'studentDatas/_queryAuditionStu' })
class AuditionList extends PureComponent {
  constructor() {
    super();
    this.state = {
      props: {
        onChange: this.handleValueChange,
        queryUrl: `${url}/stuConsult/queryAuditionStu`,
        keyValue: 'name',
        showSearch: true,
        attr: {
          id: 'stuId',
          name: 'stuName',
        },
        placeholder: '搜索学生',
      },
    };
  }

  handleAdd = (state, value) => {
    if (value) state.disabled = true;
    this.props.dispatch(routerRedux.push({
      pathname: '/student/Audition/Mgr',
      state,
    }));
  };

  handleConfirm = ({ id }) => { // do 点击确认的回调
    const { currentOrg, pagination, dispatch } = this.props;
    dispatch({
      type: 'studentDatas/DelStu',
      payload: {
        orgId: currentOrg.id,
        ids: [id],
        pagination,
        value: '_queryAuditionStu',
      },
    });
  };


  handleValueChange = (value, id, data) => {
    const { list } = data;
    const { key } = value;
    if (!list.length) return;
    if (list.length >= 2) {
      data.list = list.filter(item => item.id === Number(key));
    }
    const { dispatch } = this.props;
    dispatch({
      type: 'studentDatas/_searchData',
      payload: {
        data,
        value: 'saveAudition',
      },
    });
  };

  handleFormReset = () => {
    const { dispatch, currentOrg: { id: orgId }, pagination } = this.props;
    if (orgId) {
      dispatch({
        type: 'studentDatas/_queryAuditionStu',
        payload: {
          pagination,
          orgId,
        },
      });
    }
  };

  handleChangeStuStatus = ({ id, stuName }) => {
    const { dispatch, currentOrg: { id: orgId }, pagination } = this.props;
    confirm({
      title: `确认把${stuName}变更为正式学生吗？`,
      okText: '确认',
      cancelText: '取消',
      onOk() {
        if (orgId) {
          dispatch({
            type: 'studentDatas/changeStuStatus',
            payload: {
              id,
              pagination,
              orgId,
            },
          });
        }
      },
      onCancel() {},
    });
  };

  renderSearchForm() {
    const { currentOrg } = this.props;
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="学生">
              {getFieldDecorator('stuId')(
                <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...this.state.props} />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>重置</Button>
          </Col>
        </Row>
      </Form>
    );
  }


  render() {
    const { handleAdd } = this;
    const columns = [
      {
        title: '姓名',
        dataIndex: 'stuName',
        render: (text, record) => (
          <Tooltip title={`点击查看${text}的详细信息`}>
            <a onClick={() => handleAdd(record, true)}>{text}</a>
          </Tooltip>
          ),
      },
      {
        title: '性别',
        dataIndex: 'sex',
        render: sex => (
          <Fragment>
            {sex ? '男' : '女'}
          </Fragment>
        ),
      },
      {
        title: '年龄',
        dataIndex: 'age',
      },
      {
        title: '电话',
        dataIndex: 'phone',
      },

      {
        title: '出生日期',
        dataIndex: 'birthday',
        render: (text, record) => (
          <Fragment>
            <div>
              {parseTime(record.birthday).split(' ')[0]}
            </div>
          </Fragment>
        ),
      },
      {
        title: '操作',
        fixed: 'right',
        width: 120,
        render: (text, record) => (
          <Fragment>
            {check('学生管理-咨询-正式:变更', <a style={{ marginBottom: 15 }} onClick={() => this.handleChangeStuStatus(record)}>变更正式学生</a>)}
            <br />
            {check('学生管理-咨询学生:修改', <a onClick={() => this.handleAdd(record)}>修改</a>)}
            <Divider type="vertical" />
            {check('学生管理-咨询学生:删除', (
              <Popcon
                title="是否确认删除"
                onConfirm={() => this.handleConfirm(record)} // 点击确认时
              />
            ))}
          </Fragment>
        ),
      },
    ];
    const {
      studentDatas: { AuditionList: data },
      loading,
      handleStandardTableChange,
    } = this.props;

    return (
      <section>
        <Card className={styles.tableListForm}>
          {this.renderSearchForm()}
        </Card>
        <Card>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              {check('学生管理-咨询学生:增加', (
                <Button icon="plus" type="primary" onClick={() => this.handleAdd(undefined)}>
                  新建
                </Button>
              ))}
            </div>
            <StandardTable
              bordered
              loading={loading}
              data={data}
              columns={columns}
              onChange={handleStandardTableChange}
            />
          </div>
        </Card>
      </section>
    );
  }
}
export default AuditionList;
