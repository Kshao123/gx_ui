import React, { PureComponent } from 'react';
import {
  Form,
  Modal,
  InputNumber,
  Input,
  Radio,
} from 'antd';

const MsgForm = {
  stuName: '姓名',
  payDate: '日期',
  payRecordUserName: '录入人员',
  payMoney: '交费金额',
  fatherPhone: '父母电话',
  phone: '电话',
};

@Form.create()
class unOnlinePay extends PureComponent{
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  render() {
    const {
      userId,
      handleunOnlineOk,
      handleunOnlineCancel,
      unOnline,
      form: { getFieldDecorator },
      FormData,
      orgId,
    } = this.props;
    return(
      <Modal
        title="编辑/修改信息"
        visible={unOnline}
        onOk={() => handleunOnlineOk(this.props)}
        onCancel={() => handleunOnlineCancel(this.props)}
      >
        <Form layout="vertical">
          {getFieldDecorator('stuId', {
            initialValue: FormData.id || null })(
              <Input type="hidden" />
          )}
          {getFieldDecorator('payRecordUserId', {
            initialValue: userId || null })(
              <Input type="hidden" />
          )}
          {getFieldDecorator('orgId', {
            initialValue: orgId || null })(
              <Input type="hidden" />
          )}

          <Form.Item label={MsgForm.payMoney}>
            {getFieldDecorator('totalAmount', {
              rules: [{ required: true, message: '请输入' }],
            })(
              <InputNumber max={999999} precision={2} />
            )}
          </Form.Item>

          <Form.Item label="支付方式">
            {getFieldDecorator('payWay', {
              rules: [{ required: true, message: '请输入' }],
            })(
              <Radio.Group buttonStyle="solid">
                <Radio.Button style={{ marginRight: 10 }} value={3}>线下微信支付</Radio.Button>
                <Radio.Button style={{ marginRight: 10 }} value={4}>线下支付宝支付</Radio.Button>
                <Radio.Button style={{ marginRight: 10 }} value={5}>现金支付</Radio.Button>
                <Radio.Button value={6}>pos机支付</Radio.Button>
              </Radio.Group>
            )}
          </Form.Item>

        </Form>
      </Modal>
    )
  }
}

export default unOnlinePay;
