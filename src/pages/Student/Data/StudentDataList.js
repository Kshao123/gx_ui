import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import {
  Card,
  Form,
  Button,
  Divider,
  Tooltip,
  Row,
  Col,
  Modal,
  InputNumber,
  Input,
  message,
  Spin,
  Icon,
  notification,
  Upload,
  Menu,
  Dropdown,
} from 'antd';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import Popcon from '@/components/Popconfirm';
import { url } from '@/utils/utils';
import SearchSel from '@/components/SearchSel';
import Hoc from '@/utils/Hoc';
import Authorized from '@/utils/Authorized';
import UnOnlinePay from './unOnlinePay'
import WxCode from '@/components/WxCode';

const { check } = Authorized;
const FormItem = Form.Item;

@connect(({ studentDatas, global, loading, layoutloading }) => ({
  studentDatas,
  loading: loading.global,
  currentOrg: global.currentOrg,
  userId: layoutloading.currentUser.userid,
}))
@Form.create()
@Hoc({ fetchUrl: 'studentDatas/queryStu' })
class studentDatalist extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      props: {
        onChange: this.handleValueChange,
        queryUrl: `${url}/stuConsult/queryStu`,
        keyValue: 'name',
        showSearch: true,
        attr: {
          id: 'stuId',
          name: 'stuName',
        },
        placeholder: '搜索学生',
      },
      props1: {
        onChange: this.handleValueChangeForTea,
        queryUrl: `${url}/teacher/selectTeaByCondition`,
        keyValue: 'name',
        showSearch: true,
        attr: {
          id: 'teaId',
          name: 'name',
        },
        placeholder: '搜索对应的学生',
      },
      visible: false,
      formObj: {},
      getPay: false,
      unOnline: false,
      FormData: {},
      flag: true,
      teaId: null,
    };
  }

  handleAdd = (state, value) => {
    if (value) state.disabled = true;
    this.props.dispatch(routerRedux.push({
      pathname: '/student/data/Mgr',
      state,
    }));
  };

  handleConfirm = ({ id }) => { // do 点击确认的回调
    const { currentOrg, pagination, dispatch } = this.props;
    dispatch({
      type: 'studentDatas/DelStu',
      payload: {
        orgId: currentOrg.id,
        ids: [id],
        pagination,
        value: 'queryStu',
      },
    });
  };


  handleValueChange = (value, id, data) => {
    const { list } = data;
    const { key } = value;
    if (!list.length) return;
    if (list.length >= 2) {
      data.list = list.filter(item => item.id === Number(key));
    }
    const { dispatch } = this.props;
    dispatch({
      type: 'studentDatas/_searchData',
      payload: {
        data,
        value: 'save',
      },
    });
  };

  handleValueChangeForTea = (value) => {
    const { pagination, currentOrg: { id: orgId }, dispatch } = this.props;
    const { key } = value;
    const teaId = Number(key);
    this.setState({
      teaId,
    });
    dispatch({
      type: 'studentDatas/queryStu',
      payload: {
        orgId,
        pagination,
        teaId,
      }
    })
  };

  handleFormReset = () => {
    const { dispatch, currentOrg, pagination } = this.props;
    if (currentOrg.id) {
      dispatch({
        type: 'studentDatas/queryStu',
        payload: {
          pagination,
          orgId: currentOrg.id,
        },
      });
    }
  };

  handleOk = () => {
    let time;
    const { form, dispatch, currentOrg, pagination } = this.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      delete values.stuName;
      if (values.authCode && values.authCode.length < 18) {
        message.warn('支付授权码输入有误 请重新输入');
        return;
      }
      this.setState({
        getPay: true,
      });
      dispatch({
        type: 'studentDatas/_stuPayment',
        payload: values
      }).then((res) => {
        if (res) {
          time = setInterval(() => {
            dispatch({
              type: 'user/getPayRes',
              payload: {
                out_trade_no: res,
              },
            })
              .then((Res)=> {
                if (!Res && Res !== 0) {
                  this.setState({
                    getPay: false,
                  });
                  return
                }
                // 0：失败，1：成功，2：支付中
                switch (Res) {
                  case 1:
                    clearInterval(time);
                    form.resetFields();
                    this.setState({
                      visible: false,
                      getPay: false,
                    });
                    notification.success({
                      message: '成功',
                      description: '用户支付成功',
                      duration: 2.5,
                    });
                    setTimeout(() => {
                      if (currentOrg.id) {
                        dispatch({
                          type: 'studentDatas/queryStu',
                          payload: {
                            pagination,
                            orgId: currentOrg.id,
                          },
                        });
                      }
                    }, 1500);
                    break;
                  case 2:
                    break;
                  case 0:
                    clearInterval(time);
                    notification.error({
                      message: '失败',
                      description: '用户支付失败',
                      duration: 2.5,
                    });
                    this.setState({
                      getPay: false,
                    });
                    break;
                  default:
                    break;
                }
              })
          },1800);
        }else {
          this.setState({
            getPay: false,
          });
        }
      });
    });
  };

  handleCancel = () => {
    this.props.form.resetFields();
    this.setState({
      visible: false,
    });
  };

  handlePay = (record) => {
    this.setState({
      visible: true,
      formObj: record,
    })
  };


  renderSearchForm() {
    const { currentOrg } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="学生">
              <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...this.state.props} />
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="教师">
              <SearchSel orgId={currentOrg ? currentOrg.id : ''} {...this.state.props1} />
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>重置</Button>
          </Col>
        </Row>
      </Form>
    );
  }

  handleChange = ({ file }) => {
    if (file.status === 'done') {
      message.success('导入成功', 3);
      this.handleFormReset();
    }
  };

  handleunOnlineOk = (props) => {
    const { dispatch, pagination, currentOrg } = this.props;
    const { flag } = this.state;
    const { form } = props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      if (!flag) {
        values.totalAmount *= -1;
      }
      dispatch({
        type: 'school/_changeStuPayList',
        payload: {
          data: values,
          pagination,
          orgId: currentOrg.id,
        },
      });
      form.resetFields();
      this.setState({
        unOnline: false,
        flag: true,
      });
    });
  };

  handleunOnlineCancel = (props) => {
    props.form.resetFields();
    this.setState({
      unOnline: false,
    });
  };



  getExcel = () => {
    const { currentOrg: { id : orgId }, dispatch } = this.props;
    dispatch({
      type: 'studentDatas/_getStuExcel',
      payload: {
        orgId
      },
    })
  };

  handleUnOnlinePay = (record, flag= true) => {
    this.setState({
      flag,
      unOnline: true,
      FormData: record,
    })
  };

  handleStopCourse = record => {
    const { isStopCourse, id } = record;
    const type = {
      true: {
        text: '是否解除停课状态？',
        content: '将可继续给此学生排课',
      },
      false: {
        text: '是否停课？',
        content: '此操作是不可逆行为，将会删除学生剩于课程，并退还相应金额至余额！'
      }
    };
    Modal.confirm({
      title: type[isStopCourse].text,
      content: type[isStopCourse].content,
      okText: '确认',
      okType: 'danger',
      cancelText: '取消',
      onOk: () => {
        const { dispatch, currentOrg: { id: orgId }, pagination } = this.props;
        const { teaId } = this.state;
        if (!orgId) return;
        dispatch({
          type: 'stuApi/stopCourse',
          payload: {
            pagination,
            orgId,
            teaId,
            id,
            isStopCourse: !isStopCourse
          }
        })
      },
    });

  };

  render() {
    const columns = [
      {
        title: '姓名',
        dataIndex: 'stuName',
        align: 'center',
        render: (text, record) => (
          <WxCode data={record} name="stuName" userType={2} />
        ),
      },
      {
        title: '性别',
        dataIndex: 'sex',
        align: 'center',
        render: sex => (
          <Fragment>
            {sex ? '男' : '女'}
          </Fragment>
        ),
      },
      {
        title: '年龄',
        dataIndex: 'age',
        align: 'center',
      },
      {
        title: '电话',
        dataIndex: 'phone',
        align: 'center',
        render: (text, record) => (
          record.phone || record.motherPhone || record.fatherPhone
        ),
      },

      {
        title: '当前余额',
        dataIndex: 'totalAmount',
        align: 'center',
        render: (text) => (
          <span>
            {text ? `${text} 元` : '暂无'}
          </span>
        ),
      },
      {
        title: '操作',
        fixed: 'right',
        dataIndex: 'isStopCourse',
        width: 180,
        render: (text, record) => (
          <Fragment>
            {check('学生管理-正式学生:充值', (
              <Dropdown overlay={
                <Menu>
                  <Menu.Item>
                    <a onClick={() => this.handlePay(record)}>线上充值</a>
                  </Menu.Item>
                  <Menu.Item>
                    <a onClick={() => this.handleUnOnlinePay(record)}>线下充值</a>
                  </Menu.Item>
                  <Menu.Item>
                    <a onClick={() => this.handleUnOnlinePay(record, false)}>退款</a>
                  </Menu.Item>
                </Menu>
              }
              >
                <a className="ant-dropdown-link">
                  充值
                </a>
              </Dropdown>
            ))}
            <Divider type="vertical" />
            {/* {check('学生管理-正式学生:修改', <a onClick={() => this.handleAdd(record)}>修改</a>)} */}
            {check('学生管理-正式学生:修改', (
              <Dropdown overlay={
                <Menu>
                  <Menu.Item>
                    <a onClick={() => this.handleAdd(record)}>修改</a>
                  </Menu.Item>
                  <Menu.Item>
                    {/* <Popconfirm title="确认停止课程？" onConfirm={() => this.handleStopCourse(record)} okText="确认" cancelText="取消"> */}
                    <a onClick={e => this.handleStopCourse(record)}>{text ? '解除停课' : '停课'}</a>
                    {/* </Popconfirm> */}
                  </Menu.Item>
                  <Menu.Item>
                    <a onClick={() => this.handleAdd(record, true)}>查看</a>
                  </Menu.Item>
                </Menu>
              }
              >
                <a onClick={() => this.handleAdd(record)} className="ant-dropdown-link">
                  修改 <Icon type="down" />
                </a>
              </Dropdown>
            ))}
            <Divider type="vertical" />
            {check('学生管理-正式学生:删除', (
              <Popcon
                title="是否确认删除"
                onConfirm={() => this.handleConfirm(record)} // 点击确认时
              />
            ))}
          </Fragment>
        ),
      },
    ];

    const {
      studentDatas: { data },
      loading,
      handleStandardTableChange,
      form: { getFieldDecorator },
      currentOrg,
      userId,
    } = this.props;
    const { visible, formObj, getPay, FormData, unOnline } = this.state;
    const { handleOk, handleCancel } = this;
    const uploadprops = {
      // 这里我们只接受excel2007以后版本的文件，accept就是指定文件选择框的文件类型
      accept: 'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      name: 'file',
      action: `${url}/stuConsult/uploadStudentInfo`,
      data: { orgId: currentOrg ? currentOrg.id : null },
      // headers: {
      //   authorization: 'authorization-text',
      // },
      showUploadList: false,
      // 把excel的处理放在beforeUpload事件，否则要把文件上传到通过action指定的地址去后台处理
      // 这里我们没有指定action地址，因为没有传到后台
      onChange: this.handleChange
    };

    return (
      <section>
        <Card className={styles.tableListForm}>
          {this.renderSearchForm()}
        </Card>
        <Card>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              {check('学生管理-正式学生:增加', (
                <Button icon="plus" type="primary" onClick={() => this.handleAdd(undefined)}>
                  新建
                </Button>
              ))}
              {check('学生管理-正式学生:增加', (
                <Upload {...uploadprops}>
                  <Tooltip title='导入excel文件'>
                    <Button type="primary">
                      <Icon type="upload" /> 导入
                    </Button>
                  </Tooltip>
                </Upload>
              ))}
              {check('学生管理-正式学生:增加', (
                <Tooltip title='导出excel文件'>
                  <Button onClick={this.getExcel} type="primary">
                    <Icon type="download" /> 导出
                  </Button>
                </Tooltip>
              ))}
            </div>
            <StandardTable
              bordered
              loading={loading}
              data={data}
              columns={columns}
              onChange={handleStandardTableChange}
              scroll={{ x: 850 }}
            />
          </div>
        </Card>
        <Modal
          title="编辑/修改信息"
          visible={visible}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <Spin
            tip="查询支付结果中。。。"
            spinning={!!getPay}
            indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />}
          >
            <Form layout="vertical">
              {getFieldDecorator('stuId', {
                initialValue: formObj.id || ''
              })(
                <Input type="hidden" />
              )}

              {getFieldDecorator('orgId', {
                initialValue: formObj.orgId || ''
              })(
                <Input type="hidden" />
              )}

              <Form.Item label="姓名">
                {getFieldDecorator('stuName', {
                  initialValue: formObj.stuName || '',
                  rules: [{ required: true, message: '请输入账号' }],
                })(
                  <Input disabled placeholder="请输入账号" />
                )}
              </Form.Item>

              <Form.Item label="充值金额">
                {getFieldDecorator('totalAmount', {
                  rules: [{ required: true, message: '请输入金额' }],
                })(
                  <InputNumber style={{ width: '100%' }} min={0} max={999999} />
                )}
              </Form.Item>

              <Form.Item label="支付授权码">
                {getFieldDecorator('authCode', {
                  rules: [{ required: true, message: '请输入支付授权码' }],
                })(
                  <Input onPressEnter={handleOk} style={{ width: '100%' }} min={0} max={999999} />
                )}
              </Form.Item>

            </Form>
          </Spin>
        </Modal>
        <UnOnlinePay
          userId={userId}
          handleunOnlineOk={this.handleunOnlineOk}
          handleunOnlineCancel={this.handleunOnlineCancel}
          unOnline={unOnline}
          FormData={FormData}
          orgId={currentOrg ? currentOrg.id : null}
        />
      </section>
    );
  }
}
export default studentDatalist;
