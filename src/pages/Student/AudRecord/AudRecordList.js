import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Card,
  Form,
  Input,
  Select,
  Button,
  Divider,
  Modal,
  DatePicker,
  Cascader,
  Row,
  Col,
} from 'antd';
import moment from 'moment';
import StandardTable from '@/components/DelTable';
import styles from '@/utils/global.less';
import Popcon from '@/components/Popconfirm';
import { parseTime, url } from '@/utils/utils';
import SearchSel from "@/components/SearchSel";
import Hoc from '@/utils/Hoc';
import Authorized from '@/utils/Authorized';

const { check } = Authorized;
const { TextArea } = Input;
const { Option } = Select;
const PEOTYPE = {
  1: '教师',
  2: '其他',
};

@connect(({ studentDatas, global, loading }) => ({
  studentDatas,
  Options: studentDatas.Options,
  currentOrg: global.currentOrg,
  loading: loading.effects['studentDatas/_getAudRecordList'],
}))
@Form.create()
@Hoc({ fetchUrl: 'studentDatas/_getAudRecordList' })
class AudRecordList extends PureComponent {
  static queryId({ grade, courseName, subjectTypeName }, arr) {
    const obj = arr.filter(item => item.label === subjectTypeName);
    if (!obj.length) return [];
    const subjectTypeId = obj[0].value;
    const course = obj[0].children.filter(item => item.label === courseName);
    if (!course.length) return [];
    const courseId = course[0].value;
    const Id = course[0].children.filter(item => item.text === grade);
    if (!Id.length) return [];
    const gradeId = Id[0].value;
    return [subjectTypeId, courseId, gradeId];
  }

  constructor() {
    super();
    this.state = {
      props: {
        onChange: this.handleValueChange,
        queryUrl: `${url}/stuConsult/queryAuditionStu`,
        keyValue: 'name',
        placeholder: '搜索试听学生',
        attr: {
          id: 'stuId',
          name: 'stuName',
        },
      },
      RecordList: {
        onChange: this.search,
        queryUrl: `${url}/stuConsult/queryListenRecord`,
        keyValue: 'teaName',
        placeholder: '搜索教师',
        attr: {
          id: 'teaId',
          name: 'teaName',
        },
      },
      props1: {
        onChange: this.handleValueChange,
        queryUrl: `${url}/classroom/queryAll`,
        attr: {
          id: 'classroomId',
          name: 'classroomName',
        },
        placeholder: '搜索教室',
        keyValue: 'name',
      },
      props2: {
        onChange: this.handleValueChange,
        queryUrl: `${url}/teacher/selectTeaByCondition`,
        attr: {
          id: 'teaId',
          name: 'name',
        },
        keyValue: 'name',
        placeholder: '搜索教师',
      },
      visible: false,
      isEdit: false,
      FormObj: {},
      classroomId: undefined,
      teaId: undefined,
      stuId: undefined,
    };
  }

  componentDidMount() {
    setTimeout(() => {
      const { dispatch, currentOrg: { id } } = this.props;
      if (!id) return;
      dispatch({
        type: 'studentDatas/_queryCourse',
        payload: {
          orgId: id || '',
        },
      });
    },200)
  }

  handleEdit = (item, value) => {
    let str = false;
    let obj = {};
    if (value) {
      str = true;
      obj = item;
      const { classroomId, teaId, stuId, } = item;
      this.setState({
        classroomId,
        teaId,
        stuId,
      })
    }
    this.setState({
      visible: true,
      isEdit: str,
      FormObj: obj,
    });
  };

  handleValueChange = ({ key }, id) => {
    this.setState({
      [id]: Number(key),
    });
  };

  handleDelete = ({ id }) => {
    const { dispatch, pagination, currentOrg: { id: orgId } } = this.props;
    dispatch({
      type: 'studentDatas/delAudRecordList',
      payload: {
        ids: [id],
        pagination,
        orgId,
      },
    });
  };

  handleOk = () => {
    const { form, dispatch, pagination, currentOrg: { id: orgId } } = this.props;
    const { isEdit, stuId, classroomId, teaId } = this.state;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      const data = values;
      if (!isEdit) {
        delete (data.id);
      }
      data.stuId = stuId || '';
      data.classroomId = classroomId || '';
      data.teaId = teaId || '';
      data.listenTime = data.listenTime ? new Date(data.listenTime.toDate()).getTime() : '';
      const [_, subjectId, gradeId] = data.courseId;
      data.subjectId = subjectId;
      data.gradeId = gradeId;
      delete data.courseId;
      dispatch({
        type: 'studentDatas/_AddAudRecordList',
        payload: {
          data: values,
          pagination,
          orgId,
        },
      });

      // 在此处调用的是 重置表单内容， 不重置的话会导致 对话框再次打开不能实时更新内容的bug
      form.resetFields();
      this.setState({
        visible: false,
        isEdit: false,
      });
    });
  };

  handleCancel = () => {
    this.props.form.resetFields();
    this.setState({
      visible: false,
      isEdit: false,
    });
  };

  search = (value, id, data) => {
    const { list } = data;
    const { key } = value;
    if (!list.length) return;
    if (list.length >= 2) {
      data.list = list.filter(item => item.id === Number(key));
    }
    const { dispatch } = this.props;
    dispatch({
      type: 'studentDatas/_searchData',
      payload: {
        data,
        value: 'saveAudRecord',
      },
    });
  };


  handleFormReset = () => {
    const { dispatch, currentOrg, pagination } = this.props;
    if (currentOrg.id) {
      dispatch({
        type: 'studentDatas/_getAudRecordList',
        payload: {
          pagination,
          orgId: currentOrg.id,
        },
      });
    }
  };

  renderSearchForm() {
    const { currentOrg } = this.props;
    return (
      <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <Form.Item label="教师">
              <SearchSel showSearch orgId={currentOrg ? currentOrg.id : ''} {...this.state.RecordList} />
            </Form.Item>
          </Col>
          <Col md={8} sm={24}>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>重置</Button>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const {
      forItem,
      studentDatas: { AudRecordList: data },
      currentOrg,
      loading,
      form,
      handleStandardTableChange,
      Options,
    } = this.props;
    const columns = [
      {
        title: '教师姓名',
        dataIndex: 'teaName',
        align: 'center',
        render(text) {
          return forItem('icon-jiaoshi1', text);
        },
      },
      {
        title: '学生姓名',
        dataIndex: 'stuName',
        align: 'center',
        render(text) {
          return forItem('icon-xuesheng', text);
        },
      },
      {
        title: '教室名称',
        dataIndex: 'classroomName',
        align: 'center',
        render(text) {
          return forItem('icon-jiaoshi', text);
        },
      },
      {
        title: '试听课程',
        dataIndex: 'subjectName',
        align: 'center',
        render(_, record) {
          return forItem('icon-jiaoshi', `${record.subjectName} | ${record.gradeName}`);
        },
      },
      {
        title: '试听时间',
        dataIndex: 'listenTime',
        align: 'center',
        render(text) {
          return forItem('icon-shijian1', text ? parseTime(text, '{y}-{m}-{d} {h}:{i}') : '');
        },
      },
      {
        title: '介绍人',
        dataIndex: 'introducerName',
        align: 'center',
        render(text) {
          return forItem('icon-icon-', text);
        },
      },
      {
        title: '介绍人身份',
        dataIndex: 'introducerIdentity',
        align: 'center',
        render(text) {
          return forItem('icon-mingcheng', PEOTYPE[text]);
        },
      },
      {
        title: '操作',
        render: (text, record) => (
          <Fragment>
            {check('学生管理-试听记录:删除', <a onClick={() => this.handleEdit(record, true)}>修改</a>)}
            <Divider type="vertical" />
            {check('学生管理-试听记录:删除', (
              <Popcon
                title="是否确认删除"
                onConfirm={() => this.handleDelete(record)} // 点击确认时
              />
            ))}
          </Fragment>
        ),
      },
    ];
    const expandedRowRender = (record) => (
      <div>
        <p>
          <span>教师评价：</span>
          <span>{record.teaOpinion}</span>
        </p>
        <hr />
        <p>
          <span>教务人员评价：</span>
          <span>{record.otherOpinion}</span>
        </p>
      </div>
      );
    const { getFieldDecorator } = form;
    const { isEdit, FormObj, visible, props, props1, props2 } = this.state;
    return (
      <section>
        <Card className={styles.tableListForm}>
          {this.renderSearchForm()}
        </Card>
        <Card>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              {check('学生管理-试听记录:增加', (
                <Button icon="plus" type="primary" onClick={this.handleEdit}>
                  新建
                </Button>
              ))}
            </div>
            <StandardTable
              bordered
              loading={loading}
              data={data}
              ShowSearch={false}
              columns={columns}
              scroll={{ x: 950 }}
              onChange={handleStandardTableChange}
              expandedRowRender={expandedRowRender}
            />
          </div>
          <Modal
            title="编辑/修改信息"
            visible={visible}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
          >
            <Form layout="vertical">
              {getFieldDecorator('id', {
                initialValue: isEdit ? FormObj.id : '' })(
                  <Input type="hidden" />
              )}
              {getFieldDecorator('orgId', {
                initialValue: currentOrg && currentOrg.id ? currentOrg.id : '' })(
                  <Input type="hidden" />
              )}
              <Form.Item label="学生">
                {getFieldDecorator('stuId', {
                  initialValue: isEdit ? { key: FormObj.stuName, value: FormObj.stuId } : '',
                  rules: [{ required: true, message: `请输入` }],
                })(
                  <SearchSel
                    disabled={isEdit}
                    showSearch
                    orgId={currentOrg ? currentOrg.id : ''}
                    {...props}
                  />
                )}
              </Form.Item>

              <Form.Item label="老师">
                {getFieldDecorator('teaId', {
                  initialValue: isEdit ? { key: FormObj.teaName, value: FormObj.teaId } : '',
                  rules: [{ required: true, message: `请输入` }],
                })(
                  <SearchSel
                    showSearch
                    orgId={currentOrg ? currentOrg.id : ''}
                    {...props2}
                  />
                )}
              </Form.Item>

              <Form.Item label="教室">
                {getFieldDecorator('classroomId', {
                  initialValue: isEdit ? { key: FormObj.classroomName, value: FormObj.classroomId } : '',
                  rules: [{ required: true, message: '请选择' }],
                })(
                  <SearchSel showSearch orgId={currentOrg.id || ''} {...props1} />
                )}
              </Form.Item>

              <Form.Item label="科目">
                {getFieldDecorator('courseId', {
                  initialValue: isEdit ? [FormObj.subjectTypeId, FormObj.subjectId, FormObj.gradeId] : [],
                  rules: [{ message: '请选择', required: true, }],
                })(
                  <Cascader options={Options} placeholder="请选择科目" />
                )}
              </Form.Item>

              <Form.Item label="试听时间">
                {getFieldDecorator('listenTime', {
                  initialValue: isEdit ? moment(parseTime(FormObj.listenTime)) : moment(new Date()),
                  rules: [{ required: true, message: '请选择时间' }],
                })(
                  <DatePicker />
                )}
              </Form.Item>

              <Form.Item label="介绍人">
                {getFieldDecorator('introducer', {
                  initialValue: isEdit ? FormObj.introducerName : '',
                  rules: [{ required: true, message: '请输入介绍人' }],
                })(
                  <Input placeholder="介绍人" />
                )}
              </Form.Item>

              <Form.Item label="介绍人身份">
                {getFieldDecorator('introducerIdentity', {
                  initialValue: isEdit ? FormObj.introducerIdentity : '',
                  rules: [{ required: true, message: '请选择身份' }],
                })(
                  <Select style={{ width: '100%' }}>
                    <Option value={1}>教师</Option>
                    <Option value={2}>其他人员</Option>
                  </Select>
                )}
              </Form.Item>

              <Form.Item label="评价">
                {getFieldDecorator('otherOpinion', {
                  initialValue: isEdit ? FormObj.otherOpinion : '',
                })(
                  <TextArea placeholder="对试听学生的评价" autosize={{ minRows: 2, maxRows: 6 }} />
                )}
              </Form.Item>

            </Form>
          </Modal>
        </Card>
      </section>

    );
  }
}

export default AudRecordList;
