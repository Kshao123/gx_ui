import request from '../utils/request';
import { url } from '@/utils/utils';

// 查询请假列表
export async function queryLeave(params) {
  return request(`${url}/stuConsult/queryLeave`, {
    method: 'POST',
    body: params,
  });
}

// 是否通过请假
export async function leaveApproval(params) {
  return request(`${url}/leave/leaveApproval`, {
    method: 'POST',
    body: params,
  });
}
