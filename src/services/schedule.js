import request from '../utils/request';
import { url } from '@/utils/utils';

export async function getSchedule(params) {
  return request(`${url}/courseTable/query`, {
    method: 'POST',
    body: params,
  });
}

// 修改相关课程
export async function changeSchedule(params) {
  return request(`${url}/courseTable/adjustmentCourse`, {
    method: 'POST',
    body: params,
  });
}
