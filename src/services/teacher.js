import { stringify } from 'qs';
import request from '../utils/request';
import { url } from '@/utils/utils';

// 获取所有教师
export async function getAllTeachers(params) {
  return request(`${url}/teacher/selectTeaByCondition`, {
    method: 'POST',
    body: params,
  });
}

// 修改教师信息
export async function updateTeaById(params) {
  return request(`${url}/teacher/saveOrUpdateTea`, {
    method: 'POST',
    body: params,
  });
}

// 删除教师
export async function deleteTeaById(params) {
  return request(`${url}/teacher/deleteTea/${(params)}`);
}

// 最高权限查询所有教师
export async function queryAllTeacher(params) {
  return request(`${url}/queryAllTeacher?${stringify(params)}`);
}

// 查询所有反馈记录
export async function getFeedbackList(params) {
  return request(`${url}/teacher/selectAllFeedback`, {
    method: 'POST',
    body: params,
  });
}
// 删除反馈记录
export async function delFeedbackList({ id }) {
  return request(`${url}/teacher/deleteFeebackById/${id}`);
}
// 查询老师工资提成记录
export async function getSalary(params) {
  return request(`${url}/teacher/selectTeaSalaryInfo`, {
    method: 'POST',
    body: params,
  });
}
// 查询所有老师总工资
export async function getAllSalary(params) {
  return request(`${url}/teacher/selectTeacherInfoInMonth`, {
    method: 'POST',
    body: params,
  });
}

// 获取教师的Excel表格
export async function getTeaExcel(params) {
  return request(`${url}/teacher/downloadTeacherInfo?${stringify(params)}`);
}

// 查询打卡记录
export async function getTeaSign(params) {
  return request(`${url}/teacher/displayTeacherClassSignInfo`, {
    method: 'POST',
    body: params,
  });
}

// 老师补打卡
export async function TeaSign(params) {
  return request(`${url}teacher/teacherSupplementClassSign`, {
    method: 'POST',
    body: params,
  });
}

