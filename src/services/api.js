import { stringify } from 'qs';
import request from '@/utils/request';
import { url } from '@/utils/utils';

// ------- 2018 -----
export async function fakeAccountLogin(params) {
  return request(`${url}/login/account`, {
    method: 'POST',
    body: params,
  });
}

// 查询所有机构
export async function queryMyOrg() {
  return request(`${url}/mechanism/queryOrg`);
}
// 获取通知
export async function getNotices(params) {
  return request(`${url}/msgeNotice/query`, {
    method: 'POST',
    body: params,
  });
}
// 删除通知
export async function delNotices(params) {
  return request(`${url}/msgeNotice/deleteMsge?${stringify(params)}`);
}
// 修改添加通知
export async function changeNotices(params) {
  return request(`${url}/msgeNotice/save`, {
    method: 'POST',
    body: params,
  });
}
// 修改通知状态
export async function setNotices(params) {
  return request(`${url}/msgeNotice/lastClick?${stringify(params)}`);
}
// 获取个人通知
export async function getPersonNotices(params) {
  return request(`${url}/msgeNotice/queryOneSelfMsge`, {
    method: 'POST',
    body: params,
  });
}
// 删除个人通知
export async function delOneNotices(params) {
  return request(`${url}/msgeNotice/deleteOneSelfMsge?${stringify(params)}`);
}

export async function logout() {
  return request(`${url}/logout`);
}

// 获取绑定二维码
export async function getWxCode(params) {
  return request(`${url}/users/getQRCode`, {
    method: 'POST',
    body: params,
  });
}

// 获取学生可使用的优惠券
export async function getStuCoupon(params) {
  return request(`${url}/courseCoupon/displayCourseCouponByStuId`, {
    method: 'POST',
    body: params,
  });
}
