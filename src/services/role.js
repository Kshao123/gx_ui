import { stringify } from 'qs';
import request from '../utils/request';
import { url } from '@/utils/utils';

export async function query(params) {
  return request(`${url}/roles/query?${stringify(params)}`);
}

export async function submit(params) {
  return request(`${url}/roles/saveOrUpdateRole`, {
    method: 'POST',
    body: params,
  });
}

export async function saveFunction(params) {
  return request(`${url}/roles/saveOrUpdateRole`, {
    method: 'POST',
    body: params,
  });
}


export async function deleteRole(params) {
  return request(`${url}/roles/delete/${params}`);
}

export async function queryFunction(params) {
  return request(`${url}/roles/queryFunction/${params}`);
}
