import request from '@/utils/request';
import { url } from '@/utils/utils';

// 添加库存类别
export async function addInventory(params) {
  return request(`${url}/inventoryManagement/addOrUpdateProductType`, {
    method: 'POST',
    body: params,
  });
}

// 查询库存类别
export async function getInventory(params) {
  return request(`${url}/inventoryManagement/displayProductType`, {
    method: 'POST',
    body: params,
  });
}

// 删除库存类别
export async function delInventory(params) {
  return request(`${url}/inventoryManagement/removeProductType`, {
    method: 'POST',
    body: params,
  });
}

// 添加产品
export async function addInventoryManage(params) {
  return request(`${url}/inventoryManagement/addOrUpdateProduct`, {
    method: 'POST',
    body: params,
  });
}

// 查询产品
export async function getInventoryManage(params) {
  return request(`${url}/inventoryManagement/displayProduct`, {
    method: 'POST',
    body: params,
  });
}

// 删除产品
export async function delInventoryManage(params) {
  return request(`${url}/inventoryManagement/removeProduct`, {
    method: 'POST',
    body: params,
  });
}

// 查询本人机构
export async function getOrgList(params) {
  return request(`${url}/inventoryManagement/getOrgIdList`, {
    method: 'POST',
    body: params,
  });
}

// 产品入库
export async function rollInProduct(params) {
  return request(`${url}/inventoryManagement/rollInProduct`, {
    method: 'POST',
    body: params,
  });
}

// 产品出库
export async function rollOutProduct(params) {
  return request(`${url}/inventoryManagement/rollOutProduct`, {
    method: 'POST',
    body: params,
  });
}

// 产品变动
export async function ProductChange(params) {
  return request(`${url}/inventoryManagement/displayProductChange`, {
    method: 'POST',
    body: params,
  });
}

// 获取订单列表
export async function getProductOrder(params) {
  return request(`${url}/productSales/displayProductSales`, {
    method: 'POST',
    body: params,
  });
}

// 确认订单交付
export async function confirmProductOrder(params) {
  return request(`${url}/productSales/deliverProduct`, {
    method: 'POST',
    body: params,
  });
}

// 订单退款
export async function refundProduct(params) {
  return request(`${url}/productSales/refundProduct`, {
    method: 'POST',
    body: params,
  });
}

// 主动购买产品
export async function buyProduct(params) {
  return request(`${url}/productSales/buyProduct`, {
    method: 'POST',
    body: params,
  });
}

// 查看产品报表
export async function getProductReport(params) {
  return request(`${url}/productSales/displayProductSalesInfoInMonth`, {
    method: 'POST',
    body: params,
  });
}






