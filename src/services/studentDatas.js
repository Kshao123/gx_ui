import request from '../utils/request';
import { stringify } from 'qs';
import { url } from '@/utils/utils';

// 查询学生信息列表
export async function queryStu(body) {
  return request(`${url}/stuConsult/queryStu`, {
    method: 'POST',
    body,
  });
}
// 增加或修改学生信息
export async function AddStu(params) {
  return request(`${url}/stuConsult/saveStu`, {
    method: 'POST',
    body: params,
  });
}
// 删除学生信息
export async function DelStu(params) {
  return request(`${url}/stuConsult/delete/${params}`);
}
// 查询试听学生信息列表
export async function queryAuditionStu(body) {
  return request(`${url}/stuConsult/queryAuditionStu`, {
    method: 'POST',
    body,
  });
}
// 增加或修改试听学生信息
export async function AddAuditionStu(params) {
  return request(`${url}/stuConsult/saveOrUpdateAuditionStu`, {
    method: 'POST',
    body: params,
  });
}

// 查询科目
export async function queryCourse(params) {
  return request(`${url}/subject/selectSubject`, {
    method: 'POST',
    body: params,
  });
}
// 查询科目
export async function saveCourse(params) {
  return request(`${url}/subject/saveOrUpdateSubject`, {
    method: 'POST',
    body: params,
  });
}
// 删除科目
export async function delCourse({ ids }) {
  return request(`${url}/subject/deleteSubject/${ids}`);
}
// 查询科目类型
export async function queryCourseType(params) {
  return request(`${url}/subject/selectSubjectType`, {
    method: 'POST',
    body: params,
  });
}
// 保存科目类型
export async function saveCourseType(params) {
  return request(`${url}/subject/saveOrUpdateSubjectType`, {
    method: 'POST',
    body: params,
  });
}
// 删除科目类型
export async function delCourseType(params) {
  return request(`${url}/subject/deleteSubjectType/${params}`);
}
// 查询校区
export async function querySchool(params) {
  return request(`${url}/mechanism/query`, {
    method: 'POST',
    body: params,
  });
}

// 学生充值
export async function stuPayment(params) {
  return request(`${url}/stuPay/stuPayment`, {
    method: 'POST',
    body: params,
  });
}
// 变更试听学生的状态
export async function changeStuStatus(params) {
  return request(`${url}/stuConsult/beOfficialStident?${stringify(params)}`);
}

// 查询试听学生的记录
export async function getAudRecordList(params) {
  return request(`${url}/stuConsult/queryListenRecord`, {
    method: 'POST',
    body: params,
  });
}
// 添加/修改试听学生的记录
export async function setAudRecordList(params) {
  return request(`${url}/stuConsult/saveOrUpdateListen`, {
    method: 'POST',
    body: params,
  });
}
// 删除试听学生的记录
export async function delAudRecordList(params) {
  return request(`${url}/stuConsult/deleteListenRecord/${params}`);
}

// 获取学生的Excel表格
export async function getStuExcel(params) {
  return request(`${url}/stuConsult/downloadStudentInfo?${stringify(params)}`);
}

// 查询意向学生的记录
export async function getStuIntention(params) {
  return request(`${url}/potentialStudent/displayPotentialStudentInfo`, {
    method: 'POST',
    body: params,
  });
}

// 变更为试听学生
export async function changeAuditionStu(params) {
  return request(`${url}/potentialStudent/potentialStudentToAuditionStu`, {
    method: 'POST',
    body: params,
  });
}

// 变更为正式学生
export async function changeTrueStu(params) {
  return request(`${url}/potentialStudent/potentialStudentToStu`, {
    method: 'POST',
    body: params,
  });
}

// 销售失败
export async function delVisStu(params) {
  return request(`${url}/potentialStudent/potentialStudentConversionFailed`, {
    method: 'POST',
    body: params,
  });
}


