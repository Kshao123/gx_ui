import request from '../utils/request';
import { url } from '@/utils/utils';

// 获取系统价格
export async function getSystemPrice(params) {
  return request(`${url}/username/queryUsernamePrice`, {
    method: 'POST',
    body: params,
  });
}
// 修改系统价格
export async function setSystemPrice(params) {
  return request(`${url}/username/saveOrUpdateUsernamePrice`, {
    method: 'POST',
    body: params,
  });
}
// 修改系统价格
export async function delSystemPrice(params) {
  return request(`${url}/username/deleteUsernamePrice/${params}`);
}
// 查询系统订单
export async function getSystemOrders(params) {
  return request(`${url}/userOrder/queryAllOrder`, {
    method: 'POST',
    body: params,
  });
}
// 修改系统订单
export async function delSystemOrders(params) {
  return request(`${url}/userOrder/deleteOrder/${params}`);
}

// 查询账号租期
export async function getSystemTerm(params) {
  return request(`${url}/userOrder/queryTenancyTerm`, {
    method: 'POST',
    body: params,
  });
}
// 添加/修改 账号租期
export async function setSystemTerm(params) {
  return request(`${url}/userOrder/saveOrUpdateTerm`, {
    method: 'POST',
    body: params,
  });
}
// 删除系统订单
export async function delSystemTerm(params) {
  return request(`${url}/userOrder/deleteTenancyTerm/${params}`);
}
