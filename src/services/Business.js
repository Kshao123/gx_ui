import { stringify } from 'qs';
import request from '@/utils/request';
import { url } from '@/utils/utils';

// 获取商户 微信公众号信息
export async function getWeChatPublic(params) {
  return request(`${url}/orgSystemSettings/displayWeChatPublicAccountConfig`, {
    method: 'POST',
    body: params,
  });
}

// 更新商户 公众号信息
export async function upDateWeChatPublic(params) {
  return request(`${url}/orgSystemSettings/updateWeChatPublicAccountConfig`, {
    method: 'POST',
    body: params,
  });
}

// 获取商户 休息日
export async function getHolidayInfo(params) {
  return request(`${url}/orgSystemSettings/displayHolidayInfo`, {
    method: 'POST',
    body: params,
  });
}

// 设置商户 休息日
export async function setHolidayInfo(params) {
  return request(`${url}/orgSystemSettings/saveHolidayInfo`, {
    method: 'POST',
    body: params,
  });
}

// 删除商户 休息日
export async function delHolidayInfo(params) {
  return request(`${url}/orgSystemSettings/removeHolidayInfo`, {
    method: 'POST',
    body: params,
  });
}

// 查询 商户的 基础信息
export async function getBaseInfo(params) {
  return request(`${url}/orgSystemSettings/displayAndUpdateOrgInfo`, {
    method: 'POST',
    body: params,
  });
}

// 查询 商户的 联系信息
export async function getInformation(params) {
  return request(`${url}/orgSystemSettings/displayAndUpdateContactTeacherInfo`, {
    method: 'POST',
    body: params,
  });
}

// 查询 可请假的次数
export async function getCanLeaveNum(params) {
  return request(`${url}/orgSystemSettings/displayAndUpdateAllowLeaveNum`, {
    method: 'POST',
    body: params,
  });
}

// 查询 可补打卡的时间
export async function getCanSignTime(params) {
  return request(`${url}/orgSystemSettings/displayAndUpdateAllowClassSignTime`, {
    method: 'POST',
    body: params,
  });
}

