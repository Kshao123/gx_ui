import request from '@/utils/request';
import { url } from '@/utils/utils';

// 获取当前机构的校区负责人
export async function getSchoolManger(params) {
  return request(`${url}/people/selectSchoolManager`, {
    method: 'POST',
    body: params,
  });
}

// 修改校区负责人
export async function changeSchoolManger(params) {
  return request(`${url}/people/saveOrUpdateSchoolManager`, {
    method: 'POST',
    body: params,
  });
}
// 删除校区负责人
export async function delSchoolManger(params) {
  return request(`${url}/people/deleteSchoolManager/${params}`);
}

// 获取当前机构的教务主管
export async function getDirector(params) {
  return request(`${url}/people/selectEduAdmin`, {
    method: 'POST',
    body: params,
  });
}

// 修改教务主管
export async function changeDirector(params) {
  return request(`${url}/people/saveOrUpdateEduAdmin`, {
    method: 'POST',
    body: params,
  });
}
// 删除教务主管
export async function delDirector(params) {
  return request(`${url}/people/deleteEduAdmin/${params}`);
}
// 查询请假
export async function getLeave(params) {
  return request(`${url}/leave/selectLeaveRecord`, {
    method: 'POST',
    body: params,
  });
}
