import { stringify } from 'qs';
import request from '../utils/request';
import { url } from '@/utils/utils';

// 获取机构所有一对一课程
export async function queryOne2One(params) {
  return request(`${url}/one2one/selectOne2One`, {
    method: 'POST',
    body: params,
  });
}

export async function submitOne2One(params) {
  return request(`${url}/one2one/saveOrUpdateOne2One`, {
    method: 'POST',
    body: params,
  });
}
// 删除一对一
export async function deleteOne2One(params) {
  return request(`${url}/one2one/deleteOne2One/${params}`);
}

export async function querySubProps(params) {
  return request(`${url}/one2one/querySubProps/${params}`);
}


export async function queryCoursePurchaseByStu(params) {
  return request(`${url}/classsale/queryByStu?${stringify(params)}`);
}
// 获取机构所有一对一课程
export async function getAllOno2one(params) {
  return request(`${url}/classsale/queryByStu?${stringify(params)}`);
}

// 查询学生排课
export async function getStuCourse(params) {
  return request(`${url}/one2one/displayRemainingCourse`, {
    method: 'POST',
    body: params,
  });
}

// 给学生排课
export async function changeStuCourse(params) {
  return request(`${url}/one2one/adjustmentCourse`, {
    method: 'POST',
    body: params,
  });
}

