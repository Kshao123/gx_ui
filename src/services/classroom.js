import { stringify } from 'qs';
import request from '@/utils/request';
import { url } from '@/utils/utils';

export async function submitClassroom(params) {
  return request(`${url}/classroom/save`, {
    method: 'POST',
    body: params,
  });
}
// 查询所有的教室
export async function getAllClassroom(params) {
  return request(`${url}/classroom/queryAll`, {
    method: 'POST',
    body: params,
  });
}
// 根据条件查找教室
export async function getAptClassroom(params) {
  return request(`${url}/classroom/query?${stringify(params)}`);
}

// 添加教室
export async function addClassroom(params) {
  return request(`${url}/classroom/saveOrUpdate`, {
    method: 'POST',
    body: params,
  });
}
// 删除教室
export async function delClassroom(params) {
  return request(`${url}/classroom/delete/${(params)}`);
}

// 教室租赁 查询教室空闲时间
export async function getClassroomFreeTime(params) {
  return request(`${url}/classroom/findFreeClassroom?${stringify(params)}`);
}
// -------

// 查询教室租赁订单
export async function getClassRoomOrder(params) {
  return request(`${url}/classroom/findRentOrder`, {
    method: 'POST',
    body: params,
  });
}
