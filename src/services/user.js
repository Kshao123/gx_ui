import { stringify } from 'qs';
import request from '@/utils/request';
import { url } from '@/utils/utils';

// -------- 2018 -12 -21 -----------
export async function queryAuth() {
  return request(`${url}/users/currentAuth`);
}
export async function Query(params) {
  return request(`${url}/users/query?${stringify(params)}`);
}

export async function QueryCurrent() {
  return request(`${url}/users/currentUser`);
}

export async function reload() {
  return request(`${url}/refresh/auth`);
}

export async function submit(params) {
  return request(`${url}/users/saveOrUpdateUser`, {
    method: 'POST',
    body: params,
  });
}

export async function saveRoles(params) {
  return request(`${url}/users/changeRoles`, {
    method: 'POST',
    body: params,
  });
}

export async function queryRolesByUser(params) {
  return request(`${url}/users/queryRoles/${params}`);
}

export async function deleteUser(params) {
  return request(`${url}/users/delete/${params}`);
}
// mock 获取 用户信息
export async function userMsg(params) {
  return request(`${url}/users/selectOneselfInfo/${params}`);
}
// 登陆用户发起请假
export async function getLeave(params) {
  return request(`${url}/leave/saveLeave`, {
    method: 'POST',
    body: params,
  });
}
// 登陆用户查询请假
export async function getLeaveList(params) {
  return request(`${url}/leave/selectOneselfLeave`, {
    method: 'POST',
    body: params,
  });
}
// 登陆用户保存信息
export async function setUserMsg(params) {
  return request(`${url}/users/updateOneselfInfo`, {
    method: 'POST',
    body: params,
  });
}
// 登陆用户保存信息
export async function setPassword(params) {
  return request(`${url}/users/updatePassWord`, {
    method: 'POST',
    body: params,
  });
}

// 登陆用户查询个人信息
export async function getOneselfInfo(params) {
  return request(`${url}/user/selectOneselfInfo?${stringify(params)}`, { mock: true });
}

// 获取在线人数
export async function getOnline(params) {
  return request(`${url}/statisticsOnlinePeopleInfo.do?${stringify(params)}`);
}

// 申请账户
export async function Register(params) {
  return request(`${url}/users/userRegister`, {
    method: 'POST',
    body: params,
  });
}
// 查询账户审核状态
export async function getRegister(params) {
  return request(`${url}/username/queryRegisterResult`, {
    method: 'POST',
    body: params,
  });
}

// 查询月份所对应的价格
export async function getPrice(params) {
  return request(`${url}/username/querySystemPrice`, {
    method: 'POST',
    body: params,
  });
}
// 获取支付二维码
export async function getEwm(params) {
  return request(`${url}/username/goToPay`, {
    method: 'POST',
    body: params,
  });
}

// 获取续费支付二维码
export async function getEwmXf(params) {
  return request(`${url}/username/userRenewFee`, {
    method: 'POST',
    body: params,
  });
}

// 获取支付二维码
export async function getPayRes(params) {
  return request(`${url}/username/payResult?${stringify(params)}`);
}

// 充值退款信息查询
export async function getPayData(params) {
  return request(`${url}/indexDisplayInfo/studentRechargeAndRefund`, {
    method: 'POST',
    body: params,
  });
}

// 课程数据查询
export async function getCourseNumChange(params) {
  return request(`${url}/indexDisplayInfo/courseNumChange`, {
    method: 'POST',
    body: params,
  });
}

// 获取机构学员人数
export async function getStudentNum(params) {
  return request(`${url}/indexDisplayInfo/studentNum`, {
    method: 'POST',
    body: params,
  });
}

// 查询学员变动情况
export async function getStudentChange(params) {
  return request(`${url}/indexDisplayInfo/studentNumChange`, {
    method: 'POST',
    body: params,
  });
}

// 查询学员变动情况
export async function getStuBirthday(params) {
  return request(`${url}/indexDisplayInfo/studentBirthday`, {
    method: 'POST',
    body: params,
  });
}

// 查询学员变动情况
export async function getTeaBirthday(params) {
  return request(`${url}/indexDisplayInfo/teacherBirthday`, {
    method: 'POST',
    body: params,
  });
}

// 查询教室租赁订单
export async function getClassMoney(params) {
  return request(`${url}/indexDisplayInfo/classroomRentOrderMoney`, {
    method: 'POST',
    body: params,
  });
}

// 查询老师上课率
export async function getTeaClassRate(params) {
  return request(`${url}/indexDisplayInfo/teacherClassRate`, {
    method: 'POST',
    body: params,
  });
}
