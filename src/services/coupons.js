import request from '@/utils/request';
import { url } from '@/utils/utils';

export async function addGenerates(params) {
  return request(`${url}/courseCoupon/addCourseCoupon`, {
    method: 'POST',
    body: params,
  });
}

export async function getGenerates(params) {
  return request(`${url}/courseCoupon/displayCourseCoupon`, {
    method: 'POST',
    body: params,
  });
}
export async function delGenerates(params) {
  return request(`${url}/courseCoupon/removeCourseCoupon`, {
    method: 'POST',
    body: params,
  });
}

export async function getLuckDraw(params) {
  return request(`${url}/marketingActivities/displayMarketingActivitiesLuckDraw`, {
    method: 'POST',
    body: params,
  });
}

export async function openLuckDraw(params) {
  return request(`${url}/marketingActivities/openMarketingActivitiesLuckDraw`, {
    method: 'POST',
    body: params,
  });
}

export async function closeLuckDraw(params) {
  return request(`${url}/marketingActivities/closeMarketingActivitiesLuckDraw`, {
    method: 'POST',
    body: params,
  });
}
