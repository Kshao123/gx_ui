import request from '@/utils/request';
import { url } from '@/utils/utils';

export async function query(params) {
  return request(`${url}/mechanism/query`, {
    method: 'POST',
    body: params,
  });
}
// 添加或修改机构
export async function submit(params) {
  return request(`${url}/mechanism/save`, {
    method: 'POST',
    body: params,
  });
}
// 删除机构
export async function deleteMine(params) {
  return request(`${url}/mechanism/delete/${params}`);
}

// 查询机构法人
export async function getManager(params) {
  return request(`${url}/mechanism/queryOrgLegal`, {
    method: 'POST',
    body: params,
  });
}
// 删除机构法人
export async function delManager(params) {
  return request(`${url}/mechanism/deleteOrgLegal/${params}`);
}

// 添加/修改 机构审批
export async function setManager(params) {
  return request(`${url}/mechanism/saveOrUpdateOrgLegal`, {
    method: 'POST',
    body: params,
  });
}
// 查询机构审批
export async function getExamine(params) {
  return request(`${url}/mechanism/queryNoAuditOrg`, {
    method: 'POST',
    body: params,
  });
}

//  提交审批
export async function ToAudit(params) {
  return request(`${url}/mechanism/orgAudit`, {
    method: 'POST',
    body: params,
  });
}
