import { stringify } from 'qs';
import { url } from '@/utils/utils';
import request from '@/utils/request';

export async function querystudentDatas(params) {
  return request(`${url}/studentDatas/query?${stringify(params)}`);
}

export async function getAttendance(params) {
  return request(`${url}/mechanism/queryAttendance`, {
    method: 'POST',
    body: params,
  });
}
// 获取学生缴费信息
export async function getStuPayList(params) {
  return request(`${url}/stuPay/query`, {
    method: 'POST',
    body: params,
  });
}
// 修改学生缴费信息
export async function changeStuPayList(params) {
  return request(`${url}/stuPay/save`, {
    method: 'POST',
    body: params,
  });
}
// 删除学生缴费信息
export async function delStuPayList(params) {
  return request(`${url}/stuPay/delete?${stringify(params)}`);
}
