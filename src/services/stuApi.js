// courseTable/skipCourse
import request from '../utils/request';
import { url } from '@/utils/utils';

// 跳过课程
export async function skipCourse(body) {
  return request(`${url}/courseTable/skipCourse`, {
    method: 'POST',
    body,
  });
}

// 停止课程
export async function stopCourse(body) {
  return request(`${url}/stuConsult/stopCourse`, {
    method: 'POST',
    body,
  });
}
