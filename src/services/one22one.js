import { stringify } from 'qs';
import request from '../utils/request';
import { url } from '@/utils/utils';

// 查询一对多
export async function queryOne22One(params) {
  return request(`${url}/one2many/selectOne2Many`, {
    method: 'POST',
    body: params,
  });
}

// 添加/修改一对多
export async function submitOne22One(params) {
  return request(`${url}/one2many/saveOrUpdateOne2Many`, {
    method: 'POST',
    body: params,
  });
}

// 删除 一对多
export async function deleteOne22One(params) {
  return request(`${url}/one2many/deleteOne2Many/${params}`);
}

export async function querySubProps(params) {
  return request(`${url}/one22one/querySubProps/${params}`);
}


export async function queryCoursePurchaseByStu(params) {
  return request(`${url}/classsale/queryByStu?${stringify(params)}`);
}

// 查询所有的小组授课
export async function getAllone2Many(params) {
  return request(`${url}/one2many/selectAllOne2Many?${stringify(params)}`);
}

// 查询学生排课
export async function getStuCourse(params) {
  return request(`${url}/one2many/displayRemainingCourse`, {
    method: 'POST',
    body: params,
  });
}

// 给学生排课
export async function changeStuCourse(params) {
  return request(`${url}/one2many/adjustmentCourse`, {
    method: 'POST',
    body: params,
  });
}
